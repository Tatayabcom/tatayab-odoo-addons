# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from . import stock_picking
from . import stock_move_line


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
