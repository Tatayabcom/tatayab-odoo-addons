# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.image import image_data_uri


class Product(models.Model):
    _inherit = 'product.product'
    _barcode_field = 'barcode'

    @api.model
    def _get_fields_stock_barcode(self):
        return ['barcode','image_128', 'default_code', 'tracking', 'display_name', 'uom_id','id']

class StockMoveLine(models.Model):
    _name = 'stock.move.line'
    _inherit = ['stock.move.line', 'barcodes.barcode_events_mixin']

    image_128 = fields.Binary(string="Image",related='product_id.image_128',attachment=True,store=True)

    @api.onchange('product_id')
    def onchange_product_image(self):
        for product in self:
            product.image_128 = product.product_id.image_128

    def _get_fields_stock_barcode(self):
        return [
            'product_id',
            'location_id',
            'location_dest_id',
            'qty_done',
            'display_name',
            'product_uom_qty',
            'product_uom_id',
            'product_barcode',
            'image_128',
            'owner_id',
            'lot_id',
            'lot_name',
            'package_id',
            'result_package_id',
            'dummy_id',
        ]
