# -*- coding: utf-8 -*-

import logging
from odoo.osv import expression
from odoo.osv.query import Query
from odoo.models import BaseModel, api

logger = logging.getLogger(__name__)

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
@api.model
def _where_calc(self, domain, active_test=True):
    """Computes the WHERE clause needed to implement an OpenERP domain.
    :param domain: the domain to compute
    :type domain: list
    :param active_test: whether the default filtering of records with ``active``
                        field set to ``False`` should be applied.
    :return: the query expressing the given domain as provided in domain
    :rtype: osv.query.Query
    """
    # if the object has an active field ('active', 'x_active'),
    # filter out all inactive records unless they were explicitly asked for
    if self._active_name and active_test and self._context.get('active_test', True):
        # the item[0] trick below works for domain items and '&'/'|'/'!' operators too
        if not any(item[0] == self._active_name for item in domain):
            domain = [(self._active_name, '=', 1)] + domain

    self.env.cr.execute("SELECT id FROM ir_module_module WHERE name='sw_multi_search' and state='installed' limit 1")
    is_multi_search_installed = self.env.cr.fetchone()

    if self._table == 'stock_location':
        if domain:
            modified_domain = []
            for idx, domain_part in enumerate(domain):
                if domain_part is not None:
                    if is_multi_search_installed and type(domain_part) in [list, tuple] \
                            and domain_part[1] == 'ilike' :
                            # and ' ' in domain_part[2]
                            search_vals = str(domain_part[2]).split(',')
                            for i in range(0, len(search_vals)):
                                modified_domain.append('|')
                            modified_domain.append(domain_part)
                            for search_val in search_vals:
                                new_search = (domain_part[0], '=', search_val)
                                modified_domain.append(new_search)
                    else:
                        modified_domain.append(domain_part)

            return expression.expression(modified_domain, self).query
        else:
            return Query(self.env.cr, self._table, self._table_query)
    else:
        if domain:
            modified_domain = []
            for idx, domain_part in enumerate(domain):
                if domain_part is not None:
                    if is_multi_search_installed and type(domain_part) in [list, tuple] \
                            and domain_part[1] == 'ilike':
                        # and ' ' in domain_part[2]
                        search_vals = str(domain_part[2]).split(' ')
                        for i in range(0, len(search_vals)):
                            modified_domain.append('|')
                        modified_domain.append(domain_part)
                        for search_val in search_vals:
                            new_search = (domain_part[0], '=', search_val)
                            modified_domain.append(new_search)
                    else:
                        modified_domain.append(domain_part)

            return expression.expression(modified_domain, self).query
        else:
            return Query(self.env.cr, self._table, self._table_query)


BaseModel._where_calc = _where_calc

