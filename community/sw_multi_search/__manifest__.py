# -*- coding: utf-8 -*-
{
    'name': 'SW - Multi Search',
    'version': '15.0.1.0',
    'category': 'General',
    'summary': """ Records bulk search on any page/model.""",
    'description': """ Search for records in bulks on any page/model easily. """,
    'license': "Other proprietary",
    'author': 'Smart Way Business Solutions',
    'website': 'www.smartway.co',
    'depends': ['base'],
    'init_xml': [],
    'data': [

    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    "images": ['static/description/image.png'],

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
