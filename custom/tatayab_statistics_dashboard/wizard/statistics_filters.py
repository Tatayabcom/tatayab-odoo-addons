# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import Warning
# Ahmed Salama Code Start ---->


class StatisticsFilters(models.TransientModel):
	_name = 'statistics.filters'
	_description = "Statistics Filters"
	
	statistics_id = fields.Many2one('tatayab.statistics', "Statistics", required=True)
	warehouse_id = fields.Many2one('stock.warehouse', "Warehouse", required=True)
	
	def act_view_results(self):
		return self.statistics_id.action_view_results(self.warehouse_id)

# Ahmed Salama Code End.
