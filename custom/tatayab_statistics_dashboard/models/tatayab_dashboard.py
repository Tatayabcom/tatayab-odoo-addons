# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import Warning
# Ahmed Salama Code Start ---->


class TatayabStatistics(models.Model):
	_name = 'tatayab.statistics'
	_description = "Tatayab Statistics"
	_order = "sequence, name, id"
	_inherit = ['mail.thread', 'mail.activity.mixin']
	
	name = fields.Char("Name", required=True,  tracking=True)
	query_sql = fields.Text(string="SQL", required=True, tracking=True)
	sequence = fields.Integer("Sequence", default=1000,
	                          help="Used to order stages. Lower is better,\n 0 means first 1000 is last")
	query_limit = fields.Integer("Limit", help="Limit of showing results", tracking=True)
	model_id = fields.Many2one('ir.model', "Model", required=True, tracking=True,  ondelete='cascade')
	model_name = fields.Char(related='model_id.model')
	action_id = fields.Many2one('ir.actions.act_window', string="View Manager (Action)",
	                            tracking=True, domain="[('res_model','=',model_name)]")
	statistics_item_infos = fields.Binary(compute="_compute_statistics_item_infos")
	description = fields.Text("Description")
	query_options = fields.Text("SQL options")
	warehouse_option = fields.Boolean("Warehouse Filter")
	active = fields.Boolean('Active', default=True)
	show_sale = fields.Boolean("Show Sales?", help="Show on sales module for sales user/manager")
	show_purchase = fields.Boolean("Show Purchase?", help="Show on purchase module for purchase user/manager")
	show_warehouse = fields.Boolean("Show Warehouse?", help="Show on Inventory module for warehouse user/manager")
	
	# -------------- Generate views --------------- #
	def _compute_statistics_item_infos(self):
		"""
		Start collect data using concat query
		"""
		warehouse_obj = self.env['stock.warehouse']
		warehouses = warehouse_obj.search([])
		for statistics in self:
			print(statistics.name)
			infos = []
			if self.env.context.get('warehouse_id'):
				warehouses = warehouse_obj.browse(self.env.context.get('warehouse_id'))
			if statistics.warehouse_option:
				for warehouse_id in warehouses:
					infos.append({'type': 'header', 'name': warehouse_id.display_name, 'line_id': warehouse_id.id})
					sql = "%s " % statistics.query_sql
					sql += "AND o.warehouse_id = %s " % warehouse_id.id
					sql += "%s" % statistics.query_options
					if statistics.query_limit and not self.env.context.get('full_description'):
						sql += "LIMIT %s " % statistics.query_limit
					infos = statistics._collect_data(sql, infos)
			else:
				sql = "%s " % statistics.query_sql
				sql += "%s" % statistics.query_options
				if statistics.query_limit and not self.env.context.get('full_description'):
					sql += "LIMIT %s " % statistics.query_limit
				infos = statistics._collect_data(sql, infos)
			statistics.statistics_item_infos = infos
	
	def _collect_data(self, sql, infos):
		"""
		Execute query and append results
		:param sql:
		:param infos: list of results
		:return: appended list of results
		"""
		self.env.cr.execute(sql)
		results = self.env.cr.fetchall()
		if results:
			for line in results:
				infos.append({
					'type': 'line',
					'line_count': line[0],
					'name': line[1],
					'description': line[2],
					'line_id': line[3],
					'statistics_id': self.id,
				})
		elif not results and self.warehouse_option:
			infos.pop()
		return infos
	
	def generate_statistics_lines(self):
		"""
		Reload view using new methods
		"""
		# raise Warning("%s" % self.env.context)
		compose_kanban = self.env.ref('tatayab_statistics_dashboard.tatayab_statistics_kanban_view', False)
		action = {
			"type": "ir.actions.act_window",
			'view_mode': 'kanban',
			'view_id': compose_kanban and compose_kanban.id,
			"res_model": self._name,
			"name": _('Statistics Dashboard'),
		}
		if self.env.context.get('domain'):
			action['context'] = {'search_default_%s' % self.env.context.get('domain'): 1 }
		return action
	
	# -------------- Open views --------------- #
	def action_open_line(self):
		"""
		Open view of selected items on object of statistics
		:return: Action
		"""
		self.ensure_one()
		action = self.action_id
		if not action:
			raise Warning(_("View Manager (Action) not defined in configuration."))
		action_data = action.read()[0]
		records = []
		for row in self.statistics_item_infos:
			if row['type'] == 'line' and row not in records:
				records.append(row['line_id'])
		action_data.update({'domain': [('id', 'in', records)]})
		return action_data
	
	def action_view_results(self, warehouse_id=False):
		"""
		Open full detail for i option
		:param warehouse_id:
		:return:
		"""
		if self.warehouse_option and not warehouse_id:
			action_id = self.env.ref('tatayab_statistics_dashboard.statistics_filters_action')
			action = action_id.read()[0]
			action['context'] = {'default_statistics_id': self.id, 'statistics_id': self.id, }
			return action
		action_id = self.env.ref('tatayab_statistics_dashboard.action_tatayab_statistics_details')
		action = action_id.read()[0]
		action['target'] = 'current'
		action['domain'] = [('id', '=', self.id)]
		context = {}
		if warehouse_id:
			context['warehouse_id'] = warehouse_id.id
		context['full_description'] = True
		action['context'] = context
		print("ACTION: ", action)
		return action


# Ahmed Salama Code End.
