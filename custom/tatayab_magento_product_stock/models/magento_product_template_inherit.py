from odoo import api, fields, models


class MagentoProductTemplate(models.Model):
    _inherit = 'magento.product.template'

    seller_total_qty = fields.Float(string="Seller Quantity(B2B)",related='odoo_product_template_id.seller_total_qty')
    warehouse_quantity = fields.Char(string='Quantity per warehouse',related='odoo_product_template_id.warehouse_quantity')
    total_quantity = fields.Float(string="Total Quantity",compute="compute_total_stock_quantity",store=True)
    total_free_quantity = fields.Float(string="Total Free Quantity")

    @api.depends('odoo_product_template_id','odoo_product_template_id.seller_total_qty','odoo_product_template_id.qty_available')
    def compute_total_stock_quantity(self):
        for rec in self:
            rec.total_quantity = rec.odoo_product_template_id.seller_total_qty + rec.odoo_product_template_id.qty_available
            rec.total_free_quantity = rec.odoo_product_template_id.seller_total_qty + rec.odoo_product_template_id.free_qty

class MagentoProductProduct(models.Model):
    _inherit = 'magento.product.product'

    seller_total_qty = fields.Float(string="Seller Quantity(B2B)",related='odoo_product_id.seller_total_qty')
    warehouse_quantity = fields.Char(string='Quantity per warehouse',related='odoo_product_id.warehouse_quantity')
    total_quantity = fields.Float(string="Total Quantity",compute="compute_total_stock_quantity",store=True)
    total_free_quantity = fields.Float(string="Total Free Quantity")

    @api.depends('odoo_product_id','odoo_product_id.seller_total_qty','odoo_product_id.qty_available')
    def compute_total_stock_quantity(self):
        for rec in self:
            rec.total_quantity = rec.odoo_product_id.seller_total_qty + rec.odoo_product_id.qty_available
            rec.total_free_quantity = rec.odoo_product_id.seller_total_qty + rec.odoo_product_id.free_qty