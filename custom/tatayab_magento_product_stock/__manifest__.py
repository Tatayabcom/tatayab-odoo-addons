
{
    'name': "Tatayab Product Stock in Magento",
    'author': 'Tatayab, Doaa Negm',
    'category': 'ALL',
    'summary': """Tatayab  Product Stock in Magento
     """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
    Add new tab in Magento Product form view, the tab contains the stock details:
        
        KWT, KSA and Qatar Quantities available.
        
        Number of sellers have quantities available.
        
        Seller quantity Total.
""",
    'version': '1.0',
    'depends': ['base','tatayab_back_to_back','product_warehouse_quantity','odoo_magento2_ept'],
    'data': [
        'views/magento_product_template_inherit_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
