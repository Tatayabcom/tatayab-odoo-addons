from odoo import fields, models, api, _
from lxml import etree


class PurchaseOrderInherit(models.Model):
    _inherit = 'purchase.order'

    # Add by Omnya >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(PurchaseOrderInherit, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                                submenu=submenu)

        group_id = self.env.user.has_group('tatayab_disable_create_on_fly.group_product_disable_on_fly_create')
        if view_type == 'form' and group_id:
            doc = etree.XML(res['fields']['order_line']['views']['tree']['arch'])
            update = False
            for field in doc.xpath("//field[@name='product_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            for field in doc.xpath("//field[@name='sale_order_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            if update:
                res['fields']['order_line']['views']['tree']['arch'] = etree.tostring(doc)
            doc1 = etree.XML(res["arch"])
            for field in doc1.xpath("//field[@name='partner_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            if update:
                res['arch'] = etree.tostring(doc1)
        return res
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
