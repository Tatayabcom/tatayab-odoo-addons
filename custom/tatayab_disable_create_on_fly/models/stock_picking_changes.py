from odoo import models, fields, _
from lxml import etree


# Ahmed Salama Code Start ---->


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    # Add by Omnya >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(StockPickingInherit, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                               toolbar=toolbar,
                                                               submenu=submenu)

        group_id = self.env.user.has_group('tatayab_disable_create_on_fly.group_product_disable_on_fly_create')
        if view_type == 'form' and group_id:
            doc = etree.XML(res['fields']['move_ids_without_package']['views']['tree']['arch'])
            update = False
            for field in doc.xpath("//field[@name='product_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            if update:
                res['fields']['move_ids_without_package']['views']['tree']['arch'] = etree.tostring(doc)
            doc1 = etree.XML(res["arch"])
            for field in doc1.xpath("//field[@name='location_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            for field in doc1.xpath("//field[@name='location_dest_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            for field in doc1.xpath("//field[@name='partner_id']"):
                field.attrib['options'] = "{'no_create': True}"
                update = True
            if update:
                res['arch'] = etree.tostring(doc1)

        return res
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
