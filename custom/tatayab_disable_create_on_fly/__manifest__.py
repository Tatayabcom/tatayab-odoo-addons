{
    'name': "Tatayab Disable on fly create",
    'author': 'Tatayab, Omnya Rashwan',
    'category': 'ALL',
    'summary': """Tatayab Disable on fly create""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """""",
    'version': '1.0',
    'depends': ['account', 'sale', 'purchase', 'stock'],
    'data': [
        'security/disable_on_fly_group.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
