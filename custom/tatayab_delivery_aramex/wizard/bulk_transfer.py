
from odoo import fields, models, _
from odoo.exceptions import UserError


class BulkTransferInherit(models.TransientModel):
    _inherit = 'bulk.order.transfer'
    
    def print_international_label(self):
        self.ensure_one()
        all_pickings = self.line_ids.filtered(lambda l: l.driver_type == 'international').mapped('picking_id')
        international_pickings = all_pickings.filtered(lambda p: p.carrier_tracking_ref)
        if international_pickings:
            return international_pickings[0].with_context(
                report_field='attach_label_id').print_reports()
        else:
            raise UserError(_("Couldn't found International picking for companies OCS/ARAMEX valid to type print"))
