# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import Warning
# Ahmed Salama Code Start ---->
_TYPES = [('percentage', 'Percentage'), ('amount', 'Amount')]


class ResCompanyInherit(models.Model):
	_inherit = 'res.company'
	
	threshold_amount = fields.Monetary("Threshold Amount",
	                                   help="Amount to use on ARAMEX conditions")
	threshold_country_ids = fields.Many2many(comodel_name='res.country', string="Countries")
	custom_duties_val = fields.Float("Custom Duties Value")
	custom_duties_type = fields.Selection(_TYPES, "Custom Duties Type")
	vat_val = fields.Float("VAT Value")
	vat_type = fields.Selection(_TYPES, "VAT Type")

# Ahmed Salama Code End.
