# -*- coding: utf-8 -*-
import logging

from odoo import models, fields
from odoo.http import request

logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->


class AramexSession(models.Model):
	_name = 'aramex.session'
	_description = "Aramex Session records"
	
	user_id = fields.Many2one('res.users', "User")
	session_id = fields.Char("Session Id")
	expiration_date = fields.Datetime("Expiration Date")


class HttpInherit(models.AbstractModel):
	_inherit = 'ir.http'
	
	def session_info(self):
		res = super(HttpInherit, self).session_info()
		res['session_id'] = request.session.sid
		logger.info(blue + "Session: %s" %  res['session_id'] + reset)
		self.env['aramex.session'].sudo().create({
			'user_id': request.session.uid,
			'session_id': request.session.sid,
			'expiration_date': res['expiration_date']
		})
		return res
# Ahmed Salama Code End.