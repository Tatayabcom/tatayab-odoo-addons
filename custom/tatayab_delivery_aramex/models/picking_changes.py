import logging
from datetime import datetime

from odoo import models, fields, _, api
from odoo.exceptions import UserError

logger = logging.getLogger(__name__)
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

ARAMEX_DATETIME_FORMAT = "%Y/%m/%d %H:%M:%S"
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
ARAMEX_PINumber = [('SH005', 'Shipment Delivered'),
                   ('SH006', 'Collected By Consignee'),
                   ('SH012', 'Shipment Picked Up'),
                   ('SH013', 'Shipment Record Created'),
                   ('SH539', 'Shipment Returned to Shipper'),
                   ('SH156', 'Shipment On-Hold'),
                   ('SH597', 'SHDL - Shipment Delivered at Doorstep'),
                   ]


# def format_server_date(dt):
# 	if not dt:
# 		return
# 	return dt.strftime(TA_DATE_FORMAT)
#
#
class SyncLog(models.Model):
    _name = 'sync.log'
    _description = "Sync Log"
    _order = "create_date desc"

    name = fields.Char("Name", required=True)
    parameters = fields.Text("Parameters")
    api_response = fields.Text("Response")
    api_response_code = fields.Char("Response Code")
    success_count = fields.Integer("Success Lines")
    success_text = fields.Text("Success Response")
    ignored_count = fields.Integer("Ignored Lines")
    ignored_text = fields.Text("Ignored Text")
    error_count = fields.Integer("Error Lines")
    error_text = fields.Text("Error Text")
    product_results = fields.Char("Product Results")
    user_id = fields.Many2one('res.users', 'Requester', default=lambda self: self.env.user)
    model_id = fields.Many2one("ir.model", string="Sync Object")
    res_ids = fields.Char("List Of Created Ids")


class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'
    
    aramex_details_ids = fields.One2many('aramex.order.details', 'sale_id', "Aramex Details")


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'
    
    aramex_label_url = fields.Char("Aramex Label URL", copy=False)
    aramex_details_ids = fields.One2many('aramex.order.details', 'picking_id', "Aramex Details")
    aramex_informed = fields.Boolean("Informed By Aramex?", help="Raised if received any details from aramex")
    last_pin_number = fields.Selection(selection=ARAMEX_PINumber, string="Last PINumber")
    awb_amount = fields.Float("AWB Amount", readonly=True, help="Amount Used in AWB")
    cs_amount = fields.Float("CS Amount", readonly=True, help="Amount Used in AWB")
    manual_tracking_ref = fields.Text("Aramex Click to Ship")
    
    @api.onchange('awb_amount', 'cs_amount')
    def _get_picking_price_issue(self):
        for picking in self:
            price_issue = False
            if picking.awb_amount and picking.cs_amount and \
                    picking.awb_amount != picking.cs_amount:
                price_issue = True
            picking.price_issue_comp = price_issue
            picking.write({'price_issue': price_issue})

    price_issue = fields.Boolean("Price Not Match?", default=False,
                                 help="Price Matching is invalid between AWB price & CS Price")
    price_issue_comp = fields.Boolean(compute=_get_picking_price_issue)
    
    def print_reports(self):
        self.ensure_one()
        ctx = self.env.context
        report_field = ctx.get('report_field')
        if report_field and self.driver_type == 'international':
            report_name = ctx.get('report_name') or 'International Label '
            if self[report_field]:
                return {
                    'type': 'ir.actions.act_url',
                    'url': '/web/content/ir.attachment/%s/%s/%s.%s?download=true' % (
                        self[report_field].id, 'datas', (self.name + '-' + report_name).replace('/', '-'), 'pdf'),
                    'target': 'new',
                }
        else:
            raise UserError(_("Please contact your admin!!! \n This button couldn't effect on this case"))
    
    def _update_order_details(self, params):
        """
        Receive params from aramex and create log onn sync.log
        Create line on order/picking with this action details
        Validate and close picking in case of done SH005/SH006/SH597
        :param params: Json received from Aramex
        :return: respond with success/error/ignore details
        'status':
                    200: it's success
                    300: something unexpected happened
                    400: error with receiving data
        'message':
                    Description of respond.
        
        """
        response = {}
        print("params",params)
        # config = self.env.ref('tatayab_sync.sale_sync_config')
        current_time = fields.Datetime.context_timestamp(
            self.with_context(tz=self.env.user.tz), fields.Datetime.now()).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        log = self.env['sync.log'].create({
            'name': 'Aramex Respond Order Details :: %s' % current_time,
            # 'parameters': config.url,
            'api_response': params,
            # 'sync_config_id': config.id,
        })
        if not all(item in params for item in ['TrackingResults']):
            response["status"] = 401
            response["message"] = "Missing TrackingResults Keys "
            logger.info(red + "Aramex Response Error: \n%s" % response + reset)
            log.error_count = 1
            log.error_text = response["message"]
            return response

        # Check for PINumber code
        # if params.get('PINumber') not in ['SH005', 'SH006', 'SH012', 'SH013', 'SH156', 'SH539', 'SH597']:
        #     response["status"] = 402
        #     response["message"] = "PINumber %s isn't on our list of responds" % params.get('PINumber')
        #     logger.info(red + "Aramex Response Error: \n%s" % response + reset)
        #     log.error_count = 1
        #     log.error_text = response["message"]
        #     return response
        # Add extra future to search with Tracking ref number in case of missing other details
        for rec_tracking in params.get('TrackingResults'):
            carrier_tracking_ref = rec_tracking.get('Key')
            print("carrier_tracking_ref",carrier_tracking_ref)
            picking_id = self.env['stock.picking'].search([('carrier_tracking_ref', '=', carrier_tracking_ref)])
            if picking_id:
                sale_id = picking_id.sale_id
            if not picking_id:
                response["status"] = 403
                response["message"] = "Couldn't found Picking on odoo with those details [%s]" % params.get('Reference2')
                logger.info(red + "Aramex Response Error: \n%s" % response + reset)
                log.error_count = 1
                log.error_text = response["message"]
                return response
            for val in rec_tracking.get('Value'):
                # Remove unneeded letters on date field
                if val.get('UpdateDateTime'):
                    aramex_date = val.get('UpdateDateTime')
                    if '+0200)/' in val.get('UpdateDateTime'):
                        aramex_date = aramex_date.replace('+0200)/', '')
                    if '/Date(' in val.get('UpdateDateTime'):
                        aramex_date = aramex_date.replace('/Date(', '')
                    aramex_date = datetime.fromtimestamp(int(aramex_date)/1000)
                else:
                    aramex_date = fields.Datetime.now()
                print("aramex_date",aramex_date)
                # Check for PINumber code
                # if val.get('UpdateCode') not in ['SH005', 'SH006', 'SH012', 'SH013', 'SH156', 'SH539', 'SH597']:
                #     response["status"] = 402
                #     response["message"] = "PINumber %s isn't on our list of responds" % params.get('PINumber')
                #     logger.info(red + "Aramex Response Error: \n%s" % response + reset)
                #     log.error_count = 1
                #     log.error_text = response["message"]
                #     return response
                if val.get('UpdateCode') in ['SH005', 'SH006', 'SH012', 'SH013', 'SH156', 'SH539', 'SH597']:
                    details = {
                        'sale_id': picking_id.sale_id.id,
                        'company_id': picking_id.sale_id.company_id.id,
                        'picking_id': picking_id and picking_id.id or False,
                        'date': aramex_date,
                        'awb_number': val.get('WaybillNumber'),
                        'pin_number': val.get('UpdateCode'),
                        'comment_1': val.get('UpdateDescription'),
                        'comment_2': val.get('UpdateLocation'),
                        'log_id': log.id,
                    }
                    detail_id = self.env['aramex.order.details'].create(details)
                    if detail_id:
                        picking_id.write({'aramex_informed': True,
                                          'last_pin_number': detail_id.pin_number})
                        response["status"] = 200
                        response["message"] = "Aramex Order detail created for order: %s with id: %s" % (
                            picking_id.sale_id.display_name, detail_id.id)
                        logger.info(green + "Aramex Response success: %s" % response + reset)
                        log.success_count = 1
                        # TODO Close order as done in case of SH005/SH006/SH597
                        if detail_id.pin_number in ['SH005', 'SH006', 'SH597']:
                            # if picking_id.operation_state == 'done':
                            if picking_id.operation_state == 'ship':
                                picking_id.action_done_shipment()
                            else:
                                response["message"] += "\n Order Already Validated!!!"
                                log.success_count += 1
                        log.success_text = response["message"]
                        picking_id.message_post(body=response["message"])
                        return response
                    response["status"] = 101
                    response["message"] = "Something unexpected happen while creating aramex order: %s details!!!\n" \
                                          "Action Ignored, Please review with Odoo team." % picking_id.sale_id.display_name
                    logger.info(yellow + "Aramex Response ignore: \n%s" % response + reset)
                    log.ignore_count = 1
                    log.ignore_text = response["message"]
                    return response
    # def _update_order_details(self, params):
    #     """
    #     Receive params from aramex and create log onn sync.log
    #     Create line on order/picking with this action details
    #     Validate and close picking in case of done SH005/SH006/SH597
    #     :param params: Json received from Aramex
    #     :return: respond with success/error/ignore details
    #     'status':
    #                 200: it's success
    #                 300: something unexpected happened
    #                 400: error with receiving data
    #     'message':
    #                 Description of respond.
    #
    #     """
    #     response = {}
    #     # config = self.env.ref('tatayab_sync.sale_sync_config')
    #     current_time = fields.Datetime.context_timestamp(
    #         self.with_context(tz=self.env.user.tz), fields.Datetime.now()).strftime(
    #         DEFAULT_SERVER_DATETIME_FORMAT)
    #     log = self.env['sync.log'].create({
    #         'name': 'Aramex Respond Order Details :: %s' % current_time,
    #         # 'parameters': config.url,
    #         'api_response': params,
    #         # 'sync_config_id': config.id,
    #     })
    #     if not all(item in params for item in ['Reference1', 'Reference2', 'HAWBNumber', 'PINumber', 'date']):
    #         response["status"] = 401
    #         response["message"] = "Missing One of required Keys"
    #         logger.info(red + "Aramex Response Error: \n%s" % response + reset)
    #         log.error_count = 1
    #         log.error_text = response["message"]
    #         return response
    #     # Check for PINumber code
    #     if params.get('PINumber') not in ['SH005', 'SH006', 'SH012', 'SH013', 'SH156', 'SH539', 'SH597']:
    #         response["status"] = 402
    #         response["message"] = "PINumber %s isn't on our list of responds" % params.get('PINumber')
    #         logger.info(red + "Aramex Response Error: \n%s" % response + reset)
    #         log.error_count = 1
    #         log.error_text = response["message"]
    #         return response
    #     # Add extra future to search with Tracking ref number in case of missing other details
    #     picking_id = self.env['stock.picking'].search([('carrier_tracking_ref', '=', params.get('HAWBNumber'))])
    #     if picking_id:
    #         sale_id = picking_id.sale_id
    #     else:
    #         # Use other methodology to find order first with ref details
    #         sale_id = self.env['sale.order'].search(['|', ('origin', '=', params.get('Reference1')),
    #                                                  ('name', '=', params.get('Reference3'))])
    #         if not sale_id:
    #             response["status"] = 403
    #             response["message"] = "Couldn't found order on odoo with those details [%s][%s]" % \
    #                                   (params.get('Reference1'), params.get('Reference3'))
    #             logger.info(red + "Aramex Response Error: \n%s" % response + reset)
    #             log.error_count = 1
    #             log.error_text = response["message"]
    #             return response
    #         picking_id = self.env['stock.picking'].search([('name', '=', params.get('Reference2'))])
    #         if not picking_id:
    #             response["status"] = 403
    #             response["message"] = "Couldn't found Picking on odoo with those details [%s]" % params.get('Reference2')
    #             logger.info(red + "Aramex Response Error: \n%s" % response + reset)
    #             log.error_count = 1
    #             log.error_text = response["message"]
    #             return response
    #     # Remove unneeded letters on date field
    #     if params.get('date'):
    #         aramex_date = params.get('date')
    #         if 'T' in params.get('date'):
    #             aramex_date = params.get('date').replace('T', ' ')
    #         aramex_date = datetime.strptime(aramex_date, DEFAULT_SERVER_DATETIME_FORMAT)
    #     else:
    #         aramex_date = fields.Datetime.now()
    #     details = {
    #         'sale_id': sale_id.id,
    #         'company_id': sale_id.company_id.id,
    #         'picking_id': picking_id and picking_id.id or False,
    #         'date': aramex_date,
    #         'awb_number': params.get('HAWBNumber'),
    #         'pin_number': params.get('PINumber'),
    #         'comment_1': params.get('Comment1'),
    #         'comment_2': params.get('Comment2'),
    #         'log_id': log.id,
    #     }
    #     detail_id = self.env['aramex.order.details'].create(details)
    #     if detail_id:
    #         picking_id.write({'aramex_informed': True,
    #                           'last_pin_number': detail_id.pin_number})
    #         response["status"] = 200
    #         response["message"] = "Aramex Order detail created for order: %s with id: %s" % (
    #             sale_id.display_name, detail_id.id)
    #         logger.info(green + "Aramex Response success: %s" % response + reset)
    #         log.success_count = 1
    #         # TODO Close order as done in case of SH005/SH006/SH597
    #         if detail_id.pin_number in ['SH005', 'SH006', 'SH597']:
    #             # if picking_id.operation_state == 'done':
    #             if picking_id.operation_state == 'ship':
    #                 picking_id.action_done_shipment()
    #             else:
    #                 response["message"] += "\n Order Already Validated!!!"
    #                 log.success_count += 1
    #         log.success_text = response["message"]
    #         picking_id.message_post(body=response["message"])
    #         return response
    #     response["status"] = 101
    #     response["message"] = "Something unexpected happen while creating aramex order: %s details!!!\n" \
    #                           "Action Ignored, Please review with Odoo team." % sale_id.display_name
    #     logger.info(yellow + "Aramex Response ignore: \n%s" % response + reset)
    #     log.ignore_count = 1
    #     log.ignore_text = response["message"]
    #     return response

    @api.model
    def update_aramex_informed_details(self):
        """
        This method was set to update aramex_informed field for old data
        TODO: no need fot it
        :return:
        """
        
        un_marked_pickings = self.env['stock.picking'].search([('aramex_details_ids', '!=', False),
                                                               ('delivery_type', '=', 'aramex'),
                                                               '|', ('last_pin_number', '=', False),
                                                               ('aramex_informed', '=', False)])
        logger.info(red + "Aramex Update Old records: %s" % len(un_marked_pickings) + reset)
        if un_marked_pickings:
            for idx, picking in enumerate(un_marked_pickings):
                if picking.aramex_details_ids:
                    picking.aramex_informed = True
                    picking.last_pin_number = picking.aramex_details_ids and picking.aramex_details_ids[-1].pin_number
                    logger.info(yellow + "%s-Picking: %s, PIN: %s" %
                                (idx, picking.origin, picking.last_pin_number) + reset)
        logger.info(green + "All Updates..." + reset)


class AramexOrdersDetails(models.Model):
    _name = 'aramex.order.details'
    _description = "Aramex Order Details"
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _check_company_auto = True
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company.id)
    sale_id = fields.Many2one('sale.order', "Order", ondelete='cascade', required=True)
    picking_id = fields.Many2one('stock.picking', "Picking")
    date = fields.Datetime("Date", required=True)
    awb_number = fields.Char("HAWBNumber", required=True)
    pin_number = fields.Selection(selection=ARAMEX_PINumber, string="PINumber")
    comment_1 = fields.Text("Comment1")
    comment_2 = fields.Text("Comment2")
    log_id = fields.Many2one('sync.log', "Log")
