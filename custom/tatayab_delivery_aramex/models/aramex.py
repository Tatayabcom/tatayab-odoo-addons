# -*- coding: utf-8 -*-
import logging
import random

import suds
from odoo.addons.tatayab_delivery_core.models.delivery_changes import RANDOM_AMOUNTS
from suds.client import Client

from odoo import fields, models, _
from odoo.exceptions import UserError

CONNECTION_ERROR_MSG = 'Could not connect to Aramex payment service.'
AR_UOM_DIMENSION = 'cm'
AR_UOM_WEIGHT = 'Kg'
logger = logging.getLogger(__name__)
ARAMEX_DATETIME_FORMAT = "%Y/%m/%d %H:%M:%S"
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class AramexError(RuntimeError):
    pass


class AramexClient:
    """Set up Aramex Suds SOAP client and handle connection errors."""
    def __init__(self, wsdl):
        self.wsdl = wsdl

    def __enter__(self):
        """Set up Suds SOAP client and handle related connection errors."""
        try:
            client = Client(self.wsdl)
        except OSError as err:
            logger.error(err)
            # raise AramexError(CONNECTION_ERROR_MSG) from err
        return client

    def __exit__(self, type, value, tb):
        """Handle any connection errors in context manager body."""
        if type and issubclass(type, OSError):
            logger.error(value)
            raise AramexError(CONNECTION_ERROR_MSG) from value
        elif type == suds.WebFault:
            logger.error(value)

            print('___ value.fault : ', value.fault)
            raise value


class DeliveryCarrierInherit(models.Model):
    _inherit = 'delivery.carrier'

    delivery_type = fields.Selection(selection_add=[('aramex', 'Aramex')]
                                     , ondelete={'aramex': lambda recs: recs.write({'delivery_type': 'fixed', 'fixed_price': 0})})
    aramex_show_log = fields.Boolean(string='Show Api Log')
    # Fields required to configure
    aramex_username = fields.Char("Username")
    aramex_password = fields.Char("Password")
    aramex_account_number = fields.Char(string="Account Number")
    aramex_account_pin = fields.Char(string="Account PIN")
    aramex_account_entity = fields.Char(string="Account Entity", default="AMM")
    aramex_account_version = fields.Char(string="Account Version", default="v1.0")
    aramex_account_code = fields.Char(string="Account Country Code", default="JO")
    aramex_product_type = fields.Selection(selection=[('PPX', 'Priority Parcel Express(PPX)'),
                                                      ('PDX', 'Priority Document Express(PDX)'),
                                                      ('PLX', 'Priority Letter Express(PLX)'),
                                                      ('DDX', 'Deferred Document Express(DDX)'),
                                                      ('DPX', 'Deferred Parcel Express(DPX)'),
                                                      ('CDS', 'Cell Directory Service(CDS)'),
                                                      ('GDX', 'Ground Document Express(GDX)')], string="Product Type",
                                           default='PPX')
    aramex_description_of_gods = fields.Selection(selection=[('Cosmetics', 'Cosmetics'), ('PERFUME', 'PERFUME')],
                                                  string="Description Of Goods", default='PERFUME')
    aramex_lqud = fields.Boolean("Aramex LQUD",
                                 help="Used to define service if it will be with LQUD or not")
    aramex_source = fields.Integer(string="Source", default=0)
    aramex_product_group = fields.Selection(selection=[('EXP', 'Express'), ('DOM', 'Domestic')], string="Product Group",
                                            default='EXP')
    default_aramex_custom = fields.Float("Custom Value", default=1.0)
    aramex_wsdl_pci = fields.Char("WSDL URL",
                                  default="https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc?wsdl")

    aramex_print_label_url = fields.Char('Aramex_Print_Label_URL')
    # aramex_tracking_url = fields.Char('Aramex_TRACKING_URL')
    aramex_shipment_create_url = fields.Char('Aramex_SHIPMENT_CREATE_URL')

    def aramex_datetime_format(self, dt):
        return dt.strftime(ARAMEX_DATETIME_FORMAT) if dt else ''

    def aramex_set_value(self, var, vals):
        for k, v in vals.items():
            if v:
                var[k] = v
        return var

    def ar_get_amount(self, currency, amount):
        comp = self.env.company
        comp_cur = comp.currency_id
        if comp_cur.id == currency.id:
            return amount
        return round(comp_cur.compute(amount, currency))
        # return round(comp_cur._convert(amount, currency, comp, fields.Date.today()), 4)

    def aramex_parseMoney(self, client, currency, is_cod=False, amount=0.0):
        # TODO:: Stopped to be handle use it in warehouse KWT only
        # if is_cod:
        #     amount = self.ar_get_amount(currency, amount)
        Money = client.factory.create('ns1:Money')
        Money.Value = amount
        Money.CurrencyCode = currency.name
        return Money

    def aramex_parseParty(self, client, Element, record, picking, consignee=False):
        addr_vals = {
            "Line1": record.street or 0,
            "Line2": record.street2 or 0,
            "Line3": 0,
            "Latitude": 0,
            "Longitude": 0,
            "PostCode": record.zip or '',
            "City": record.state_id and record.state_id.name or record.city or '',
            "StateOrProvinceCode": record.state_id.code or '',
            "CountryCode": record.country_id.code or '',
        }
        if consignee:
            # Add National ID
            addr_vals['Line3'] = "National ID: %s" % record.national_id or ""
        Address = self.aramex_set_value(client.factory.create('ns1:Address'), addr_vals)
        if record._name == 'res.company':
            mobile = phone = record.phone
            company_name = record.name
        else:
            phone = record.phone
            # mobile = record.mobile
            mobile = record.phone
            # company_name = record.name if record.is_company else (record.parent_id.name or '')
        contact_vals = {
            'Department': '',
            'PersonName': record.name,
            'CompanyName': record.name,
            'PhoneNumber1': phone,
            'PhoneNumber1Ext': '',
            'PhoneNumber2': '',
            'PhoneNumber2Ext': '',
            'FaxNumber': '',
            'CellPhone': mobile,
            'EmailAddress': record.email,
            'Type': 0,
        }
        if consignee:
            contact_vals['PersonName'] = record.name or 'Customer'
            contact_vals['CompanyName'] = record.name or 'Customer'
        Contact = self.aramex_set_value(client.factory.create('ns1:Contact'), contact_vals)
        if consignee:
            Element.Reference1 = picking.sale_id.name
            Element.Reference2 = picking.name
            # Element.Reference3 = picking.sale_id.name or picking.origin
        else:
            Element.Reference1 = record.name
            Element.Reference2 = str(record.id)
        Element.AccountNumber = self.aramex_account_number
        Element.PartyAddress = Address
        Element.Contact = Contact
        return Element

    def aramex_get_package(self, client, qty, picking, wt_pr_pkg):
        prod_name = '%s-%s' % (picking.origin, picking.name)
        ShipmentItem = client.factory.create('ns1:ShipmentItem')
        ShipmentItem.PackageType = 'Box'
        ShipmentItem.Quantity = int(qty)
        ShipmentItem.Comments = prod_name
        ShipmentItem.GoodsDescription = picking.product_id.description_of_goods
        ShipmentItem.Reference = prod_name

        Weight = client.factory.create('ns1:Weight')
        Weight.Unit = AR_UOM_WEIGHT
        Weight.Value = wt_pr_pkg
        ShipmentItem.Weight = Weight

        return [ShipmentItem]

    def aramex_send_shipping(self, pickings):
        if self.aramex_show_log:
            logger.info(blue + "__ Start Aramex Sending Order __" + reset)
        company = self.env.company
        wsdl_pci = self.aramex_wsdl_pci
        Attachment = self.env['ir.attachment']
        carrier_log_obg = self.env['delivery.carrier.log']

        with AramexClient(wsdl_pci) as client:

            ClientInfo = client.factory.create('ns1:ClientInfo')
            ClientInfo.UserName = self.aramex_username
            ClientInfo.Password = self.aramex_password
            ClientInfo.Version = self.aramex_account_version
            ClientInfo.AccountNumber = self.aramex_account_number
            ClientInfo.AccountPin = self.aramex_account_pin
            ClientInfo.AccountEntity = self.aramex_account_entity
            ClientInfo.AccountCountryCode = self.aramex_account_code or ''
            ClientInfo.Source = self.aramex_source
            LabelInfo = client.factory.create('ns1:LabelInfo')
            # LabelInfo.ReportID = 9201
            LabelInfo.ReportID = 9729
            LabelInfo.ReportType = 'RPT'
            client.sd[0].service.setlocation(wsdl_pci)
            Transaction = client.factory.create('ns1:Transaction')
            Transaction.Reference1 = "-".join(pickings.mapped('sale_id.name'))
            Transaction.Reference2 = "-".join([picking.sale_id.origin and picking.sale_id.origin or '' for picking in pickings])
            # Transaction.Reference3 = "-".join(pickings.mapped('name'))

            def create_attachment(picking, name, datas):
                return Attachment.sudo().create({
                    'name': ("%s-%s" % (picking.name, name)).replace('/', '_'),
                    'datas': datas,
                    'res_model': picking._name,
                    'res_id': picking.id
                }).id
            if self.aramex_show_log:
                logger.info(blue + "__ Pickings %s __" % pickings.mapped('sale_id.name') + reset)
            for pick in pickings:
                goods_type = pick.move_ids_without_package.mapped('product_id.description_of_goods')
                description_of_goods = pick.type_of_goods
                # Shipper
                Shipper = client.factory.create('ns1:Party')
                warehouse = pick.sale_id.warehouse_id
                Shipper = self.aramex_parseParty(client, Shipper, warehouse.partner_id, pick)
                order = pick.sale_id
                is_cod, cod_amount, currency = self.order_cod_amount(order)  # Method implemented on tatayab_delivery_core

                # Add by Omnya##########################################################
                # To handle split of orders transfer
                # if cod_amount >= 0.0:
                #     for order_line in order.order_line:
                #         for line in pick.move_ids_without_package.mapped('product_id'):
                #             if line == order_line.product_id:
                #                 if cod_amount == 0.0:
                #                     cod_amount += order_line.usd_row_total
                #                 elif cod_amount > 0.0:
                #                     cod_amount = order_line.usd_row_total
                # End of Code ##########################################################
                cur_name = currency.name
                logging.info(bold_red + "Currency: %s" % cur_name + reset)
                Consignee = client.factory.create('ns1:Party')

                # Yes: Intentionally send picking here, to grab address details
                Consignee = self.aramex_parseParty(client, Consignee, pick.partner_id, pick, consignee=True)

                # ThirdParty = client.factory.create('ns1:Party')
                # ThirdParty = self.aramex_parseParty(client, ThirdParty, ResPartner, pick)

                # Prepare ArrayOfShipment
                Shipment = client.factory.create('ns1:Shipment')
                Shipment.Shipper = Shipper
                Shipment.Consignee = Consignee
                Shipment.ThirdParty = None

                Shipment.Reference1 = order.name
                Shipment.Reference2 = pick.name
                # Shipment.Reference3 = str(order.id)
                # Shipment.Reference3 = ""
                # cur_amount_total = self.ar_get_amount(currency, order.amount_total)

                Shipment.ForeignHAWB = ''
                # Shipment.TransportType = 0
                Shipment.ShippingDateTime = fields.Datetime.now()
                Shipment.DueDate = fields.Datetime.now()
                Shipment.PickupLocation = 'Reception'
                Shipment.PickupGUID = ''

                Shipment.AccountingInstrcutions = ''
                Shipment.OperationsInstructions = ''

                # Details
                ShipmentDetails = client.factory.create('ns1:ShipmentDetails')

                Dimensions = client.factory.create('ns1:Dimensions')
                Dimensions.Length = 10.0
                Dimensions.Width = 10.0
                Dimensions.Height = 10.0
                Dimensions.Unit = AR_UOM_DIMENSION
                ShipmentDetails.Dimensions = Dimensions

                tot_qty = tot_weight = 0
                for move in pick.move_ids_without_package:
                    tot_qty += move.product_uom_qty
                    tot_weight += move.product_id.weight

                tot_pkg = pick.pkg_half + pick.pkg_one + pick.pkg_two + pick.pkg_three
                tot_pkg_weight = pick.pkg_half + pick.pkg_one + pick.pkg_two + pick.pkg_three
                wt_pr_pkg = tot_weight / (tot_pkg or 1)

                shipment_item_list = []
                if pick.pkg_half:
                    tot_pkg_weight += (pick.pkg_half * 0.5)
                    shipment_item_list += self.aramex_get_package(
                        client, pick.pkg_half, pick, wt_pr_pkg)
                if pick.pkg_one:
                    tot_pkg_weight += pick.pkg_one
                    shipment_item_list += self.aramex_get_package(
                        client, pick.pkg_one, pick, wt_pr_pkg)
                if pick.pkg_two:
                    tot_pkg_weight += pick.pkg_two * 2
                    shipment_item_list += self.aramex_get_package(
                        client, pick.pkg_two, pick, wt_pr_pkg)
                if pick.pkg_three:
                    tot_pkg_weight += pick.pkg_three * 3
                    shipment_item_list += self.aramex_get_package(
                        client, pick.pkg_three, pick, wt_pr_pkg)

                tot_weight = tot_weight or tot_pkg_weight or 1

                ActualWeight = client.factory.create('ns1:Weight')
                ActualWeight.Unit = AR_UOM_WEIGHT
                ActualWeight.Value = tot_weight
                ShipmentDetails.ActualWeight = ActualWeight

                ShipmentDetails.ProductGroup = self.aramex_product_group
                # ShipmentDetails.ProductType = 'PDX'
                ShipmentDetails.ProductType = self.aramex_product_type
                ShipmentDetails.PaymentType = 'P'
                # ShipmentDetails.PaymentOptions = payment_details
                ShipmentDetails.PaymentOptions = ""
                if 'liquid' in goods_type and order.warehouse_id.kwt_warehouse:
                    # Only warehouse KWT use LQUD on it's service field, KSA doesn't use it
                    ShipmentDetails.Services = "LQUD,"
                    ShipmentDetails.ProductType = "DGX"
                    if is_cod:
                        ShipmentDetails.Services = "CODS,LQUD"
                else:
                    ShipmentDetails.Services = "CODS" if is_cod else ""
                ShipmentDetails.NumberOfPieces = int(tot_pkg)
                # None-Docs -
                # ShipmentDetails.DescriptionOfGoods = self.aramex_description_of_gods
                ShipmentDetails.DescriptionOfGoods = description_of_goods
                ShipmentDetails.GoodsOriginCountry = company.country_id.code
                # collect_amount = order.amount_total - sum(order.order_line.
                #                                           filtered(lambda l: l.product_id.type == 'service')
                #                                           .mapped('price_subtotal'))
                # Check Country Value Rule
                other_notes = ""
                custom_amount = cod_amount
                if not is_cod:
                    custom_amount = 1
                # else:
                #     random_amounts = RANDOM_AMOUNTS[cur_name]
                #     custom_amount = random.randrange(random_amounts[0], random_amounts[1])
                #     other_notes = "<br/> This order used rando, amount on customs: %s <br/>" % custom_amount
                #     logger.error("______ARAMEX: %s" % other_notes)
                Shipment.Comments = None
                # # TODO:: Convert amount to USD only if KWT Warehouse ---> stoped to depend on local currency
                ShipmentDetails.CashOnDeliveryAmount = self.aramex_parseMoney(client, currency, is_cod, cod_amount)
                ShipmentDetails.InsuranceAmount = None
                ShipmentDetails.CollectAmount = None
                ShipmentDetails.CustomsValueAmount = self.aramex_parseMoney(client, currency, is_cod, custom_amount)
                ShipmentDetails.CashAdditionalAmount = None
                ShipmentDetails.CashAdditionalAmountDescription = None
                Weight = client.factory.create('ns1:Weight')
                Weight.Unit = AR_UOM_WEIGHT
                Weight.Value = tot_weight

                ShipmentDetails.ChargeableWeight = Weight
                ShipmentDetails.Items = shipment_item_list

                # Adding National ID
                national_id = pick.partner_id.national_id
                if national_id:
                    add_prop_item = client.factory.create('ns1:AdditionalProperty')
                    add_prop_item.CategoryName = "CustomsClearance"
                    add_prop_item.Name = "ConsigneeTaxIdVATEINNumber"
                    add_prop_item.Value = national_id
                    ShipmentDetails.AdditionalProperties = [[add_prop_item]]
                Shipment.Details = ShipmentDetails
                ArrayOfShipment = client.factory.create('ns1:ArrayOfShipment')
                ArrayOfShipment.Shipment = [Shipment]
                if self.aramex_show_log:
                    logger.info(blue + "__ ARAMEX Client Data %s" % ClientInfo + reset)
                    logger.info(blue + "__ ARAMEX Label info %s" % LabelInfo + reset)
                    logger.info(blue + "__ ARAMEX Transactions %s" % Transaction + reset)
                    logger.info(blue + "__ Shipment DATA %s" % ArrayOfShipment + reset)
                pick.carrier_log_id = carrier_log_obg.create({
                    'carrier_id': self.id,
                    'carrier_request': "__ ARAMEX Client Data %s \n __ ARAMEX Label info %s \n"
                                       " __ ARAMEX Transactions %s \n __ Shipment DATA %s"
                                       % (ClientInfo, LabelInfo, Transaction, ArrayOfShipment),
                })
                details = client.service.CreateShipments(
                    ClientInfo, Transaction, ArrayOfShipment, LabelInfo)
                if details:
                    if details.HasErrors:
                        msg_list = []
                        for notifs in details.Notifications:
                            msg_list.append(notifs[1][0].Message)

                        if details.Shipments:
                            for Ship in details.Shipments.ProcessedShipment:
                                for notifs in Ship.Notifications:
                                    msg_list.append(notifs[1][0].Message)
                        if msg_list:
                            raise UserError("Picking %s for Order: %s sent to Aramex"
                                            "\n Response With ERROR:\n %s"
                                            % (pick.display_name, pick.sale_id.display_name, "\n".join(msg_list)))
                    elif details.Shipments:
                        for Ship in details.Shipments.ProcessedShipment:
                            att_label_id = create_attachment(pick, 'Label-Aramex.pdf',
                                                             Ship.ShipmentLabel.LabelFileContents)
                            write_vals = {
                                'aramex_label_url': Ship.ShipmentLabel.LabelURL,
                                'attach_label_id': att_label_id,
                                'carrier_tracking_ref': Ship.ID}
                            if is_cod:
                                # Change cod tracking fields
                                write_vals['awb_amount'] = cod_amount
                                write_vals['cs_amount'] = order.amount_total
                                # if order.amount_total != custom_amount:
                                #     write_vals['price_issue'] = True
                            pick.write(write_vals)
                            if Ship.ID:
                                # TODO:: Inform customer by his shipments tracking ref
                                mail_template = self.env.ref('tatayab_delivery_aramex.aramex_tracking_ref_email_template')
                                mail_template.send_mail(pick.id, force_send=True)
                            # TODO: to be called and handle to inform backend
                            pick.inform_backend_tracking_ref('aramex')
                            pick.carrier_log_id.tracking_ref = Ship.ID
                            msg = "Shipment created successfully. <br /> Aramex Reference No: %s %s" % (Ship.ID, other_notes)
                            pick.message_post(body=msg)
                            pick.sale_id.message_post(body=msg)

