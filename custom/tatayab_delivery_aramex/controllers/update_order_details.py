from odoo import http
from odoo.http import request, Response
import logging
import functools
logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"

def webservice(f):
    @functools.wraps(f)
    def wrap(*args, **kw):
        try:
            return f(*args, **kw)
        except Exception as e:
            return Response(response=str(e), status=500)
    return wrap


class UpdateOrderDetails(http.Controller):
    
    # def _check_token(self, token=None):
    #     """
    #     - Check if the token is exist in res.users
    #     :return: User that has the token if exists
    #     """
    #     logger.info(blue + "user: %s, token: %s" % (request.env.user.display_name, token) + reset)
    #     user_id = request.env['res.users'].sudo().search([('aramex_token', '=', token)])
    #     logger.info(blue + "user_id: %s" % user_id + reset)
    #     return user_id or False
    
    @http.route('/aramex/update_order_details', auth='public', csrf=False, type='json', methods=['POST'])
    @webservice
    def aramex_update_order_details(self, **kwargs):
        """
        Receive update of order using JSON-RPC
        :param kwargs: argument from Aramex
        :return: response from stock.picking object method _update_order_details
        :JSON_Example: {"HAWBNumber":"33095888464","PINumber":"SH069","date": "2021-02-01T16:56:00",
        "Comment1":"Return under AWB# 33375912721","Comment2":"","ConsigneeReference":"#4173280"}
        """
        # logger.info(blue + "Auth headers: %s" % request.httprequest.headers + reset)
        uid = request.session.uid
        # logger.info(bold_red + "SESSION UID: %s" % uid + reset)
        if uid is None:
            logger.info(yellow + "-------------- Auto Auth -------------" + reset)
            # TODO: depend on token to connect -> Replaced with user/pass
            # access_token = request.httprequest.headers['Authorization']
            # uid = self._check_token(access_token)
            # logger.info(green + "Token: %s, UID:: %s" % (access_token, uid) + reset)
            # if not user_id:
            #     response = {'status': 401, 'message': "No valid token provided!!!"}
            #     return response
            # logger.info(yellow + "UID: %s, \n ARAMEX JSON: %s" % (user_id.display_name, kwargs) + reset)
            # TODO:: Depend on user/pass to connect
            email = request.httprequest.headers.get('Login')
            password = request.httprequest.headers.get('Password')
            db = request.httprequest.headers.get('DB')
            uid = request.session.authenticate(db, email, password)
            # logger.info(green + "NEW UID: %s" % uid + reset)
            if not uid:
                response = {"status": 103,
                            "message": "Credentials Incorrect Details\n"}
                return response
        try:
            response = http.request.env['stock.picking'].with_user(uid)._update_order_details(kwargs)
        except Exception as e:
            response = {"status": 102,
                        "message": "Something unexpected happened while creating aramex order\n"\
                                   "Details: \n %s." % str(e)}
        return response

"""
# SET UP Session:
URL: http://erp-beta.tatayab.com/web/session/authenticate
Method: GET
HEADERS: {'Authorization': {{token}}}
JSON: {"jsonrpc":"2.0",
"params":{
   "db":"tatayab_beta_07feb2021_salama",
   "login":"aramex",
   "password":"kE'pcz6LAMje(@+"
}}
--------------------------------------
# Create Request
URL: http://erp-beta.tatayab.com/aramex/update_order_details?db=tatayab_beta_07feb2021_salama
Method: POST
HEADERS: {'Authorization': {{token}}}
JSON: {"jsonrpc":"2.0",
"params":{
   "db":"tatayab_beta_07feb2021_salama",
   "login":"aramex",
   "password":"kE'pcz6LAMje(@+",
   "HAWBNumber":"33095888464",
   "PINumber":"SH156",
   "date":"2021-02-01T16:56:00",
   "Comment1":"Return under AWB# 33375912721",
   "Comment2":"",
   "Reference1":"#4260128",
   "Reference2":"S97298",
   "Reference3":"KWT/OUT/105828"
}}
--------------------------------------
Beta:
?db=tatayab_beta_07feb2021_salama

"""