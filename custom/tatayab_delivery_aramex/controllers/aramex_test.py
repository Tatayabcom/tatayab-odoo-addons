from odoo import http
from odoo.http import request
import logging
logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class AramexTest(http.Controller):
	
	def _check_token(self, token=None):
		"""
		- Check if the token is exist in res.users
		:return: User that has the token if exists
		"""
		user_id = request.env['res.users'].sudo().search([('aramex_user', '=', True),
		                                                  ('aramex_token', '=', token)])
		return user_id or False
	
	@http.route('/web/aramex/checking', auth='public', methods=['GET'], type='http')
	def aramex_update_order_details(self):
		"""
		URL: http://erp-beta.tatayab.com/web/aramex/checking?db=tatayab_beta_07feb2021_salama
		"""
		access_tocken = request.httprequest.headers['Authorization']
		user_id = self._check_token(access_tocken)
		# Response.status = 200
		if not user_id:
			response = {'result': {'status': 401, 'message': "No valid token provided!!!"}}
			return response
		logger.info(yellow + "UID: %s" % user_id.display_name + reset)
		response = {'status': 200, 'message': "Success"}
		return "Success"
