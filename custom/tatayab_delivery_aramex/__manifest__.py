{
    'name': 'Aramex Delivery',
    'author': 'Tatayab, Ahmed Salama',
    'category': 'ALL',
    'summary': """Tatayab """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': '''
- Aramex Delivery
''',
    'depends': ['tatayab_delivery_core', 'tatayab_operation_status'],
    'data': [
        'security/ir.model.access.csv',

        'data/delivery_data.xml',
        'data/aramex_mail_data.xml',

        'views/res_company_view_changes.xml',
        'views/aramex_delivery_views.xml',
        'views/stock_views.xml',

        'report/sale_report_template_changes.xml',

        'wizard/runsheet_wizard_views.xml',
    ],
    # 'external_dependencies': {
    #     'python': [
    #         'suds', 'SOAPpy',
    #     ]}
}
