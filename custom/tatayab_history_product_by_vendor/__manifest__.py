# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Trace Product by vendor",
    'author': 'Tatayab, Doaa Negm',
    'category': 'ALL',
    'summary': """Tatayab """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '1.0',
    'depends': ['base','sale','tatayab_purchase_changes','tatayab_magento_connect_changes',
                'tatayab_return_cycle','tatayab_operation_status','tatayab_back_to_back'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/register_payment_wizard_view.xml',
        'views/product_history_byvendor_view.xml',
        'views/purchase_order_inherit_view.xml',
        'views/sale_order_inherit_view.xml',
        'views/stock_picking_inherit_view.xml',
        'views/res_partner_view_inherit.xml',
        'views/res_config_view.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
