from odoo import api, fields, models,_
from odoo.exceptions import ValidationError

class RegisterPaymentWizard(models.TransientModel):
    _name = 'register.payment.wizard'

    journal_id = fields.Many2one('account.journal',string="Payment Method",required=True)
    amount = fields.Float(string="Amount",readonly=1)
    memo = fields.Char(string="Memo")
    currency_id = fields.Many2one('res.currency', string="Currency",readonly=1)
    payment_date = fields.Date(string="Payment Date" ,required=True,
        default=fields.Date.context_today)
    vendor_id = fields.Many2one('res.partner',string="Vendor" ,readonly=1)
    lines_ids = fields.Many2many('product.byvendor.line',string="lines")


    def create_payment(self):

        if not self.vendor_id:
            raise ValidationError(_("Please add the vendor"))
        if self.amount <= 0.0:
            raise ValidationError(_(" The Amount must to be greater than zero "))
        payment = self.env['account.payment'].create({
            'payment_type': 'outbound',
            'partner_type': 'supplier',
            'journal_id': self.journal_id.id,
            'amount': self.amount,
            'currency_id': self.currency_id.id,
            'partner_id': self.vendor_id.id,
            'ref': self.memo,
        })
        for line in self.lines_ids:
            line.account_payment_id = payment.id
            line.is_payed = True
        # payment.action_post()
        return {
            'effect': {
                'type': 'rainbow_man',
                'message': _("Successfully Created Payment !"),
            }
        }
