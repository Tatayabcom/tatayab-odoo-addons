from odoo import api, fields, models
from lxml import etree


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    product_byvendor_ids = fields.Many2many('product.byvendor', string="Product Vendor",copy=False)
    po_vendor_id = fields.Many2one('res.partner', string="PO Vendor")
    type = fields.Selection(selection=[('consignment','Consignment'),('cash','Cash'),('credit','Credit'),('return','Return'),
                                       ('b2b','B2B'),], string="PO Type")


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def create_line_in_product_by_vendor(self):
        for rec in self:
            for line in rec.order_line:
                if not line.product_byvendor_ids: # to handle b2b order
                    sale_quantity = line.product_uom_qty
                    product_vendor_object = self.env['product.byvendor'].search([('product_id', '=', line.product_id.id),
                                                                                 ('active', '=', True),('is_payed','=',False),
                                                                                 ('warehouse_id','=',rec.warehouse_id.id)],order='price')
                    if sale_quantity:
                        byvendor_lines_list = []
                        for i in range(int(sale_quantity)):
                            if sale_quantity:
                                products = product_vendor_object.filtered(lambda l: (l.sale_quantity+l.done_sale_quantity) < l.quantity and l.type == 'cash').sorted(key=lambda p: (p.price, p.date))
                                if not products:
                                    products = product_vendor_object.filtered(lambda l: (l.sale_quantity+l.done_sale_quantity) < l.quantity and l.type == 'return').sorted(key=lambda p: (p.price, p.date))
                                if not products:
                                    products = product_vendor_object.filtered(lambda l: (l.sale_quantity+l.done_sale_quantity) < l.quantity and l.type == 'credit').sorted(key=lambda p: (p.price, p.date))
                                if not products:
                                    products = product_vendor_object.filtered(lambda l: ( l.sale_quantity + l.done_sale_quantity) < l.quantity and l.type == 'consignment').sorted(key=lambda p: (p.price, p.date))
                                if not products:
                                    products = product_vendor_object.filtered(lambda l: ( l.sale_quantity + l.done_sale_quantity) < l.quantity and l.type == 'b2b').sorted(key=lambda p: (p.price, p.date))
                                if products:
                                    product = products[0]
                                    new_sale_quantity = product.sale_quantity + sale_quantity
                                    if new_sale_quantity > product.quantity:
                                        split_quantity = product.quantity - product.sale_quantity
                                        product.sale_quantity += split_quantity
                                        product.sales_ids = [(4, line.id)]
                                        sale_quantity -= split_quantity
                                        line.product_byvendor_ids = [(4, product.id)]
                                        byvendor_lines = self.env['product.byvendor.line'].create({
                                            'so_line_id':line.id,
                                            'sale_price': line.price_unit,
                                            'sub_total_price': line.row_total_incl_tax,
                                            'sub_total_price_kwd': line.base_row_total_incl_tax,
                                            'sub_total_price_usd': line.usd_row_total,
                                            'sale_quantity':split_quantity,
                                            'so_quantity':split_quantity,
                                            'product_byvendor_id':product.id,
                                        })
                                        byvendor_lines_list.append(byvendor_lines.id)
                                    else:
                                        product.sale_quantity += sale_quantity
                                        product.sales_ids = [(4, line.id)]
                                        line.product_byvendor_ids = [(4, product.id)]
                                        byvendor_lines = self.env['product.byvendor.line'].create({
                                            'so_line_id': line.id,
                                            'sale_price': line.price_unit,
                                            'sale_quantity': sale_quantity,
                                            'so_quantity': sale_quantity,
                                            'sub_total_price': line.row_total_incl_tax,
                                            'sub_total_price_kwd': line.base_row_total_incl_tax,
                                            'sub_total_price_usd': line.usd_row_total,
                                            'product_byvendor_id': product.id,
                                        })
                                        byvendor_lines_list.append(byvendor_lines.id)
                                        sale_quantity = 0
                                    line.po_vendor_id = product.vendor_id.id
                                    for move in line.move_ids:
                                        move.product_byvendor_line_ids =[(6,0,byvendor_lines_list)]
                                        # move.po_vendor_id = product.vendor_id.id
                                        # move.type = product.type
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        # self.create_line_in_product_by_vendor()
        return res

    def action_cancel(self):
        result = super(SaleOrder, self).action_cancel()
        for rec in self:
            for line in rec.order_line:
                by_vendor_line = self.env['product.byvendor.line'].search([('so_line_id','=',line.id)])
                for by_line in by_vendor_line:
                    by_line.unlink()
        return result


class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    def cancel_order_shipments_invoice(self):
        res = super(SaleOrderInherit, self).cancel_order_shipments_invoice()
        for rec in self:
            if rec.operation_state not in  ['return_done','return_other_done','return_other','partial_return_other_done','returned']:
                for line in rec.order_line:
                    by_vendor_line = self.env['product.byvendor.line'].search([('so_line_id','=',line.id)])
                    for by_line in by_vendor_line:
                        by_line.unlink()
        return res
