from odoo import api, fields, models


class StockMoveInherit(models.Model):
    _inherit = 'stock.move'

    po_vendor_id = fields.Many2one('res.partner', string="PO Vendor")
    type = fields.Selection(selection=[('consignment','Consignment'),('cash','Cash'),('return','Return'),
                                       ('credit','Credit'),('b2b','B2B'),], string="PO Type")
    product_byvendor_line_ids = fields.Many2many('product.byvendor.line', string="Product Vendor")
    type_with_quantity = fields.Char(string="Quantity Detailed", compute="compute_type_with_quantity")
    @api.depends('product_byvendor_line_ids','product_byvendor_line_ids.done_sale_quantity','product_byvendor_line_ids.sale_quantity')
    def compute_type_with_quantity(self):
        for rec in self:
            rec.type_with_quantity = ''
            for line in rec.product_byvendor_line_ids:
                if line.done_sale_quantity:
                    rec.type_with_quantity += line.type+ ": "+str(line.done_sale_quantity)+ ", "
                else:
                    rec.type_with_quantity += line.type+ ": "+str(line.sale_quantity)+ ", "



class StockMoveLineInherit(models.Model):
    _inherit = 'stock.move.line'

    po_vendor_id = fields.Many2one('res.partner', string="PO Vendor")
    type = fields.Selection(selection=[('consignment','Consignment'),('cash','Cash'),('return','Return'),
                                       ('credit','Credit'),('b2b','B2B'),], string="PO Type")


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    # TODO split order to multi picking
    def action_done_shipment(self):
        res = super(StockPickingInherit, self).action_done_shipment()
        for rec in self:
            if rec.sale_id:
                for line in rec.sale_id.order_line:
                    by_vendor_line = self.env['product.byvendor.line'].search([('so_line_id','=',line.id)])
                    for by_line in by_vendor_line:
                        by_line.done_sale_quantity = by_line.sale_quantity
                        by_line.sale_quantity = 0

    def button_validate(self):
        res = super(StockPickingInherit,self).button_validate()
        for rec in self:
            if rec.purchase_id:
                for line in rec.purchase_id.order_line:
                    if line.product_vendor_id:
                        line.product_vendor_id.quantity = line.qty_received
                        line.product_vendor_id.date = rec.scheduled_date
        return res


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    def process(self):
        action = super(StockImmediateTransfer, self).process()
        for rec in self.pick_ids:
            if rec.is_return:
                if rec.returned_picking_id:
                    for line in rec.move_lines:
                        product_vendor_obj = self.env['product.byvendor'].create({
                            'name': "Return from Picking "+ rec.name,
                            'product_id': line.product_id.id,
                            'purchase_quantity': line.quantity_done,
                            'quantity': line.quantity_done,
                            'date': fields.Date.today(),
                            'type': 'return',
                            'is_return': True,
                            'active': True,
                            'warehouse_id': line.warehouse_id.id,
                            'price': line.unit_price,
                        })
                        print("product_vendor_obj===",product_vendor_obj)
        return action


class BulkTransfer(models.TransientModel):
    _inherit = 'bulk.order.transfer'

    def action_pick_to_pack(self):
        res = super(BulkTransfer,self).action_pick_to_pack()
        pickings = self.line_ids.mapped('picking_id')
        for picking in pickings:
            picking.sale_id.create_line_in_product_by_vendor()
        return res
