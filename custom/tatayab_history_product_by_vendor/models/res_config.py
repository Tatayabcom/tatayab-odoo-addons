from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    journal_id_by_vendor = fields.Many2one('account.journal', string="journal to Payment")

    def get_values(self):
        vals = super(ResConfigSettings, self).get_values()
        vals.update(
                    journal_id_by_vendor=int(self.env['ir.config_parameter'].sudo().get_param('journal_id_by_vendor')),
                    )
        return vals

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].set_param('journal_id_by_vendor', self.journal_id_by_vendor.id)
