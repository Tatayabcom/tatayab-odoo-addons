# -*- coding: utf-8 -*-
import logging

from lxml import etree

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class PurchaseOrderLineInherit(models.Model):
    _inherit = 'purchase.order.line'

    product_vendor_id = fields.Many2one('product.byvendor', string="Purchase2Vendor")


class PurchaseOrderInherit(models.Model):
    _inherit = 'purchase.order'

    type = fields.Selection(selection=[('consignment','Consignment'),('cash','Cash'),('return','Return'),
                                       ('credit','Credit'),('b2b','B2B'),], string="PO Type")

    def add_history_product2vendor(self):
        for rec in self:
            for line in rec.order_line:
                lines_list = []
                if line.sale_line_id: # to pass sale order line automatic ,because this product is b2b
                    lines_list.append((0,0,{
                        'so_line_id':line.sale_line_id.id,
                        'sale_quantity':line.product_qty,
                    }))
                product_vendor_obj = self.env['product.byvendor'].create({
                    'name': rec.partner_id.name + "/" + rec.product_id.name,
                    # 'vendor_id': rec.partner_id.id if rec.type == 'consignment' else False ,
                    'vendor_id': rec.partner_id.id ,
                    'product_id': line.product_id.id,
                    'categ_id': line.product_id.categ_id.id,
                    'purchase_quantity': line.product_qty,
                    'quantity': line.qty_received,
                    'margin_per_unit': line.profit_margin,
                    'line_margin': line.line_margin,
                    'date': rec.date_approve,
                    'type': rec.type,
                    'active': True,
                    'purchase_id': rec.id,
                    'currency_id':rec.currency_id.id,
                    'warehouse_id': rec.warehouse_id.id,
                    'purchase_line_id': line.id,
                    'price': line.price_unit,
                    'lines_ids':lines_list
                })
                line.sale_line_id.product_byvendor_ids = [(4, product_vendor_obj.id)]
                line.product_vendor_id = product_vendor_obj.id
                print("product_vendor_obj",product_vendor_obj)


    def action_accounting_manager_approve(self):
        res = super(PurchaseOrderInherit, self).action_accounting_manager_approve()
        self.add_history_product2vendor()
        for rec in self:
            for line in rec.order_line:
                for move in line.move_ids:
                    move.po_vendor_id = rec.partner_id.id
                    move.type = rec.type
                    for int_move in move.move_dest_ids:
                        int_move.po_vendor_id = rec.partner_id.id
                        int_move.type = rec.type

        return res
    def action_bulk_accounting_manager_approve(self):
        res = super(PurchaseOrderInherit, self).action_bulk_accounting_manager_approve()
        context = dict(self._context or {})
        active_ids = context.get('active_ids')
        purchase_orders = self.browse(active_ids)
        for rec in purchase_orders:
            rec.add_history_product2vendor()
            for line in rec.order_line:
                for move in line.move_ids:
                    move.po_vendor_id = rec.partner_id.id
                    move.type = rec.type
                    for int_move in move.move_dest_ids:
                        int_move.po_vendor_id = rec.partner_id.id
                        int_move.type = rec.type
        return res

    def button_cancel(self):
        res = super(PurchaseOrderInherit, self).button_cancel()
        for rec in self:
            product_byvendor_object = self.env['product.byvendor'].search([('purchase_id','=',rec.id)])
            for line in product_byvendor_object:
                line.active = False
                line.sale_quantity = 0.0
                line.done_sale_quantity = 0.0
                line.sales_ids = [(6,0,{})]
        return res