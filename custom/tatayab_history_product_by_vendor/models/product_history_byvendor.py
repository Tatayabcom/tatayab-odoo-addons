from odoo import api, fields, models, _
from odoo.exceptions import UserError

_PICKING_STAGES = [('draft', 'Draft'), ('without_pickings', 'Without Pickings'), ('pick', 'Picking'),
                   ('pack', 'Packing'), ('ship', 'Shipping'), ('done', 'Done Shipment'), ('cancel', 'Canceled')]


class ProductByVendorLine(models.Model):
    _name = 'product.byvendor.line'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'so_line_id'

    so_line_id = fields.Many2one('sale.order.line', string="SO line")
    so_id = fields.Many2one('sale.order', string="Sale Order", related='so_line_id.order_id',store=True)
    currency_id = fields.Many2one('res.currency', string="Currency",related='so_line_id.currency_id',store=True)
    operation_state = fields.Selection(_PICKING_STAGES, "Operation State",
                                       default='pick', tracking=True, related='so_id.operation_state')
    sale_quantity = fields.Float(string="Sales Ordered")
    so_quantity = fields.Float(string="Sales Ordered")
    sale_price = fields.Float(string="Sale Price")
    sub_total_price = fields.Float(string="Total Price")
    sub_total_price_kwd = fields.Float(string="Total Price KWD")
    sub_total_price_usd = fields.Float(string="Total Price USD")
    done_sale_quantity = fields.Float(string="Sales Done")
    is_payed = fields.Boolean(string="Paid to Vendor")
    product_byvendor_id = fields.Many2one('product.byvendor',string="Product By Vendor")
    account_payment_id = fields.Many2one('account.payment',string="Payment")
    vendor_id = fields.Many2one('res.partner',string="Vendor",tracking=True,store=True,
                                related='product_byvendor_id.vendor_id')
    type = fields.Selection(selection=[('consignment','Consignment'),('cash','Cash'),('credit','Credit'),('return','Return'),
                                       ('b2b','B2B'),], string="PO Type",related='product_byvendor_id.type')
    real_margin = fields.Float(string="Margin", compute="compute_real_margin")

    @api.depends('product_byvendor_id.price','sale_quantity','sub_total_price_kwd')
    def compute_real_margin(self):
        for rec in self:
            rec.real_margin = (rec.product_byvendor_id.margin_per_unit * rec.so_quantity )

    def bulk_action_payment(self):
        vendors = []
        for vendor in self.mapped('vendor_id'):
            if vendor not in vendors:
                vendors.append(vendor)
        if len(vendors) > 1:
            raise UserError(_("One payment for multiple vendors cannot be made, Please select only one vendor "))
        currencies = []
        for currency in self.mapped('product_byvendor_id.currency_id'):
            if currency not in currencies:
                currencies.append(currency)
        if len(currencies) > 1:
            raise UserError(_("One payment for multiple lines cannot be made, Please select only one currency "))

        amount = 0.0
        ids = []
        for line in self:
            if line.is_payed:
                raise UserError(_("Please select only lines not Paid "))
            ids.append(line.id)
            amount += (line.done_sale_quantity * line.product_byvendor_id.price)
        currency_id = self.env.ref('base.KWD')
        if currencies:
            currency_id = currencies[0]
        return {
            'name': _('Register Payment'),
            'res_model': 'register.payment.wizard',
            'view_mode': 'form',
            'target': 'new',
            'context':{
                 'default_amount': amount,
                 'default_currency_id': currency_id.id,
                 'default_vendor_id': vendors[0].id if vendors else False,
                 'default_lines_ids':[(6,0,ids)]
            },
            'type': 'ir.actions.act_window',
        }

    def update_quantity_so(self):
        for rec in self:
            rec.so_quantity = rec.sale_quantity
            if rec.done_sale_quantity > 0.0:
                rec.sale_quantity = 0.0

    def bulk_action_payment_test(self):
        vendors = []
        for vendor in self.mapped('vendor_id'):
            if vendor not in vendors:
                vendors.append(vendor)
        print("vendors",vendors)
        if len(vendors) > 1:
            raise UserError(_("One payment for multiple vendors cannot be made, Please select only one vendor "))
        amount = 0
        for line in self:
            amount += (line.sale_quantity * line.product_byvendor_id.price)
        journal_id = int(self.env['ir.config_parameter'].sudo().get_param('journal_id_by_vendor'))
        if not journal_id:
            raise UserError(_("Please add the journal to create payment in configuration >> setting >> Product by vendor"))
        payment = self.env['account.payment'].create({
            'payment_type': 'outbound',
            'partner_type': 'supplier',
            'journal_id':journal_id,
            'amount': amount,
            'currency_id': self.env.company.currency_id.id,
            'partner_id': vendors[0].id,

        })
        for line in self:
            line.account_payment_id = payment.id
            line.is_payed = True
        # payment.action_post()
        return {
            'effect': {
                'type': 'rainbow_man',
                'message': _("Successfully updated the marketplaces available to this account !"),
            }
        }

class ProductByVendor(models.Model):
    _name = 'product.byvendor'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date'

    name = fields.Char(string="Name")
    product_id = fields.Many2one('product.product', string="Product")
    tem_product_id = fields.Many2one('product.template', string="Product Template")
    categ_id = fields.Many2one('product.category', string="Product Category",related="product_id.categ_id")
    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse")
    vendor_id = fields.Many2one('res.partner',string="Vendor",tracking=True)
    purchase_quantity = fields.Float(string="P. Quantity",tracking=True)
    quantity = fields.Float(string="Received Quantity",tracking=True)
    # quantity = fields.Float(string="Actual Quantity",tracking=True,related='purchase_line_id.qty_received')
    sale_quantity = fields.Float(string="Sales Ordered",tracking=True,compute="compute_quantity")
    done_sale_quantity = fields.Float(string="Sales Done",tracking=True,compute="compute_quantity")
    free_quantity = fields.Float(string="Free Quantity",tracking=True,compute="compute_quantity")
    price = fields.Float(string="Cost",tracking=True)
    active = fields.Boolean(string="Active",tracking=True)
    date = fields.Datetime(string="Date",tracking=True)
    purchase_id = fields.Many2one('purchase.order', string="Purchase Order",tracking=True)
    currency_id = fields.Many2one('res.currency', string="Currency")
    purchase_line_id = fields.Many2one('purchase.order.line', string="Purchase Order Line",tracking=True)
    sales_ids = fields.Many2many('sale.order.line', string="Sale Order",tracking=True)
    lines_ids = fields.One2many('product.byvendor.line', 'product_byvendor_id')
    is_payed = fields.Boolean(string="Paid to Vendor",readonly=1,tracking=True)
    total_cost = fields.Float(string="Total Cost", compute="compute_quantity")
    total_due = fields.Float(string="Amount Due", compute="compute_quantity")
    is_return = fields.Boolean(string="Returned")
    type = fields.Selection(selection=[('consignment','Consignment'),('cash','Cash'),('return','Return'),
                                       ('credit','Credit'),('b2b','B2B'),], string="PO Type")
    margin_per_unit = fields.Float(string=' Margin Per Unit', digits='Product Price')
    line_margin = fields.Monetary(string='Total Profit')

    def bulk_action_update_quantity(self):
        for rec in self:
            if rec.purchase_line_id:
                rec.purchase_quantity = rec.purchase_line_id.product_qty
                rec.quantity = rec.purchase_line_id.qty_received
                rec.margin_per_unit = rec.purchase_line_id.profit_margin
                rec.line_margin = rec.purchase_line_id.line_margin
            else:
                if rec.purchase_id and rec.product_id:
                    po_lines = self.env['purchase.order.line'].search([('order_id','=',rec.purchase_id.id),('product_id','=',rec.product_id.id)])
                    purchase_quantity = 0.0
                    quantity = 0.0
                    margin_per_unit = 0.0
                    line_margin = 0.0
                    for line in po_lines:
                        purchase_quantity += line.product_qty
                        quantity += line.qty_received
                        margin_per_unit += line.profit_margin
                        line_margin += line.line_margin
                    rec.purchase_quantity = purchase_quantity
                    rec.quantity = quantity
                    rec.margin_per_unit = margin_per_unit
                    rec.line_margin = line_margin

        return {
            'effect': {
                'type': 'rainbow_man',
                'message': _("Successfully updated the marketplaces available to this account !"),
            }
        }

    def bulk_action_update_vendor(self):
        for rec in self:
            if rec.purchase_id:
                rec.vendor_id = rec.purchase_id.partner_id
                for line in rec.lines_ids:
                    if line.so_line_id:
                        line.sale_price = line.so_line_id. price_unit

    def action_done_pay_to_vendor(self):
        for rec in self:
            rec.is_payed = True

    @api.depends('lines_ids', 'lines_ids.sale_quantity', 'lines_ids.done_sale_quantity')
    def compute_quantity(self):
        for rec in self:
            sale_quantity = 0.0
            done_sale_quantity = 0.0
            for line in rec.lines_ids:
                sale_quantity += line.sale_quantity
                done_sale_quantity += line.done_sale_quantity
            rec.done_sale_quantity = done_sale_quantity
            rec.sale_quantity = sale_quantity
            rec.free_quantity = rec.quantity - (done_sale_quantity + sale_quantity)
            rec.total_cost = rec.quantity * rec.price
            rec.total_due = done_sale_quantity * rec.price
