# -*- coding: utf-8 -*-

{
    'name': "Tatayab Access Right",
    'author': 'Tatayab, Doaa Negm',
    'category': 'ALL',
    'summary': """Tatayab  Groups pn magento """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
    1- create new group to show  magento 
    2- create new group to access delete product brand
""",
    'version': '1.0',
    'depends': ['base','odoo_magento2_ept','tatayab_magento_connect_changes'],
    'data': [
        'security/group.xml',
        'security/ir.model.access.csv',
        'views/menuitems.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
