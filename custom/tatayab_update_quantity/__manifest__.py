# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab  update Quantity Without Cron ",
    'author': 'Tatayab, Doaa Negm',
    'category': 'ALL',
    'summary': """Tatayab """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
    Tatayab  update Quantity Without Cron 
""",
    'version': '1.0',
    'depends': ['base','odoo_magento2_ept','tatayab_back_to_back',
                'tatayab_out_of_stock_report','tatayab_print_barcode'],
    'data': [
        'data/ir_cron.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
