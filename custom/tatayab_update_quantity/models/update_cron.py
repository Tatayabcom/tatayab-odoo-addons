import math
from datetime import datetime

from urllib3.connectionpool import xrange
from odoo import api, fields, models
from odoo import models, fields, api, _
import odoo
from odoo.exceptions import UserError
from odoo.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import logging

from odoo import fields, models, _
import threading
_logger = logging.getLogger('MagentoEPT')
from odoo.exceptions import UserError
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"

class MagentoProductTemplate(models.Model):
    """
    Describes fields and methods for Magento products templates
    """
    _inherit = 'product.template'


    def update_all_stock_product_in_magento_ept(self):
        self.process_fill_data_parallel()
    def update_all_stock_product_in_magento_ept_old(self):
        """
        This method are used to export stock in Magento. And it will be called from Multi Action
        Wizard in Magento product tree view.
        :return:
        """
        m_template = self.env['magento.product.template']
        m_export_product = self.env['magento.export.product.ept']
        odoo_product_tem = []
        product_template = self.env['product.template'].search([]).ids
        product_lists = [product_template[x:x + 400] for x in xrange(0, len(product_template), 400)]
        for sublist in product_lists:
            odoo_product_tem = sublist
            product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
            m_product_ids = product_magento
            instances = self.env['magento.instance'].search([])
            logs = list()
            m_templates = m_template.search([('id', 'in', m_product_ids),
                                             ('sync_product_with_magento', '=', True)])
            for instance in instances:
                log = instance.create_common_log_book('export', 'magento.product.template')
                product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
                m_export_product.export_product_stock_magento(instance, product_ids, log)
                if log and not log.log_lines:
                    log.unlink()
                else:
                    logs.append(log.id)
            if logs:
                return {
                    'name': 'Export Product Log',
                    'type': 'ir.actions.act_window',
                    'res_model': 'common.log.book.ept',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'domain': [('id', 'in', logs)],
                }
            for item in sublist:
                product_obj = self.env['product.template'].search([('id','=',item)])
                logging.info(green + "___Done Update Product  ____________:%s " % (product_obj.display_name) + reset)
                free_qty = product_obj.free_qty
                if product_obj.is_btb:
                    free_qty = product_obj.free_qty + product_obj.seller_total_qty
                product_obj.message_post(body="<b style='color:green'>Update Quantity To Magento : </b><br/><span> update quantity from (Cron job 'Update All Quantity For Product' ) with (%s) </span>" % free_qty)

        return {
            'effect': {
                'fadeout': 'slow',
                'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
                    ""),
                'img_url': '/web/static/src/img/smile.svg',
                'type': 'rainbow_man',
            }
        }
    def process_fill_data(self,limit, offset, is_commit):
        m_template = self.env['magento.product.template']
        m_export_product = self.env['magento.export.product.ept']
        odoo_product_tem = []
        print("offset===",offset)
        print("limit===",limit)
        product_template = self.env['product.template'].search([],limit=limit,offset=offset).ids
        product_lists = [product_template[x:x + 400] for x in xrange(0, len(product_template), 400)]
        for sublist in product_lists:
            odoo_product_tem = sublist
            product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
            m_product_ids = product_magento
            instances = self.env['magento.instance'].search([])
            logs = list()
            m_templates = m_template.search([('id', 'in', m_product_ids),
                                             ('sync_product_with_magento', '=', True)])
            for instance in instances:
                log = instance.create_common_log_book('export', 'magento.product.template')
                product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
                m_export_product.export_product_stock_magento(instance, product_ids, log)
                if log and not log.log_lines:
                    log.unlink()
                else:
                    logs.append(log.id)
            if logs:
                return {
                    'name': 'Export Product Log',
                    'type': 'ir.actions.act_window',
                    'res_model': 'common.log.book.ept',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'domain': [('id', 'in', logs)],
                }
            for item in sublist:
                product_obj = self.env['product.template'].search([('id', '=', item)])
                logging.info(green + "___Done Update Product  ____________:%s " % (product_obj.display_name) + reset)
                free_qty = product_obj.free_qty
                if product_obj.is_btb:
                    free_qty = product_obj.free_qty + product_obj.seller_total_qty
                product_obj.message_post(
                    body="<b style='color:green'>Update Quantity To Magento : </b><br/><span> update quantity from (Cron job 'Update All Quantity For Product' ) with (%s) </span>" % free_qty)

        return {
            'effect': {
                'fadeout': 'slow',
                'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
                    ""),
                'img_url': '/web/static/src/img/smile.svg',
                'type': 'rainbow_man',
            }
        }

    def manage_env_process_fill_data(self, limit=0, offset=0, is_commit=False):
        with api.Environment.manage():
            with odoo.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                new_env['product.template'].process_fill_data(limit, offset, is_commit)
    @api.model
    def process_fill_data_parallel(self, limit=0, offset=0, is_commit=False):
        start_date = datetime.now()
        employee_count = 0
        if limit:
            employee_count = len(self.env['product.template'].search([], order='id', limit=limit, offset=offset))
        else:
            employee_count = len(self.env['product.template'].search([], order='id', offset=offset))

        # itq_leaves_db_conn_data = self.env.ref('itq_payroll.itq_leaves_db_conn_data')
        # db_conn_rec = self.env.ref('itq_payroll.db_conn_data')
        # cpu_count = int(itq_leaves_db_conn_data.cpu) or int(db_conn_rec.cpu) or 1
        cpu_count = 4
        employee_count_per_cpu = math.ceil(employee_count / cpu_count)
        pro_list = []
        remaining_employee_count = employee_count
        current_offset = offset
        for i in range(0, cpu_count):
            employee_count_per_cpu = min(remaining_employee_count, employee_count_per_cpu)
            remaining_employee_count -= employee_count_per_cpu
            p = threading.Thread(target=self.manage_env_process_fill_data,
                                 args=(employee_count_per_cpu, current_offset, is_commit))
            current_offset += employee_count_per_cpu
            p.start()
            pro_list.append(p)
        for p in pro_list:
            p.join()
        _logger.info("process_fill_data_parallel(limit={limit}, offset={offset}) ... ran in {time_diff}".format(
            limit=limit, offset=offset, time_diff=datetime.now() - start_date))
