from odoo import models


class ProductProduct(models.Model):
    _inherit = 'product.product'

    def update_stock_product_in_magento_ept(self):
        """
        This method are used to export stock in Magento. And it will be called from Multi Action
        Wizard in Magento product tree view.
        :return:
        """
        for rec in self:
            if rec.active:
                m_template = self.env['magento.product.template']
                m_export_product = self.env['magento.export.product.ept']
                odoo_product_tem = []
                if self.env.context.get('active_model') == 'product.template':
                    odoo_product_tem = self.env.context.get('active_ids', [])
                else:
                    odoo_product_tem = [rec.product_tmpl_id.id]
                product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
                m_product_ids = product_magento
                instances = self.env['magento.instance'].search([])
                logs = list()
                m_templates = m_template.search([('id', 'in', m_product_ids),
                                                 ('sync_product_with_magento', '=', True)])

                for instance in instances:
                    log = instance.create_common_log_book('export', 'magento.product.template')
                    product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
                    m_export_product.export_product_stock_magento(instance, product_ids, log)
                    if log and not log.log_lines:
                        log.unlink()
                    else:
                        logs.append(log.id)
                if logs:
                    return {
                        'name': 'Export Product Log',
                        'type': 'ir.actions.act_window',
                        'res_model': 'common.log.book.ept',
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'domain': [('id', 'in', logs)],
                    }
                message = " update quantity from (Form Product Product ) "
                if rec.id:
                    rec.product_tmpl_id.message_post(
                        body="<b style='color:green'>Update Quantity To Magento : </b><br/><span>%s</span>" % message)

            return {
                'effect': {
                    'fadeout': 'slow',
                    'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
                        ""),
                    'img_url': '/web/static/src/img/smile.svg',
                    'type': 'rainbow_man',
                }
            }

    def write(self, values):
        res = super(ProductProduct, self).write(values)
        if not values.get('print_qty'):
            self.update_stock_product_in_magento_ept()
        return res


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def update_stock_product_in_magento_ept(self):
        """
        This method are used to export stock in Magento. And it will be called from Multi Action
        Wizard in Magento product tree view.
        :return:
        """
        for rec in self:
            if rec.active:
                m_template = self.env['magento.product.template']
                m_export_product = self.env['magento.export.product.ept']
                odoo_product_tem = []
                # if self.env.context.get('active_model') == 'product.template':
                # self._context.get('params').get('model')
                odoo_product_tem = [rec.id]
                product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
                m_product_ids = product_magento
                instances = self.env['magento.instance'].search([])
                logs = list()
                m_templates = m_template.search([('id', 'in', m_product_ids),
                                                 ('sync_product_with_magento', '=', True)])

                for instance in instances:
                    log = instance.create_common_log_book('export', 'magento.product.template')
                    product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
                    m_export_product.export_product_stock_magento(instance, product_ids, log)
                    if log and not log.log_lines:
                        log.unlink()
                    else:
                        logs.append(log.id)
                if logs:
                    return {
                        'name': 'Export Product Log',
                        'type': 'ir.actions.act_window',
                        'res_model': 'common.log.book.ept',
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'domain': [('id', 'in', logs)],
                    }
                quantity = rec.qty_available
                if rec.is_btb:
                    quantity = rec.qty_available + rec.seller_total_qty
                message = " update quantity from (Form Product Template ) with (%s)" % (quantity)
                if rec.id:
                    rec.message_post(
                        body="<b style='color:green'>Update Quantity To Magento : </b><br/><span>%s</span>" % message)

            return {
                'effect': {
                    'fadeout': 'slow',
                    'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
                        ""),
                    'img_url': '/web/static/src/img/smile.svg',
                    'type': 'rainbow_man',
                }
            }

    def write(self, values):
        print("values",values)
        if not values.get('print_qty') or not values.get('active'):
            self.update_stock_product_in_magento_ept()
        for rec in self:
            # to get free quantity for KWT
            if rec.kwt_total_free_qty <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.product_variant_id.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer'),
                                                                    ('warehouse_id.code','=','KWT')
                                                                    ],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = rec.create_date

                values.update({
                    'last_update_stock_qty': last_update_stock_qty,
                    'last_qty_update_stock': last_update_stock_qty,
                    'is_out_of_stock': True
                })
            else:
                values.update({'is_out_of_stock': False})
            # to get free quantity for KSA
            if rec.ksa_total_free_qty <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.product_variant_id.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer'),
                                                                    ('warehouse_id.code','=','KSA')
                                                                    ],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = rec.create_date

                values.update({
                    'last_update_stock_qty': last_update_stock_qty,
                    'last_qty_update_stock_ksa': last_update_stock_qty,
                    'is_out_of_stock_ksa': True
                })
            else:
                values.update({'is_out_of_stock_ksa': False})
            # to get free quantity for Qatar
            if rec.qatar_total_free_qty <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.product_variant_id.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer'),
                                                                    ('warehouse_id.code','=','QAT')
                                                                    ],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = rec.create_date

                values.update({
                    'last_update_stock_qty': last_update_stock_qty,
                    'last_qty_update_stock_qatar': last_update_stock_qty,
                    'is_out_of_stock_qatar': True
                })
            else:
                values.update({'is_out_of_stock_qatar': False})
        return super(ProductTemplate, self).write(values)

    def update_product_out_stock(self):
        for rec in self:
            values = {}
            quantity = rec.qty_available + rec.seller_total_qty
            if quantity <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.product_variant_id.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer')],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    values.update({'last_update_stock_qty': rec.create_date})
                    last_update_stock_qty = rec.create_date

                # rec.last_update_stock_qty = last_update_stock_qty
                values.update({'last_update_stock_qty': last_update_stock_qty,
                               'last_qty_update_stock': last_update_stock_qty,
                               'is_out_of_stock': True
                               })
            else:
                values.update({'is_out_of_stock': False})
            rec.write(values)


class StockMove(models.Model):
    _inherit = 'stock.move'

    def update_stock_move_in_magento_ept(self):
        """
        This method are used to export stock in Magento. And it will be called from Multi Action
        Wizard in Magento product tree view.
        :return:
        """
        m_template = self.env['magento.product.template']
        m_export_product = self.env['magento.export.product.ept']
        odoo_product_tem = []
        if self.env.context.get('active_model') == 'product.template':
            odoo_product_tem = self.env.context.get('active_ids', [])
        else:
            for rec in self:
                odoo_product_tem.append(rec.product_id.product_tmpl_id.id)
        product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
        m_product_ids = product_magento
        instances = self.env['magento.instance'].search([])
        logs = list()
        m_templates = m_template.search([('id', 'in', m_product_ids),
                                         ('sync_product_with_magento', '=', True)])

        for instance in instances:
            log = instance.create_common_log_book('export', 'magento.product.template')
            product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
            m_export_product.export_product_stock_magento(instance, product_ids, log)
            if log and not log.log_lines:
                log.unlink()
            else:
                logs.append(log.id)
        if logs:
            return {
                'name': 'Export Product Log',
                'type': 'ir.actions.act_window',
                'res_model': 'common.log.book.ept',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'domain': [('id', 'in', logs)],
            }
        message = ""
        for pick in self.picking_id:
            message = " update quantity from (Form Picking %s ) <br/>" % (pick.name)
        for rec in self:
            free_qty = rec.product_id.qty_available + rec.product_id.seller_total_qty
            rec.product_id.product_tmpl_id.message_post(
                body="<b style='color:green'>Update Quantity To Magento : </b><br/><span>total Free quantity %s</span><span>%s</span>" % (
                message, free_qty))

        return {
            'effect': {
                'fadeout': 'slow',
                'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
                    ""),
                'img_url': '/web/static/src/img/smile.svg',
                'type': 'rainbow_man',
            }
        }

    def _action_done(self, cancel_backorder=False):
        res = super(StockMove, self)._action_done(cancel_backorder=cancel_backorder)
        self.update_stock_move_in_magento_ept()
        values = {}
        for rec in self:
            # to get free quantity for KWT
            if rec.product_id.kwt_total_free_qty <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer'),
                                                                    ('warehouse_id.code','=','KWT')
                                                                    ],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = rec.product_id.create_date

                values.update({
                    'last_update_stock_qty': last_update_stock_qty,
                    'last_qty_update_stock': last_update_stock_qty,
                    'is_out_of_stock': True
                })
            else:
                values.update({'is_out_of_stock': False})
            # to get free quantity for KSA
            if rec.product_id.ksa_total_free_qty <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer'),
                                                                    ('warehouse_id.code','=','KSA')
                                                                    ],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = rec.product_id.create_date

                values.update({
                    'last_update_stock_qty': last_update_stock_qty,
                    'last_qty_update_stock_ksa': last_update_stock_qty,
                    'is_out_of_stock_ksa': True
                })
            else:
                values.update({'is_out_of_stock_ksa': False})
            # to get free quantity for Qatar
            if rec.product_id.qatar_total_free_qty <= 0.0:
                last_stock_move_id = self.env['stock.move'].search([('product_id', '=', rec.id),
                                                                    ('state', '=', 'done'),
                                                                    ('location_dest_id.usage', '=', 'customer'),
                                                                    ('warehouse_id.code','=','QAT')
                                                                    ],
                                                                   order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = rec.product_id.create_date

                values.update({
                    'last_update_stock_qty': last_update_stock_qty,
                    'last_qty_update_stock_qatar': last_update_stock_qty,
                    'is_out_of_stock_qatar': True
                })
            else:
                values.update({'is_out_of_stock_qatar': False})
            rec.product_id.write(values)
        return res


class ProductSupplierinfo(models.Model):
    _inherit = "product.supplierinfo"

    def write(self, vals):
        res = super(ProductSupplierinfo, self).write(vals)
        for rec in self:
            if rec.product_tmpl_id:
                rec.product_tmpl_id.update_stock_product_in_magento_ept()
                message = " update quantity from vendor (%s) (Form Vendor Vendor Pricelists ) with (%s)" % (rec.name.name,rec.curr_qty)
                rec.product_tmpl_id.message_post(
                        body="<b style='color:green'>Update Quantity To Magento : </b><br/><span>%s</span>" % message)
        return res



# class StockQuant(models.Model):
#     _inherit = 'stock.quant'
#
#     def update_stock_in_magento_ept(self):
#         """
#         This method are used to export stock in Magento. And it will be called from Multi Action
#         Wizard in Magento product tree view.
#         :return:
#         """
#         for rec in self:
#             m_template = self.env['magento.product.template']
#             m_export_product = self.env['magento.export.product.ept']
#             odoo_product_tem = []
#             if self.env.context.get('active_model') == 'product.template':
#                 odoo_product_tem = self.env.context.get('active_ids', [])
#             else:
#                 odoo_product_tem = [rec.product_id.product_tmpl_id.id]
#             product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
#             m_product_ids = product_magento
#             instances = self.env['magento.instance'].search([])
#             logs = list()
#             m_templates = m_template.search([('id', 'in', m_product_ids),
#                                              ('sync_product_with_magento', '=', True)])
#             for instance in instances:
#                 log = instance.create_common_log_book('export', 'magento.product.template')
#                 product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
#                 m_export_product.export_product_stock_magento(instance, product_ids, log)
#                 if log and not log.log_lines:
#                     log.unlink()
#                 else:
#                     logs.append(log.id)
#             if logs:
#                 return {
#                     'name': 'Export Product Log',
#                     'type': 'ir.actions.act_window',
#                     'res_model': 'common.log.book.ept',
#                     'view_type': 'form',
#                     'view_mode': 'tree,form',
#                     'domain': [('id', 'in', logs)],
#                 }
#
#         return {
#             'effect': {
#                 'fadeout': 'slow',
#                 'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
#                     ""),
#                 'img_url': '/web/static/src/img/smile.svg',
#                 'type': 'rainbow_man',
#             }
#         }

    # def action_apply_inventory(self):
    #     super(StockQuant, self).action_apply_inventory()
    #     self.update_stock_in_magento_ept()

    # def write(self, vals):
    #     # res = super(StockQuant, self).write(vals)
    #     self.update_stock_in_magento_ept()
    #     return super(StockQuant, self).write(vals)
