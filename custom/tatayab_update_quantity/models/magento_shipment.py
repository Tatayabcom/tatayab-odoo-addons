from odoo import api, fields, models
import logging

from odoo.addons.odoo_magento2_ept.models.api_request import req

_logger = logging.getLogger("MagentoEPT")


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def bulk_send_shipment_to_magento(self):
        raise_error = False
        for picking in self:
            values = picking.get_export_ship_values(raise_error=raise_error)
            if not values:
                return False
            instance = picking.magento_instance_id
            try:
                api_url = f'/V1/order/{picking.sale_id.magento_order_id}/ship/'
                response = req(instance, api_url, 'POST', values)
            except Exception as error:
                _logger.error(error)
                return picking._handle_magento_shipment_exception(instance, picking)
            if response:
                picking.write({
                    'magento_shipping_id': int(response),
                    'is_exported_to_magento': True
                })
        return True
