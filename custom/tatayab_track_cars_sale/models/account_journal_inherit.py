from odoo import api, fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    journal_for_car = fields.Boolean(string="Journal for Car")