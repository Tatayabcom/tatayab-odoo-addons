from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    is_car = fields.Boolean(string="Picking From Car")

    @api.model
    def create(self, vals):
        print("vals_list",vals)
        res = super(StockPicking,self).create(vals)
        res._onchange_picking_type()
        # res.move_lines._onchange_product_id()
        return res


class StockMove(models.Model):
    _inherit = 'stock.move'

    def _get_new_picking_values(self):
        vals = super(StockMove, self)._get_new_picking_values()
        is_car = self.mapped('sale_line_id.order_id.is_car')
        if is_car and is_car[0] ==True:
            vals['picking_type_id'] = self.mapped('sale_line_id.order_id.picking_type_id').id
            vals['location_id'] = self.mapped('sale_line_id.order_id.picking_type_id.default_location_src_id').id
            # vals['location_dest_id']=self.mapped('sale_line_id.order_id.picking_type_id.default_location_dest_id').id
        return vals