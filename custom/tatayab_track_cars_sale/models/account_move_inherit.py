from odoo import api, fields, models,_


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    is_car = fields.Boolean(string="Payment From Car")
class AccountMove(models.Model):
    _inherit = 'account.move'

    is_car = fields.Boolean(string="Invoice From Car")
    total_payments = fields.Float(string="Total Register Payment")
    is_full_payment = fields.Boolean(string="Full Payment")
    def action_register_payment(self):
        ''' Open the account.payment.register wizard to pay the selected journal entries.
        :return: An action opening the account.payment.register wizard.
        '''
        for rec in self:
            return {
                'name': _('Register Payment'),
                'res_model': 'account.payment.register',
                'view_mode': 'form',
                'context': {
                    'active_model': 'account.move',
                    'active_ids': self.ids,
                    'default_is_car':rec.is_car,
                },
                'target': 'new',
                'type': 'ir.actions.act_window',
            }


class AccountPaymentRegister(models.TransientModel):
    _inherit = 'account.payment.register'

    is_car = fields.Boolean(string="Invoice From Car")
    journal_id = fields.Many2one('account.journal', store=True, readonly=False,
                                 compute='_compute_journal_id',
                                 domain="[('company_id', '=', company_id), ('type', 'in', ('bank', 'cash')),('journal_for_car','=',is_car)]")

    @api.depends('can_edit_wizard', 'company_id','is_car')
    def _compute_journal_id(self):
        for wizard in self:
            if wizard.can_edit_wizard:
                print("111111111111")
                batch = wizard._get_batches()[0]
                wizard.journal_id = wizard._get_batch_journal(batch)
            else:
                print("eeeeeeeeeeeeeeeee")
                wizard.journal_id = self.env['account.journal'].search([
                    ('type', 'in', ('bank', 'cash')),
                    ('company_id', '=', wizard.company_id.id),
                    ('journal_for_car', '=', wizard.is_car)
                ], limit=1)

    @api.model
    def _get_batch_journal(self, batch_result):
        """ Helper to compute the journal based on the batch.

        :param batch_result:    A batch returned by '_get_batches'.
        :return:                An account.journal record.
        """
        payment_values = batch_result['payment_values']
        foreign_currency_id = payment_values['currency_id']
        partner_bank_id = payment_values['partner_bank_id']

        currency_domain = [('currency_id', '=', foreign_currency_id)]
        partner_bank_domain = [('bank_account_id', '=', partner_bank_id)]
        print("batch_result['lines']",batch_result['lines'])
        default_domain = [
            ('type', 'in', ('bank', 'cash')),
            ('company_id', '=', batch_result['lines'].company_id.id),
            ('journal_for_car', '=', self.is_car)

        ]

        if partner_bank_id:
            extra_domains = (
                currency_domain + partner_bank_domain,
                partner_bank_domain,
                currency_domain,
                [],
            )
        else:
            extra_domains = (
                currency_domain,
                [],
            )

        for extra_domain in extra_domains:
            journal = self.env['account.journal'].search(default_domain + extra_domain, limit=1)
            if journal:
                return journal

        return self.env['account.journal']
    def _create_payments(self):
        self.ensure_one()
        if self.is_car:
            print("hreeeeeeeeeeeeeeeeee")
            batches = self._get_batches()
            edit_mode = self.can_edit_wizard and (len(batches[0]['lines']) == 1 or self.group_payment)
            to_process = []

            if edit_mode:
                payment_vals = self._create_payment_vals_from_wizard()
                to_process.append({
                    'create_vals': payment_vals,
                    'to_reconcile': batches[0]['lines'],
                    'batch': batches[0],
                })
            else:
                # Don't group payments: Create one batch per move.
                if not self.group_payment:
                    new_batches = []
                    for batch_result in batches:
                        for line in batch_result['lines']:
                            new_batches.append({
                                **batch_result,
                                'payment_values': {
                                    **batch_result['payment_values'],
                                    'payment_type': 'inbound' if line.balance > 0 else 'outbound'
                                },
                                'lines': line,
                            })
                    batches = new_batches

                for batch_result in batches:
                    to_process.append({
                        'create_vals': self._create_payment_vals_from_batch(batch_result),
                        'to_reconcile': batch_result['lines'],
                        'batch': batch_result,
                    })

            payments = self._init_payments(to_process, edit_mode=edit_mode)
            print("payments",payments)
            payments.is_car = self.is_car
            moves = self.env['account.move'].browse(self.env.context.get('active_ids'))
            for move in moves:
                move.total_payments += self.amount
                if move.total_payments >= move.amount_total:
                    move.is_full_payment = True
                else:
                    move.is_full_payment = False
            # self._post_payments(to_process, edit_mode=edit_mode)
            # self._reconcile_payments(to_process, edit_mode=edit_mode)
            return payments
        else:
            super(AccountPaymentRegister,self)._create_payments()
