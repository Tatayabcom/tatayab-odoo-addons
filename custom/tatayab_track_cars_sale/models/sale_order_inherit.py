from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_car = fields.Boolean(string="Order From Car")
    picking_type_id = fields.Many2one('stock.picking.type', string="Picking type")

    def _prepare_invoice(self):
        invoice_vals = super(SaleOrder, self)._prepare_invoice()
        invoice_vals.update({
            'is_car': self.is_car,
        })
        return invoice_vals

    def action_confirm(self):
        for order in self:
            if order.is_car:
                msgs = ''
                for line in order.order_line:
                    msg = order.check_product_lines_availability(line=line)
                    if msg:
                        msgs += msg['warning']
                if msgs:
                    raise ValidationError(_(msgs))
        res = super(SaleOrder, self).action_confirm()
        for sale in self:
            skip_sms = {"skip_sms": True}
            if sale.is_car:
                sale.is_approved=True
                for picking in sale.picking_ids:
                    picking.is_car = sale.is_car
                    picking._onchange_picking_type()
                    (picking.move_lines | picking.move_ids_without_package).update({
                        "location_id": picking.location_id,
                        "location_dest_id": picking.location_dest_id
                    })
                    for line in picking.move_ids_without_package:
                        line.quantity_done = line.product_uom_qty
                    for sol_mo in sale.order_line:
                        for mo in sol_mo.move_ids:
                            mo.quantity_done = mo.product_uom_qty
                        # for ml in line.move_line_ids:
                        #     ml.qty_done = ml.product_uom_qty
                    # for ml in
                    result = picking.with_context(**skip_sms).button_validate()
                    if isinstance(result, dict):
                        dict(result.get("context")).update(skip_sms)
                        context = result.get("context")  # Merging dictionaries.
                        model = result.get("res_model", "")
                        # model can be stock.immediate.transfer or stock backorder.confirmation
                        if model:
                            record = self.env[model].with_context(context).create({})
                            record.process()
                            picking.operation_state = 'done'
            invoice = sale._create_invoices()
            print("invoice",invoice)
        return res


    def check_product_lines_availability(self, line):
        if line.product_id.type == 'product':
            product = line.product_id.with_context(
                warehouse=line.order_id.warehouse_id.id,
                lang=line.order_id.partner_id.lang or self.env.user.lang or 'en_US'
            )
            product_available_qty = self.env['stock.quant']._get_available_quantity(line.product_id,
                                                                                    line.order_id.picking_type_id.default_location_src_id,
                                                                                    strict=True)

            if line.product_uom_qty > product_available_qty:
                message = _(
                    '- You plan to sale %s %s of %s but you only have %s %s available in %s.\n') % \
                          (
                              line.product_uom_qty, line.product_uom.name,
                              line.product_id.display_name,
                              product_available_qty, product.uom_id.name,
                              line.order_id.picking_type_id.default_location_src_id.display_name)
                return {'warning': message}
            else:
                return {}
        else:
            return {}