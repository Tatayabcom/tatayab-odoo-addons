
{
    'name': "Tatayab Cars  ",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'ALL',
    'summary': """Tatayab """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '1.0',
    'depends': ['base','sale','stock','account','tatayab_operation_status',
                'tatayab_magento_connect_changes','tatayab_return_cycle'],
    'data': [
        'views/sale_order_inherit_view.xml',
        'views/account_move_inherit_view.xml',
        'views/stock_picking_view_inherit.xml',
        'views/account_journal_inherit_view.xml',
        'views/menuitems.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
