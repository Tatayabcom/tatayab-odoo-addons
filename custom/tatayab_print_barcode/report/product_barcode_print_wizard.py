
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class ProductBarcodePrintLines(models.TransientModel):
    _name = 'product.barcode.print.lines'
    _description = "Product Barcode Print Lines"

    product_id = fields.Many2one(
        'product.product',
        string="Product")
    lot_id = fields.Many2one('stock.production.lot')
    qty = fields.Integer('Quantity To Print', default=1, required=True)
    wizard_id = fields.Many2one('product.barcode.print.wizard')
    print_prod_barcode = fields.Boolean("Print Product Barcode ?")
    lot_on_hand_qty = fields.Float(string='Lot Quantity', related='lot_id.product_qty')

    @api.onchange('product_id')
    def _onchange_product_id(self):
        self.lot_id = False


class ProductBarcodePrintWizard(models.TransientModel):
    _name = 'product.barcode.print.wizard'
    _description = "Product Barcode Print Wizard"

    line_ids = fields.One2many('product.barcode.print.lines', 'wizard_id')


    def action_print_product(self):
        lines = self.line_ids.filtered("print_prod_barcode")
        if not lines:
            raise UserError(_('Missing Lines, Please tick the Print Product Barcode too.'))

        products = lines.mapped('product_id')
        for line in lines:
            line.product_id.print_qty = line.qty
        if products:
            return self.env.ref('tatayab_print_barcode.report_product_barcode_n_times_action') \
                .with_context({'discard_logo_check': True}) \
                .report_action(products, {})


class ProductLotPrintingNTimes(models.AbstractModel):
    _name = 'report.tatayab_print_barcode.report_lot_barcode_documents'
    _description = 'Product Barcode Details'

    def _get_report_values(self, docids, data=None):
        lots = self.env['stock.production.lot'].browse(docids)
        if lots:
            print_qty_by_lot = {l.id: l.print_qty for l in lots}
            print_qty = lots[0].print_qty
            lots.write({'print_qty': 1})
        return {
            'doc_ids': docids,
            'doc_model': 'stock.production.lot',
            'docs': lots,
            'o': lots,
            'qty': print_qty,
            'print_qty_by_lot': print_qty_by_lot,
        }


class ProductPrintingNTimes(models.AbstractModel):
    _name = 'report.tatayab_print_barcode.report_product_barcode_n_times'
    _description = 'Product Barcode N Time Prints'

    def _get_report_values(self, docids, data=None):
        products = self.env['product.product'].browse(docids)

        print_qty_by_prod = {}
        for product in products:
            print_qty_by_prod.setdefault(product.id, 0)
            print_qty_by_prod[product.id] += product.print_qty
            product.write({"print_qty": 1})

        return {
            'doc_ids': docids,
            'doc_model': 'product.product',
            'docs': products,
            'print_qty_by_prod': print_qty_by_prod,
        }
