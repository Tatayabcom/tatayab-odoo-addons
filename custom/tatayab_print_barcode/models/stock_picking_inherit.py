from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def action_stock_product_barcode(self):
        product_lines = []
        context = dict(self._context or {})
        active_ids = context.get('active_ids')
        picking = self.browse(active_ids)
        # product_ids = []
        for pick in picking:
            for line in pick.move_ids_without_package:
                product_lines.append((0, 0, {
                    'product_id': line.product_id.id,
                    'qty': line.product_uom_qty,
                    'print_prod_barcode': True}))
        domain_list = [i for n, i in enumerate(product_lines) if i not in product_lines[n + 1:]]
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'product.barcode.print.wizard',
            'view_id': self.env.ref("tatayab_print_barcode.view_product_barcode_print_wizard_form").id,
            'type': 'ir.actions.act_window',
            'context': {'default_line_ids': domain_list},
            'target': 'new',

        }