
from odoo import api, fields, models


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    print_qty = fields.Integer("Print Qty")