{
    'name': 'Tatayab DHL Delivery',
    'version': '13.0.1.0',
    'sequence': 1,
    'author': 'Tatayab, Ahmed Salama',
    'website': 'http://tatayab.com/',
    'category': 'Delivery',
    'summary': 'DHL Delivery',
    'description': '''
- DHL Delivery
''',
    'depends': ['delivery_dhl', 'tatayab_delivery_core'],
    'data': [
        # 'security/ir.model.access.csv',
        
        'data/dhl_data.xml',
        
        'views/stock_picking_view_changes.xml',
        
        # 'wizard/bulk_transfer_changes_views.xml',
        
    ],
}
