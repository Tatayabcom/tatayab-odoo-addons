import logging
from odoo import api, models, fields, _
_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class BulkTransferInherit(models.TransientModel):
    _inherit = 'bulk.order.transfer'

    def action_internation_shipping(self):
        """
        This could be helpful if user forget to assign all results
        this shall update picking dhl_boxes by new numbers
        :return:
        """
        if self.carrier_id and\
                self.carrier_id == self.env.ref('tatayab_delivery_dhl.delivery_carrier_dhl_international')\
                and not self.assign_all:
            for line in self.line_ids:
                if line.picking_id:
                    line.picking_id.write({'dhl_boxes': line.dhl_boxes})
        return super(BulkTransferInherit, self).action_internation_shipping()


class BulkTransferLine(models.TransientModel):
    _inherit = 'wiz.transfer.line'
    
    dhl_boxes = fields.Integer(related='picking_id.dhl_boxes', store=True)
    
    def get_line_update_data(self):
        """
        Collect data from line to edit
        """
        results = super(BulkTransferLine, self).get_line_update_data()
        results['dhl_boxes'] = self.dhl_boxes
        return results
