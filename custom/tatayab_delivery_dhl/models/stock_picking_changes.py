# -*- coding: utf-8 -*-
from odoo import models, fields


# Ahmed Salama Code Start ---->


class StockPickingInherit(models.Model):
	_inherit = 'stock.picking'
	
	dhl_boxes = fields.Integer("DHL Boxes")

# Ahmed Salama Code End.
