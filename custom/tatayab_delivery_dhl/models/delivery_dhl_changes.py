# -*- coding: utf-8 -*-
# from odoo.addons.delivery_dhl.models.dhl_request import DHLProvider
import logging

from odoo import models, _, fields
from .dhl_request import DHLProvider
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class ProviderDHLInherit(models.Model):
    _inherit = 'delivery.carrier'

    dhl_region_code = fields.Selection(default='AP')
    dhl_account_number = fields.Char(string="DHL Account Number", groups="base.group_system,stock.group_stock_manager")

    def dhl_send_shipping(self, pickings):
        res = []
        logging.info(bold_red + "___ Start DHL API Connection ___" + reset)
        carrier_log_obg = self.env['delivery.carrier.log']
        for picking in pickings:
            order = picking.sale_id
            shipment_request = {}
            srm = DHLProvider(self.log_xml, request_type="ship", prod_environment=self.prod_environment)
            site_id = self.sudo().dhl_SiteID
            password = self.sudo().dhl_password
            shipment_request['Request'] = srm._set_request(site_id, password)
            shipment_request['RegionCode'] = srm._set_region_code(self.dhl_region_code)
            shipment_request['PiecesEnabled'] = srm._set_pieces_enabled(True)
            shipment_request['RequestedPickupTime'] = srm._set_requested_pickup_time(True)
            shipment_request['Billing'] = srm._set_billing(self.dhl_account_number, "S", self.dhl_duty_payment,
                                                           self.dhl_dutiable)
            shipment_request['Consignee'] = srm._set_consignee(picking)
            shipment_request['Reference'] = srm._set_reference(picking)
            shipment_request['Shipper'] = srm._set_shipper(self.dhl_account_number, picking.company_id.partner_id,
                                                           picking.picking_type_id.warehouse_id.partner_id)
            SAR = self.env.ref('base.SAR')
            # Check if it's COD
            is_cod, cod_amount, currency = self.order_cod_amount(order)  # Method implemented on tatayab_delivery_core
            if is_cod:
                raise UserError(_("DHL doesn't deal with COD orders!!!\n"
                                "Please review order: %s payment details" % order.display_name))
            # products_lines = order.order_line.filtered(lambda l: l.line_type == 'product')
            # amount_subtotal = sum(ln.price_unit_sa for ln in products_lines)
            if self.dhl_dutiable:
                shipment_request['Dutiable'] = srm._set_dutiable(cod_amount, currency.name)
            self.check_packages(picking)
            shipment_request['ShipmentDetails'] = srm._set_shipment_details(picking)
            shipment_request['LabelImageFormat'] = srm._set_label_image_format(self.dhl_label_image_format)
            shipment_request['Label'] = srm._set_label(self.dhl_label_template)
            shipment_request['schemaVersion'] = 10.0
            shipment_request['LanguageCode'] = 'en'
            logging.info(yellow + "___ shipment_request: %s" % shipment_request + reset)
            picking.carrier_log_id = carrier_log_obg.create({
                'carrier_id': self.id,
                'carrier_request': shipment_request})
            dhl_response = srm._process_shipment(shipment_request)
            logging.info(green + "___ RESPONSE: %s" % dhl_response + reset)
            traking_number = dhl_response.AirwayBillNumber
            picking.carrier_log_id.tracking_ref = traking_number
            logmessage = (_("Shipment created into DHL <br/> <b>Tracking Number : </b>%s") % traking_number)
            message = picking.message_post(body=logmessage, attachments=[('LabelDHL-%s.%s' % (
                traking_number, self.dhl_label_image_format), dhl_response.LabelImage[0].OutputImage)])
            picking.write({
                'attach_label_id': message.attachment_ids and message.attachment_ids[0] or False,
                'carrier_tracking_ref': traking_number
            })
            # TODO: to be called and handle to inform backend
            picking.inform_backend_tracking_ref('dhl')
            print("Message: ", message)
            shipping_data = {
                'exact_price': 0,
                'tracking_number': traking_number,
            }
            rate = self._rate_shipment_vals(picking=picking)
            shipping_data['exact_price'] = rate['price']
            if self.return_label_on_delivery:
                self.get_return_label(picking)
            res = res + [shipping_data]

        return res

    def check_packages(self, picking):
        if not picking.package_ids:
            count = 0
            if picking.pkg_half:
                count += 0.5
            if picking.pkg_one:
                count += 1
            if picking.pkg_two:
                count += 2
            if picking.pkg_three:
                count += 3
            self._create_shipping_pack(count, picking)

    def _create_shipping_pack(self, weight, picking):
        wiz_action = picking.action_put_in_pack()
        wiz_context = wiz_action and wiz_action.get('context') or {}
        wiz = self.env['choose.delivery.package'].with_context(wiz_context).create({
            'picking_id': picking.id,
            'delivery_package_type_id': picking.carrier_id.dhl_default_package_type_id.id,
            'shipping_weight': weight
        })
        wiz.action_put_in_pack()

# Ahmed Salama Code End.
