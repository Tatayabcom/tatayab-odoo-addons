{
    'name': 'Barq Delivery',
    'author': 'Tatayab , Omnya Rashwan',
    'category': 'Stock',
    'summary': """Tatayab""",
    'website': 'http://www.tatayab.com',
    'description': ''' Barq Delivery Shipment API''',
    'depends': ['stock', 'sale_stock', 'delivery', 'tatayab_delivery_core'],
    'data': [
        'data/delivery_data.xml',
        'data/barq_mail_data.xml',
        
        'views/delivery_carrier_view.xml',
    ],
}
