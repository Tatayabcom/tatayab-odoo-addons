import json
import logging
import requests
import base64
from odoo import fields, models, _
from odoo.exceptions import UserError, ValidationError

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
logger = logging.getLogger(__name__)
_DAYS = [('0', 'Sunday'),
         ('1', 'Monday'),
         ('2', 'Tuesday'),
         ('3', 'Wednesday'),
         ('4', 'Thursday'),
         ('5', 'Friday'),
         ('6', 'Saturday')]


class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'
    
    delivery_type = fields.Selection(selection_add=[('barq', 'Barq')]
                                     , ondelete={'barq': lambda recs: recs.write({'delivery_type': 'fixed', 'fixed_price': 0})})

    barq_token = fields.Char("Barq Token")
    barq_user_email = fields.Char("Barq Email")
    barq_password = fields.Char("Barq Password")
    # content_type = fields.Char(string="Content-Type")
    barq_url = fields.Char("Login Url")
    barq_show_log = fields.Boolean(string='Show Api Log')
    barq_order_url = fields.Char("Create Order Url")
    barq_active_cities_url = fields.Char(string='Active Cities Url')
    barq_hubs_cities_url = fields.Char(string='Hubs Cities Url')
    barq_hub_url = fields.Char(string='Create Hub Url')
    barq_airwaybill_url = fields.Char(string='Airwaybill Url')
    
    # HUB DATA
    opening_time = fields.Float(string='Opening Time', default="8")
    closing_time = fields.Float(string='Closing Time', default="4")
    start_day = fields.Selection(_DAYS, default="6")
    end_day = fields.Selection(_DAYS, default="4")
    hub_id = fields.Char(string='Hub ID')  # TODO: No need for it
    hub_code = fields.Char(string='Hub Code')  # TODO: No need for it
    manager_id = fields.Many2one(comodel_name='hr.employee', string='Manager')
    longitude = fields.Float(string='Longitude')
    latitude = fields.Float(string='Latitude')
    
    def _get_barq_token(self, url_header):
        req_details = {"email": self.barq_user_email, "password": self.barq_password}
        payload = json.dumps(req_details)
        headers = {'Content-Type': "application/json"}
        token_url = "%s.%s" % (url_header, self.barq_url)
        response = requests.request("POST", token_url, headers=headers, data=payload)
        result = json.loads(response.text)
        if not result.get('token'):
            raise ValidationError("Something went wrong while login to barq\n and returned with error message %s" % result)
        if self.barq_show_log:
            logger.info(yellow + "\n__ Barq login %s" % req_details + reset)
            logger.info(yellow + "\n__ login token: %s" % result.get('token') + reset)
        return req_details, result.get('token')
    
    def _set_hub(self, city_id, url_header):
        payload = {
            "code": 1,  # Identifier code (MerchantUID)
            "city_id": city_id,
            "phone": self.manager_id.work_phone,
            "mobile": self.manager_id.mobile_phone,
            "manager": self.manager_id.name,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "is_active": True,
            "opening_time": self.opening_time,
            "closing_time": self.closing_time,
            "start_day": self.start_day,
            "end_day": self.end_day
        }
        headers = {'Authorization': self.barq_token}
        hub_url = "%s.%s" % (url_header, self.barq_hub_url)
        hub_response = requests.request("PUT", hub_url, headers=headers, data=payload)
       
        hub = hub_response.json()
        if hub.get('errors'):
            raise ValidationError("Something went wrong while get barq hun\n and returned with error message %s" % hub)
        if self.barq_show_log:
            logger.info(yellow + "\n__ Barq Hub %s" % payload + reset)
            logger.info(yellow + "\n__ Hub %s" % hub + reset)
        return payload, hub
    
    def _get_barq_cities(self, city_url):
        # Get cities
        headers = {'Authorization': self.barq_token}
        response = requests.request("GET", city_url, headers=headers, data={})
        cities = json.loads(response.text)
        if (isinstance(cities, dict) and cities.get('errors')) or not cities:
            logger.info(red + "\n__ Barq cities url: %s" % city_url + reset)
            logger.info(red + "\n__ Barq cities headers: %s" % headers + reset)
            raise ValidationError(
                "Something went wrong while get Barq Cities\n and returned with error message %s" % cities)
        return cities
    
    def _get_hubs(self, city_hub_url):
        # Get hub for specific city
        headers = {'Authorization': self.barq_token}
        response = requests.request("GET", city_hub_url, headers=headers, data={})
        hub = json.loads(response.text)
        if isinstance(hub, dict) and hub.get('errors'):
            logger.info(red + "\n__ Barq city hub url: %s" % city_hub_url + reset)
            logger.info(red + "\n__ Barq city headers: %s" % headers + reset)
            raise ValidationError(
                "Something went wrong while get Barq Cities\n and returned with error message %s" % hub.get('errors'))
        return hub

    def barq_send_shipping(self, pickings):
        carrier_log_obj = self.env['delivery.carrier.log']
        attachment_obj = self.env['ir.attachment']
        request_details = ""
        url_header = self.prod_environment and "https://live" or "https://staging"
        if not self.barq_token:
            login_details, self.barq_token = self._get_barq_token(url_header)
            request_details += "\n_LOGIN DETAILS: \n %s\n" % login_details
            request_details += "\n_LOGIN Response: \n %s\n" % self.barq_token
        
        def create_attachment(picking, name, datas):
            return attachment_obj.sudo().create({
                'name': ("%s-%s" % (picking.name, name)).replace('/', '_'),
                'datas': datas,
                'res_model': picking._name,
                'res_id': picking.id
            }).id
        for picking in pickings:
            order = picking.sale_id
            partner = picking.partner_id
            moves = picking.move_ids_without_package
            
            # Cities
            warehouse_address_id = picking.picking_type_id.warehouse_id.partner_id
            hub_city = warehouse_address_id.state_id and warehouse_address_id.state_id.name or warehouse_address_id.city
            order_city = partner.state_id and partner.state_id.name or partner.city
            # All Barq Active Cities
            active_cities_url = "%s.%s" % (url_header, self.barq_active_cities_url)
            active_cities = self._get_barq_cities(active_cities_url)
            active_cities_name = [city.get('name') for city in active_cities]
            # Validate Cities
            if order_city not in active_cities_name:
                raise ValidationError(
                    _("Validation Error\n order customer city:%s \nis not matching Barq active cities."
                      "\n Please select from Barq Available Cities %s.") % (order_city, active_cities_name))
            if hub_city not in active_cities_name:
                raise ValidationError(
                    _("Validation Error\n Our hub city(Warehouse City):%s \nis not matching Barq active cities."
                      "\n Please select from Barq Available Cities %s.") % (hub_city, active_cities_name))
            # getting cities id
            order_city_id = False
            for city in active_cities:
                if city.get('name') == order_city:
                    order_city_id = city.get('id')
            hub_city_id = False
            for city in active_cities:
                if city.get('name') == hub_city:
                    hub_city_id = city.get('id')
                    
            # TODO: Check for hub city
            # Create Hubs if not exist
            # hub_cities_url = "%s.%s" % (url_header, self.barq_hubs_cities_url)
            # hub_cities = self._get_barq_cities(hub_cities_url)
            # hub_cities_name = [city.get('name') for city in hub_cities]
            city_hub_url = "%s.%s/%s/hubs" % (url_header, self.barq_hubs_cities_url, hub_city_id)
            city_hub = self._get_hubs(city_hub_url)
            if not city_hub or not city_hub[0]:
                hub_details, hub = self._set_hub(hub_city_id, url_header)
                request_details += "\n_New HUB DETAILS: \n %s\n" % hub_details
                request_details += "\n_New HUB Response: \n %s\n" % hub
                self.hub_id = hub['id']
                self.hub_code = hub['code']
            else:
                # Assign hub details
                request_details += "\n_Exist HUB Response: \n ID: %s, CODE: %s\n" % (city_hub[0]['id'],  city_hub[0]['code'])
                self.hub_id = city_hub[0]['id']
                self.hub_code = city_hub[0]['code']
                print("hub_id %s, hub_code %s " % (self.hub_id, self.hub_code))
            # Collect Payment Details
            is_cod, cod_amount, currency = self.order_cod_amount(order)  # Method implemented on tatayab_delivery_core
            # Collect product details
            product_details = []
            for move in moves:
                product = move.product_id
                qty = move.product_qty
                unit_price = move.sale_line_id.price_unit
                so_price_unit = unit_price
                # Get Products
                product_details.append({
                    "sku": product.default_code,
                    "serial_no": product.barcode,
                    "name": product.name,
                    "color": "",
                    "brand": "",
                    "price": so_price_unit,
                    "weight_kg": product.weight,
                    "qty": qty
                })
            # Request Body
            partner_name = partner.name and partner.name.split(" ") or ["Without Name", ""]
            order_details = json.dumps({
                "payment_type": is_cod and 1 or 0,
                "shipment_type": 0,
                "hub_id": int(self.hub_id),
                "hub_code": self.hub_code,
                "merchant_order_id": picking.origin,
                "invoice_total": cod_amount,
                "customer_details": {
                    "first_name": partner_name[0],
                    "last_name": " ".join(partner_name[1:]),
                    "country": partner.country_id.name,
                    "city": order_city_id,
                    "mobile": partner.mobile,
                    "address": "%s, %s" % (partner.street, partner.street2),
                },
                "products": product_details,
                "packages_count": picking.no_of_boxes,
                # "destination": {
                #     "latitude": self.latitude,
                #     "longitude": self.longitude
                # }
            })
            payload = order_details.encode('utf8')
            request_details += "\n_Order DETAILS: \n %s\n" % payload
            # Get Request Header
            headers = {'Content-Type': "application/json", 'Authorization': self.barq_token}
            order_url = "%s.%s" % (url_header, self.barq_order_url)
            response = requests.request("POST", order_url, headers=headers, data=payload)
            result = response.json()
            if self.barq_show_log:
                logger.info(yellow + "\n__ Barq url %s" % order_url + reset)
                logger.info(yellow + "\n__ Barq headers %s" % headers + reset)
                logger.info(yellow + "\n__ Barq req_body %s" % payload + reset)
                logger.info(yellow + "\n__ Barq result %s" % result + reset)
            request_details += "\n__ Barq url \n %s\n" % order_url
            request_details += "\n_Result: \n %s\n" % result
            if result.get('code'):
                logging.error(response.text)
                if result.get('code') == 302:
                    # Order already exist
                    picking.message_post(body=str(result))
                    barq_tracking_ref = result.get('tracking_no')
                    airwaybill_id = result.get('order_id')
                else:
                    raise UserError("Response Code is %s. \n-----------------------------\n Data send: %s \n %s"
                                    % (result.get('code'), result.get('message'),
                                       result.get('tracking_no') and "Tracking ref: " + result.get('tracking_no') or ""))
            else:
                # Get Tracking Ref
                barq_tracking_ref = result.get('tracking_no')
                airwaybill_id = result.get('id')
            
            picking.carrier_log_id = carrier_log_obj.create({'carrier_id': self.id,
                                                             'carrier_request': request_details,
                                                             'airwaybill_id': airwaybill_id,
                                                             'tracking_ref': barq_tracking_ref})
            # GET AirWayBill PDF
            airwaybill_url = "%s.%s%s" % (url_header, self.barq_airwaybill_url, airwaybill_id)
            pdf_response = requests.get(airwaybill_url, headers=headers)
            file_content = base64.encodebytes(pdf_response.content)
            pdf_lable_file = create_attachment(picking, 'Label-Barq-%s.pdf' % picking.origin, file_content)
            # Update Picking
            picking.write({
                'carrier_tracking_ref': barq_tracking_ref,
                'attach_label_id': pdf_lable_file,
            })
            if barq_tracking_ref:
                # Inform customer by his shippment tracking ref
                mail_template = self.env.ref('tatayab_delivery_barq.barq_tracking_ref_email_template')
                mail_template.send_mail(picking.id, force_send=True)
            # TODO: to be called and handle to inform backend
            picking.inform_backend_tracking_ref('aramex')
            msg = "Shipment created successfully. <br /> Barq Reference No: %s" % ("'{}'".format(barq_tracking_ref))
            picking.message_post(body=msg)
            picking.sale_id.message_post(body=msg)


class DeliveryCarrierLogInherit(models.Model):
    _inherit = 'delivery.carrier.log'
    
    airwaybill_id = fields.Char("Barq AirWayBill ID")
