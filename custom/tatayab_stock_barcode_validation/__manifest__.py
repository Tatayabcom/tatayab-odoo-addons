# -*- coding: utf-8 -*-

{
    'name': "Barcode Validation",
    'summary': "Use barcode scanners to process logistics operations",
    'description': """
This module enables the barcode scanning feature for the warehouse management system.
    """,
    'category': 'Inventory/Inventory',
    'sequence': 255,
    'version': '1.0',
    'depends': ['barcodes_gs1_nomenclature', 'stock', 'tatayab_operation_status',
                'web_tour', 'web_mobile','stock_barcode'],
    'data': [
        'views/stock_move_line_views.xml',
    ],

    'installable': True,
    'application': True,
    'license': 'OEEL-1',

}
