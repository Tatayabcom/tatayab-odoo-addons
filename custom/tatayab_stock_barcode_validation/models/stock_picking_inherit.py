# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import datetime

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def button_validate(self):
        for rec in self:
            if not rec.picking_type_id.pos_picking:
                if rec.picking_type_id.code == 'outgoing' and not rec.picking_type_id.stock_type_return_vendor:
                    if rec.operation_state == 'pick':
                        raise ValidationError(_("You must validate this picking from bulk scan window"))
                    for line in rec.move_ids_without_package:
                        if line.quantity_done != line.product_uom_qty:
                            raise ValidationError(_("Quantity Done must to be  equal Demand"))
                else:
                    rec.operation_state = 'done'
        return super().button_validate()



class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'


    @api.constrains('expiration_date')
    def constrains_expiration_date_input(self):
        for rec in self:
            if rec.expiration_date:
                number_days = (rec.expiration_date - fields.Datetime.today()).days
                if number_days < 150:
                    raise ValidationError(_("The Expiration Date of product [ %s ] incorrect ")%rec.product_id.display_name)


