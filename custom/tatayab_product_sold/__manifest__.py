# -*- coding: utf-8 -*-
{
    'name': "Tatayab Product Sold Report",
    'author': 'Tatayab, ',
    'category': 'Accounting',
    'summary': """Tatayab Product Sold Report""",
    'website': 'http://www.tatayab.com',
    'description': """""",
    'version': '15.0',
    'depends': ['web', 'stock_account', 'tatayab_purchase_changes'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/product_sold_report_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
