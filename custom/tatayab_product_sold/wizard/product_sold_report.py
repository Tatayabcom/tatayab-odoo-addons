import base64
import calendar
import datetime as dd
import logging
from collections import OrderedDict
from datetime import datetime, timedelta
from io import BytesIO

import xlsxwriter
from dateutil.relativedelta import relativedelta
from urllib3.connectionpool import xrange

from odoo import fields, models, _

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class ProductSoldWithDate(models.TransientModel):
    _name = 'product.sold.with.date'

    partner_id = fields.Many2one('res.partner', string="Vendor", required=True)
    date_from = fields.Date(string="Date From", required=True)
    date_to = fields.Date(string="Date To", required=True)
    purchase_type_id = fields.Many2one('purchase.type', "Purchase Type", readonly=False, required=True)
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", required=True)
    export_data = fields.Binary()

    def print_excel(self):
        self.ensure_one()
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        header_format = workbook.add_format({'font_size': 20, 'align': 'center', 'border': True})
        bold = workbook.add_format({'bold': True, 'align': 'left', 'font_size': 10})
        center = workbook.add_format({'bold': False, 'align': 'center'})
        header_center = workbook.add_format({'bold': True, 'align': 'center'})
        lang_code = self.env.user.lang or 'en_US'
        date_format = self.env['res.lang']._lang_get(lang_code).date_format
        worksheet = workbook.add_worksheet(_('Sales Invoice in Details'))
        warehouses = "All Warehouse"
        if self.warehouse_ids:
            warehouses = '-'.join(self.warehouse_ids.mapped('name'))

        row = 0
        worksheet.merge_range(row, 1, row + 1, 10, 'Vendor Product Details', header_format)
        row += 2
        worksheet.merge_range(row, 1, row + 1, 4, 'Vendor :' + self.partner_id.name, bold)
        worksheet.merge_range(row, 5, row, 11,
                              'Date From :' + str(self.date_from) + " : Date To :" + str(self.date_to)
                              , bold)
        row += 1
        worksheet.merge_range(row, 5, row, 11, 'Warehouses: ' + warehouses, bold)

        row += 2
        worksheet.write(5, 2, 'Due In Month', header_center)
        worksheet.write(6, 2, 'Total Due', header_center)
        worksheet.write(7, 2, 'Paid', header_center)
        worksheet.write(8, 2, 'Net Due', header_center)
        worksheet.write(9, 0, 'Count', header_center)
        worksheet.write(9, 1, 'SKU', header_center)
        worksheet.write(9, 2, 'Product', header_center)
        dates = [str(self.date_from), str(self.date_to)]
        start, end = [datetime.strptime(_, "%Y-%m-%d") for _ in dates]
        list_date = OrderedDict(
            ((start + timedelta(_)).strftime(r"%b-%y"), None) for _ in xrange((end - start).days)).keys()
        col = 3
        col_body = 3
        row_body = 11
        cost_list_per_month = []

        products = []
        products_name = []
        all_purchase_order = self.env['purchase.order'].search([('partner_id', '=', self.partner_id.id),
                                                                ('state', 'in', ['purchase', 'done']),
                                                                ])
        if self.purchase_type_id:
            all_purchase_order = self.env['purchase.order'].search([('partner_id', '=', self.partner_id.id),
                                                                    ('purchase_type_id', '=', self.purchase_type_id.id),
                                                                    ('state', 'in', ['purchase', 'done']),
                                                                    ])
        logging.info(yellow + "___ all_purchase_order: %s" % all_purchase_order + reset)
        for purchase in all_purchase_order:
            for line in purchase.order_line:
                if line.product_id not in products:
                    products.append(line.product_id)
                    products_name.append(line.product_id.name)

        count = 1
        row = 11
        for so_product in products:
            worksheet.write(row, 0, count, center)
            worksheet.write(row, 1, so_product.default_code)
            worksheet.write(row, 2, so_product.name)
            row += 1
            count += 1

        # =============== to fill date for each cell ===========
        num_months = (self.date_to.year - self.date_from.year) * 12 + (self.date_to.month - self.date_from.month)
        add_month, initial_due = 0, 0.0
        total_cost, total_paid, total_net_due = [], [], []
        first_day = self.date_from.replace(day=1)
        account_move_line_before = self.env['account.move.line'].search([('date', '<', first_day),
                                                                         ('partner_id', '=', self.partner_id.id),
                                                                         ('account_id.user_type_id.type', '=',
                                                                          'payable'),
                                                                         ])
        for move_line in account_move_line_before:
            initial_due += move_line.credit

        for item_date in list_date:
            worksheet.merge_range(9, col, 9, col + 2, item_date, header_center)
            worksheet.write(10, col, "Sold", header_center)
            col += 1
            worksheet.write(10, col, "Cost", header_center)
            col += 1
            worksheet.write(10, col, "Margin", header_center)
            total_cost_products, total_paid_products = 0.0, 0.0
            first_day_month = self.date_from.replace(day=1) + relativedelta(months=add_month)
            last_day_month = dd.date(first_day_month.year, first_day_month.month,
                                     calendar.monthrange(first_day_month.year, first_day_month.month)[1])
            account_move_line = self.env['account.move.line'].search([('date', '>=', first_day_month),
                                                                      ('date', '<', last_day_month),
                                                                      ('partner_id', '=', self.partner_id.id),
                                                                      ('account_id.user_type_id.type', '=', 'payable'),
                                                                      ])
            sale_order_line_in_month = self.env['sale.order.line'].search(
                [('order_id.date_order', '>=', first_day_month),
                 ('order_id.date_order', '<=', last_day_month),
                 ])
            if self.warehouse_ids:
                sale_order_line_in_month = self.env['sale.order.line'].search(
                    [('order_id.date_order', '>=', first_day_month),
                     ('order_id.date_order', '<=', last_day_month),
                     ('order_id.warehouse_id', 'in', self.warehouse_ids.ids),
                     ])
            for product in products:
                sale_order_line = sale_order_line_in_month.filtered(lambda l: l.product_id.id == product.id)
                sale_qty, cost_product, total_sales = 0, 0.0, 0.0
                for so_line in sale_order_line:
                    for move in so_line.move_ids:
                        for layer in move.stock_valuation_layer_ids:
                            sale_qty += (layer.quantity * -1)
                            total_sales += (layer.quantity * -1 * so_line.price_unit)
                            cost_product += (layer.value * -1)
                            total_cost_products += (layer.value * -1)

                cost_list_per_month.append({
                    'cost_product': total_sales,
                    'margin': total_sales - cost_product,
                    'sale_qty': sale_qty,
                    'row': row_body,
                    'col': col_body,
                    'sku': product.default_code,
                    'product': product.name,
                    'product_id': product.id,
                    'month': item_date
                })
                row_body += 1
            for move_line in account_move_line:
                total_paid_products += move_line.debit
            total_cost.append(total_cost_products)
            total_paid.append(total_paid_products)
            total_net_due_products = total_cost_products - total_paid_products
            total_net_due.append(total_net_due_products)
            col += 1
            row_body = 11
            col_body += 3
            add_month += 1
        # worksheet.write(row, col, 'Amount', header_center)
        row += 1
        for item_cost in cost_list_per_month:
            worksheet.write(item_cost['row'], item_cost['col'], item_cost['sale_qty'], center)
            worksheet.write(item_cost['row'], item_cost['col'] + 1, item_cost['cost_product'], center)
            worksheet.write(item_cost['row'], item_cost['col'] + 2, item_cost['margin'], center)
        col_due = 3
        for due in total_cost:
            worksheet.merge_range(5, col_due, 5, col_due + 2, due, center)
            # worksheet.write(5, col_due, due)
            col_due += 3
        logging.info(yellow + "___ total_cost: %s" % total_cost + reset)
        col_paid = 3
        for paid in total_paid:
            worksheet.merge_range(7, col_paid, 7, col_paid + 2, paid, center)
            # worksheet.write(7, col_paid, paid)
            col_paid += 3
        logging.info(blue + "___ total_paid: %s" % total_paid + reset)
        due_plus_previous_month = [total_cost[0] + initial_due]
        actual_net_due = [total_cost[0] - total_paid[0] + initial_due]
        for i in range(1, len(total_cost)):
            amount_due = sum(total_cost[0:i + 1]) - sum(total_paid[0:i])
            # amount_due = total_cost[i-1] + total_cost[i] - total_paid[i-1]
            due_plus_previous_month.append(amount_due)
            item = amount_due - total_paid[i]
            actual_net_due.append(item)

        col_due_pre = 3
        for due_pre in due_plus_previous_month:
            # worksheet.write(6, col_due_pre, due_pre)
            worksheet.merge_range(6, col_due_pre, 6, col_due_pre + 2, due_pre, center)
            col_due_pre += 3
        logging.info(red + "___ due_plus_previous_month: %s" % due_plus_previous_month + reset)

        col_net_due = 3
        for net_due in actual_net_due:
            worksheet.merge_range(8, col_net_due, 8, col_net_due + 2, net_due, center)
            col_net_due += 3
        logging.info(bold_red + "___ actual_net_due: %s" % actual_net_due + reset)
        workbook.close()
        export_data = base64.b64encode(fp.getvalue())
        fp.close()
        self.write({"export_data": export_data})
        file_name = "VendorwiseReport.xlsx"
        return {
            'target': 'new',
            'type': 'ir.actions.act_url',
            'url': '/web/content/%s/%s/%s/%s'
                   % (self._name, self.id, 'export_data', file_name),
        }
