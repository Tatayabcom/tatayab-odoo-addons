from odoo import fields, models, api, _


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    is_checks_journal = fields.Boolean(string='Checks Journal')  # Visible only in case of bank
    # steps of pay by check in case of customer and in case of vendor
    customer_flow = fields.Selection(selection=[('two_steps', '2 Steps'),
                                                ('Three_steps', '3 Steps')], string='Customer Flow')
    vendor_flow = fields.Selection(selection=[('two_steps', '2 Steps'),
                                              ('Three_steps', '3 Steps')], string='Vendor Flow')

    # Accounts
    notes_rec_account = fields.Many2one(comodel_name='account.account', string='Notes Receivable Account')
    notes_pay_account = fields.Many2one(comodel_name='account.account', string='Notes Payable Account')
    check_u_coll_account = fields.Many2one(comodel_name='account.account', string='Checks Under Collection Account')
    check_u_pay_account = fields.Many2one(comodel_name='account.account', string='Checks Under Payment Account')
