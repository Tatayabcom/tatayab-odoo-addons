from odoo import fields, models, api, _
from odoo.exceptions import ValidationError, UserError


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    # New state when pay by checks
    new_state = fields.Selection(selection=[
        ('draft', 'Draft'),
        ('confirmed', 'confirmed'),
        ('notes_validated', 'Validated'),
        ('under_collection', 'Under Collection'),
        ('under_payment', 'Under Payment'),
        ('collected', 'Collected'),
        ('cancelled', 'Cancelled')], default='draft')
    check_checks_journal = fields.Boolean(compute='check_journals', readonly=False)
    two_steps_check = fields.Boolean()  # if check paid on 2 steps
    control_bank_collect = fields.Boolean()
    check_roll_back = fields.Boolean()  # if user want to roll back the check

    # Method check if journal had checks and check if it works with 2 or 3 steps.
    def check_journals(self):
        for rec in self:
            rec.check_checks_journal = False
            rec.two_steps_check = False

            if rec.journal_id.is_checks_journal:
                rec.check_checks_journal = True

            if rec.partner_type == 'supplier' and rec.journal_id.vendor_flow == 'two_steps' or \
                    rec.partner_type == 'customer' and rec.journal_id.customer_flow == 'two_steps':
                rec.two_steps_check = True

    # prepare lines created in journal entries.
    def prepare_payment_moves(self):
        all_move_vals = []
        for payment in self:

            # Compute amounts.
            write_off_amount = 0.0  # -payment.payment_difference or

            if payment.payment_type in ('outbound', 'transfer'):  # send money
                counterpart_amount = payment.amount
                liquidity_line_account = payment.journal_id.notes_pay_account
                under_collection_account = payment.journal_id.check_u_pay_account
                default_account = payment.journal_id.default_account_id
            else:  # receive money
                counterpart_amount = -payment.amount
                liquidity_line_account = payment.journal_id.notes_rec_account
                under_collection_account = payment.journal_id.check_u_coll_account
                default_account = payment.journal_id.default_account_id

            # Manage currency.
            if payment.currency_id == payment.company_id.currency_id:
                # Single-currency.
                balance = counterpart_amount
                write_off_balance = write_off_amount
                counterpart_amount = write_off_amount = 0.0
                currency_id = False
            else:
                # Multi-currencies.
                balance = payment.currency_id._convert(counterpart_amount, payment.company_id.currency_id,
                                                       payment.company_id,
                                                       payment.date)
                write_off_balance = payment.currency_id._convert(write_off_amount, payment.company_id.currency_id,
                                                                 payment.company_id,
                                                                 payment.date)
                currency_id = payment.currency_id.id

            # Manage custom currency on journal for liquidity line.
            if payment.journal_id.currency_id and payment.currency_id != payment.journal_id.currency_id:
                # Custom currency on journal.
                if payment.journal_id.currency_id == payment.company_id.currency_id:
                    # Single-currency
                    liquidity_line_currency_id = False
                else:
                    liquidity_line_currency_id = payment.journal_id.currency_id.id

                liquidity_amount = payment.company_id.currency_id._convert(
                    balance, payment.journal_id.currency_id, payment.company_id, payment.date)
            else:
                # Use the payment currency.
                liquidity_line_currency_id = currency_id
                liquidity_amount = counterpart_amount

            # Compute 'name' to be used in receivable/payable line.
            rec_pay_line_name = ''
            if payment.payment_type == 'transfer':
                rec_pay_line_name = payment.name
            else:
                if payment.partner_type == 'customer':
                    if payment.payment_type == 'inbound':
                        rec_pay_line_name += _("Customer Payment")
                    elif payment.payment_type == 'outbound':
                        rec_pay_line_name += _("Customer Credit Note")
                elif payment.partner_type == 'supplier':
                    if payment.payment_type == 'inbound':
                        rec_pay_line_name += _("Vendor Credit Note")
                    elif payment.payment_type == 'outbound':
                        rec_pay_line_name += _("Vendor Payment")

            if payment.new_state == 'confirmed' or payment.new_state == 'notes_validated' and payment.check_roll_back:
                move_vals = {
                    'date': payment.date,
                    'ref': payment.ref,
                    'journal_id': payment.journal_id.id,
                    'currency_id': payment.journal_id.currency_id.id or payment.company_id.currency_id.id,
                    'partner_id': payment.partner_id.id,
                    'line_ids': [
                        # Receivable / Payable / Transfer line.
                        (0, 0, {
                            'name': rec_pay_line_name,
                            'amount_currency': counterpart_amount + write_off_amount if currency_id else 0.0,
                            'currency_id': currency_id,
                            'debit': balance + write_off_balance > 0.0 and balance + write_off_balance or 0.0,
                            'credit': balance + write_off_balance < 0.0 and -balance - write_off_balance or 0.0,
                            'date_maturity': payment.date,
                            'partner_id': payment.partner_id.commercial_partner_id.id,
                            'account_id': payment.destination_account_id.id if not payment.check_roll_back else liquidity_line_account.id,
                            'payment_id': payment.id,
                        }),
                        # Liquidity line.
                        (0, 0, {
                            'name': payment.name,
                            'amount_currency': -liquidity_amount if liquidity_line_currency_id else 0.0,
                            'currency_id': liquidity_line_currency_id,
                            'debit': balance < 0.0 and -balance or 0.0,
                            'credit': balance > 0.0 and balance or 0.0,
                            'date_maturity': payment.date,
                            'partner_id': payment.partner_id.commercial_partner_id.id,
                            'account_id': liquidity_line_account.id if not payment.check_roll_back else payment.destination_account_id.id,
                            'payment_id': payment.id,
                        }),
                    ],
                }
            else:
                if not payment.two_steps_check and not payment.control_bank_collect:
                    move_vals = {
                        'date': payment.date,
                        'ref': payment.ref,
                        'journal_id': payment.journal_id.id,
                        'currency_id': payment.journal_id.currency_id.id or payment.company_id.currency_id.id,
                        'partner_id': payment.partner_id.id,
                        'line_ids': [
                            # Receivable / Payable / Transfer line.
                            (0, 0, {
                                'name': rec_pay_line_name,
                                'amount_currency': counterpart_amount + write_off_amount if currency_id else 0.0,
                                'currency_id': currency_id,
                                'debit': balance + write_off_balance > 0.0 and balance + write_off_balance or 0.0,
                                'credit': balance + write_off_balance < 0.0 and -balance - write_off_balance or 0.0,
                                'date_maturity': payment.date,
                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                'account_id': liquidity_line_account.id if not payment.check_roll_back else under_collection_account.id,
                                'payment_id': payment.id,
                            }),
                            # Liquidity line.
                            (0, 0, {
                                'name': payment.name,
                                'amount_currency': -liquidity_amount if liquidity_line_currency_id else 0.0,
                                'currency_id': liquidity_line_currency_id,
                                'debit': balance < 0.0 and -balance or 0.0,
                                'credit': balance > 0.0 and balance or 0.0,
                                'date_maturity': payment.date,
                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                'account_id': under_collection_account.id if not payment.check_roll_back else liquidity_line_account.id,
                                'payment_id': payment.id,
                            }),
                        ],
                    }
                else:
                    def_account = default_account.id
                    if not payment.control_bank_collect:
                        account = liquidity_line_account.id
                    else:
                        account = under_collection_account.id
                        if payment.check_roll_back:
                            if payment.new_state == 'collected':
                                account = default_account.id
                                def_account = under_collection_account.id if not payment.two_steps_check else liquidity_line_account.id
                            else:
                                def_account = liquidity_line_account.id
                                account = under_collection_account.id
                                if payment.two_steps_check:
                                    def_account = default_account.id if not payment.control_bank_collect else payment.destination_account_id.id
                                    account = liquidity_line_account.id
                    move_vals = {
                        'date': payment.date,
                        'ref': payment.ref,
                        'journal_id': payment.journal_id.id,
                        'currency_id': payment.journal_id.currency_id.id or payment.company_id.currency_id.id,
                        'partner_id': payment.partner_id.id,
                        'line_ids': [
                            # Receivable / Payable / Transfer line.
                            (0, 0, {
                                'name': rec_pay_line_name,
                                'amount_currency': counterpart_amount + write_off_amount if currency_id else 0.0,
                                'currency_id': currency_id,
                                'debit': balance + write_off_balance > 0.0 and balance + write_off_balance or 0.0,
                                'credit': balance + write_off_balance < 0.0 and -balance - write_off_balance or 0.0,
                                'date_maturity': payment.date,
                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                # 'account_id': under_collection_account.id,
                                'account_id': account,
                                'payment_id': payment.id,
                            }),
                            # Liquidity line.
                            (0, 0, {
                                'name': payment.name,
                                'amount_currency': -liquidity_amount if liquidity_line_currency_id else 0.0,
                                'currency_id': liquidity_line_currency_id,
                                'debit': balance < 0.0 and -balance or 0.0,
                                'credit': balance > 0.0 and balance or 0.0,
                                'date_maturity': payment.date,
                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                'account_id': def_account,
                                'payment_id': payment.id,
                            }),
                        ],
                    }

            all_move_vals.append(move_vals)
        return all_move_vals

    def action_validate_checks(self):
        for rec in self:
            account_moves = self.env['account.move'].with_context(default_move_type='entry').search(
                [('payment_id', '=', rec.id)])

            # keep the name in case of a payment reset to draft
            if not rec.name:
                # Use the right sequence to set the name
                if rec.payment_type == 'transfer':
                    sequence_code = 'account.payment.transfer'
                else:
                    if rec.partner_type == 'customer':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.customer.invoice'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.customer.refund'
                    if rec.partner_type == 'supplier':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.supplier.refund'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.supplier.invoice'

                rec.name = self.env['ir.sequence'].next_by_code(sequence_code, sequence_date=rec.date)
                if not rec.name and rec.payment_type != 'transfer':
                    raise UserError(_("You have to define a sequence for %s in your company.") % (sequence_code,))

            moves = account_moves.create(rec.prepare_payment_moves())
            print(moves)
            for record in moves:
                record.write({'payment_id': rec.id, 'state': 'posted'})
            invoices = self.env['account.move'].search([('ref', '=', rec.ref)])
            print(invoices, '222222222222222222222222222')
            for line in invoices:
                line.write({'check_journal': True})
                # 'payment_id': line.id
                # rec.write({'move_id': line.move_id.id})

            # Update the state / move before performing any reconciliation.
            if rec.new_state == 'confirmed':
                rec.write({'new_state': 'notes_validated'})

            if rec.payment_type in ('inbound', 'outbound'):
                # ==== 'inbound' / 'outbound' ====
                if rec.move_id:
                    if rec.new_state == 'collected':
                        move_lines = self.env['account.move.line'].search([('payment_id', '=', rec.id),
                                                                           ('reconciled', '=', False),
                                                                           ('account_id', '=',
                                                                            rec.destination_account_id.id)])
                        (move_lines + (rec.move_id.line_ids.filtered(lambda
                                                                         line: not line.reconciled and line.account_id == rec.destination_account_id))).reconcile()
            elif rec.payment_type == 'transfer':
                # ==== 'transfer' ====
                moves.mapped('line_ids') \
                    .filtered(lambda line: line.account_id == rec.company_id.transfer_account_id).reconcile()
            if rec.two_steps_check and rec.new_state not in ('draft', 'confirmed'):
                rec.control_bank_collect = True
            rec.check_roll_back = False
        return True

    def action_bank_collect(self):
        for item in self:
            if item.two_steps_check:
                item.control_bank_collect = False
            item.write({'new_state': 'collected'})
            item.action_validate_checks()
            item.control_bank_collect = False
            item.check_roll_back = False

    def action_bank_revise(self):
        for item in self:
            item.action_validate_checks()
            if item.partner_type == 'customer':
                item.write({'new_state': 'under_collection'})
            else:
                item.write({'new_state': 'under_payment'})
            item.control_bank_collect = True
            item.check_roll_back = False

    def action_set_to_draft(self):
        self.write({'new_state': 'draft'})

    def action_cancel(self):
        moves = self.mapped('move_id')
        moves.filtered(lambda move: move.state == 'posted').button_draft()
        moves.button_cancel()
        self.write({'new_state': 'cancelled'})

    def action_roll_back(self):
        for rec in self:
            rec.check_roll_back = True
            rec.control_bank_collect = True
            rec.action_validate_checks()
            rec.control_bank_collect = False
            if rec.new_state == 'notes_validated' or rec.new_state in ('under_collection', 'under_payment') and \
                    rec.two_steps_check:
                rec.write({'new_state': 'confirmed'})
            elif rec.new_state in ('under_collection', 'under_payment') and not rec.two_steps_check:
                rec.write({'new_state': 'notes_validated'})
            elif rec.new_state == 'collected':
                rec.control_bank_collect = True
                if rec.partner_type == 'customer':
                    rec.write({'new_state': 'under_collection'})
                else:
                    rec.write({'new_state': 'under_payment'})

    # action confirm to change state of payment.
    def action_confirm_checks(self):
        for item in self:
            item.action_post()
            item.write({'new_state': 'confirmed'})


class RegisterPayment(models.TransientModel):
    _inherit = 'account.payment.register'

    check_journal_wizard = fields.Boolean(related='journal_id.is_checks_journal')

    def _create_payments(self):
        result = super(RegisterPayment, self)._create_payments()
        if self.check_journal_wizard:
            # result.action_post()
            result.update({'new_state': 'confirmed'})
        return result
