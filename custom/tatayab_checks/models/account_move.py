from odoo import fields, models, api, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    check_journal = fields.Boolean(store=True)
