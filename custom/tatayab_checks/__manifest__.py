# -*- coding: utf-8 -*-
{
    'name': 'Tatayab Checks Module',
    'version': '1.0',
    'category': 'account',
    'author': 'Tatayab, Omnya Rashwan',
    'website': 'http://www.tatayab.com',
    'summary': """
    Tatayab checks
    """,
    'depends': ['base', 'account'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_journal_view.xml',
        'views/account_payment_view.xml',
        'views/account_move_view.xml',
    ],
}