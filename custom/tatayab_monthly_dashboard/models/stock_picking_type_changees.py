# -*- coding: utf-8 -*-
import logging
import calendar
from odoo import models, fields

igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->


class StockPickingType(models.Model):
	_inherit = 'stock.picking.type'
	
	# Current Month Dashboard Counters
	month_orders_count = fields.Integer("Month Orders", compute="_compute_month_operation_count")
	month_orders_count_approved = fields.Integer("Month Orders Approved")
	month_orders_count_not_approved = fields.Integer("Month Orders Not Approved",
	                                                 compute="_compute_month_operation_count")
	month_orders_count_waiting = fields.Integer("Month Orders Waiting", compute="_compute_month_operation_count")
	
	month_picking_count = fields.Integer("Month Picking", compute="_compute_month_operation_count")
	month_picked_count = fields.Integer("Month Picked", compute="_compute_month_operation_count")
	month_pick_range = fields.Integer("Month Picked Range", compute="_compute_month_operation_count")
	last_picked_date = fields.Datetime("Last Picked Date", compute="_compute_month_operation_count")
	
	month_packing_count = fields.Integer("Month Packing", compute="_compute_month_operation_count")
	month_packed_count = fields.Integer("Month Packed", compute="_compute_month_operation_count")
	month_pack_range = fields.Integer("Month Packed Range", compute="_compute_month_operation_count")
	last_packed_date = fields.Datetime("Last Packed Date", compute="_compute_month_operation_count")
	
	month_shipping_count = fields.Integer("Month shipping", compute="_compute_month_operation_count")
	month_shipped_count = fields.Integer("Month shipped", compute="_compute_month_operation_count")
	month_ship_range = fields.Integer("Month shipped Range", compute="_compute_month_operation_count")
	last_shipped_date = fields.Datetime("Last Shipped Date", compute="_compute_month_operation_count")
	
	month_returning_count = fields.Integer("Month Returning", compute="_compute_month_operation_count")
	month_returned_count = fields.Integer("Month Returned", compute="_compute_month_operation_count")
	month_returned_range = fields.Integer("Month Returned Range", compute="_compute_month_operation_count")
	last_returned_date = fields.Datetime("Last Returned Date", compute="_compute_month_operation_count")
	
	def _get_domain(self, month_start_date, month_end_date, operation_state=False):
		domain = [('picking_type_id', '=', self.id),
		          ('create_date', '>=', month_start_date),
		          ('create_date', '<=', month_end_date),
		          ('state', '!=', 'cancel')]
		if operation_state == 'non_approved':
			domain.append(('is_approved', '=', False))
		if operation_state == 'waiting':
			domain.append(('is_approved', '=', True))
			domain[3] = ('state', '=', ('waiting', 'confirmed'))
		if operation_state == 'pick':
			domain.append(('state', '=', 'assigned'))
			domain.append(('operation_state', '=', 'pick'))
			domain.append(('is_return', '=', False))
			domain.append(('is_approved', '=', True))
		if operation_state == 'pack':
			domain.append(('operation_state', '=', 'pack'))
			domain.append(('is_return', '=', False))
			domain.append(('is_approved', '=', True))
		if operation_state == 'ship':
			domain.append(('operation_state', '=', 'ship'))
			domain.append(('is_return', '=', False))
			domain.append(('is_approved', '=', True))
		if operation_state == 'non-pick':
			domain.append(('state', '=', 'done'))
			domain.append(('operation_state', '!=', 'pick'))
			domain.append(('is_return', '=', False))
			domain.append(('is_approved', '=', True))
		if operation_state == 'return':
			domain[0] = ('picking_type_id.warehouse_id', '=', self.warehouse_id.id)
			domain.append(('is_return', '=', True))
			domain.append(('is_approved', '=', True))
		print("Domain ----> ", domain)
		return domain
	
	def _get_month_period(self):
		date_today = fields.Datetime.now()
		filter_month = date_today.month
		filter_year = date_today.year
		if self.env.context.get('filter_month') or self.env.context.get('filter_year'):
			filter_month = int(self.env.context.get('filter_month'))
			filter_year = int(self.env.context.get('filter_year'))
		logging.info(yellow + "Filters : %s, %s" % (filter_month, filter_year) + reset)
		month_range = calendar.monthrange(filter_year, filter_month)
		logging.info(yellow + "month_range %s %s" % month_range + reset)
		return date_today.replace(day=1, hour=00, minute=00, second=00, month=filter_month, year=filter_year), \
		       date_today.replace(day=month_range[1], hour=23, minute=59, second=59, month=filter_month, year=filter_year)
	
	def _compute_month_operation_count(self):
		picking_obj = self.env['stock.picking']
		for op_type in self:
			picking_count = picked_count = packing_count = returnes = not_approved = waiting = \
				packed_count = shipping_count = shipped_count = returning_count = returned_count = 0
			all_picking_orders = last_picked_date = last_packed_date = last_shipped_date = last_returned_date = False
			if op_type.code == 'outgoing':
				month_start_date, month_end_date = self._get_month_period()
				all_picking_orders = picking_obj.search(op_type._get_domain(month_start_date, month_end_date, 'all'))
				not_approved = picking_obj.search_count(
					op_type._get_domain(month_start_date, month_end_date, 'non_approved'))
				waiting = picking_obj.search_count(op_type._get_domain(month_start_date, month_end_date, 'waiting'))
				picking_count = picking_obj.search_count(op_type._get_domain(month_start_date, month_end_date, 'pick'))
				
				non_pickings_picking_ids = picking_obj.search(
					op_type._get_domain(month_start_date, month_end_date, 'non-pick'))
				if non_pickings_picking_ids:
					# All picking on this month which is packing or shipping or done or return
					if non_pickings_picking_ids:
						sorted_pickings_picking_ids = non_pickings_picking_ids.filtered(lambda p: p.picking_date). \
							sorted(key=lambda p: p.picking_date)
						last_picked_date = sorted_pickings_picking_ids and sorted_pickings_picking_ids[
							-1].picking_date or False
						picked_count = len(non_pickings_picking_ids)
					packing_count = len(non_pickings_picking_ids.filtered(lambda p: p.operation_state == 'pack'
					                                                                and not p.is_returned))
					packed_ids = non_pickings_picking_ids.filtered(lambda p: p.operation_state != 'pack'
					                                                         and not p.is_returned)
					if packed_ids:
						packed_count = len(packed_ids)
						sorted_packed_date = packed_ids.filtered(lambda p: p.packing_date) \
							.sorted(key=lambda p: p.packing_date)
						last_packed_date = sorted_packed_date and sorted_packed_date[-1].packing_date or False
					shipping_count = len(non_pickings_picking_ids.filtered(lambda p: p.operation_state == 'ship'
					                                                                 and not p.is_returned))
					shipped_ids = non_pickings_picking_ids. \
						filtered(lambda p: p.operation_state in ('done', 'return_ship', 'return_done'))
					if shipped_ids:
						shipped_count = len(shipped_ids)
						sorted_shipped_date = shipped_ids.filtered(lambda p: p.date_done) \
							.sorted(key=lambda p: p.date_done)
						last_shipped_date = sorted_shipped_date and sorted_shipped_date[-1].date_done or False
				return_picking_ids = picking_obj.search(op_type._get_domain(month_start_date, month_end_date, 'return'))
				logging.info(
					yellow + "%s Returns: %s " % (op_type.warehouse_id.display_name, len(return_picking_ids)) + reset)
				if return_picking_ids:
					returnes = len(return_picking_ids)
					returning_count = len(return_picking_ids.filtered(lambda p: p.operation_state == 'return_pick'))
					returned_ids = return_picking_ids.filtered(lambda p: p.operation_state == 'return_done')
					if returned_ids:
						returned_count = len(returned_ids)
						sorted_returned_date = returned_ids.filtered(lambda p: p.date_done) \
							.sorted(key=lambda p: p.date_done)
						last_returned_date = sorted_returned_date and sorted_returned_date[-1].date_done or False
					logging.info(yellow + "Returns: %s : %s " % (returning_count, returned_count) + reset)
			all_orders = len(all_picking_orders)
			op_type.month_orders_count = all_orders
			
			op_type.month_orders_count_not_approved = not_approved
			op_type.month_orders_count_waiting = waiting
			op_type.month_picking_count = picking_count
			op_type.month_picked_count = picked_count
			op_type.month_pick_range = all_orders and (all_orders - picking_count) / all_orders * 100 or 100
			op_type.last_picked_date = last_picked_date
			
			op_type.month_packing_count = packing_count
			op_type.month_packed_count = packed_count
			op_type.month_pack_range = all_orders and (all_orders - packing_count) / all_orders * 100 or 100
			op_type.last_packed_date = last_packed_date
			
			op_type.month_shipping_count = shipping_count
			op_type.month_shipped_count = shipped_count
			op_type.month_ship_range = all_orders and (all_orders - shipping_count) / all_orders * 100 or 100
			op_type.last_shipped_date = last_shipped_date
			
			op_type.month_returning_count = returning_count
			op_type.month_returned_count = returned_count
			op_type.month_returned_range = returnes and (returnes - returning_count) / returnes * 100 or 100
			op_type.last_returned_date = last_returned_date
	
	def action_open_monthly_picking(self):
		action_name = "stock.stock_picking_action_picking_type"
		action = self.env.ref(action_name).read()[0]
		month_start_date, month_end_date = self._get_month_period()
		domain = [('code', '=', 'outgoing')]
		filter_type = self.env.context.get('filter_type')
		if filter_type:
			domain = self._get_domain(month_start_date, month_end_date, filter_type)
		action['domain'] = domain
		action['context'] = {'contact_display': 'partner_address'}
		return action

# Ahmed Salama Code End.
