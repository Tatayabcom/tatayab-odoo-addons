# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'

YEAR = 2020  # replace 2000 with your start year
YEAR_LIST = []
while YEAR != 2035:  # replace 2030 with your end year
	YEAR_LIST.append((str(YEAR), str(YEAR)))
	YEAR += 1
# Ahmed Salama Code Start ---->


class MonthFilterWizard(models.TransientModel):
	_name = 'month.filter.wizard'
	
	filter_month = fields.Selection(string="Month", required=True,
	                                selection=[('1', 'Jan'), ('2', 'Feb'), ('3', 'Mar'), ('4', 'Apr'), ('5', 'May'),
	                                           ('6', 'Jun'), ('7', 'Jul'), ('8', 'Aug'), ('9', 'Sep'), ('10', 'Oct'),
	                                           ('11', 'Nov'), ('12', 'Dec')])
	filter_year = fields.Selection(YEAR_LIST, string="Year", default=str(fields.Date.today().year), required=True)
	
	def act_view_results(self):
		action_id = self.env.ref('tatayab_monthly_dashboard.monthly_stock_picking_type_kanban_action')
		action = action_id.read()[0]
		action['name'] = 'Filter Month %s Year %s' % (self.filter_month, self.filter_year)
		action['context'] = {'filter_month': self.filter_month, 'filter_year': self.filter_year,
		                     'search_default_groupby_warehouse_id': 1}
		return action


# Ahmed Salama Code End.
