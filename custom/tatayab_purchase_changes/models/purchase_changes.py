# -*- coding: utf-8 -*-
import logging
from lxml import etree

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class PurchaseOrderInherit(models.Model):
    _inherit = 'purchase.order'

    amount_margin = fields.Monetary("Total Profit", store=True, readonly=True,
                                    compute='_amount_all')
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('update_rfq', 'RFQ Update'),
        ('to approve', 'Head Approve'),
        ('account_approve', 'Accounting Manager Approval'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ])

    total_qty = fields.Float("Total Quantity", compute="_compute_all_line_qty", required=False)
    purchase_type_id = fields.Many2one('purchase.type', "Purchase Type", readonly=False, required=True)
    reopen_request = fields.Boolean('Re-open Request')
    receipt_status = fields.Char("Receipt Status", search='_search_orders_receipt_state',
                                 compute='_compute_receipts_status_for_order')
    status_type = fields.Char("Status Type", search='_search_orders_status_type'
                              , compute='_compute_receipts_status_for_order')
    categ_id = fields.Many2one('product.category', "Product Category", domain="[('parent_id', '=', False)]")
    po_categ = fields.Selection(selection=[('Arabic','Arabic'),('French','French'),('Skin-Care','Skin Care'),('Makeup','Makeup')], string="Product Category")
    warehouse_id = fields.Many2one('stock.warehouse', "Warehouse", required=True)
    picking_type_id = fields.Many2one('stock.picking.type', 'Deliver To',
                                      required=True,
                                      related='warehouse_id.in_type_id',
                                      domain="['|', ('warehouse_id', '=', False), ('warehouse_id.company_id', '=', company_id)]",
                                      help="This will determine operation type of incoming shipment")

    partner_ref = fields.Char(tracking=True)
    backend_ref = fields.Char("Backend Ref's", help="Used to fill backend ref on it", tracking=True)
    merged_po = fields.Char()
    # amount_total = fields.Monetary(string='TTTTTTTTTTTTTTTTTTTTTotal', store=True, readonly=True, compute='_amount_all')
    received_total = fields.Monetary(compute='get_received_total_amount')

    def get_received_total_amount(self):
        for rec in self:
            rec.received_total = 0.0
            for line in rec.order_line:
                rec.received_total += line.received_subtotal_amount

    @api.onchange('warehouse_id')
    def _change_picking_type(self):
        """
        UPDATE purchase_order
        SET warehouse_id = p.warehouse_id
        FROM stock_picking_type p
        WHERE picking_type_id == p.id;
        :return:
        """
        for purchase in self:
            picking_type_id = False
            if purchase.warehouse_id:
                picking_type_id = purchase.warehouse_id.in_type_id.id
            purchase.picking_type_id = picking_type_id

    @api.onchange('partner_id', 'company_id')
    def onchange_partner_id(self):
        res = super(PurchaseOrderInherit, self).onchange_partner_id()
        if self.partner_id and self.partner_id.purchase_type_id:
            self.purchase_type_id = self.partner_id.purchase_type_id.id
        return res

    @api.depends('picking_ids', 'picking_ids.state')
    def _compute_receipts_status_for_order(self):
        for purchase in self:
            receipt_status, status_type = False, False
            if purchase.state == 'purchase':
                done_receipt = purchase.picking_ids.filtered(lambda p: p.state == 'done')
                if done_receipt:
                    receipt_status = 'Received'
                    picking_state_list = purchase.picking_ids.mapped('state')
                    result = all(elem == picking_state_list[0] for elem in picking_state_list)
                    if result:
                        status_type = 'Complete'
                    else:
                        status_type = 'Partial'
                else:
                    not_done_receipt = purchase.picking_ids.filtered(lambda p: p.state != 'done')
                    if not_done_receipt:
                        receipt_status = 'Pending'
            purchase.receipt_status = receipt_status
            purchase.status_type = status_type

    def _search_orders_status_type(self, operator, value):
        """ search for status type of orders """
        orders_ids = self.env['purchase.order'].search([('state', 'in', ['purchase', 'done'])])
        orders = []
        if value in "Complete":
            orders = orders_ids.filtered(lambda p: all(elem == orders_ids[0] for elem in orders_ids))
        elif value in "Partial":
            orders = orders_ids.filtered(lambda p: any(elem != orders_ids[0] for elem in orders_ids))
        return [('id', 'in', orders)]

    def action_bulk_accounting_manager_approve(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids')
        purchase_orders = self.browse(active_ids)

        email_notif_group = self.env.ref('tatayab_purchase_changes.group_accounting_manager_approval_notify').id
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_notif_group])]).mapped('email')

        if email_users:
            if False in email_users:
                email_users.remove(False)
            mail_content = "Dear Sir, <br/>"
            mail_flag = False
            for purchase in purchase_orders:
                if purchase.state == 'account_approve':
                    lock_po = purchase.filtered(lambda p: p.company_id.po_lock == 'lock')
                    if lock_po:
                        lock_po.write({'state': 'done'})
                    else:
                        purchase.write({'state': 'purchase', 'date_approve': fields.Date.context_today(self)})
                    purchase._create_picking()
                    # send email notify ***
                    state = purchase.state and dict(purchase._fields['state'].selection).get(purchase.state) or ''
                    mail_flag = True
                    mail_content += "- Changed State Purchase Order Number : %s To %s.<br/>" % (purchase.name, state)
                else:
                    raise ValidationError(_("Should Select Order in Accounting Approval State .. "))
            if mail_flag:
                send_notif = self.env['mail.mail'].sudo().create({
                    'subject': _('Purchase Order Stage'),
                    'author_id': self.env.user.partner_id.id,
                    'body_html': mail_content,
                    'email_to': ', '.join(email_users),
                })
                send_notif.send()

    def action_bulk_cancel(self):
        """ Cancel more than one purchase record one time """
        context = dict(self._context or {})
        active_ids = context.get('active_ids')
        purchase_orders = self.browse(active_ids). \
            filtered(lambda p: p.state in ('draft', 'to approve', 'sent', 'purchase', 'account_approve'))
        purchase_orders.button_cancel()

    def action_accounting_manager_approve(self):
        """
        Action Accounting Manager Approval + email notify
        :return:
        """
        self.write({'state': 'purchase', 'date_approve': fields.Date.context_today(self)})
        self.filtered(lambda p: p.company_id.po_lock == 'lock').write({'state': 'done'})
        self._create_picking()
        pickings = self.env['stock.picking'].search([('origin', '=', self.name)])
        for picking in pickings:
            picking.write({'is_approved': True})
        # send email notify
        email_notif_group = self.env.ref('tatayab_purchase_changes.group_accounting_manager_approval_notify')
        state = self.state and dict(self._fields['state'].selection).get(self.state) or ''
        mail_content = "Dear Sir, <br/> Changed State Purchase Order Number :" \
                       " %s To %s" % (self.name, state)
        subject = _('Purchase Order Stage')
        self.purchase_email_notify(email_notif_group, subject, mail_content)
        return {}

    def button_reopen_request(self):
        """ Reopen request sent notification. """
        self.reopen_request = True
        # send email notify
        subject = _('Purchase Order Re-open Request')
        mail_content = "Dear Sir, <br/> Please Confirm To Re-open Purchase Order Number : %s" % self.name
        email_notif_group = self.env.ref('tatayab_purchase_changes.group_reopen_notify')
        self.purchase_email_notify(email_notif_group, subject, mail_content)
        return {}

    def button_reopen_head_approve(self):
        self.button_draft()
        self.reopen_request = False
        self.state = 'update_rfq'

    def button_reopen_approve(self):
        """ Reopen approve sent notification """
        self.button_draft()
        self.reopen_request = False
        self.state = 'update_rfq'
        # send email notify
        subject = _('Purchase Order Re-open Approved')
        state = self.state and dict(self._fields['state'].selection).get(self.state) or ''
        mail_content = "Dear Sir, <br/> Accounting manager Approved For Change State Purchase Order Number :" \
                       " %s To %s" % (self.name, state)
        email_notif_group = self.env.ref('tatayab_purchase_changes.group_reopen_notify')
        self.purchase_email_notify(email_notif_group, subject, mail_content)
        return {}

    def _compute_all_line_qty(self):
        """ Get total qty for all order lines """
        for rec in self:
            rec.total_qty = 0.0
            if rec.order_line:
                for lin in rec.order_line:
                    rec.total_qty += lin.product_qty

    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent', 'update_rfq']:
                continue
            order._add_supplier_to_product()
            order.write({'state': 'to approve'})
        return True

    def button_first_confirm(self):
        for order in self:
            order.write({'state': 'update_rfq'})
        return True

    def button_approve(self, force=False):
        self.write({'state': 'account_approve'})
        # send email notify
        subject = _('Purchase Order Head Approve')
        state = self.state and dict(self._fields['state'].selection).get(self.state) or ''
        mail_content = "Dear Sir, <br/> Head Approve Change State For Purchase Order Number :" \
                       " %s To %s" % (self.name, state)
        email_notif_group = self.env.ref('tatayab_purchase_changes.group_head_approve_notify')
        self.purchase_email_notify(email_notif_group, subject, mail_content)

    @api.depends('order_line.price_total', 'order_line.line_margin')
    def _amount_all(self):
        """ Update amount margin in all order lines"""
        super(PurchaseOrderInherit, self)._amount_all()
        for order in self:
            order.update({'amount_margin': sum(l.line_margin for l in order.order_line)})

    def update_lines_data(self):
        """ Update changes on product to all related fields in order lines"""
        for picking in self:
            for line in picking.order_line:
                line._product_id_change()
    def delete_po_lines(self):
        for rec  in self:
            for line in rec.order_line:
                line.unlink()
    def purchase_email_notify(self, email_group, subject, mail_content):
        # send email notify
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', email_group.ids)]).mapped('email')

        if email_users:
            if False in email_users:
                email_users.remove(False)
            send_notif = self.env['mail.mail'].sudo().create({
                'subject': subject,
                'author_id': self.env.user.partner_id.id,
                'body_html': mail_content,
                'email_to': ', '.join(email_users),
            })
            send_notif.send()

    @api.model
    def _prepare_picking(self):
        res = super(PurchaseOrderInherit, self)._prepare_picking()
        if self.backend_ref:
            res['backend_ref'] = self.backend_ref
        return res


class PurchaseOrderLineInherit(models.Model):
    _inherit = 'purchase.order.line'

    code = fields.Char("Barcode")
    brand_id = fields.Many2one('product.brand', "Brand")
    retail_price = fields.Float(string='Retail Price', digits='Product Price')
    profit_margin = fields.Float(string='Profit Margin', digits='Product Price')
    line_margin = fields.Monetary(compute='_compute_amount', string='Profit', store=True)
    margin_percentage = fields.Float(string='Margin(%)', related='product_id.margin_percent')
    qty_available = fields.Float(related='product_id.qty_available')
    virtual_available = fields.Float(related='product_id.virtual_available')
    vendor_barcode = fields.Char(string='Vendor Barcode', related='product_id.vendor_barcode')
    original_price = fields.Float(
        compute='_compute_original_price', string='Price Unit (Original)', digits='Product Price', store=True)
    free_qty = fields.Float(related='product_id.free_qty', string="Free/Actual")
    received_subtotal_amount = fields.Monetary(compute='get_received_subtotal')

    def get_received_subtotal(self):
        for line in self:
            line.received_subtotal_amount = 0.0
            if line.price_subtotal:
                line.received_subtotal_amount = line.price_unit * line.qty_received

    @api.depends('price_unit')
    def _compute_original_price(self):
        for line in self:
            if line.order_id.currency_id != line.order_id.company_id.currency_id:
                price_unit = line.order_id.currency_id._convert(
                    line.price_unit,
                    line.order_id.currency_id,
                    line.order_id.company_id,
                    line.order_id.date_order or fields.Date.today())
            else:
                price_unit = line.price_unit
            line.original_price = price_unit

    def _prepare_compute_all_values(self):
        self.ensure_one()
        vals = super(PurchaseOrderLineInherit, self)._prepare_compute_all_values()
        self.profit_margin = self.product_id.lst_price - self.price_unit
        return vals

    @api.depends('product_qty', 'price_unit', 'taxes_id', 'profit_margin')
    def _compute_amount(self):
        super(PurchaseOrderLineInherit, self)._compute_amount()
        for line in self:
            # vals = line._prepare_compute_all_values()
            line.update({'line_margin': line.product_qty * line.profit_margin})

    def _product_id_change(self):
        super(PurchaseOrderLineInherit, self)._product_id_change()
        self.write({
            'brand_id': self.product_id.brand_id and self.product_id.brand_id.id or False,
            'code': self.product_id.barcode,
            'retail_price': self.product_id.lst_price,
            'price_unit': self.product_id.standard_price,
            'profit_margin': self.product_id.lst_price - self.price_unit
        })

    @api.model
    def create(self, vals):
        if vals.get('product_id'):
            product_id = self.env['product.product'].browse(vals.get('product_id'))
            if product_id:
                vals['code'] = product_id.barcode
                vals['retail_price'] = product_id.lst_price
                vals['profit_margin'] = product_id.lst_price - vals.get('price_unit') or 0.0
        return super(PurchaseOrderLineInherit, self).create(vals)

    def write(self, vals):
        if vals.get('product_id') or vals.get('price_unit'):
            product_id = vals.get('product_id') and \
                         self.env['product.product'].browse(vals.get('product_id')) or self.product_id
            price_unit = vals.get('price_unit') or self.price_unit
            if product_id:
                vals['code'] = product_id.barcode
                vals['retail_price'] = product_id.lst_price
                vals['profit_margin'] = product_id.lst_price - price_unit
        return super(PurchaseOrderLineInherit, self).write(vals)

    @api.onchange('brand_id')
    def onchange_brand_id(self):
        """ Get domain on product when change brand id """
        for line in self:
            if line.brand_id:
                if line.brand_id != line.product_id.brand_id:
                    line.product_id = False
                domain = [('brand_id', '=', line.brand_id.id),
                          ('id', 'not in', line.order_id.order_line.mapped('product_id.id'))]
                return {'domain': {'product_id': domain}}

# Ahmed Salama Code End.
