# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


# Ahmed Salama Code Start ---->


class ResPartnerInherit(models.Model):
    _inherit = 'res.partner'

    buyer = fields.Char("Buyer")
    head = fields.Char("Head")
    brand_ids = fields.Many2many(comodel_name='product.brand', string="Brands")
    purchase_type_id = fields.Many2one(comodel_name="purchase.type", string="Purchase Type", required=False, )

# Ahmed Salama Code End.
