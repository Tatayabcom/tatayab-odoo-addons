from odoo import api, fields, models


class PurchaseReport(models.Model):
    _inherit = "purchase.report"

    product_parent_category_id = fields.Many2one('product.parent.category', string="Odoo Category")
    product_sub_category_id = fields.Many2one('product.sub.category', string=" Sub Category")
    product_vendor_id = fields.Many2one('res.partner', string="Product Vendor")


    def _select(self):
        return super(PurchaseReport, self)._select() + ", t.product_parent_category_id as product_parent_category_id, t.product_sub_category_id as product_sub_category_id, t.product_vendor_id as product_vendor_id"


    def _group_by(self):
        return super(PurchaseReport, self)._group_by() + ", t.product_parent_category_id, t.product_sub_category_id, t.product_vendor_id "

