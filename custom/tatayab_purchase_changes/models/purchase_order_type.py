from odoo import fields, models, api, _


class PurchaseType(models.Model):
    _name = 'purchase.type'
    _rec_name = 'name'

    name = fields.Char("Name", required=1)
