# -*- coding: utf-8 -*-
from odoo import models, fields, _


# Ahmed Salama Code Start ---->


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    # used for move_demand_qty_to_done() to make button invisible With "True" Value.
    is_updated = fields.Boolean(string="Is Updated", copy=False)
    # but add here to insure because of using on _prepare_picking method
    
    def move_demand_qty_to_done(self):
        for stock in self:
            stock.is_updated = True
            for move_line in stock.move_ids_without_package:
                if move_line.product_uom_qty > 0.0:
                    move_line.update({'quantity_done': move_line.product_uom_qty})


class StockWarehouseInherit(models.Model):
    _inherit = 'stock.warehouse'

    ignore_purchase_using = fields.Boolean("Ignore on PO?",
                                           help="If checked this warehouse want appear on PO to be selected")


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    def process(self):
        action = super(StockImmediateTransfer, self).process()

        # Send email notification to specific user.
        email_notif_group = self.env.ref('tatayab_purchase_changes.group_receipt_order_state_notify').id
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_notif_group])]).mapped('email')

        if email_users:
            if False in email_users:
                email_users.remove(False)
            mail_content = "Dear Sir, <br/>"
            mail_flag = False
            for pick in self.pick_ids:
                if pick.purchase_id and pick.picking_type_id.code == 'incoming':
                    state = pick.state and dict(pick._fields['state'].selection).get(pick.state) or ''
                    mail_flag = True
                    mail_content += "- Changed State Receipt  Number : %s" \
                                    " To %s, For Purchase Order Number : %s.<br/>" \
                                    % (pick.name or '', state or '', pick.purchase_id.name or '')
            if mail_flag:
                send_notif = self.env['mail.mail'].sudo().create({
                    'subject': _('Receipt Order - Fully Received'),
                    'author_id': self.env.user.partner_id.id,
                    'body_html': mail_content,
                    'email_to': ', '.join(email_users),
                })
                send_notif.send()
        return action


class StockBackorderConfirmation(models.TransientModel):
    _inherit = 'stock.backorder.confirmation'

    def process(self):
        action = super(StockBackorderConfirmation, self).process()

        # Send email notification to specific user.
        email_notif_group = self.env.ref('tatayab_purchase_changes.group_receipt_order_state_notify').id
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_notif_group])]).mapped('email')

        if email_users:
            if False in email_users:
                email_users.remove(False)
            mail_content = "Dear Sir, <br/>"
            mail_flag = False
            for pick in self.pick_ids:
                if pick.purchase_id and pick.picking_type_id.code == 'incoming':
                    state = pick.state and dict(pick._fields['state'].selection).get(pick.state) or ''
                    mail_flag = True
                    mail_content += "- Changed State Receipt Number : %s To %s and Create Backorder," \
                                    " For Purchase Order Number : %s.<br/>" % \
                                    (pick.name, state, pick.purchase_id.name)
            if mail_flag:
                send_notif = self.env['mail.mail'].sudo().create({
                    'subject': _('Receipt Order - Partially Received'),
                    'author_id': self.env.user.partner_id.id,
                    'body_html': mail_content,
                    'email_to': ', '.join(email_users),
                })
                send_notif.send()
        return action
