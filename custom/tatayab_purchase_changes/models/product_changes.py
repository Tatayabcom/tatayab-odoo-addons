# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    margin_percent = fields.Float(string='Margin Percent', compute='get_margin_percentage')
    # brand_id = fields.Many2one(comodel_name='product.brand', string="Brand", tracking=True)
    property_stock_bin_id = fields.Many2one(comodel_name='stock.location', string="Bin Location")
    default_code = fields.Char(string="SKU", tracking=True)
    active = fields.Boolean(tracking=True)
    barcode = fields.Char(tracking=True)
    name = fields.Char(tracking=True)
    vendor_barcode = fields.Char(string='Vendor Barcode')
    # TODO: give ability to edit SKU only of have group or field is empty otherwise prevent that
    can_edit_sku = fields.Boolean(string="Can Edit SKU?", compute='check_sku_editable')

    @api.depends('list_price', 'standard_price')
    def get_margin_percentage(self):
        for record in self:
            record.margin_percent = 0.0
            if record.list_price > 0:
                record.margin_percent = ((record.list_price - record.standard_price) / record.list_price) * 100

    @api.depends('default_code', 'name')
    def check_sku_editable(self):
        for product in self:
            can_edit_sku = False
            if self.env.user.has_group('tatayab_purchase_changes.group_product_sku_access') \
                    or not isinstance(product.id, int):
                can_edit_sku = True
            product.can_edit_sku = can_edit_sku

    def write(self, vals):
        for product in self:
            # Prevent user to edit product sku..
            if product.default_code and vals.get('default_code') \
                    and not self.env.user.has_group('tatayab_purchase_changes.group_product_sku_access'):
                raise ValidationError(_("You have no access to edit product SKU while it have value"))
            # Prevent user to archive or un archive product..
            if 'active' in vals:
                if vals.get('active') and not product.active and \
                        not self.env.user.has_group('tatayab_purchase_changes.group_product_archive_access'):
                    raise ValidationError(_("You have no access to Enable product!!!"))
                elif not vals.get('active') and product.active and \
                        not self.env.user.has_group('tatayab_purchase_changes.group_product_archive_access'):
                    raise ValidationError(_("You have no access to Disable product!!!"))
        return super(ProductTemplateInherit, self).write(vals)

    @api.constrains('default_code')
    def _sku_unique(self):
        """ Constrain in SKU it should be unique """
        for product in self:
            if product.default_code:
                other_products = self.env['product.template'].search(
                    [('default_code', '=', product.default_code.lstrip().rstrip()),
                     ('id', '!=', product.id)])
                if other_products:
                    raise ValidationError(_("Product SKU Must Be Unique!!!, this sku is used on products: \n%s"
                                            % '\n'.join(other_products.mapped('name'))))


class ProductProductInherit(models.Model):
    _inherit = 'product.product'

    default_code = fields.Char(string="SKU", tracking=True)
    barcode = fields.Char(tracking=True)
    active = fields.Boolean(tracking=True)
    # TODO: give ability to edit SKU only of have group or field is empty otherwise prevent that
    can_edit_sku = fields.Boolean(string="Can Edit SKU?", compute='check_sku_editable')

    @api.depends('default_code', 'name')
    def check_sku_editable(self):
        for product in self:
            can_edit_sku = False
            if self.env.user.has_group('tatayab_purchase_changes.group_product_sku_access') or not isinstance(
                    product.id, int):
                can_edit_sku = True
            self.can_edit_sku = can_edit_sku

    def write(self, vals):
        for product in self:
            if product.default_code and vals.get('default_code') \
                    and not self.env.user.has_group('tatayab_purchase_changes.group_product_sku_access'):
                raise ValidationError(_("You have no access to edit product SKU while it have value"))
            if 'active' in vals:
                if vals.get('active') and not product.active and \
                        not self.env.user.has_group('tatayab_purchase_changes.group_product_archive_access'):
                    raise ValidationError(_("You have no access to Enable product!!!"))
                elif not vals.get('active') and product.active and \
                        not self.env.user.has_group('tatayab_purchase_changes.group_product_archive_access'):
                    raise ValidationError(_("You have no access to Disable product!!!"))
        return super(ProductProductInherit, self).write(vals)


# class ProductBrand(models.Model):
#     _name = 'product.brand'
#     _description = "Product Brands"
#
#     name = fields.Char("Brand", Translate=True, required=True)
#     magento_attribute_option_id = fields.Char(string='Magento ID')
#     notes = fields.Text("Description", Translate=True)
#
#     @api.constrains('name')
#     def _name_unique(self):
#         """ Constrain in brand it should be unique """
#         for brand in self:
#             if self.env['product.brand'].search([('name', '=', brand.name),
#                                                  ('id', '!=', brand.id)]):
#                 raise ValidationError(_("Brand Name Must Be Unique!!!"))


# class MagentoAttributeOption(models.Model):
#     _inherit = "magento.attribute.option"
#
#     def create_attribute_option(self, attribute, m_attribute, o_attribute):
#         for option in attribute.get('options', []):
#             option.update({'label': option.get('label').strip()})
#             if option.get('label'):
#                 o_option = self._get_odoo_attribute_value(o_attribute.id, option.get('label'))
#                 m_option = self.search([('instance_id', '=', m_attribute.instance_id.id),
#                                         ('magento_attribute_id', '=', m_attribute.id),
#                                         ('odoo_option_id', '=', o_option.id),
#                                         ('odoo_attribute_id', '=', o_attribute.id)])
#                 if not m_option:
#                     values = self._prepare_option_value(option, o_option, o_attribute, m_attribute)
#                     option_obj = self.create(values)
#                     print("option_obj.magento_attribute_id.magento_attribute_code:::",option_obj.magento_attribute_id.magento_attribute_code)
#                     if option_obj.magento_attribute_id.magento_attribute_code == 'manufacturer':
#                         product_brand_obj = self.env['product.brand'].search([('magento_attribute_option_id','=',option_obj.magento_attribute_option_id)])
#                         if not product_brand_obj:
#                             product_brand = self.env['product.brand'].create({
#                                 'name': option_obj.name,
#                                 'magento_attribute_option_id':option_obj.magento_attribute_option_id,
#                             })
#                             logging.info(blue + "Automatic create brand:%s" % product_brand.name + reset)
#         return True


class ProductCategoryInherit(models.Model):
    _inherit = 'product.category'

    # @api.constrains('name')
    # def _category_name_unique(self):
    #     """ check uniqueness of category name """
    #     for category in self:
    #         if category.name:
    #             other_categories = self.env['product.category'].search(
    #                 [('name', '=', category.name.lstrip().rstrip()),
    #                  ('id', '!=', category.id)])
    #             if other_categories:
    #                 raise ValidationError(_("Category name must be unique!!!, this name is used on categories: \n%s"
    #                                         % '\n'.join(other_categories.mapped('name'))))

# Ahmed Salama Code End.
