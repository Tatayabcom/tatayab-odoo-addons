# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Purchase Modifications",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'Purchase',
    'summary': """Tatayab Purchase Modifications""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """""",
    'version': '15.0',
    'depends': ['base', 'web', 'purchase', 'sale', 'stock',
                'tatayab_magento_connect_changes'],
    'data': [
        'security/purchase_security.xml',
        'security/ir.model.access.csv',
        
        'views/product_brand_view.xml',
        'views/product_view_changes.xml',
        'views/purchase_order_type_view.xml',
        'views/res_partner_view_changes.xml',
        'views/purchase_view_changes.xml',
        'views/stock_picking_view_changes.xml',
        'views/purchase_report_inherit_view.xml',

        'reports/purchase_reports_changes.xml',
        'reports/purchase_order_with_retail_report.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
