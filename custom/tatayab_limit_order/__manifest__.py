# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Limit order",
    'author': 'Tatayab, Doaa Negm',
    'category': 'ALL',
    'summary': """Tatayab Limit Order""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '13.0',
    'depends': ['sale_stock', 'account','stock','tatayab_operation_status'],
    'data': [

        'views/sale_order_view_changes.xml',
        'views/stock_picking_view_changes.xml',
        'views/res_country_view_changes.xml',

    ],
'assets': {
    'web.assets_backend': [
        'tatayab_limit_order/static/src/scss/ribbon.scss',
        'tatayab_limit_order/static/src/js/widgets/ribbon.js'
    ],
},
'qweb': [
        'static/src/xml/*.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
