# -*- coding: utf-8 -*-
import logging

import uuid

from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_exceeded_limit = fields.Boolean(string="Exceeded Limit", compute="get_order_is_exceeded_limit", store=True)

    @api.depends('warehouse_id', 'amount_total', 'warehouse_id.exceeded_limit_order',  'country_id.exceeded_limit_order', 'warehouse_id.country_id','country_id')
    def get_order_is_exceeded_limit(self):
        for rec in self:
            rec.is_exceeded_limit = False
            if rec.warehouse_id.country_id != rec.country_id:
                if rec.country_id.exceeded_limit_order:
                    if rec.amount_total > rec.country_id.exceeded_limit_order:
                        rec.is_exceeded_limit = True

