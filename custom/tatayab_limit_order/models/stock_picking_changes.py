# -*- coding: utf-8 -*-

from odoo import models, fields, api


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    is_exceeded_limit = fields.Boolean(string="Exceeded Limit", compute="get_order_is_exceeded_limit", store=True)

    @api.depends('sale_id.is_exceeded_limit', 'sale_id', 'picking_warehouse_id',
                 'picking_warehouse_id.exceeded_limit_order', 'country_id.exceeded_limit_order',
                 'picking_warehouse_id.country_id')
    def get_order_is_exceeded_limit(self):
        for rec in self:
            rec.is_exceeded_limit = False
            if rec.sale_id.is_exceeded_limit:
                rec.is_exceeded_limit = True


class StockWarehouseInherit(models.Model):
    _inherit = 'stock.warehouse'

    exceeded_limit_order = fields.Float(string="Exceeded Limit")
    country_id = fields.Many2one( 'res.country', string='Country' )
