# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, _, api


class ResCountryInherit(models.Model):
    _inherit = 'res.country'

    exceeded_limit_order = fields.Float(string="Exceeded Limit")
