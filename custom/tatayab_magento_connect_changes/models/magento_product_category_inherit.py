from odoo import api, fields, models


class ProductCategoryTag(models.Model):
    _name = "product.category.tag"

    name = fields.Char(string="Name", required=1)

class ProductCategory(models.Model):
    _inherit = "product.category"

    magento_category_id = fields.Char(string="Magento ID", help="Magento ID")
    product_category_tag_ids = fields.Many2many('product.category.tag', string="Tags")


class MagentoProductCategory(models.Model):
    """
        Describes Magento Product Category
    """
    _inherit = "magento.product.category"


    def create_magento_category(self, instance, category):
        """
        Create the category list in odoo
        :param instance: magento.instance object
        :param category: dict categories, If we get multiple categories in the response then
        response will be in list
        :return:
        """
        parent_category = self._get_parent_category(instance, category)
        m_category = self.search([('category_id', '=', category.get('id', 0)),
                                  ('instance_id', '=', instance.id)], limit=1)
        odoo_parent_category = False
        if not m_category:
            if parent_category:
                category.update({'parent_id': parent_category.id})
                odoo_parent_category_obj = self.env['product.category'].search([('magento_category_id', '=', parent_category.category_id)])
                if odoo_parent_category_obj:
                    odoo_parent_category = odoo_parent_category_obj.id
            values = self.prepare_category_values(instance, category)
            self.create(values)
            category_odoo = {
                'magento_category_id': values.get('category_id', 0),
                'name': values.get('name', ''),
                'parent_id': odoo_parent_category,
            }
            self.env['product.category'].create(category_odoo)
        else:
            if parent_category:
                category.update({'parent_id': parent_category.id})
            values = self.prepare_category_values(instance, category)
            m_category.write(values)
        if category.get('children_data', []):
            for categ in category.get('children_data'):
                self.create_magento_category(instance, categ)
        return True

