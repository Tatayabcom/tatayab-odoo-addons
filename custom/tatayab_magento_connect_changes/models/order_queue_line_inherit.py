# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
"""
Describes methods to store Order Data queue line
"""
import json
import logging

import pytz
from dateutil import parser

from odoo import models, fields, _

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"

utc = pytz.utc


class MagentoOrderDataQueueLineEpt(models.Model):
    """
    Describes Order Data Queue Line
    """
    _inherit = "magento.order.data.queue.line.ept"

    # ======== create server action to get missing data in sale order line  in odoo
    def get_missing_data_in_so_line(self):
        for rec in self:
            sale_order_object = self.env['sale.order'].search([('magento_order_reference','=',rec.magento_id)])
            if sale_order_object:
                order_date = json.loads(rec.data)
                for line in sale_order_object.order_line:
                    # to set the  discount
                    if line.product_id.default_code == 'MAGENTO DISCOUNT':
                        line.base_row_total_incl_tax = (abs(order_date.get('base_discount_amount'))*-1)
                    elif line.product_id.default_code == 'MAGENTO_SHIP':
                        line.base_row_total_incl_tax = order_date.get('base_shipping_amount')
                    elif line.product_id.default_code == 'gift_fees' or line.is_line_gift:
                        line.base_row_total_incl_tax = order_date.get('extension_attributes', {}).get('base_gift_wrap_fee',0.0)
                    elif line.product_id.default_code == 'pay_fees':
                        line.base_row_total_incl_tax = order_date.get('extension_attributes', {}).get('base_cod_fee', 0.0)
                    else:
                        for item in order_date.get('items'):
                            if line.product_id.default_code == item.get('sku'):
                                line.base_row_total_incl_tax = item.get('base_row_total_incl_tax') + abs(item.get('base_discount_amount'))
                                line.base_row_subtotal_incl_tax = item.get('base_row_total_incl_tax')
    #===============================================================================

    def process_order_queue_line(self, line, log):
        item = json.loads(line.data)
        order_ref = item.get('increment_id')
        original_order_ref = item.get('original_increment_id')
        order = self.env['sale.order']
        instance = self.instance_id
        is_exists = order.search([('magento_instance_id', '=', instance.id),
                                  ('magento_order_reference', '=', order_ref),])
        # is_exists = order.search([('magento_instance_id', '=', instance.id),
        #                           '|',
        #                           ('magento_order_reference', '=', order_ref),
        #                           ('magento_order_reference', '=', original_order_ref)])
        if is_exists:
            # ==========================To handel COD ===========================================
            is_invoice = item.get('extension_attributes').get('is_invoice')
            is_shipment = item.get('extension_attributes').get('is_shipment')
            method = item.get('payment', dict()).get('method')
            if method in ['cashondelivery','free']:
                if is_invoice and item.get('state') == 'processing':
                    gateway = self.instance_id.payment_method_ids.filtered(
                        lambda x: x.payment_method_code == method)
                    magento_financial_object = self.env['magento.financial.status.ept'].search([('payment_method_id','=',gateway.id),
                                                                     ('financial_status','=','processing_paid'),
                                                                     ('magento_instance_id','=',self.instance_id.id),
                                                                     ])
                    if magento_financial_object:
                        is_exists.auto_workflow_process_id = magento_financial_object.auto_workflow_id.id
                        is_exists.is_approved = True
                        is_exists.validate_and_paid_invoices_ept(is_exists.auto_workflow_process_id)
                        is_exists.confirm_po_after_approve()
                        for pick in is_exists.picking_ids:
                            pick.is_approved = True
                        for inv in is_exists.invoice_ids:
                            inv.get_invoice_id_from_magento()
                        logging.info(bold_red + "magento financial : %s" % is_exists.auto_workflow_process_id.name + reset)
                elif item.get('state') == 'canceled':
                    is_exists._action_cancel()
                    is_exists.message_post(body="<b style='color:red'>Cancel order from Magento </b><br/>" )
            else:
                if item.get('state') == 'processing':
                    is_exists.is_approved = True
                    for pick in is_exists.picking_ids:
                        pick.is_approved = True
            logging.info(green + "is_exists: %s" % is_exists + reset)
            logging.info(green + "order_ref: %s" % order_ref + reset)
            # =====================================================================
            # ======================to add log in order============================
            self.env['sale.order.log'].create({
                'magento_id': item.get('increment_id'),
                'data': item,
                'date': fields.Datetime.now(),
                'sale_id': is_exists.id
            })
            # =====================================================================

            return True
        create_at = item.get("created_at", False)
        # Need to compare the datetime object
        date_order = parser.parse(create_at).astimezone(utc).strftime("%Y-%m-%d %H:%M:%S")
        if str(instance.import_order_after_date) > date_order:
            message = _(f"""
            There is a configuration mismatch in the import of order #{order_ref}.\n
            The order receive date is {date_order}.\n
            Please check the date set in the configuration in Magento2 Connector -> Configuration 
            -> Setting -> Select Instance -> 'Import Order After Date'.
            """)
            log.write({'log_lines': [(0, 0, {
                'message': message, 'order_ref': line.magento_id,
                'magento_order_data_queue_line_id': line.id
            })]})
            return False
        is_processed = self.financial_status_config(item, instance, log, line)
        if is_processed:
            carrier = self.env['delivery.carrier']
            is_processed = carrier.find_delivery_carrier(item, instance, log, line)
            if is_processed:
                # add create product method
                item_ids = self.__prepare_product_dict(item.get('items'))
                m_product = self.env['magento.product.product']
                p_items = m_product.with_context(is_order=True).get_products(instance, item_ids, line)

                order_item = self.env['sale.order.line'].find_order_item(item, instance, log, line.id)
                if not order_item:
                    if p_items:
                        p_queue = self.env['sync.import.magento.product.queue.line']
                        self._update_product_type(p_items, item)
                        for p_item in p_items:
                            is_processed = p_queue.with_context(is_order=True).import_products(p_item, line)
                            if not is_processed:
                                break
                    else:
                        is_processed = False
                if is_processed:
                    is_processed = order.create_sale_order_ept(item, instance, log, line.id)
                    if is_processed:
                        line.write({'sale_order_id': item.get('sale_order_id').id})
                        self.env['sale.order.log'].create({
                            'magento_id': item.get('increment_id'),
                            'data': item,
                            'date': fields.Datetime.now(),
                            'sale_id':item.get('sale_order_id').id
                        })
        return is_processed

    # def create_order_queue_line(self, instance, order, queue):
    #     # res = super(MagentoOrderDataQueueLineEpt, self).create_order_queue_line(instance, order, queue)
    #     self.env['sale.order.log'].create({
    #         'magento_id': order.get('increment_id'),
    #         'data': json.dumps(order),
    #         'name': queue.name,
    #         'date': fields.Datetime.now(),
    #     })
    #     self.create({
    #         'magento_id': order.get('increment_id'),
    #         'instance_id': instance.id,
    #         'data': json.dumps(order),
    #         'queue_id': queue.id
    #     })
    #     return True
