from odoo import models, fields, api


class SaleWorkflowProcess(models.Model):
    _inherit = "sale.workflow.process.ept"
    _description = "sale workflow process"

    draft_payment = fields.Boolean(string=" Draft Register Payment")

    @api.model
    def auto_workflow_process_ept(self, auto_workflow_process_id=False, order_ids=[]):
        """
        This method will find draft sale orders which are not having invoices yet, confirmed it and done the payment
        according to the auto invoice workflow configured in sale order.
        :param auto_workflow_process_id: auto workflow process id
        :param order_ids: ids of sale orders
        Migration done by Haresh Mori on September 2021
        """
        sale_order_obj = self.env['sale.order']
        workflow_process_obj = self.env['sale.workflow.process.ept']
        if not auto_workflow_process_id:
            work_flow_process_records = workflow_process_obj.search([])
        else:
            work_flow_process_records = workflow_process_obj.browse(auto_workflow_process_id)

        if not order_ids:
            orders = sale_order_obj.search([('auto_workflow_process_id', 'in', work_flow_process_records.ids),
                                            ('state', 'not in', ('done', 'cancel')),
                                            ('invoice_status', '!=', 'invoiced')])
            # orders = sale_order_obj.search([('auto_workflow_process_id', 'in', work_flow_process_records.ids),
            #                                 ('state', 'not in', ('done', 'cancel', 'sale')),
            #                                 ('invoice_status', '!=', 'invoiced')])
        else:
            orders = sale_order_obj.search([('auto_workflow_process_id', 'in', work_flow_process_records.ids),
                                            ('id', 'in', order_ids)])
        orders.process_orders_and_invoices_ept()

        return True

    def shipped_order_workflow_ept(self, orders):
        """
        This method is for processing the shipped orders.
        :param orders: list of order objects
        Migration done by Haresh Mori on September 2021
        """
        self.ensure_one()
        stock_location_obj = self.env["stock.location"]
        product_product_obj = self.env["product.product"]

        mrp_module = product_product_obj.search_installed_module_ept('mrp')
        customer_location = stock_location_obj.search([("usage", "=", "customer")], limit=1)

        shipped_orders = orders.filtered(lambda x: x.order_line)

        for order in shipped_orders:
            order.state = 'sale'
            order.auto_shipped_order_ept(customer_location, mrp_module)

        shipped_orders.validate_and_paid_invoices_ept(self)
        return True
