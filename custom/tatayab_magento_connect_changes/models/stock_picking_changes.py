# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    # TODO: those fields are static for now as it was receiving from CS-cart and we need to handle them with magento next
    is_approved = fields.Boolean("Approved?", default=False, tracking=True,
                                 help="This field used to mark orders that is approved for warehouse to be used")
    customer_note = fields.Html(string="Customer Note", required=False, )
    stuff_note = fields.Html(string="Stuff Note", required=False, )
    is_gift = fields.Boolean("Is Gift?", help="Orders that is marked as gifts.")
    gift_message = fields.Html(string="Gift Message", required=False, help="Gift message from user")
    gift_sender_name = fields.Char(string="Gift Sender")
    gift_receiver_name = fields.Char(string="Gift Receiver")

    active = fields.Boolean('Active', default=True)
    backend_ref = fields.Char("Magento Ref.")
    payment_collected = fields.Boolean('Payment Collected', default=True,
                                       help="This field is used to show pickings which is collected")

    def toggle_approve(self):
        """
		Approve Waiting Call center approval orders
		"""
        for picking in self:
            if self.env.user.has_group('tatayab_magento_connect_changes.group_approve_orders_manual'):
                if picking.is_approved:
                    edit_vals = {'is_approved': False}
                else:
                    edit_vals = {'is_approved': True}
                picking.sale_id.write(edit_vals)
                picking.sale_id.picking_ids.write(edit_vals)
            else:
                raise UserError(_("You are not authorized to do this action, Please contact system admin"))

    # ================= send state to magento ================
    # def get_export_ship_values(self, raise_error):
    #     """
    #     export shipment values create with item details.
    #     param: wizard - open this values export shipment (t/f).
    #     return : export shipment dict.
    #     """
    #     values = super(StockPicking, self).get_export_ship_values(raise_error)
    #     values.update({"operation_state": self.operation_state})
    #     return values

# Ahmed Salama Code End.
