# -*- coding: utf-8 -*-
from odoo import models, fields

igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'


# Ahmed Salama Code Start ---->


class ProductParentCategory(models.Model):
    _name = 'product.parent.category'
    name = fields.Char(string="Name", required=True)


class ProductSubCategory(models.Model):
    _name = 'product.sub.category'
    name = fields.Char(string="Name", required=True)


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    ref = fields.Char("Product Reference")
    description = fields.Html("Internal Notes")
    t_service_type = fields.Selection([('tax', 'Tax'),
                                       ('univ_disc', 'Universal Discount'),
                                       ('add_fees', 'Additional Fees'),
                                       ('shipping_fees', 'Shipping Fees'),
                                       ('pay_fees', 'Payment Fees')],
                                      "Service Type")
    gift_magento_id = fields.Char(string="Gift ID")
    oddo_parent_category = fields.Many2one('magento.attribute.option', string="Magento Parent Category", domain=[
        ('magento_attribute_id.magento_attribute_code', '=', 'oddo_parent_category')])
    oddo_sub_category = fields.Many2one('magento.attribute.option', string="Magento Sub Category", domain=[
        ('magento_attribute_id.magento_attribute_code', '=', 'oddo_sub_category')])
    product_parent_category_id = fields.Many2one('product.parent.category', string="Odoo Category")
    product_sub_category_id = fields.Many2one('product.sub.category', string=" Sub Category")
    product_vendor_id = fields.Many2one('res.partner',string="Product Vendor")

# Ahmed Salama Code End.
