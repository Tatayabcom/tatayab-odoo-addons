from odoo import models, fields, api, _


class MagentoResPartnerEpt(models.Model):
    _inherit = "magento.res.partner.ept"

    def _prepare_partner_values(self, data, instance, **kwargs):
        values = super(MagentoResPartnerEpt, self)._prepare_partner_values(data, instance)
        if data.get('building_no') and data.get('block_no') and data.get('additional_direction'):
            values['street2'] = 'Building No. ' + data.get('building_no', '') + ', Block No. ' + data.get('block_no', '')\
                                + ', Additional Direction ' + data.get(
                'additional_direction', '')
        else:
            values['street2'] = data.get('street2')
        values['building_no'] = data.get('building_no', '') if data.get('building_no') else False
        values['block_no'] = data.get('block_no', '') if data.get('block_no') else False
        values['additional_direction'] = data.get('additional_direction', '') if data.get(
            'additional_direction') else False
        values['mobile']  = data.get('telephone') if data.get(
            'telephone') else False
        return values
