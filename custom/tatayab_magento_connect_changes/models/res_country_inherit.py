from odoo import api, fields, models

class ResCountryInherit(models.Model):
    _inherit = 'res.country'

    sale_taxes_id = fields.Many2many('account.tax', 'saleorder_taxes_rel',
                                     help="Default taxes used when selling the product.",
                                     string='Sales Taxes',
                                     domain=[('type_tax_use', '=', 'sale')],
                                     default=lambda self: self.env.company.account_sale_tax_id)
