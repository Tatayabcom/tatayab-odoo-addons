from odoo import api, fields, models


class SaleOrderLog(models.Model):
    _name = "sale.order.log"

    name = fields.Char(string="Queue")
    data = fields.Text(string="Date From Magento")
    date = fields.Datetime(string="Imported At")
    user_id = fields.Many2one('res.users', string="Crested By",default=lambda self: self.env.user)
    sale_id = fields.Many2one('sale.order',string='Order')
    magento_id = fields.Char(string="Magento Order#", help="Id of imported order.", copy=False)

