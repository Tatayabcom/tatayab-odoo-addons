# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
"""
Describes fields and methods for Magento products
"""
import logging

from odoo import fields, models, _

_logger = logging.getLogger('MagentoEPT')
from odoo.exceptions import UserError
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class MagentoProductProduct(models.Model):
    """
    Describes fields and methods for Magento products
    """
    _inherit = 'magento.product.product'

    def search_product_in_layer(self, line, item):
        self.update_custom_option(item)
        m_product = self.search(
            ['|', ('magento_product_id', '=', item.get('id')), ('magento_sku', '=', item.get('sku')),
             ('magento_instance_id', '=', line.instance_id.id)], limit=1)
        if not m_product:
            m_product = self.search(['|', ('magento_product_id', '=', item.get('id')),
                                     ('magento_sku', '=', item.get('sku')),
                                     ('magento_instance_id', '=', line.instance_id.id),
                                     ('active', '=', False)], limit=1)
            m_product.write({'active': True})
        if m_product:
            m_template = m_product.magento_tmpl_id
            product = m_product.odoo_product_id
            ###### add new custom code ============================================
            oddo_parent_category = item.get('custom_attributes').get('oddo_parent_category')
            if oddo_parent_category:
                oddo_parent_category_object = self.env['magento.attribute.option'].search(
                    [('magento_attribute_option_id', '=', oddo_parent_category)], limit=1)
                if oddo_parent_category_object:
                    product.oddo_parent_category= oddo_parent_category_object.id
            # to set the  odoo sub category from magento to odoo
            oddo_sub_category = item.get('custom_attributes').get('oddo_sub_category')
            if oddo_sub_category:
                oddo_sub_category_object = self.env['magento.attribute.option'].search(
                    [('magento_attribute_option_id', '=', oddo_sub_category)], limit=1)
                if oddo_sub_category_object:
                    product.oddo_sub_category = oddo_sub_category_object.id
            ###### End  code ==========================================
            if 'is_order' not in list(self.env.context.keys()):
                if not line.do_not_update_existing_product:
                    self.__update_layer_product(line, item, m_product)
                    m_template.update_layer_template(line, item, product)
        else:
            product = self._search_odoo_product(line, item)
        prices = item.get('extension_attributes', {}).get('website_wise_product_price_data', [])
        if 'is_order' not in list(self.env.context.keys()):
            if not line.do_not_update_existing_product and prices and product:
                self.__update_prices(prices)
                self.env['magento.product.template'].update_price_list(line, product, prices)
        return product

    def _create_odoo_product(self, item):
        self.update_custom_option(item)
        values = self._prepare_product_values(item)
        custom_attributes = item.get('custom_attributes', [])
        brand_id = item.get('custom_attributes').get('manufacturer')
        brand_object = self.env['product.brand'].search([('magento_attribute_option_id','=',brand_id)],limit=1)
        if brand_object:
            values.update({'brand_id':brand_object.id})
        category_ids= item.get('custom_attributes').get('category_ids')
        for category in category_ids:
            product_category_object = self.env['product.category'].search([('magento_category_id', '=', category)])
            if product_category_object:
                values.update({'categ_id': product_category_object.id})
        # to set the  odoo parent category from magento to odoo
        oddo_parent_category = item.get('custom_attributes').get('oddo_parent_category')
        if oddo_parent_category:
            oddo_parent_category_object = self.env['magento.attribute.option'].search([('magento_attribute_option_id', '=', oddo_parent_category)],limit=1)
            if oddo_parent_category_object:
                values.update({'oddo_parent_category': oddo_parent_category_object.id})
        # to set the  odoo sub category from magento to odoo
        oddo_sub_category = item.get('custom_attributes').get('oddo_sub_category')
        if oddo_sub_category:
            oddo_sub_category_object = self.env['magento.attribute.option'].search([('magento_attribute_option_id', '=', oddo_sub_category)],limit=1)
            if oddo_sub_category_object:
                values.update({'oddo_sub_category': oddo_sub_category_object.id})
        return self.env['product.product'].create(values)

    def _create_odoo_template(self, item):
        self.update_custom_option(item)
        values = self._prepare_product_values(item)
        custom_attributes = item.get('custom_attributes', [])
        brand_id = item.get('custom_attributes').get('manufacturer')
        brand_object = self.env['product.brand'].search([('magento_attribute_option_id', '=', brand_id)],limit=1)
        if brand_object:
            values.update({'brand_id': brand_object.id})
       # to set the  product category from magento to odoo
        category_ids= item.get('custom_attributes').get('category_ids')
        for category in category_ids:
            product_category_object = self.env['product.category'].search([('magento_category_id', '=', category)])
            if product_category_object:
                values.update({'categ_id': product_category_object.id})
        # to set the  odoo parent category from magento to odoo
        oddo_parent_category = item.get('custom_attributes').get('oddo_parent_category')
        if oddo_parent_category:
            oddo_parent_category_object = self.env['magento.attribute.option'].search([('magento_attribute_option_id', '=', oddo_parent_category)],limit=1)
            if oddo_parent_category_object:
                values.update({'oddo_parent_category': oddo_parent_category_object.id})
        # to set the  odoo sub category from magento to odoo
        oddo_sub_category = item.get('custom_attributes').get('oddo_sub_category')
        if oddo_sub_category:
            oddo_sub_category_object = self.env['magento.attribute.option'].search([('magento_attribute_option_id', '=', oddo_sub_category)],limit=1)
            if oddo_sub_category_object:
                values.update({'oddo_sub_category': oddo_sub_category_object.id})

        return self.env['product.template'].create(values)
