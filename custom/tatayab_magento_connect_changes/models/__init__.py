# -*- coding: utf-8 -*-
###############################################################################
#
#    Tatayab Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tatayab(<http://www.tatayab.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from . import res_country_inherit
from . import product_brand
from . import sale_order_log
from . import magento_product_category_inherit
from . import product_template_changes
from . import sale_order_changes
from . import stock_picking_changes
from . import account_move_changes
from . import product_product_changes
from . import magento_payment_method_changes
from . import order_queue_line_inherit
from . import sale_workflow_process_ept_inherit
from . import product_queue_inherit
from . import res_partner_inherit
from . import magento_res_partner_ept
