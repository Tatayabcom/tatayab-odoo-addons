from odoo import api, fields, models
import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class ProductBrand(models.Model):
    _name = 'product.brand'
    _description = "Product Brands"

    name = fields.Char("Brand", translate=True, required=True)
    magento_attribute_option_id = fields.Char(string='Magento ID')
    notes = fields.Text("Description", translate=True)

    @api.constrains('name')
    def _name_unique(self):
        """ Constrain in brand it should be unique """
        for brand in self:
            if self.env['product.brand'].search([('name', '=', brand.name),
                                                 ('id', '!=', brand.id),('magento_attribute_option_id', '=', brand.magento_attribute_option_id)]):
                raise ValidationError(_("Brand Name Must Be Unique!!!"))


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    brand_id = fields.Many2one(comodel_name='product.brand', string="Brand", tracking=True)

class MagentoAttributeOption(models.Model):
    _inherit = "magento.attribute.option"

    def create_attribute_option(self, attribute, m_attribute, o_attribute):
        for option in attribute.get('options', []):
            option.update({'label': option.get('label').strip()})
            if option.get('label'):
                o_option = self._get_odoo_attribute_value(o_attribute.id, option.get('label'))
                m_option = self.search([('instance_id', '=', m_attribute.instance_id.id),
                                        ('magento_attribute_id', '=', m_attribute.id),
                                        ('odoo_option_id', '=', o_option.id),
                                        ('odoo_attribute_id', '=', o_attribute.id)])
                if not m_option:
                    values = self._prepare_option_value(option, o_option, o_attribute, m_attribute)
                    option_obj = self.create(values)
                    print("option_obj.magento_attribute_id.magento_attribute_code:::",option_obj.magento_attribute_id.magento_attribute_code)
                    if option_obj.magento_attribute_id.magento_attribute_code == 'manufacturer':
                        product_brand_obj = self.env['product.brand'].search([('magento_attribute_option_id','=',option_obj.magento_attribute_option_id)])
                        if not product_brand_obj:
                            product_brand = self.env['product.brand'].create({
                                'name': option_obj.name,
                                'magento_attribute_option_id':option_obj.magento_attribute_option_id,
                            })
                            logging.info(blue + "Automatic create brand:%s" % product_brand.name + reset)
                        else:
                            for brand in product_brand_obj:
                                brand.name = option_obj.name
        return True
