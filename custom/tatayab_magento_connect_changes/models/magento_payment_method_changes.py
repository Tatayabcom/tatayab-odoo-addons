# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->


class MagentoPaymentMethod(models.Model):
	_inherit = 'magento.payment.method'
	
	is_cod = fields.Boolean("COD Payment Method",
	                        help="- This flag used to mark payment method as Cash On delivery\n"
	                             "- This will effect directly on payment method amount calcs.")

# Ahmed Salama Code End.
