# -*- coding: utf-8 -*-
import json
from odoo.tools.misc import format_date

from odoo.addons.odoo_magento2_ept.models import api_request
from odoo import models, fields, api, _
from odoo.exceptions import UserError, _logger
import logging

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # TODO: Used for now to auto confirm order, will remove next as magento module has it's owen config for auto confirm
    origin = fields.Char("Magento Ref")
    # TODO: those fields are static for now as it was receiving from CS-cart and we need to handle them with magento next
    is_approved = fields.Boolean("Approved?", default=False, tracking=True,
                                 help="This field used to mark orders that is approved for warehouse to be used")
    customer_note = fields.Html(string="Customer Note", required=False, )
    stuff_note = fields.Html(string="Stuff Note", required=False, )
    is_gift = fields.Boolean("Is Gift?", help="Orders that is marked as gifts.")
    gift_message = fields.Html(string="Gift Message", required=False, help="Gift message from user")
    gift_sender_name = fields.Char(string="Gift Sender")
    gift_receiver_name = fields.Char(string="Gift Receiver")
    # Extra Fields
    cancellation_date = fields.Datetime("Cancellation Date", readonly=True)
    payment_collected = fields.Boolean('Payment Collected', default=True,
                                       help="This field is used to show pickings which is collected")
    sale_order_log_ids = fields.One2many('sale.order.log', 'sale_id',string="Order Log")

    def action_confirm(self):
        result = super(SaleOrder, self).action_confirm()
        # Create Pickings
        logging.info(yellow + "Automatic confirm order:%s" % self.display_name + reset)
        # EDIT PICKING
        self._pickings_automatic_workflow()
        # EDIT INVOICE
        self._invoice_automatic_workflow()
        return result

    def _pickings_automatic_workflow(self):
        """
		Add custom codes to edit picking fields from magento data
		"""
        if self.picking_ids:
            picking_write = {'payment_collected': self.payment_collected}
            if self.magento_order_reference:
                picking_write['backend_ref'] = self.magento_order_reference
            if self.warehouse_id.code == 'KSA':
                picking_write['pkg_three'] = 1
            if self.customer_note:
                picking_write['customer_note'] = self.customer_note
            if self.stuff_note:
                picking_write['stuff_note'] = self.stuff_note
            if self.is_gift:
                picking_write['is_gift'] = self.is_gift
                picking_write['gift_message'] = self.gift_message
                picking_write['gift_sender_name'] = self.gift_sender_name
                picking_write['gift_receiver_name'] = self.gift_receiver_name
            picking_write['is_approved'] = self.is_approved

            # In case of KSA add box size 3 by default
            if picking_write:
                self.picking_ids.write(picking_write)

    def _invoice_automatic_workflow(self):
        """
		Add custom codes to edit picking fields from magento data
		"""
        if self.invoice_ids:
            invoice_dict = {}
            if self.magento_order_reference:
                invoice_dict['backend_ref'] = self.magento_order_reference
            # if self.customer_note:
            #     invoice_dict['customer_note'] = self.customer_note
            # if self.stuff_note:
            #     invoice_dict['stuff_note'] = self.stuff_note
            # if self.is_gift:
            #     invoice_dict['is_gift'] = self.is_gift
            #     invoice_dict['gift_message'] = self.gift_message
            #     invoice_dict['gift_sender_name'] = self.gift_sender_name
            #     invoice_dict['gift_receiver_name'] = self.gift_receiver_name
            if invoice_dict:
                self.invoice_ids.write(invoice_dict)

    def toggle_approve(self):
        """
		Approve Waiting Call center approval orders manual
		"""
        for order in self:
            if self.env.user.has_group('tatayab_magento_connect_changes.group_approve_orders_manual'):
                if order.is_approved:
                    edit_vals = {'is_approved': False}
                else:
                    edit_vals = {'is_approved': True}
                order.write(edit_vals)
                order.picking_ids.write(edit_vals)
            else:
                raise UserError(_("You are not authorized to do this action, Please contact system admin"))

    # ================= #
    # Cancellation Part
    # ================= #

    # def action_cancel(self):
    # 	"""
    # 	Change Cancel Action to be dynamic with new automatic sequence
    # 	"""
    # 	for order in self:
    # 		edit_vals = {'cancellation_date': fields.Datetime.now()}
    # 		if order.auto_confirm:
    # 			# order.invoice_full_cancel(False)
    # 			order.workflow_full_cancel()
    # 		else:
    # 			edit_vals['state'] = 'cancel'
    # 		order.write(edit_vals)
    # 		return True

    def action_custom_cancel(self):
        """
		Original Cancel method will raise UserErrors in case any picking has been Done.
		"""
        self.ensure_one()
        # # self.history_invoice_ids += self.invoice_ids
        # # Cancel and Refund Invoice
        # self.invoice_full_cancel()
        self.with_context(stop_cancellation_inform=True).workflow_full_cancel()
        self.action_draft()
        self.order_line.unlink()

    def workflow_full_cancel(self):
        logging.info(yellow + "Full Cancellation Workflow Initiated : %s ::  %s"
                     % (self.display_name, self.state) + reset)
        PickingReturn = self.env['stock.return.picking']
        self.ensure_one()
        if self.state in ['draft', 'sent']:
            self.write({'state': 'cancel'})
        elif self.state == 'sale':
            # Cancel Existing Undone Pickings FIRST
            pickings_to_cancel = self.picking_ids.filtered(lambda p: p.state not in ['done', 'cancel'])
            pickings_to_cancel.action_cancel()
            logging.info(yellow + "___ pickings_to_cancel : %s are cancelled"
                         % pickings_to_cancel.mapped('display_name') + reset)
            # Order Has Been Cancelled
            self.write({'state': 'cancel'})

    # Magento Connector Part
    # ======================

    # Global Currency
    base_grand_total_usd = fields.Monetary("Global Amount Total", help="This field is used to show amount on USD")
    global_currency_id = fields.Many2one('res.currency', "Global Currency",
                                         default=lambda self: self.env.ref('base.USD').id)
    # Customer Currency
    grand_total = fields.Monetary("Customer Amount Total",
                                  help="This field is used to show amount on Customer Local Currency")
    customer_currency_id = fields.Many2one('res.currency', "Customer Currency")
    # Company Currency
    base_grand_total = fields.Monetary("Company Amount Total",
                                       help="This field is used to show amount on Company Local Currency")
    company_currency_id = fields.Many2one('res.currency', "Company Currency",
                                          default=lambda self: self.env.ref('base.KWD').id)
    shipping_amount_customer = fields.Monetary(string='Shipping charges')
    base_shipping_amount = fields.Monetary(string='Shipping charges KWD')
    discount_amount_customer = fields.Monetary(string='Discount amount')
    base_discount_amount = fields.Monetary(string='Discount amount KWD')
    base_subtotal = fields.Monetary(string='Order Subtotal KWD')
    subtotal_customer = fields.Monetary(string='Order Subtotal ')
    base_subtotal_incl_tax = fields.Monetary(string='Order Subtotal with tax KWD')
    subtotal_incl_tax_customer = fields.Monetary(string='Order Subtotal with tax')
    coupon_code = fields.Char(string="Coupon Code")

    def get_old_date_for_order(self):
        for rec in self:
            subtotal_customer = 0.0
            discount_amount_customer = 0.0
            shipping_amount_customer = 0.0
            for line in rec.order_line:
                if line.product_id.detailed_type == 'product':
                    subtotal_customer += line.price_subtotal
                elif line.product_id.id == rec.magento_instance_id.discount_product_id.id:
                    discount_amount_customer += line.price_subtotal
                elif line.product_id.id == rec.magento_instance_id.shipping_product_id.id:
                    shipping_amount_customer += line.price_subtotal
            rec.subtotal_customer = subtotal_customer
            rec.discount_amount_customer = discount_amount_customer
            rec.shipping_amount_customer = shipping_amount_customer


    @api.model
    def create(self, vals):
        """
		add Payment Status
		:param vals:
		:return: Super
		"""
        vals['payment_collected'] = True
        if vals.get('magento_payment_method_id'):
            magento_payment_method_id = self.env['magento.payment.method'].browse(vals.get('magento_payment_method_id'))
            if magento_payment_method_id and magento_payment_method_id.is_cod:
                vals['payment_collected'] = False
        return super(SaleOrder, self).create(vals)
    def __create_discount_order_line(self, item, instance):
        order_line = self.env['sale.order.line']
        sale_order_id = item.get('sale_order_id')
        price = float(item.get('base_discount_amount') or 0.0) or False if instance.is_order_base_currency else float(
            item.get('discount_amount') or 0.0) or False
        if price:
            default_product = self.env.ref('odoo_magento2_ept.magento_product_product_discount')
            product = sale_order_id.magento_instance_id.discount_product_id or default_product
            line = order_line.prepare_order_line_vals(item, {}, product, (abs(price)*-1))
            # if item.get('discount_tax'):
            #     line.update({'tax_id': [(6, 0, item.get('discount_tax'))]})
            if item.get('base_discount_amount'):
                line.update({'base_row_total_incl_tax': (abs(item.get('base_discount_amount'))*-1) })
                line.update({'row_total_incl_tax': (abs(price)*-1) })
            order_line.create(line)
        return True

    def __create_shipping_order_line(self, item, instance):
        order_line = self.env['sale.order.line']
        incl_amount = float(item.get('base_shipping_incl_tax', 0.0)) if instance.is_order_base_currency else float(
            item.get('shipping_incl_tax', 0.0))
        excl_amount = float(item.get('base_shipping_amount', 0.0)) if instance.is_order_base_currency else float(
            item.get('shipping_amount', 0.0))
        sale_order_id = item.get('sale_order_id')
        if incl_amount or excl_amount:
            tax_type = self.__find_tax_type(item.get('extension_attributes'),
                                            'apply_shipping_on_prices')
            price = incl_amount if tax_type else excl_amount
            default_product = self.env.ref('odoo_magento2_ept.product_product_shipping')
            product = sale_order_id.magento_instance_id.shipping_product_id or default_product
            shipping_line = order_line.prepare_order_line_vals(item, {}, product, price)
            shipping_line.update({'is_delivery': True})
            if item.get('shipping_tax'):
                shipping_line.update({'tax_id': [(6, 0, item.get('shipping_tax'))]})
            if item.get('base_shipping_amount'):
                shipping_line.update({'base_row_total_incl_tax':  item.get('base_shipping_amount')})
                shipping_line.update({'row_total_incl_tax':  price})
            order_line.create(shipping_line)
        return True

    def prepare_order_vals_after_onchange_ept(self, vals, fpos):
        """
		Inherited to
		:param vals:
		:param fpos:
		:return:
		"""
        order_vals = super(SaleOrder, self).prepare_order_vals_after_onchange_ept(vals, fpos)
        # order_vals = self._get_currency_amounts(order_vals, vals)
        update_vals = {
            # Global Currency
            'coupon_code': vals.get('coupon_code', 0.0),
            'shipping_amount_customer': vals.get('shipping_amount', 0.0),
            'base_shipping_amount': vals.get('base_shipping_amount', 0.0),
            'discount_amount_customer': vals.get('discount_amount', 0.0),
            'base_discount_amount': vals.get('base_discount_amount', 0.0),
            'subtotal_customer': vals.get('subtotal', 0.0),
            'base_subtotal': vals.get('base_subtotal', 0.0),
            'base_subtotal_incl_tax': vals.get('base_subtotal_incl_tax', 0.0),
            'subtotal_incl_tax_customer': vals.get('subtotal_incl_tax', 0.0),



            'base_grand_total_usd': vals.get('base_grand_total_usd', 0.0),
            'global_currency_id': vals.get('global_currency_id') or self.env.ref('base.USD').id,
            # Company Currency
            'base_grand_total': vals.get('base_grand_total', 0.0),  # Due to base_currency_code
            # Customer Currency
            'customer_currency_id': vals.get('customer_currency_id', 0.0),
            'grand_total': vals.get('grand_total', 0.0),
        }
        logging.info(bold_red + "Update Currency Vals: %s" % update_vals + reset)
        order_vals.update(update_vals)
        return order_vals

    def _prepare_order_dict(self, item, instance):
        source = item.get('extension_attributes').get('source', "KW")
        logging.info(yellow + "source:%s " % source + reset)
        logging.info(yellow + "instance:%s " % instance.id + reset)
        source = source + "-WH"
        magento_inventory_location_obj = self.env["magento.inventory.locations"]
        magento_location = magento_inventory_location_obj.search([
            ('magento_location_code', '=', source),
            ('magento_instance_id', '=', instance.id)
        ])
        store_view = item.get('store_view')
        order_vals = {
            'company_id': instance.company_id.id,
            'partner_id': item.get('parent_id'),
            'partner_invoice_id': item.get('invoice'),
            'partner_shipping_id': item.get('delivery') if item.get('delivery') else item.get(
                'invoice'),
            # 'warehouse_id': item.get('website').warehouse_id.id,
            'warehouse_id': magento_location.import_stock_warehouse.id,
            'picking_policy': item.get('workflow_id') and item.get(
                'workflow_id').picking_policy or False,
            'date_order': item.get('created_at', False),
            'pricelist_id': item.get('price_list_id') and item.get('price_list_id').id or False,
            'team_id': store_view and store_view.team_id and store_view.team_id.id or False,
            'payment_term_id': item.get('payment_term_id').id,
            'carrier_id': item.get('delivery_carrier_id'),
            'client_order_ref': item.get('increment_id')
        }
        # TODO: Tatayab Part
        order_vals = self._get_currency_amounts(order_vals, item)
        order_vals = self.wallet_changes(order_vals, item)
        # Same As core
        order_vals = self.create_sales_order_vals_ept(order_vals)
        order_vals = self.__update_order_dict(item, instance, order_vals)
        return order_vals

    def create_sale_order_ept(self, item, instance, log, line_id):
        is_processed = self._find_price_list(item, log, line_id,instance)
        order_line = self.env['sale.order.line']
        if is_processed:
            customers = self.__update_partner_dict(item, instance)
            data = self.env['magento.res.partner.ept'].create_magento_customer(customers, True)
            item.update(data)
            is_processed = self.__find_order_warehouse(item, log, line_id)
            if is_processed:
                is_processed = order_line.find_order_item(item, instance, log, line_id)
                if is_processed:
                    is_processed = self.__find_order_tax(item, instance, log, line_id)
                    if is_processed:
                        vals = self._prepare_order_dict(item, instance)
                        magento_order = self.create(vals)
                        magento_order.write({'shipping_amount_customer': item.get('shipping_amount', 0.0),
                        'base_shipping_amount': item.get('base_shipping_amount', 0.0),
                        'discount_amount_customer': item.get('discount_amount', 0.0),
                        'base_discount_amount': item.get('base_discount_amount', 0.0),
                        'subtotal_customer': item.get('subtotal', 0.0),
                        'base_subtotal': item.get('base_subtotal', 0.0),
                        'base_subtotal_incl_tax': item.get('base_subtotal_incl_tax', 0.0),
                        'subtotal_incl_tax_customer': item.get('subtotal_incl_tax', 0.0),})

                        method = item.get('payment', dict()).get('method')
                        # if method == 'cashondelivery' and item.get('state') != 'processing':
                        if item.get('state') != 'processing':
                            magento_order.is_approved = False
                        else:
                            magento_order.is_approved = True
                        item.update({'sale_order_id': magento_order})
                        order_line.create_order_line(item, instance, log, line_id)
                        self.__create_discount_order_line(item,instance)
                        self.__create_shipping_order_line(item,instance)
                        self.__create_gift_wrap_line(item)
                        self.__create_cod_fees_line(item)
                        self.__process_order_workflow(item, log)
        return is_processed

    # TODO: Then next 2 methods are priver
    def __update_order_dict(self, item, instance, order_vals):
        payment_info = item.get('extension_attributes').get('order_response')
        store_view = item.get('store_view')
        website_id = store_view.magento_website_id
        order_vals.update({
            'magento_instance_id': instance.id,
            'magento_website_id': website_id.id,
            'store_id': item.get('store_view').id,
            'auto_workflow_process_id': item.get('workflow_id') and item.get('workflow_id').id,
            'magento_payment_method_id': item.get('payment_gateway'),
            'magento_shipping_method_id': item.get('magento_carrier_id'),
            'is_exported_to_magento_shipment_status': False,
            'magento_order_id': item.get('entity_id'),
            'magento_order_reference': item.get('increment_id'),
            'order_transaction_id': self.__find_transaction_id(payment_info),
            'analytic_account_id': instance.magento_analytic_account_id and instance.magento_analytic_account_id.id or False,
        })
        if store_view and not store_view.is_use_odoo_order_sequence:
            name = "%s%s" % (store_view and store_view.sale_prefix or '', item.get('increment_id'))
            order_vals.update({"name": name})
        return order_vals

    @staticmethod
    def __find_transaction_id(payment_info):
        payment_additional_info = payment_info if payment_info else False
        transaction_id = False
        if payment_additional_info:
            for payment_info in payment_additional_info:
                if payment_info.get('key') == 'transaction_id':
                    transaction_id = payment_info.get('value')
        return transaction_id

    # Extended Methods
    # =================
    def wallet_changes(self, order_vals, item):
        """
		This method will be implemented on tatayab_customer_wallet
		:param order_vals:
		:param item:
		:return:
		"""
        return order_vals

    def confirm_po_after_approve(self):
        """
        This method will be implemented on tatayab_back_to_back
        :return: True
        """
        return True

    def _get_currency_amounts(self, order_vals, vals):
        extension_attributes = vals.get('extension_attributes')
        base_grand_total_usd = 0.0
        if extension_attributes:
            base_grand_total_usd = extension_attributes.get('base_grand_total_usd')
        customer_currency_id = self.env['res.currency'].search([('name', '=', vals.get('order_currency_code', False))])
        update_vals = {
            # Global Currency
            'coupon_code': vals.get('coupon_code', 0.0),
            'shipping_amount_customer': vals.get('shipping_amount', 0.0),
            'base_shipping_amount': vals.get('base_shipping_amount', 0.0),
            'discount_amount_customer': vals.get('discount_amount', 0.0),
            'base_discount_amount': vals.get('base_discount_amount', 0.0),
            'subtotal_customer': vals.get('subtotal', 0.0),
            'base_subtotal': vals.get('base_subtotal', 0.0),
            'base_subtotal_incl_tax': vals.get('base_subtotal_incl_tax', 0.0),
            'subtotal_incl_tax_customer': vals.get('subtotal_incl_tax', 0.0),

            'base_grand_total_usd': base_grand_total_usd,
            'global_currency_id': self.env.ref('base.USD').id,
            # Company Currency
            'base_grand_total': vals.get('base_grand_total', 0.0),
            'company_currency_id': self.env.ref('base.KWD').id,  # Due to base_currency_code
            # Customer Currency
            'customer_currency_id': customer_currency_id and customer_currency_id.id or False,
            'grand_total': vals.get('grand_total', 0.0),
        }
        logging.info(blue + "Update Currency Vals: %s" % update_vals + reset)
        order_vals.update(update_vals)
        return order_vals

    # ===========================
    # ==        Gift          ===
    # ===========================
    def __create_gift_wrap_line(self, item):
        order_line = self.env['sale.order.line']
        sale_order_id = item.get('sale_order_id')
        order_currency_code = item.get('order_currency_code')
        gift_wraps = item.get('extension_attributes', {}).get('gift_wraps', [])
        for gift in gift_wraps:
            # gift_dict = json.loads(gift)
            gift_dict = gift
            warp_price = gift_dict.get('wrap_price')
            base_wrap_price = gift_dict.get('base_wrap_price')
            words = list(warp_price)
            str_price = ''
            for word in words:
                if word not in list(order_currency_code):
                    str_price += word
            price = float(str_price)
            product_object = self.env['product.product'].search([('gift_magento_id', '=', gift_dict.get('wrap_id'))],
                                                                 limit=1)
            default_product = self.env.ref('tatayab_magento_connect_changes.gift_wrap_fees')
            product = default_product
            if product_object:
                product = product_object
            gift_line = order_line.prepare_order_line_vals(item, {}, product, price)
            gift_line.update({'magento_sale_order_line_ref': gift_dict.get('wrap_item_id')})
            gift_line.update({'is_line_gift': True})
            gift_line.update({'base_row_total_incl_tax': float(base_wrap_price)})
            order_object = order_line.create(gift_line)
            sale_order_id.is_gift = True
            sale_order_id.gift_message = gift_dict.get('message')
            sale_order_id.gift_sender_name = gift_dict.get('sender')
            sale_order_id.gift_receiver_name = gift_dict.get('receiver')
        return True

    def action_print_gift_receipt(self):
        """
		Print Gift Receipt
		:return: report view
		"""
        return self.env.ref('tatayab_magento_connect_changes.action_report_gift_receipt').report_action(self)

    def action_print_gift_receipt_small(self):
        """
		Print Gift Receipt small
		:return: report view
		"""
        return self.env.ref('tatayab_magento_connect_changes.action_report_gift_receipt_small').report_action(self)

    # =============================================
    # =====to create draft payment for invoice ====
    # =============================================
    def validate_and_paid_invoices_ept(self, work_flow_process_record):
        """
        This method will create invoices, validate it and register payment it, according to the configuration in
        workflow sets in quotation.
        :param work_flow_process_record:
        :return: It will return boolean.
        Migration done by Haresh Mori on September 2021
        """
        self.ensure_one()
        if work_flow_process_record.create_invoice:
            if work_flow_process_record.invoice_date_is_order_date:
                fiscalyear_lock_date = self.company_id._get_user_fiscal_lock_date()
                if self.date_order.date() <= fiscalyear_lock_date:
                    log_book_id = self._context.get('log_book_id')
                    if log_book_id:
                        message = "You cannot create invoice for order (%s) " \
                                  "prior to and inclusive of the lock date %s. " \
                                  "So, order is created but invoice is not created." % (self.name, format_date(
                            self.env, fiscalyear_lock_date))
                        self.env['common.log.lines.ept'].create({
                            'message': message,
                            'order_ref': self.name,
                            'log_book_id': log_book_id
                        })
                        _logger.info(message)
                    return True
            ctx = self._context.copy()
            if work_flow_process_record.sale_journal_id:
                ctx.update({'journal_ept': work_flow_process_record.sale_journal_id})
            if not self.invoice_ids:
                invoices = self._create_invoices()
                self.validate_invoice_ept(invoices)
                if work_flow_process_record.register_payment:
                    self.paid_invoice_ept(invoices)
                if work_flow_process_record.draft_payment:
                    self.paid_invoice_ept(invoices)

    def paid_invoice_ept(self, invoices):
        """
        This method auto paid invoice based on auto workflow method.
        @author: Dipesh Tanna
        @param invoices: Recordset of Invoice.
        Migration done by Haresh Mori on September 2021
        """
        self.ensure_one()
        account_payment_obj = self.env['account.payment']
        for invoice in invoices:
            if invoice.amount_residual:
                vals = invoice.prepare_payment_dict(self.auto_workflow_process_id)
                payment_id = account_payment_obj.create(vals)
                if not self.auto_workflow_process_id.draft_payment:
                    payment_id.action_post()
                    self.reconcile_payment_ept(payment_id, invoice)
        return True

    # ===========================
    # ==        COD Fees      ===
    # ===========================
    def __create_cod_fees_line(self, item):
        order_line = self.env['sale.order.line']
        sale_order_id = item.get('sale_order_id')
        order_currency_code = item.get('order_currency_code')
        cod_fee = item.get('extension_attributes', {}).get('cod_fee', 0.0)
        base_cod_fee = item.get('extension_attributes', {}).get('base_cod_fee', 0.0)
        if cod_fee:
            default_product = self.env.ref('tatayab_magento_connect_changes.tatayab_pay_fees')
            cod_fee_line = order_line.prepare_order_line_vals(item, {}, default_product, cod_fee)
            # cod_fee_line.update({'is_cod_fees ': True}) # TODO remove hash this line after runing
            order_object = order_line.create(cod_fee_line)
            order_object.is_cod_fees = True
            order_object.base_row_total_incl_tax = base_cod_fee
        return True

    # ==================================
    # == send state order in magento ===
    #  ==================================
    def send_state_order_in_magento(self):
        """
         This method use for send state order in magento.
         @return: result
        """
        for rec in self:
            if rec.is_approved:
                data = {}
                if rec.operation_state in ['pick','ship','done']:
                    data = {
                        "entity": {
                            "entity_id": rec.magento_order_id,
                            "status": rec.operation_state,
                            "increment_id": rec.magento_order_reference
                        }
                    }

                elif rec.operation_state in ['partial_return','partial_return_other_done']:
                    data = {
                        "entity": {
                            "entity_id": rec.magento_order_id,
                            "status": 'partial_returned',
                            "increment_id": rec.magento_order_reference
                        }
                    }

                elif rec.operation_state in ['returned','return_other_done','return_done']:
                    data = {
                        "entity": {
                            "entity_id": rec.magento_order_id,
                            "status": 'fully_returned',
                            "increment_id": rec.magento_order_reference
                        }
                    }
                result = False
                magento_order_id = rec.magento_order_id
                if data:
                    if magento_order_id:
                        magento_instance = rec.magento_instance_id
                        try:
                            api_url = '/V1/orders'
                            # api_url = '/V1/orders/%s/comments' % magento_order_id
                            result = api_request.req(magento_instance, api_url, 'POST',data)
                            logging.info(green + " Send  state order in magento "  + reset)
                        except Exception:
                            logging.info(yellow + "Exception when  Send  state order in magento" + reset)
                            print("Error while requesting send state order")
                            # raise UserError("Error while requesting send state order")
                        return result

    def send_state_order_from_bulk_in_magento(self,data):
        """
         This method use for send state order in magento.
         @return: result
        """
        for rec in self:
            if rec.is_approved:
                result = False
                magento_order_id = rec.magento_order_id
                if magento_order_id:
                    magento_instance = rec.magento_instance_id
                    try:
                        api_url = '/V1/orders'
                        result = api_request.req(magento_instance, api_url, 'POST', data)
                        logging.info(green + " Send  state order in magento" + reset)
                        message = " update Order State to  %s" %rec.operation_state
                        rec.message_post(
                            body="<b style='color:green'>Update Order State : </b><br/><span>%s</span>" % message)
                    except Exception:
                        message = " Exception when  Send  state order in magento  "
                        rec.message_post(
                            body="<b style='color:yellow'>Update Order State : </b><br/><span>%s</span>" % message)

                        logging.info(yellow + "Exception when  Send  state order in magento" + reset)
                        print("Error while requesting send state order")
                        # raise UserError("Error while requesting send state order")
                    return result
    # Ahmed Salama Code End.

    # override by omnya 5/9/2022 to sync building number, block and additional directions to customer address
    @staticmethod
    def __update_partner_address_dict(item, addresses):
        city = addresses.get('city')
        if addresses.get('city') == '-':
            city = addresses.get('region')
        address = {
            'firstname': addresses.get('firstname'),
            'lastname': addresses.get('lastname'),
            'email': item.get('email'),
            'street': addresses.get('street'),
            'building_no': addresses.get('building_no'),
            'street2': addresses.get('street2'),
            'block_no': addresses.get('block_no'),
            'additional_direction': addresses.get('additional_direction'),
            'city': city,
            'postcode': addresses.get('postcode'),
            'company': item.get('company', False),
            'id': addresses.get('customer_address_id') or addresses.get('entity_id'),
            'website_id': item.get('website_id'),
            'customer_id': item.get('id'),
            'region': {
                "region_code": addresses.get('region_code')
            },
            # 'state_id': addresses.get('region'),
            'state_id': addresses.get('region_code'),
            'country_id': addresses.get('country_id'),
            'telephone': addresses.get('telephone')
        }
        if addresses.get('address_type') == 'billing':
            address.update({'default_billing': True})
        if addresses.get('address_type') == 'shipping':
            address.update({'default_shipping': True})
        return address

    def __update_partner_dict(self, item, instance):
        addresses = []
        magento_store = instance.magento_website_ids.store_view_ids.filtered(
            lambda l: l.magento_storeview_id == str(item.get('store_id')))
        m_customer_id = self.__get_customer_id(item)
        website_id = magento_store.magento_website_id.magento_website_id
        customers = {
            'instance_id': instance,
            'firstname': item.get('customer_firstname'),
            'lastname': item.get('customer_lastname'),
            'email': item.get('customer_email'),
            'website': magento_store.magento_website_id,
            'website_id': website_id and int(website_id) or '',
            'store_view': magento_store,
            'id': m_customer_id,
            'is_guest': item.get('customer_is_guest'),
            'is_faulty': not item.get('customer_is_guest') and not item.get('customer_id', False),
        }
        if item.get('customer_is_guest'):
            customers.update({
                'firstname': item.get('billing_address').get('firstname'),
                'lastname': item.get('billing_address').get('lastname'),
            })
        bill_addr = item.get('billing_address')
        sw_assign = item.get('extension_attributes', dict()).get('swissup_address_fields')
        for sw in sw_assign:
            # if sw_assign:
            if sw.get('address_type') == 'billing':
                if sw.get('attribute_code') == 'house_building':
                    bill_addr['building_no'] = sw.get('value')
                elif sw.get('attribute_code') == 'block':
                    bill_addr['block_no'] = sw.get('value')
                elif sw.get('attribute_code') == 'add_direction':
                    bill_addr['additional_direction'] = sw.get('value')
                elif sw.get('attribute_code') == 'street_add':
                    bill_addr['street2'] = sw.get('value')
                elif sw.get('attribute_code') == 'lane_pathway':
                    bill_addr['street2'] = sw.get('value')

        b_address = self.__update_partner_address_dict(customers, bill_addr)
        addresses.append(b_address)

        ship_assign = item.get('extension_attributes', dict()).get('shipping_assignments')
        for ship_addr in ship_assign:
            if isinstance(ship_addr, dict) and 'address' in list(
                    ship_addr.get('shipping', {}).keys()):
                ship_addr = ship_addr.get('shipping', dict()).get('address')
                sw_assign = item.get('extension_attributes', dict()).get('swissup_address_fields')
                street2 = ''
                for sw in sw_assign:
                    # if sw_assign:
                    if sw.get('address_type') == 'shipping':
                        if sw.get('attribute_code') == 'house_building':
                            ship_addr['building_no'] = sw.get('value')
                        elif sw.get('attribute_code') == 'block':
                            ship_addr['block_no'] = sw.get('value')
                        elif sw.get('attribute_code') == 'add_direction':
                            ship_addr['additional_direction'] = sw.get('value')
                        elif sw.get('attribute_code') == 'street_add':
                            street2 += sw.get('value')
                            street2 += "/"
                        elif sw.get('attribute_code') == 'lane_pathway':
                            street2 += sw.get('value')
                        ship_addr['street2'] = street2

                # if sw_assign:
                #     if sw_assign[0] and sw_assign[0].get('attribute_code') == 'house_building':
                #         ship_addr['building_no'] = sw_assign[0].get('value')
                #     if sw_assign[3] and sw_assign[3].get('attribute_code') == 'block':
                #         ship_addr['block_no'] = sw_assign[3].get('value')
                #     if sw_assign[2] and sw_assign[2].get('attribute_code') == 'add_direction':
                #         ship_addr['additional_direction'] = sw_assign[2].get('value')
                s_address = self.__update_partner_address_dict(customers, ship_addr)
                addresses.append(s_address)
        customers.update({'addresses': addresses})
        item.update(customers)
        return customers
    # End of modifications ######


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    usd_row_total = fields.Float(string="Total USD",help="the value without discount")
    row_total_incl_tax = fields.Float(string="Total Customer")
    base_row_total_incl_tax = fields.Float(string="Total KWD", help="The value in this  field  after discount the there are discount on lines")
    base_row_subtotal_incl_tax = fields.Float(string="SubTotal KWD", help="The value in this  field  before discount the there are discount on lines")
    gift_order_line_id = fields.Many2one('sale.order.line', string="Gift Line")
    is_line_gift = fields.Boolean("Is Gift?", help="line that is marked as gift.")
    is_cod_fees = fields.Boolean("Is Cod Fees?", help="line that is marked as COD Fes.")
    product_parent_category_id = fields.Many2one('product.parent.category', store=True,
                                                 related='product_id.product_parent_category_id', string="Odoo Category")
    product_sub_category_id = fields.Many2one('product.sub.category',store=True,related='product_id.product_sub_category_id' ,string=" Sub Category")
    brand_id = fields.Many2one('product.brand',related='product_id.brand_id' ,string='Product Brand',store=True)
    detailed_type = fields.Selection([
        ('consu', 'Consumable'),
        ('service', 'Service'),
        ('product', 'Storable Product')
    ], string='Product Type',store=True,related='product_id.detailed_type')
    operation_state = fields.Selection(selection=[('draft', 'Draft'),
                                                  ('without_pickings', 'Without Pickings'),
                                                  ('pick', 'Picking'),
                                                  ('pack', 'Packing'),
                                                  ('ship', 'Shipping'),
                                                  ('done', 'Done Shipment'),
                                                  ('lock', 'Locked'),
                                                  ('cancel', 'Canceled'), ('return_pick', 'Returning'),
                                                  ('partial_returning', 'Partial Returning'),
                                                  ('partial_return', 'Partial Returned'),
                                                  ('returned', 'Returned'),
                                                  ('return_done', 'Done Return'),
                                                  ('return_other', 'Returning to other WH'),
                                                  ('return_other_done', 'Returned to other WH'),
                                                  ('partial_return_other_done', 'Partial Returned to other WH'),
                                                  ], string="Operation State",store=True)
    country_id = fields.Many2one(comodel_name="res.country", string="Country", tracking=True,
                                 related='order_id.partner_shipping_id.country_id', store=True)

    def prepare_order_line_vals(self, item, line, product, price):
        order_qty = float(line.get('qty_ordered', 1.0))
        sale_order = item.get('sale_order_id')
        tag_ids = sale_order.magento_instance_id.magento_analytic_tag_ids and sale_order.magento_instance_id.magento_analytic_tag_ids.ids or []
        if not tag_ids:
            tag_ids = sale_order.magento_website_id.m_website_analytic_tag_ids and sale_order.magento_website_id.m_website_analytic_tag_ids.ids or []
        order_line_ref = line.get('parent_item_id') or line.get('item_id')
        line_vals = {
            'order_id': sale_order.id,
            'product_id': product.id,
            'company_id': sale_order.company_id.id,
            'name': item.get('name'),
            'description': product.name or (sale_order and sale_order.name),
            'product_uom': product.uom_id.id,
            'order_qty': order_qty,
            'price_unit': price,
        }
        line_vals = self.create_sale_order_line_ept(line_vals)
        line_vals.update({
            'magento_sale_order_line_ref': order_line_ref,
            "analytic_tag_ids": [(6, 0, tag_ids)],
        })
        # if item.get(f'order_tax_{line.get("item_id")}'):
        #     line_vals.update({'tax_id': [(6, 0, item.get(f'order_tax_{line.get("item_id")}'))]})
        # elif line.get('tax_percent', 0.0):
        #     pass
        # else:
        #     line_vals.update({'tax_id': False})
        if line.get('extension_attributes', {}).get('usd_row_total'):
            line_vals.update({
                'usd_row_total': float(line.get('extension_attributes', {}).get('usd_row_total'))
            })
        if line.get('base_row_total_incl_tax'):
            line_vals.update({
                'base_row_total_incl_tax': float(line.get('base_row_total_incl_tax')) + abs(float(line.get('base_discount_amount'))),
                'base_row_subtotal_incl_tax': float(line.get('base_row_total_incl_tax')),
            })
        if line.get('row_total_incl_tax'):
            line_vals.update({
                'row_total_incl_tax': float(line.get('row_total_incl_tax')),
            })

        logging.info(bold_red + "Update Currency line with usd Vals: %s" % line_vals + reset)
        return line_vals

    def _compute_tax_id(self):
        for line in self:
            line.tax_id = False
            if line.order_id.country_id and line.order_id.country_id.sale_taxes_id:
                fpos = line.order_id.fiscal_position_id or line.order_id.partner_id.property_account_position_id
                # If company_id is set, always filter taxes by the company
                taxes = line.order_id.country_id.sale_taxes_id.filtered(
                    lambda r: not line.company_id or r.company_id == line.company_id)
                if line.product_id.detailed_type != 'service':
                    line.tax_id = fpos.map_tax(taxes, line.product_id,
                                               line.order_id.partner_shipping_id) if fpos else taxes
                else:
                    line.tax_id = [(6, 0, [])]
            else:
                line.tax_id = [(6, 0, [])]
                # fpos = line.order_id.fiscal_position_id or line.order_id.partner_id.property_account_position_id
                # # If company_id is set, always filter taxes by the company
                # taxes = line.product_id.taxes_id.filtered(lambda r: not line.company_id or r.company_id == line.company_id)
                # line.tax_id = fpos.map_tax(taxes, line.product_id, line.order_id.partner_shipping_id) if fpos else taxes
