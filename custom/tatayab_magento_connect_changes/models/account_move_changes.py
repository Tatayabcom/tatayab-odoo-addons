# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, _logger
from odoo.addons.odoo_magento2_ept.models import api_request
# from odoo.addons.odoo_magento2_ept.models.python_library.php import Php
from odoo.addons.odoo_magento2_ept.python_library.php import Php

import logging
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->



class AccountMoveInherit(models.Model):
	_inherit = 'account.move'
	
	backend_ref = fields.Char("Magento Ref.")
	state_send_invoice_status = fields.Selection(selection=[('not_send','Not Send Yet'),
															('done','Done'),
															('fail','Fail'),
															], default='not_send')
	old_invoice = fields.Boolean(string="Old Invoice")

# Ahmed Salama Code End.

class AccountInvoice(models.Model):
	_inherit = 'account.move'

	# ==================================
	# == get invoice id from magento ===
	#  ==================================
	def get_invoice_id_from_magento(self):
		for rec in self:
			instance = rec.magento_instance_id
			order = rec.invoice_line_ids.mapped('sale_line_ids.order_id')
			if order:
				filters = api_request.create_search_criteria({'order_id': order.magento_order_id})
				query_str = Php.http_build_query(filters)
				path = f"/V1/invoices?{query_str}"
				result = api_request.req(instance, path, 'GET')
				invoice_id = False
				if result and result.get('items'):
					# FIXME: Need to handle the case when one order has multiple invoices at Magento
					invoice_id = result.get('items')[0].get('entity_id')
					rec.magento_invoice_id = invoice_id
					rec.message_post(body="<b style='color:green'> magento invoice with id (%s) </b>" % rec.magento_invoice_id)
				logging.info(red + "Magento  invoice of  order : %s" % invoice_id + reset)
				return invoice_id
	def auto_get_invoice_id_from_magento(self):
		invoices = self.env['account.move'].search([('magento_invoice_id','=',False),
													('move_type', '=', 'out_invoice'),
													], limit=7)
		for rec in invoices:
			instance = rec.magento_instance_id
			order = rec.invoice_line_ids.mapped('sale_line_ids.order_id')
			if order:
				filters = api_request.create_search_criteria({'order_id': order.magento_order_id})
				query_str = Php.http_build_query(filters)
				path = f"/V1/invoices?{query_str}"
				result = api_request.req(instance, path, 'GET')
				invoice_id = False
				if result and result.get('items'):
					# FIXME: Need to handle the case when one order has multiple invoices at Magento
					invoice_id = result.get('items')[0].get('entity_id')
					rec.magento_invoice_id = invoice_id
				logging.info(red + "Magento  invoice of  order : %s" % invoice_id + reset)
				logging.info(red + "Magento  invoice of  order : %s" % order.magento_order_id + reset)
				# return invoice_id
	# =========================================
	# == cancel  pending invoice in magento ===
	#   curl -s -o -  --insecure  -g -X POST “https://magento-dev.tatayab.com/rest/V1/invoice/1207/cancel”
	#   -H “Content-Type:application/json”    -H “Authorization: Bearer ca8gjeg5ir30arg2mv2tni6ilan02a27” -d ‘{}’
	#  ========================================

	def cancel_invoice_from_odoo_to_magento(self):
		for rec in self:
			if not rec.magento_invoice_id:
				invoice_id = rec.get_invoice_id_from_magento()
				rec.message_post(body="<b style='color:green'> magento invoice with id (%s) </b>"%rec.magento_invoice_id)
				if invoice_id:
					instance = rec.magento_instance_id
					request_path = '/V1/invoice/{}/cancel'.format(invoice_id)
					result = api_request.req(instance, request_path, 'POST')
					rec.message_post(body="<b style='color:red'>Done cancel invoice with id (%s)from magento </b>"%rec.magento_invoice_id)
					if result:
						logging.info(green + " %s" % result + reset)
			else:
				instance = rec.magento_instance_id
				request_path = '/V1/invoice/{}/cancel'.format(rec.magento_invoice_id)
				result = api_request.req(instance, request_path, 'POST')
				rec.message_post(
					body="<b style='color:red'>Done cancel invoice with id (%s)from magento </b>" % rec.magento_invoice_id)
				if result:
					logging.info(green + " %s" % result + reset)

	def send_state_invoice_in_magento(self):
		"""
         This method use for send state order in magento.
         @return: result
        """
		for rec in self:
			rec.export_invoice_magento(wizard=False)
			rec.get_invoice_id_from_magento()
			magento_invoice_id = rec.magento_invoice_id
			if magento_invoice_id:
				magento_instance = rec.magento_instance_id
				data = {"capture": 'paid'}
				try:
					api_url = '/V1/invoices/%s/capture' % magento_invoice_id
					result = api_request.req(magento_instance, api_url, 'POST',data)
					logging.info(green + " ======= Result  invoice of  order : %s =======" % result + reset)
				except Exception:
					logging.info(yellow + " ------  Exception Result  invoice of  order : ------ " + reset)
					raise UserError("Error while requesting send state invoice")
				return result
	def auto_send_invoice_status(self):
		"""
         This method use for send state order in magento by cron job .
         @return: result
        """
		invoices = self.env['account.move'].search([('state_send_invoice_status','=','not_send'),
													('magento_payment_method_id.is_cod','=',True),
													('payment_state','in',['in_payment', 'paid']),
													],limit=7)
		for rec in invoices:
			rec.export_invoice_magento(wizard=False)
			rec.get_invoice_id_from_magento()
			magento_invoice_id = rec.magento_invoice_id
			if magento_invoice_id:
				magento_instance = rec.magento_instance_id
				data = {"capture": 'paid'}
				try:
					api_url = '/V1/invoices/%s/capture' % magento_invoice_id
					result = api_request.req(magento_instance, api_url, 'POST',data)
					rec.state_send_invoice_status = 'done'
					logging.info(green + " =======CRON Result  invoice of  order : %s =======" % result + reset)
				except Exception:
					logging.info(yellow + " ------ CRON Exception Result  invoice of  order : ------ " + reset)
					rec.state_send_invoice_status = 'fail'
					raise UserError("Error while requesting send state invoice")
				return result

	def _prepare_line_data(self):
		"""
		This method is used to prepare items data's
		:task_id: 173739
		-------------------
		:param: True if refund process online
		:return: list of dictionary
		"""
		items = list()
		product_ids = self._get_shipping_discount_product_ids()
		pay_fees = self.env.ref('tatayab_magento_connect_changes.tatayab_pay_fees').id
		product_ids.append(pay_fees)
		product_gift = self.env['product.product'].search([('gift_magento_id','in',[1,2,3,4,5,6,7,8,9,10,11,12,13])])
		print("product_gift =========>>>",product_gift)
		for gift in product_gift:
			product_ids.append(gift.id)
		credit_lines_p = self.invoice_line_ids.filtered(
			lambda l: l.product_id.id and l.product_id.id not in product_ids)
		credit_lines = credit_lines_p.filtered(
			lambda l: l.product_id.id and l.product_id.detailed_type not in ['service'])
		for line in credit_lines:
			item_id = line.sale_line_ids.magento_sale_order_line_ref
			values = self._prepare_line_values(line, item_id, items)
			if values:
				items.append(values)
		return items

	# def action_post(self):
	# 	for rec in self:
	# 		if not rec.old_invoice:
	# 			if rec.move_type == 'out_refund' and rec.magento_instance_id:
	# 				# rec.action_create_credit_memo('online', True)
	# 				rec.action_create_credit_memo('offline', True)
	# 			return super(AccountInvoice, self).action_post()