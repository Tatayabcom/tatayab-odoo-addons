# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
"""
Describes methods for sync/ Import product queues.
"""
import math
import logging
import time
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons.odoo_magento2_ept.models.api_request import req, create_search_criteria
from odoo.addons.odoo_magento2_ept.python_library.php import Php

_logger = logging.getLogger('MagentoProductQueue')


class MagentoProductQueueInherit(models.Model):
    """
    Describes sync/ Import product queues.
    """
    _inherit = "sync.import.magento.product.queue"


    # def create_product_queues(self, instance, from_date, to_date, is_update=True, current=0):
    #     """
    #     Creates product queues when sync/ import products from Magento.
    #     :param instance: current instance of Magento
    #     :param from_date:  Sync product start from this date
    #     :param to_date: Sync product end to this date
    #     :return:
    #     """
    #     queues = []
    #     queue_line = self.env['sync.import.magento.product.queue.line']
    #     current_page = instance.magento_import_product_page_count
    #     for product_type in ['configurable', 'simple', 'bundle']:
    #         filters = self._get_product_search_filter(from_date=from_date, to_date=to_date,
    #                                                   product_type=product_type)
    #         products = self._get_product_response(instance, filters, current_page, get_pages=True)
    #         self._update_import_product_counter(instance, products)
    #         total_page = math.ceil(int(products.get('total_count')) / 50)
    #         if current:
    #             current_page = current
    #         for page in range(current_page, total_page + 1):
    #             products = self._get_product_response(instance, filters, page=page)
    #             if not products.get('items', []):
    #                 self._update_import_product_counter(instance, products)
    #                 break
    #             queue = self._create_product_queue(instance)
    #             queues.append(queue.id)
    #             try:
    #                 for product in products.get('items'):
    #                     # Create new queue if the 50 queue_line count is reached.
    #                     if len(queue.line_ids) == 50:
    #                         queue = self._create_product_queue(instance)
    #                     queue_line.create_product_queue_line(product=product,
    #                                                          instance_id=instance.id,
    #                                                          is_update=is_update,
    #                                                          queue_id=queue.id)
    #                 self._cr.commit()
    #             except Exception as error:
    #                 _logger.error(error)
    #                 instance.write({'magento_import_product_page_count': page})
    #                 self._cr.commit()
    #     instance.write({'magento_import_product_page_count': 1})
    #     self._cr.commit()
    #     return queues

class MagentoProductQueueLine(models.Model):
    """
    Describes sync/ Import product Queue Line
    """
    _inherit = "sync.import.magento.product.queue.line"

    def import_products(self, item, line):
        instance = line.instance_id
        m_product = self.env['magento.product.product']
        attribute = item.get('extension_attributes', {})
        if item.get('type_id') in ['simple','bundle']:
            if 'simple_parent_id' in list(attribute.keys()):
                m_product = m_product.search([('magento_product_id', '=', item.get('id'))], limit=1)
                if not m_product or 'is_order' in list(self.env.context.keys()) or line.do_not_update_existing_product:
                    # This case only runs when we get the simple product which are used as an
                    # Child product of any configurable product in Magento.
                    items = m_product.get_products(instance, [attribute.get('simple_parent_id')], line)
                    for item in items:
                        return m_product.import_configurable_product(line, item)
            else:
                # This case identifies that we get only simple product which are not set as an
                # Child product of any configurable product.
                return m_product.search_product_in_layer(line, item)
        elif item.get('type_id') == 'configurable':
            return m_product.import_configurable_product(line, item)
        elif item.get('type_id') in ['virtual', 'downloadable']:
            return m_product.search_service_product(item, line)
        else:
            # We are not allowing to import other types of products. Add log for that.
            return self.__search_product(line, item)
        return True