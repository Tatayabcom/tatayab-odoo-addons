from odoo import fields, models, api, _


class Partner(models.Model):
    _inherit = 'res.partner'

    building_no = fields.Char()
    block_no = fields.Char()
    additional_direction = fields.Char()
