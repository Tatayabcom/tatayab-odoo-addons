# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Magento Connect Changes ",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'Sales',
    'summary': """Tatayab Magento Connect Changes to fit tatayab needs """,
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '15.1',
    'depends': ['sale', 'stock', 'odoo_magento2_ept','sale_stock'],
    'data': [
        'data/product_data.xml',
        'data/cron_job_data.xml',

        'security/security.xml',
        'security/ir.model.access.csv',
        
        'reports/report_gift_receipt.xml',
        'reports/report_gift_receipt_small.xml',
        'reports/gift_report.xml',
        
        'views/order_queue_line_inherit_view.xml',
        'views/res_country_inherit_view.xml',
        'views/product_view_changes.xml',
        'views/sale_order_view_changes.xml',
        'views/stock_picking_view_changes.xml',
        'views/account_move_view_inherit.xml',
        'views/magento_payment_method_view_changes.xml',
        'views/magento_instance_inherit_view.xml',
        'views/sale_workflow_process_ept_view_inherit.xml',
        'views/res_partner_inh_view.xml',
        'views/product_category_view_inherit.xml',
        'views/product_parent_category_view.xml',
        'views/menuitems.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
