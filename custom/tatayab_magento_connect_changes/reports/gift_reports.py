# -*- coding: utf-8 -*-

from odoo import models
import logging

_logger = logging.getLogger(__name__)


class GiftSmallReport(models.AbstractModel):
	_name = 'report.tatayab_magento_connect_changes.small_gift'
	_description = "Report Gift Small"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['sale.order'].browse(docids)
		print("RECORDS::", records)
		return {
			'doc_ids': records.ids,
			'doc_model': 'sale.order',
			'docs': records,
		}