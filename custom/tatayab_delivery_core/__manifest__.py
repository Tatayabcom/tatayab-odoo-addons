# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Delivery Core",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'Inventory',
    'summary': """Tatayab Delivery Core""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '1.0',
    'depends': ['delivery', 'hr', 'tatayab_magento_connect_changes'],
    'data': [

        'security/ir.model.access.csv',
        'data/shipping_method_data.xml',
        
        'views/shipping_method_view.xml',
        'views/sale_order_view_changes.xml',
        'views/stock_picking_view_changes.xml',
        'views/account_move_view_changes.xml',
        'views/hr_employee_view_changes.xml',
        'views/res_partner_view_changes.xml',
        'views/delivery_carrier_log_view.xml',
        'views/product_view.xml',
        'views/stock_move_line_view_inherit.xml',

        'reports/commercial_template_report.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
