# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
# Ahmed Salama Code Start ---->


DRIVER_TYPE = [('internal', "Internal Driver"),
               ('external', "External Driver"),
               ('international', 'Shipping Comp.')]


class ResPartnerInherit(models.Model):
	_inherit = 'res.partner'
	
	is_driver = fields.Boolean("Is Driver?", help="This is External Driver for Local Delivery Company")
	attachment_ids = fields.Many2many('ir.attachment', 'partner_attachment_rel', 'partner_id_ref', 'attach_ref',
	                                  string="Attachment", help='You can attach the copy of your Id')
	national_id = fields.Char("National ID")
	

class HrEmployeeInherit(models.Model):
	_inherit = 'hr.employee'

	is_driver = fields.Boolean("Is Driver?", help="This Employee is Internal Driver")
	is_picker = fields.Boolean("Is Picker/Packer?", help="This Employee is Picker/ Packer")


class BackendShippingMethod(models.Model):
	_name = 'backend.shipping.method'
	_description = "Tatayab Shipping Method"
	_inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
	_check_company_auto = True

	@api.onchange('order_ids')
	def _count_orders(self):
		for shipping in self:
			shipping.orders_count = len(shipping.order_ids)
			
	name = fields.Char("Shipping Method", required=True)
	ref = fields.Char("Backend Ref")
	driver_type = fields.Selection(DRIVER_TYPE, "Shipping Type", default='international')
	carrier_ids = fields.Many2many(comodel_name="delivery.carrier", string="Carrier", tracking=True,)
	warehouse_id = fields.Many2one('stock.warehouse')
	company_id = fields.Many2one('res.company', default=lambda self: self.env.company, required=True)
	order_ids = fields.One2many('sale.order', 'shipping_method_id', "Orders")
	orders_count = fields.Integer(compute=_count_orders)
	
	def action_open_orders(self):
		action_id = self.env.ref('sale.action_orders')
		action = action_id.read()[0]
		action['domain'] = [('id', '=', self.order_ids.ids)]
		action['context'] = {'search_default_filter_today': 0, 'search_default_group_warehouse_id': 1}
		return action
	
	@api.constrains('ref')
	def _ref_unique(self):
		for rec in self:
			if isinstance(rec.id, int) and self.search_count([('ref', '=', rec.ref), ('id', '!=', rec.id)]):
				raise UserError(_("This Tatayab Shipping Method ref %s is used before, and it must be unique!!!"))

# Ahmed Salama Code End.
