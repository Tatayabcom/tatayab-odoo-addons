# -*- coding: utf-8 -*-
from odoo import models, fields
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->


class AccountInvoiceInherit(models.Model):
	_inherit = 'account.move'
	
	shipping_method_id = fields.Many2one('backend.shipping.method', "Shipping Method",
	                                     check_company=True, tracking=True)

# Ahmed Salama Code End.
