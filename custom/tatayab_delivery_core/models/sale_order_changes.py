# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons.tatayab_operation_status.models.stock_picking_changes import _PICKING_STAGES
import logging

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    shipping_method_id = fields.Many2one('backend.shipping.method', "Shipping Method",
                                         default=lambda self: self.env.ref(
                                             'tatayab_delivery_core.shipping_method_kwt_local'),
                                         check_company=True, tracking=True, required=True,)

    # default = lambda self: self.env.ref('tatayab_delivery_core.shipping_method_kwt_local')
    shipping_type = fields.Selection(related="shipping_method_id.driver_type", store=True)
    national_id = fields.Char("National ID", related='partner_id.national_id',store=True)

    def _prepare_invoice(self):
        """
        Sync shipping_method_id to invoice
        :return: Super
        """
        res = super(SaleOrderInherit, self)._prepare_invoice()
        if self.shipping_method_id:
            res['shipping_method_id'] = self.shipping_method_id.id
        return res

    def action_confirm(self):
        """
        Sync shipping_method_id to picking
        :return: SUPER
        """
        res = super(SaleOrderInherit, self).action_confirm()
        for sale in self:
            if sale.shipping_method_id:
                for picking in sale.picking_ids:
                    picking.shipping_method_id = sale.shipping_method_id.id
        return res
    
    def write(self, vals):
        """
        Update shipping methods on pickings as well
        :return: Super
        """
        if vals.get('shipping_method_id'):
            self.picking_ids.write({'shipping_method_id': vals.get('shipping_method_id')})
        return super(SaleOrderInherit, self).write(vals)

# Ahmed Salama Code End.
