from odoo import fields, models, api


class StockMove(models.Model):
    _inherit = "stock.move"

    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(string="SubTotal",compute='compute_subtotal')

    @api.depends('quantity_done', 'unit_price')
    def compute_subtotal(self):
        for rec in self:
            rec.subtotal = rec.unit_price * rec.quantity_done

class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    demand_qty = fields.Float(string="Demand")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(string="SubTotal",compute='compute_subtotal')
    # property_stock_bin_id = fields.Many2one(comodel_name='stock.location')
    qty_available = fields.Float(related='product_id.qty_available')
    virtual_available = fields.Float(related='product_id.virtual_available')
    # free_qty = fields.Float(related='product_id.free_qty', string="Actual/Free")

    @api.depends('qty_done', 'unit_price')
    def compute_subtotal(self):
        for rec in self:
            rec.subtotal = rec.unit_price * rec.qty_done