# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->
RANDOM_AMOUNTS = {
    'SAR': (100, 200),
    'KWD': (10, 20),
    'USD': (50, 80),
    'BHD': (10, 20),
}


class Carrier(models.Model):
    _inherit = 'delivery.carrier'
    
    warehouse_ids = fields.Many2many(comodel_name='stock.warehouse', string="Warehouse")

    def order_cod_amount(self, order):
        """
        Compute order currency , cod_amount, is cod
        :param order: order ref
        :return:  currency , cod_amount, is cod
        """
        currency, amount = self._get_order_custom_amount(order)
        is_cod = False
        cod_amount = 0.0
        # if order.payment_journal_id and not order.payment_journal_id.auto_payed:
        if order.magento_payment_method_id and order.magento_payment_method_id.is_cod:
            is_cod = True
            cod_amount = amount
        logging.info(bold_red + "Currency: %s, COD amount: %s, Currency: %s" % (is_cod, cod_amount, currency) + reset)
        return is_cod, cod_amount, currency
    
    def _check_international_order(self, order):
        """
        This method will work on check if this order is delivered internal or international
        :param order: order ref
        :return: True/False
        """
        order_country = order.country_id
        warehouse_country = order.warehouse_id.partner_id.country_id
        logging.info(yellow + "%s - %s " % (order_country, warehouse_country) + reset)
        if order_country != warehouse_country:
            return True
        return False
    
    def _get_order_custom_amount(self, order):
        international_order = self._check_international_order(order)
        amount = order.grand_total
        currency = order.customer_currency_id
        if international_order:
            currency = order.global_currency_id
            amount = order.base_grand_total_usd
        return currency, amount
        

class DeliveryLog(models.Model):
    _name = 'delivery.carrier.log'
    _description = "Delivery Carrier Log"
    _rec_name = 'carrier_id'
    
    tracking_ref = fields.Char("Tracking Reference")
    carrier_request = fields.Text("Sent Request")
    log_datetime = fields.Datetime(default=fields.Datetime.now(), string="Time")
    carrier_id = fields.Many2one("delivery.carrier", string="Carrier")
    delivery_type = fields.Selection(related='carrier_id.delivery_type')
# Ahmed Salama Code End.
