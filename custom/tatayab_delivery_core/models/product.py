from odoo import fields, models, api, _
_SEKECTION_VALUE = [('liquid', 'Perfume'),
                    ('cosmetics', 'Cosmetics'),
                    ('electronics', 'Electronics')]


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    description_of_goods = fields.Selection(selection=_SEKECTION_VALUE, string='Type of Goods',
                                            default='liquid', required=True)

