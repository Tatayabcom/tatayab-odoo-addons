# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from .shipping_method import DRIVER_TYPE
import logging

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->
_PICKING_STAGES = [('draft', 'Draft'),
                   ('without_pickings', 'Without Pickings'),
                   ('pick', 'Picking'),
                   ('pack', 'Packing'),
                   ('ship', 'Shipping'),
                   ('done', 'Done Shipment'),
                   ('lock','Locked'),
                   ('cancel', 'Canceled')]


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    @api.onchange('pkg_half', 'pkg_one', 'pkg_two', 'pkg_three')
    def _compute_pkg_count(self):
        for rec in self:
            rec.no_of_boxes = rec.pkg_half + rec.pkg_one + rec.pkg_two + rec.pkg_three

    # Packages Fields
    @api.depends('move_lines')
    @api.onchange('pkg_half', 'pkg_one', 'pkg_two', 'pkg_three')
    def _cal_weight(self):
        for picking in self:
            # TODO: DEFAULT CODE
            # picking.weight = sum(move.weight for move in picking.move_lines if move.state != 'cancel')
            picking.weight = (0.5 * picking.pkg_half) + picking.pkg_one + (2 * picking.pkg_two) + (
                        3 * picking.pkg_three)

    pkg_half = fields.Integer("Half KG")
    pkg_one = fields.Integer("One KG")
    pkg_two = fields.Integer("Two KG")
    pkg_three = fields.Integer("Three KG")
    no_of_boxes = fields.Integer("No. Of Boxes", compute=_compute_pkg_count)
    picking_warehouse_id = fields.Many2one(related='picking_type_id.warehouse_id')

    # Delivery methods
    shipping_method_id = fields.Many2one('backend.shipping.method', "Shipping Method",
                                         check_company=True, tracking=True)
    driver_type = fields.Selection(DRIVER_TYPE, "Shipping Type", default='internal', tracking=True)
    picking_emp_id = fields.Many2one('hr.employee', string="Employee", tracking=True)
    external_driver_id = fields.Many2one('res.partner', string="External Driver", tracking=True,
                                         domain=[('is_driver', '=', True)])
    carrier_tracking_ref = fields.Char(tracking=True)
    carrier_id = fields.Many2one(tracking=True)
    carrier_log_id = fields.Many2one('delivery.carrier.log', "Carrier Log")
    # add by omnya
    type_of_goods = fields.Char(compute='get_goods_type', string='Desc. Of Goods')
    barcode_label = fields.Binary()
    attach_label_id = fields.Many2one("ir.attachment", string="Shipment Label (PDF)")
    attach_commercial_inv_id = fields.Many2one("ir.attachment", string="Commercial Invoice (PDF)")
    api_response = fields.Text("API Response")
    delivery_type = fields.Selection(related='carrier_id.delivery_type', readonly=True)
    total_qty = fields.Float(string="Total Quantity", compute="_compute_all_line_qty", required=False, )
    total_qty_done = fields.Float(string="Total Quantity Done", compute="_compute_all_line_qty", required=False, )
    national_id = fields.Char("National ID", related='partner_id.national_id', store=True)
    def _compute_all_line_qty(self):
        for picking in self:
            if picking.move_ids_without_package:
                for lin in picking.move_ids_without_package:
                    picking.total_qty += lin.product_uom_qty
                    picking.total_qty_done += lin.quantity_done
            else:
                picking.total_qty = 0.0
                picking.total_qty_done = 0.0


    def print_reports(self):
        self.ensure_one()
        ctx = self.env.context
        report_field = ctx.get('report_field')
        if report_field and self.driver_type == 'international':
            report_name = ctx.get('report_name') or 'International Label '
            if self[report_field]:
                return {
                    'type': 'ir.actions.act_url',
                    'url': '/web/content/ir.attachment/%s/%s/%s.%s?download=true' % (
                        self[report_field].id, 'datas', (self.name + '-' + report_name).replace('/', '-'), 'pdf'),
                    'target': 'new'
                }
        else:
            raise Warning(_("Please contact your admin!!! \n This button couldn't effect on this case"))

    def print_picking_order(self):
        sale_orders = self.env['sale.order']
        for picking in self:
            sale_orders += picking.sale_id or picking.group_id.sale_id
        datas = {
            'ids': sale_orders.ids,
            'model': 'sale.order',
            'active_ids': sale_orders.ids,
        }
        return self.env.ref('sale.action_report_saleorder').report_action(sale_orders, data=datas)
    
    def print_commercial_picking_order(self):
        sale_orders = self.env['stock.picking']
        for picking in self:
            sale_orders += picking
        return self.env.ref('tatayab_delivery_core.action_commercial_report_sale_order').report_action(sale_orders)


    @api.onchange('move_ids_without_package', 'move_lines')
    @api.depends('move_ids_without_package.product_id.description_of_goods',
                 'move_lines.product_id.description_of_goods')
    def get_goods_type(self):
        for picking in self:
            list_of_types = []
            type_of_goods = False
            for move_line in picking.move_ids_without_package:
                desc_of_goods = move_line.product_id.product_tmpl_id.description_of_goods
                if desc_of_goods not in list_of_types:
                    list_of_types.append(desc_of_goods)
                    if not type_of_goods:
                        type_of_goods = picking._get_desc_Value(
                            move_line.product_id.product_tmpl_id)  # desc_of_goods.capitalize()
                    else:
                        type_of_goods += " + %s" % picking._get_desc_Value(
                            move_line.product_id.product_tmpl_id)  # desc_of_goods.capitalize()
                    if desc_of_goods == 'electronics':
                        type_of_goods += ' %s' % move_line.product_id.name
            picking.type_of_goods = type_of_goods

    def create_global_shipment(self):
        self.ensure_one()
        if not self.carrier_id:
            raise UserError(_('Please select the carrier.'))
        if not self.driver_type != 'international':
            self.write({'driver_type': 'international'})
        logging.info(blue + "__ Carrier Sending Shipment __" + reset)
        self.carrier_id.send_shipping(self)

    def inform_backend_tracking_ref(self, carrier=False):
        """
        Called From all shipping method modules to inform backend
        :return: True
        """
        # TODO:: This method needs to be back to live to inform backend with tracking ref
        pass
        if self.carrier_tracking_ref and self.driver_type == 'international':
            params = {"tracking_no": self.carrier_tracking_ref,
                      "order_id": self.sale_id.origin,
                      "carrier": carrier or (self.carrier_id and self.carrier_id.name) or ''}
        # tacking_ref_config = self.env.ref('tatayab_multi_address.tracking_ref_sync_config')
        # tacking_ref_config.inform_backend(label="Tracking Ref %s" % self.origin,
        #                                   params=params,
        #                                   method='post', res_ids=[self.id])
        else:
            raise Warning("%s Not valid record to use this method, please check picking details" % self.name)
        return True

    def _get_desc_Value(self, product_id):
        """
        Return field value from selection
        :param product_id:
        :return:
        """
        return dict(product_id._fields['description_of_goods'].selection).get(product_id.description_of_goods)

# Ahmed Salama Code End.
