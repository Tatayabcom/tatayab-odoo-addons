# -*- coding: utf-8 -*-

import logging

from odoo import models, fields, _, api
from odoo.exceptions import ValidationError

logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    stock_bin_ids = fields.One2many("stock.bin.location", 'product_id', string="Bin Locations",
                                    help="Location based on warehouse, so this product could exist"
                                         " on one location per warehouse and, but location should contain"
                                         " only on product, and product exist on one location/warehouse")


class StockBinLocation(models.Model):
    _name = 'stock.bin.location'
    _description = "Stock Bin Location"
    _inherit = ['mail.thread', 'image.mixin']
    _rec_name = 'product_id'
    
    product_id = fields.Many2one('product.template', "Product", tracking=True,  ondelete='cascade')
    warehouse_id = fields.Many2one('stock.warehouse', "Warehouse", compute='_get_location_warehouse', store=True
                                   , tracking=True, ondelete='cascade')
    location_id = fields.Many2one('stock.location', "Location", required=True,
                                  domain=[('usage', '=', 'internal')], tracking=True, ondelete='cascade')
    
    @api.onchange('location_id')
    @api.depends('location_id.location_id')
    def _get_location_warehouse(self):
        """
        Compute warehouse of curent location
        """
        for line in self:
            warehouse_id = False
            if line.location_id:
                warehouse_obj = self.env['stock.warehouse']
                if line.location_id.location_id:
                    warehouse_id = warehouse_obj.search([('lot_stock_id', '=', line.location_id.location_id.id)])
                else:
                    warehouse_id = warehouse_obj.search([('lot_stock_id', '=', line.location_id.id)])
            line.warehouse_id = warehouse_id and warehouse_id[0].id or False
    
    @api.constrains('warehouse_id', 'product_id', 'location_id')
    def _constrain_bin_location(self):
        """
        Check for 2 types of Constrains
        - Location to be used on 2 products
        - TODO:: Stopped Product to have more than 1 bin location ber warehouse
        """
        for line in self:
            if self.env.context.get('avoid_bin_constrains'):
                # TODO:: used to avoid those constrains on create copy data
                return True
            used_location_id = self.search([('product_id', '!=', line.product_id.id),
                                            ('location_id', '=', line.location_id.id)])
            if used_location_id:
                raise ValidationError(_("This Bin Location: %s \n Selected before for product: %s" % (
                    line.location_id.display_name, used_location_id.mapped('product_id.display_name'))))
            # used_product_domain = [('product_id', '=', line.product_id.id),
            #                        ('warehouse_id', '=', line.warehouse_id.id)]
            # if isinstance(line.id, int):
            #     used_product_domain.append(('id', '!=', line.id))
            # used_product_id = self.search(used_product_domain)
            # if used_product_id:
            #     raise ValidationError(_("This Product: %s \n assigned before to bin %s on warehouse: %s"
            #                     % (line.product_id.display_name, used_product_id[0].location_id.display_name,
            #                        line.warehouse_id.display_name)))

# Ahmed Salama Code End.
