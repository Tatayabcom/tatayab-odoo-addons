# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->


class StockPickingChanges(models.Model):
    _inherit = 'stock.picking'
    
    def internal_trans_locations_update(self):
        for picking in self:
            # update Detailed operation locations, update Operation Locations Also
            # Only if picking type is not outgoing...
            for move_id in picking.move_lines.filtered(lambda mv: mv.property_stock_bin_id):
                property_stock_bin_id = move_id.property_stock_bin_id.id
                if property_stock_bin_id:
                    logger.info(yellow + "\n___________Start Update locations Manual" + reset)
                    move_id._update_locations_with_bin_location(property_stock_bin_id)
    
    def write(self, vals):
        """
        Do same core action to change dest and location of picking to be also for picking_type and warehouse
        """
        picking_type_obj = self.env['stock.picking.type']
        res = super(StockPickingChanges, self).write(vals)
        after_vals = {}
        if vals.get('picking_type_id'):
            after_vals['picking_type_id'] = vals['picking_type_id']
            picking_type_id = picking_type_obj.browse(vals['picking_type_id'])
            if picking_type_id:
                after_vals['warehouse_id'] = picking_type_id.warehouse_id.id
        if after_vals:
            self.mapped('move_lines').filtered(lambda move: not move.scrapped).write(after_vals)
        return res
    
    def picking_return_locations_update(self):
        for stock in self:
            # Load Bin location on destination of this operation
            logging.info(yellow + "\n update bin location in moves -------" + reset)
            if stock.picking_type_id.warehouse_id.delivery_steps == 'ship_only' and\
                    stock.is_return and stock.from_picking_id:
                for move_id in stock.move_lines.filtered(lambda mv: mv.property_stock_bin_id):
                    property_stock_bin_id = move_id.property_stock_bin_id.id
                    # logging.info(yellow + "\nBin: %s" % move_id.property_stock_bin_id.display_name + reset)
                    move_vals = {'stock_bin_id': property_stock_bin_id,
                                 'location_dest_id': property_stock_bin_id}
                    logging.info(yellow + "\nmove_vals: " % move_vals + reset)
                    move_id.write(move_vals)
                    move_id.move_line_ids.write(move_vals)
    
    def action_update_bin_location(self):
        """
        Update Bin location using button
        """
        self.move_lines.get_property_stock_bin_location()


class StockLocationChanges(models.Model):
    _inherit = 'stock.location'

    @api.onchange('product_bin_ids')
    def compute_product_using_bin(self):
        for bin in self:
            bin.used_in_products = len(bin.product_bin_ids)
    
    barcode = fields.Char(string="Barcode", required=False, related='name')
    used_in_products = fields.Integer(compute='compute_product_using_bin',
                                      string="Used In Products", store=True)
    product_bin_ids = fields.One2many("stock.bin.location", 'location_id', string="Bin Products",
                                      help="bin locations which use this location for product")
    
    @api.constrains('name')
    def _identify_same_name_location(self):
        for record in self:
            domain = [('name', '=ilike', record.name),('company_id','=',record.company_id.id)]
            if isinstance(record.id, int):
                domain.append(('id', '!=', record.id))
            if record.location_id:
                domain.append(('location_id', '=', record.location_id.id))
            same_location_name = self.search(domain, limit=1)
            if same_location_name:
                raise ValidationError(_("This Location Name [%s] Already Exist!!!\n Current Location: %s" %
                                        (record.name, same_location_name.display_name)))
    
    
class StockMoveInherit(models.Model):
    _inherit = 'stock.move'

    @api.onchange('product_id', 'warehouse_id', 'picking_type_id')
    @api.depends('product_id.stock_bin_ids')
    def get_property_stock_bin_location(self):
        for move in self:
            property_stock_bin_id = move._get_property_stock_bin()
            move.property_stock_bin_id = property_stock_bin_id
            move.stock_bin_id = property_stock_bin_id

    def _get_property_stock_bin(self):
        property_stock_bin_id = False
        warehouse_id = self.picking_type_id.warehouse_id
        product_bins = self.product_id.stock_bin_ids
        if product_bins:
            bin_ids = product_bins.filtered(lambda b: b.location_id.location_id == warehouse_id.lot_stock_id)
            if len(bin_ids) > 1:
                raise ValidationError(_("This Product[%s] exist on more that 1 bin/warehouse [%s]"
                                % (self.product_id.display_name,
                                   bin_ids.mapped('location_id.display_name'))))
            if bin_ids:
                property_stock_bin_id = bin_ids[0].location_id.id
        return property_stock_bin_id
    
    property_stock_bin_id = fields.Many2one('stock.location', "Bin Location(COMP)",
                                            compute='get_property_stock_bin_location')
    stock_bin_id = fields.Many2one('stock.location', "Bin Location")
    
    def _update_locations_with_bin_location(self, property_stock_bin_id):
        move_vals = {'stock_bin_id': property_stock_bin_id}
        if self.env.context.get('update_destination') \
                and self.location_dest_id.id != property_stock_bin_id:
            move_vals['location_dest_id'] = property_stock_bin_id
        elif self.env.context.get('update_location') \
                and self.location_id.id != property_stock_bin_id:
            move_vals['location_id'] = property_stock_bin_id
        logger.info(yellow + "\n new locations: %s" % move_vals + reset)
        self.write(move_vals)
        self.move_line_ids.write(move_vals)


class StockMoveLineInherit(models.Model):
    _inherit = 'stock.move.line'
    
    property_stock_bin_id = fields.Many2one('stock.location', "Bin Location", compute='get_property_stock_bin_location')
    stock_bin_id = fields.Many2one('stock.location', "Bin Location",related='move_id.stock_bin_id')
    
    @api.onchange('product_id', 'move_id')
    @api.depends('product_id.stock_bin_ids', 'move_id.warehouse_id')
    def get_property_stock_bin_location(self):
        for line in self:
            property_stock_bin_id = line.move_id._get_property_stock_bin()
            line.property_stock_bin_id = property_stock_bin_id
            line.stock_bin_id = property_stock_bin_id


class StockReturnPickingInherit(models.TransientModel):
    _inherit = 'stock.return.picking'

    def _create_returns(self):
        """
        Update destination location on move lines to be same as source of main picking
        :return: Super
        """
        picking_obj = self.env['stock.picking']
        new_picking_id, picking_type_id = super(StockReturnPickingInherit, self)._create_returns()
        return_picking = picking_obj.browse(new_picking_id)
        for return_move in return_picking.move_lines:
            match_move = self.picking_id.move_line_ids.filtered(lambda ml: ml.product_id == return_move.product_id)
            if match_move:
                for m_move in match_move:
                    logging.info(yellow + "\nNEW Return Dest LOCATION: %s Type:%s"
                                 % (m_move.location_id.name, m_move.location_id.usage) + reset)
                    move_vals = {'location_dest_id': m_move.location_id.id}
                    return_move.write(move_vals)
                    return_move.move_line_ids.write(move_vals)
        return new_picking_id, picking_type_id

# Ahmed Salama Code End.
