# -*- coding: utf-8 -*-
{
    'name': "Tatayab Product Bin Location",
    'summary': """Manage Bin Locations With Multi Company""",
    'author': "Tatayab, Ahmed Salama",
    'website': "https://tatayab.com",
    'category': 'Stock',
    'version': '1.0',
    'depends': ['stock', 'product'],
    'data': [
        'security/stock_bin_location_security.xml',
        'security/ir.model.access.csv',
        
        'views/product_template.xml',
        'views/picking_view_changes.xml',
        'views/stock_bin_view.xml',
        # TODO: To be activated once tatayab_shipping_methods & tatayab_accounting_changes activated
        # 'reports/sale_report_template_changes.xml',
        # 'reports/report_invoice_changes.xml',
    ],
}
