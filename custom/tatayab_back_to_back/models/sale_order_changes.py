# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, _, api
from odoo.exceptions import UserError
from odoo.tools import float_compare

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class SaleOrderInherit(models.Model):
	_inherit = 'sale.order'
	
	btb_rfq = fields.Char("BTB RfQ")
	# is_create_po = fields.Boolean(string="Create PO",tracking=True)
	
	def action_confirm(self):
		"""
		Append main method of confirm order to handle BTB lines which have no available qty
		:param journal_id:
		:return: True
		"""
		# res = super(SaleOrderInherit, self).action_confirm()
		logging.info(yellow + "\n==========B2B Part =========\n" + reset)
		warehouses = self.env['ir.config_parameter'].sudo().get_param('btb_warehouse_ids')
		btb_warehouses = warehouses and eval(warehouses) or False
		for order in self:
			if btb_warehouses and order.warehouse_id.id in btb_warehouses:  # Only work with KWT warehouse orders
				btb_lines = order.order_line.filtered(
					lambda line: line.product_id.product_tmpl_id.is_btb)
				btb_lines and logging.info(blue + "\n===================\n BTB Lines: %s "
				                           % btb_lines.mapped('product_id.default_code') + reset)
				if btb_lines:
					if order.is_approved:
						order._apply_btb_logic(btb_lines)
					else:
						# In this case btb wont create bo waiting for abbroval
						message = "<b style='color:orange'>BTB Missing Approval</b><br/>for lines with products %s" % btb_lines.mapped(
							'product_id.default_code')
						logging.info(red + message + reset)
						order.message_post(body=message)
		res = super(SaleOrderInherit, self).action_confirm()
		return res

	def confirm_po_after_approve(self):
		"""
        This method will be implemented on tatayab_back_to_back
        :return: True
        """
		logging.info(yellow + "\n==========B2B Part after approve order  =========\n" + reset)
		warehouses = self.env['ir.config_parameter'].sudo().get_param('btb_warehouse_ids')
		btb_warehouses = warehouses and eval(warehouses) or False
		for order in self:
			if btb_warehouses and order.warehouse_id.id in btb_warehouses:  # Only work with KWT warehouse orders
				btb_lines = order.order_line.filtered(
					lambda line: line.product_id.product_tmpl_id.is_btb)
				btb_lines and logging.info(blue + "\n===================\n BTB Lines: %s "
				                           % btb_lines.mapped('product_id.default_code') + reset)
				if btb_lines:
					if order.is_approved :
						order._apply_btb_logic_cod(btb_lines)
					else:
						# In this case btb wont create bo waiting for abbroval
						message = "<b style='color:orange'>BTB Missing Approval SO</b><br/>for lines with products %s" % btb_lines.mapped(
							'product_id.default_code')
						logging.info(red + message + reset)
						order.message_post(body=message)
		return True
	def _apply_btb_logic(self, btb_lines):
		for line in btb_lines:
			quantity = line.product_uom_qty - line.free_qty_today
			# if line.free_qty_today <= 0:
			# 	quantity = line.product_uom_qty
			if line.order_id.operation_state in ['draft','pick']:
				if quantity > 0 and not line.purchase_line_ids:
					if quantity > line.product_id.seller_total_qty:
						message = "Products %s total seller qty:%s" \
								  " is less than requested qty %s" % \
								  (line.product_id.default_code, line.product_id.seller_total_qty, quantity)
						self.message_post(body="<b style='color:red'>BTB Logic Issue: </b><br/><span>%s</span>" % message)
						logging.info(red + message + reset)
					# raise UserError(_(message))
					logging.info(blue + "\n lines that need RFQ: %s QTY: %s" %
								 (line.mapped('product_id.display_name'), quantity) + reset)
					result = line._create_btb_rfq(quantity)
					if result == "Done":
						message = "<b style='color:green'>BTB Logic Applied: </b><br/>for lines with products %s On RFQ's: %s" % (
						line.product_id.default_code, line.mapped('purchase_line_ids.order_id.name'))
					else:
						message = result
					self.message_post(body=message)
				else:
					line.order_id.write({'btb_rfq': 'no need for rfq'})
	def _apply_btb_logic_cod(self, btb_lines):
		for line in btb_lines:
			quantity = 0.0
			if line.free_qty_today <0:
				quantity = line.product_uom_qty
			else:
				free_old = line.free_qty_today - line.product_uom_qty
				quantity = line.product_uom_qty - line.free_qty_today
			print("quantity=============",quantity)
			# if line.free_qty_today <= 0:
			# 	quantity = line.product_uom_qty
			if line.order_id.operation_state in ['draft','pick']:
				if quantity > 0 and not line.purchase_line_ids:
					if quantity > line.product_id.seller_total_qty:
						message = "Products %s total seller qty:%s" \
								  " is less than requested qty %s" % \
								  (line.product_id.default_code, line.product_id.seller_total_qty, quantity)
						self.message_post(body="<b style='color:red'>BTB Logic Issue: </b><br/><span>%s</span>" % message)
						logging.info(red + message + reset)
					# raise UserError(_(message))
					logging.info(blue + "\n lines that need RFQ: %s QTY: %s" %
								 (line.mapped('product_id.display_name'), quantity) + reset)
					result = line._create_btb_rfq(quantity)
					if result == "Done":
						message = "<b style='color:green'>BTB Logic Applied:: </b><br/>for lines with products %s On RFQ's: %s" % (
						line.product_id.default_code, line.mapped('purchase_line_ids.order_id.name'))
					else:
						message = result
					self.message_post(body=message)
				else:
					line.order_id.write({'btb_rfq': 'no need for rfq'})

	def check_btb_logic(self):
		btb_warehouses = eval(self.env['ir.config_parameter'].sudo().get_param('btb_warehouse_ids'))
		if self.warehouse_id.id in btb_warehouses:  # Only work with KWT warehouse orders
			btb_lines = self.order_line.filtered(
				lambda line: line.product_id.product_tmpl_id.is_btb)
			# btb_lines.btb_action_cancel()  # Call cancel logic to insure no old rfq is exits
			btb_lines and logging.info(green + "\n===================\n BTB Lines: %s "
			                           % btb_lines.mapped('product_id.default_code') + reset)
			if btb_lines:
				self._apply_btb_logic(btb_lines)
	
	def action_cancel(self):
		super(SaleOrderInherit, self).action_cancel()
		for order in self:
			if order.purchase_order_count:
				btb_ids = order.order_line.filtered(lambda line: line.product_id.is_btb and line.purchase_line_count)
				if btb_ids:
					try:
						btb_ids.btb_action_cancel()
					
					except Exception as e:
						message = "<b style='color:red'>Error while cancel</b>Couldn't cancel " \
						          "related purchase order lines due to:</b><br/> %s" % str(e)
						order.message_post(body=message)
						logging.info(red + "Order:%s %s" % (order.display_name, message) + reset)
	
	show_create_btb = fields.Boolean()  # TODO:: to be removed
	is_approved = fields.Boolean("Approved?", default=False, tracking=True,
	                             help="This field used to mark orders that is approved for warehouse to be used")


class SaleOrderLineInherit(models.Model):
	_inherit = 'sale.order.line'
	
	def _create_btb_rfq(self, quantity):
		"""
		Create BTB RFQ with sale_purchase core method with exact needed qty
		"""
		# Todo:: Simple Solution
		# self._purchase_service_create(quantity)
		
		# Todo:: Complicated Solution
		precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
		alkhalij_countries = self.env['ir.config_parameter'].sudo().get_param('alkhalij_country_ids')
		alkhalij_country = alkhalij_countries and eval(alkhalij_countries) or False
		# B2B multi warehouse kwt and saudi
		seller_details = []
		code = 'KWT'
		all_seller_lines = self.product_id._prepare_sellers(params=False) \
			.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
		logging.info(yellow + "\n all_seller_lines: %s" % all_seller_lines + reset)
		if (not self.order_id.country_id.is_ksa_country) and (not self.order_id.country_id.is_qat_country) and (self.order_id.country_id.id not in alkhalij_country):
			code = 'KWT'
			all_seller_lines = self.product_id._prepare_sellers(params=False) \
				.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
			for seller in all_seller_lines:
				if seller.curr_qty >= quantity:
					seller_details.append({'seller': seller.name.name, 'qty': seller.curr_qty, 'supplierinfo': seller})
			if not seller_details:
				seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
									   'supplierinfo': seller_line} for seller_line in all_seller_lines]

		else:
			if self.order_id.country_id.id in alkhalij_country:
				code = 'KWT'
				seller_details_ksa = []
				all_seller_lines = self.product_id._prepare_sellers(params=False) \
					.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
				for seller in all_seller_lines:
					if seller.curr_qty >= quantity:
						seller_details.append(
							{'seller': seller.name.name, 'qty': seller.curr_qty, 'supplierinfo': seller})
				if not seller_details:
					all_ksa_seller_lines = self.product_id._prepare_sellers(params=False) \
						.filtered(lambda sl: sl.warehouse_id.code == 'KSA').sorted('price')
					for line in all_ksa_seller_lines:
						if line.curr_qty >= quantity:
							seller_details_ksa.append(
								{'seller': line.name.name, 'qty': line.curr_qty, 'supplierinfo': line})
					if not seller_details_ksa:
						seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
										   'supplierinfo': seller_line} for seller_line in all_ksa_seller_lines]
					else:
						seller_details = seller_details_ksa

		# TO HANDEL KSA B2B ORDER
		if self.order_id.country_id.is_ksa_country:
			code = 'KSA'
			all_seller_lines_ksa = self.product_id._prepare_sellers(params=False) \
				.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
			if all_seller_lines_ksa:
				all_seller_lines = all_seller_lines_ksa
		# Check if any vendor have tot price first
			seller_details = []
			for seller in all_seller_lines:
				if seller.curr_qty >= quantity:
					seller_details.append({'seller': seller.name.name, 'qty': seller.curr_qty, 'supplierinfo': seller})
			# Work with multiple
			if not seller_details:
				seller_details_ksa = []
				if code == 'KSA':
					all_seller_lines_kwt = self.product_id._prepare_sellers(params=False) \
						.filtered(lambda sl: sl.warehouse_id.code == 'KWT').sorted('price')
					for line in all_seller_lines_kwt:
						if line.curr_qty >= quantity:
							seller_details_ksa.append(
								{'seller': line.name.name, 'qty': line.curr_qty, 'supplierinfo': line})
					if not seller_details_ksa:
						seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
										   'supplierinfo': seller_line} for seller_line in all_seller_lines_kwt]
					else:
						seller_details = seller_details_ksa

				else:

					seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
								   'supplierinfo': seller_line} for seller_line in all_seller_lines]
		# TO HANDEL QATER B2B ORDER
		if self.order_id.country_id.is_qat_country:
			code = 'QAT'
			all_seller_lines_qat = self.product_id._prepare_sellers(params=False) \
				.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
			if all_seller_lines_qat:
				all_seller_lines = all_seller_lines_qat
		# Check if any vendor have tot price first
			seller_details = []
			for seller in all_seller_lines:
				if seller.curr_qty >= quantity:
					seller_details.append({'seller': seller.name.name, 'qty': seller.curr_qty, 'supplierinfo': seller})
			# Work with multiple
			if not seller_details:
				seller_details_qat = []
				seller_details_qat_ksa = []
				if code == 'QAT':
					all_seller_lines_kwt = self.product_id._prepare_sellers(params=False) \
						.filtered(lambda sl: sl.warehouse_id.code == 'KWT').sorted('price')
					for line in all_seller_lines_kwt:
						if line.curr_qty >= quantity:
							seller_details_qat.append(
								{'seller': line.name.name, 'qty': line.curr_qty, 'supplierinfo': line})
					if seller_details_qat:
						seller_details = seller_details_qat

					else:
						all_seller_lines_qat_ksa = self.product_id._prepare_sellers(params=False) \
							.filtered(lambda sl: sl.warehouse_id.code == 'KSA').sorted('price')
						for line in all_seller_lines_qat_ksa:
							if line.curr_qty >= quantity:
								seller_details_qat_ksa.append(
									{'seller': line.name.name, 'qty': line.curr_qty, 'supplierinfo': line})
						if seller_details_qat_ksa:
							seller_details = seller_details_qat_ksa
						else:
							seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
											   'supplierinfo': seller_line} for seller_line in all_seller_lines_kwt]

				else:

					seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
								   'supplierinfo': seller_line} for seller_line in all_seller_lines]
		logging.info(yellow + "\n Filtered seller: %s" % seller_details + reset)
		for detail in seller_details:
			if quantity:
				# Check for date
				if detail['supplierinfo'].date_start and detail['supplierinfo'].date_start > fields.Date.context_today(
						self):
					continue
				# Check for qty that vendor have cover our case or not
				if detail['qty'] >= quantity:  # vendor qty cover remain qty he will cover all demand TODO:: wont happen
					demand_qty = quantity
				elif detail['qty'] and detail['qty'] < quantity:  # vendor qty less than demand take his and complete from other
					demand_qty = detail['qty']
				else:
					continue
				# Compare demand qty and min of this vendor for min qty
				if float_compare(demand_qty, detail['supplierinfo'].min_qty, precision_digits=precision) == -1:
					continue
				quantity -= demand_qty
				detail['qty'] -= demand_qty
				logging.info(yellow + "\n Demand: %s  -->  seller: %s" % (demand_qty, detail['seller']) + reset)
				self.with_context(force_seller=detail['supplierinfo'], btb_logic=True)._purchase_service_create(
					demand_qty)
		if quantity:
			return "<b style='color:red'>BTB Logic Issue: </b><br/>" \
			       "Couldn't found available vendor for quantity: <b>%s</b> for btb product <b>%s</b> " \
			       % (quantity, self.product_id.display_name)  # Some quantity failed to add to rfq
		logging.info(blue + "\n All seller: %s" % seller_details + reset)
		return "Done"  # All quantity have reserved with RFQ
	# def _create_btb_rfq(self, quantity):
	# 	"""
	# 	Create BTB RFQ with sale_purchase core method with exact needed qty
	# 	"""
	# 	# Todo:: Simple Solution
	# 	# self._purchase_service_create(quantity)
	#
	# 	# Todo:: Complicated Solution
	# 	precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
	# 	# B2B multi warehouse kwt and saudi
	# 	seller_details = []
	# 	code = 'KWT'
	# 	all_seller_lines = self.product_id._prepare_sellers(params=False) \
	# 		.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
	# 	logging.info(yellow + "\n all_seller_lines: %s" % all_seller_lines + reset)
	# 	if self.order_id.country_id == self.order_id.warehouse_id.country_id and self.order_id.warehouse_id.code == 'KSA':
	# 		code = 'KSA'
	#
	# 		all_seller_lines_ksa = self.product_id._prepare_sellers(params=False) \
	# 			.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
	# 		if all_seller_lines_ksa:
	# 			all_seller_lines = all_seller_lines_ksa
	# 	if self.order_id.country_id != self.order_id.warehouse_id.country_id and self.order_id.warehouse_id.code == 'KSA':
	# 		code = 'KSA'
	#
	# 		all_seller_lines_ksa = self.product_id._prepare_sellers(params=False) \
	# 			.filtered(lambda sl: sl.warehouse_id.code == code).sorted('price')
	# 		if all_seller_lines_ksa:
	# 			all_seller_lines = all_seller_lines_ksa
	# 	# all_seller_lines = self.product_id._prepare_sellers(params=False) \
	# 	# 	.filtered(lambda sl: sl.warehouse_id == self.order_id.warehouse_id).sorted('price')
	#
	# 	# Check if any vendor have tot price first
	# 	seller_details = []
	# 	for seller in all_seller_lines:
	# 		if seller.curr_qty >= quantity:
	# 			seller_details.append({'seller': seller.name.name, 'qty': seller.curr_qty, 'supplierinfo': seller})
	# 	# Work with multiple
	# 	if not seller_details:
	# 		seller_details_ksa = []
	# 		if code == 'KSA':
	# 			all_seller_lines_kwt = self.product_id._prepare_sellers(params=False) \
	# 				.filtered(lambda sl: sl.warehouse_id.code == 'KWT').sorted('price')
	# 			for line in all_seller_lines_kwt:
	# 				if line.curr_qty >= quantity:
	# 					seller_details_ksa.append(
	# 						{'seller': line.name.name, 'qty': line.curr_qty, 'supplierinfo': line})
	# 			if not seller_details_ksa:
	# 				seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
	# 								   'supplierinfo': seller_line} for seller_line in all_seller_lines_kwt]
	# 			else:
	# 				seller_details = seller_details_ksa
	#
	# 		else:
	#
	# 			seller_details = [{'seller': seller_line.name.name, 'qty': seller_line.curr_qty,
	# 		                   'supplierinfo': seller_line} for seller_line in all_seller_lines]
	# 	logging.info(yellow + "\n Filtered seller: %s" % seller_details + reset)
	# 	for detail in seller_details:
	# 		if quantity:
	# 			# Check for date
	# 			if detail['supplierinfo'].date_start and detail['supplierinfo'].date_start > fields.Date.context_today(
	# 					self):
	# 				continue
	# 			# Check for qty that vendor have cover our case or not
	# 			if detail['qty'] >= quantity:  # vendor qty cover remain qty he will cover all demand TODO:: wont happen
	# 				demand_qty = quantity
	# 			elif detail['qty'] and detail[
	# 				'qty'] < quantity:  # vendor qty less than demand take his and complete from other
	# 				demand_qty = detail['qty']
	# 			else:
	# 				continue
	# 			# Compare demand qty and min of this vendor for min qty
	# 			if float_compare(demand_qty, detail['supplierinfo'].min_qty, precision_digits=precision) == -1:
	# 				continue
	# 			quantity -= demand_qty
	# 			detail['qty'] -= demand_qty
	# 			logging.info(yellow + "\n Demand: %s  -->  seller: %s" % (demand_qty, detail['seller']) + reset)
	# 			self.with_context(force_seller=detail['supplierinfo'], btb_logic=True)._purchase_service_create(
	# 				demand_qty)
	# 	if quantity:
	# 		return "<b style='color:red'>BTB Logic Issue: </b><br/>" \
	# 		       "Couldn't found available vendor for quantity: <b>%s</b> for btb product <b>%s</b> " \
	# 		       % (quantity, self.product_id.display_name)  # Some quantity failed to add to rfq
	# 	logging.info(blue + "\n All seller: %s" % seller_details + reset)
	# 	return "Done"  # All quantity have reserved with RFQ

	def _purchase_service_prepare_order_values(self, supplierinfo):
		"""
		Add extra needed fields
		:param supplierinfo: get best seller according to method _select_seller()
		:return:
		"""
		vals = super(SaleOrderLineInherit, self)._purchase_service_prepare_order_values(supplierinfo)
		vals['warehouse_id'] = self.order_id.warehouse_id.id
		vals['purchase_type_id'] = self.env.ref('tatayab_back_to_back.purchase_type_btb').id
		vals['backend_ref'] = self.order_id.magento_order_reference
		vals['type'] = 'b2b'
		return vals
	
	def _purchase_service_create(self, quantity=False):
		""" On Sales Order confirmation, some lines (services ones) can create a purchase order line and maybe a purchase order.
			If a line should create a RFQ, it will check for existing PO. If no one is find, the SO line will create one, then adds
			a new PO line. The created purchase order line will be linked to the SO line.
			:param quantity: the quantity to force on the PO line, expressed in SO line UoM
		"""
		PurchaseOrder = self.env['purchase.order']
		supplier_po_map = {}
		sale_line_purchase_map = {}
		btb_type = self.env.ref('tatayab_back_to_back.purchase_type_btb')  # TODO: Added by Tatayab
		for line in self:
			product_quantity = line.product_uom_qty  # TODO: Added by Tatayab
			if quantity:  # TODO: Added by Tatayab
				product_quantity = quantity  # TODO: Added by Tatayab
			line = line.with_company(line.company_id)
			# determine vendor of the order (take the first matching company and product)
			if self.env.context.get('force_seller'):  # TODO: Added by Tatayab In case of BTB case
				supplierinfo = self.env.context.get('force_seller')  # TODO: Added by Tatayab
			else:
				suppliers = line.product_id._select_seller(quantity=line.product_uom_qty, uom_id=line.product_uom,
				                                           params=line.order_id.warehouse_id)
				if not suppliers:
					raise UserError(
						_("There is no vendor associated to the product %s. Please define a vendor for this product.") % (
							line.product_id.display_name,))
				supplierinfo = suppliers[0]
			partner_supplier = supplierinfo.name  # yes, this field is not explicit .... it is a res.partner !
			# determine (or create) PO
			purchase_order = supplier_po_map.get(partner_supplier.id)
			if not purchase_order:
				purchase_order = PurchaseOrder.search([
					('partner_id', '=', partner_supplier.id),
					('state', '=', 'draft'),
					('company_id', '=', line.company_id.id),
				], limit=1)
			if not purchase_order:
				values = line._purchase_service_prepare_order_values(supplierinfo)
				purchase_order = PurchaseOrder.create(values)
			# TODO: All next codes Added by Tatayab
			else:  # update origin of existing PO
				so_name = line.order_id.name
				backend_ref = line.order_id.magento_order_reference
				origins = []
				write_vals = {}
				if purchase_order.backend_ref:
					backend_refs = purchase_order.backend_ref.split(', ')
					if backend_ref and backend_ref not in backend_refs:
						backend_refs += [backend_ref]
						write_vals['backend_ref'] = ', '.join(backend_refs)
				else:
					write_vals['backend_ref'] = backend_ref
				if purchase_order.purchase_type_id != btb_type:
					write_vals['purchase_type_id'] = btb_type.id
				if purchase_order.origin:
					origins = purchase_order.origin.split(', ') + origins
				if so_name not in origins:
					origins += [so_name]
					write_vals['origin'] = ', '.join(origins)
				if write_vals:
					purchase_order.write(write_vals)
			supplier_po_map[partner_supplier.id] = purchase_order
			
			# add a PO line to the PO
			values = line._purchase_service_prepare_line_values(purchase_order, quantity=product_quantity)
			purchase_line = line.env['purchase.order.line'].create(values)
			
			# link the generated purchase to the SO line
			sale_line_purchase_map.setdefault(line, line.env['purchase.order.line'])
			sale_line_purchase_map[line] |= purchase_line
			
			# Deduct Qty from Vendor
			supplierinfo._deduct_qty(product_quantity, purchase_order)
			po_ref = purchase_order.display_name
			order_po_ref = False
			if line.order_id.btb_rfq:
				po_refs = line.order_id.btb_rfq.split(', ')
				if po_ref not in po_refs:
					po_refs += [po_ref]
					order_po_ref = ', '.join(po_refs)
			else:
				order_po_ref = po_ref
			if order_po_ref:
				line.order_id.write({'btb_rfq': order_po_ref,

									 })
		return sale_line_purchase_map
	
	is_btb = fields.Boolean(related='product_id.is_btb', readonly=True)
	
	def action_create_btb_purchase(self):
		form_view_id = self.env.ref('purchase.purchase_order_form')
		taxes = self.product_id.supplier_taxes_id
		if taxes:
			taxes = taxes.filtered(lambda t: t.company_id.id == self.company_id.id)
		# TODO:: Stopped as we don't depend on Pack flag on tatayab15
		# if self.pack_line:
		# 	order_lines = []
		# 	for pack_line in self.pack_line_ids:
		# 		if pack_line.product_id.is_btb:
		# 			order_lines.append((0, 0, {
		# 				'name': '[%s] %s' % (
		# 					pack_line.product_id.default_code, self.name) if pack_line.product_id.default_code else self.name,
		# 				'product_qty': pack_line.quantity,
		# 				'product_id': pack_line.product_id.id,
		# 				'product_uom': pack_line.product_id.uom_po_id.id,
		# 				'date_planned': fields.Date.today(),
		# 				'taxes_id': [(6, 0, taxes.ids)],
		# 				'sale_line_id': self.id,
		# 				'sale_order_id': self.order_id.id,
		# 			}))
		# else:
		order_lines = [(0, 0, {
			'name': '[%s] %s' % (
			self.product_id.default_code, self.name) if self.product_id.default_code else self.name,
			'product_qty': self.product_uom_qty,
			'product_id': self.product_id.id,
			'product_uom': self.product_id.uom_po_id.id,
			'date_planned': fields.Date.today(),
			'taxes_id': [(6, 0, taxes.ids)],
			'sale_line_id': self.id,
			'sale_order_id': self.order_id.id,
		})]
		result = {
			'name': "Create BTB Rfq",
			'type': 'ir.actions.act_window',
			'views': [[form_view_id.id, 'form']],
			'target': 'new',
			'res_model': 'purchase.order',
			'context': {'default_warehouse_id': self.order_id.warehouse_id.id,
			            'default_purchase_type_id': self.env.ref('tatayab_back_to_back.purchase_type_btb').id,
			            'default_backend_ref': self.order_id.magento_order_reference,
			            'default_order_line': order_lines}
		}
		return result
	
	def btb_action_cancel(self):
		for line in self:
			# Apply action of return Qty
			line.purchase_line_ids._cancel_btb()
			
			# handle purchase order
			for po_line in line.purchase_line_ids:
				if po_line.product_qty > line.product_uom_qty:
					message = "<span style='color:red'>Cancel Confirmed</span><br/>" \
					          "Po line for product[<b>%s</b>]  Qty has been deduct by [<b>%s</b>] due to" \
					          " cancel BTB order [<b>%s</b>] line that related to it" \
					          % (po_line.product_id.display_name, line.product_uom_qty, line.order_id.display_name)
					po_line.order_id.message_post(body=message)
					po_line.product_qty.write({'product_qty': po_line.product_qty - line.product_uom_qty})
				else:
					message = "<span style='color:red'>Cancel Confirmed</span><br/>" \
					          "Po line for product[<b>%s</b>] has been deleted due to" \
					          " cancel BTB order [<b>%s</b>] line that related to it" \
					          % (po_line.product_id.display_name, line.order_id.display_name)
					po_line.order_id.message_post(body=message)
					po_line.unlink()

# Ahmed Salama Code End.

