# -*- coding: utf-8 -*-
from odoo import models, fields, api,_


# Ahmed Salama Code Start ---->
from odoo.exceptions import ValidationError


class PurchaseOrderInherit(models.Model):
    _inherit = 'purchase.order'

    po_type = fields.Selection(selection=[('b2b', 'B2B'), ('manual', 'Manual'), ('manual_b2b', 'Manual + B2B')],
                               string="Purchase Order Type", compute="compute_po_type", store=True)

    @api.depends('order_line', 'order_line.sale_line_id', 'order_line.sale_order_id')
    def compute_po_type(self):
        for rec in self:
            flag_b2b = 0
            flag_manual = 0
            rec.po_type = False
            for line in rec.order_line:
                if line.sale_line_id or line.sale_order_id:
                    flag_b2b += 1
                else:
                    flag_manual += 1
            if flag_b2b and flag_manual:
                rec.po_type = 'manual_b2b'
            elif flag_b2b:
                rec.po_type = 'b2b'
            elif flag_manual:
                rec.po_type = 'manual'

    def button_first_confirm(self):
        for order in self:
            if not order.partner_id.vendor_code:
                raise ValidationError(_("Please set vendor code for vendor   %s ")%order.partner_id.name)
            if self.env.ref('tatayab_back_to_back.purchase_type_btb').id == self.purchase_type_id.id:
                if any(not line.sale_order_id for line in order.order_line):
                    raise ValidationError(_('You can set sale order for line OR change type of B2B'))
            order.write({'state': 'update_rfq'})
            super(PurchaseOrderInherit, self).button_first_confirm()
        return True


class PurchaseOrderLineInherit(models.Model):
    _inherit = 'purchase.order.line'

    def _cancel_btb(self):
        """
        return qty to supplier info line
        """
        supplier_info_obj = self.env['product.supplierinfo']
        for line in self:
            supplier_info_id = supplier_info_obj.search([('name', '=', line.partner_id.id),
                                                         ('warehouse_id', '=', line.order_id.warehouse_id.id),
                                                         ('product_tmpl_id', '=', line.product_id.product_tmpl_id.id)],
                                                        limit=1)
            if supplier_info_id:
                supplier_info_id.write({'curr_qty': supplier_info_id.curr_qty + line.product_qty})
# Ahmed Salama Code End.
