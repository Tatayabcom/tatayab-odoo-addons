# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, _, api
from odoo.exceptions import Warning

# Ahmed Salama Code Start ---->
logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    is_b2b_pick = fields.Boolean(string="B2B Picking", compute="get_po_type")

    @api.depends('group_id')
    def get_po_type(self):
        for picking in self:
            picking.is_b2b_pick = False
            purchase_order_b2b = self.env['purchase.order'].search([('group_id', '=', picking.group_id.id)])
            for po in purchase_order_b2b:
                if self.env.ref('tatayab_back_to_back.purchase_type_btb').id == po.purchase_type_id.id:
                    picking.is_b2b_pick = True

    def action_done(self):

        res = super(StockPickingInherit, self).action_done()
        for picking in self:
            if picking.picking_type_id.code == 'internal':
                purchase_order_b2b = self.env['purchase.order'].search([('group_id', '=', picking.group_id.id)])
                for po in purchase_order_b2b:
                    for line in po.order_line:
                        if line.sale_order_id:
                            for so_pic in line.sale_order_id.picking_ids:
                                if so_pic.state == 'confirmed' and so_pic.is_approved:
                                    so_pic.action_assign()
        return res

    def b2b_internal_trans_locations_update(self):
        for picking in self:
            # update Detailed operation locations, update Operation Locations Also
            # Only if picking type is not outgoing...
            # for move_id in picking.move_lines.filtered(lambda mv: mv.property_stock_bin_id):
            for move_id in picking.move_lines:
                property_stock_bin_id = move_id.property_stock_bin_id.id
                # if move_id.product_id.is_btb:
                if picking.picking_warehouse_id.code == 'KWT':
                    property_stock_bin_id = int(self.env['ir.config_parameter'].sudo().get_param('btb_location_kwt'))
                if picking.picking_warehouse_id.code == 'KSA':
                    property_stock_bin_id = int(self.env['ir.config_parameter'].sudo().get_param('btb_location_ksa'))
                if property_stock_bin_id:
                    logger.info(yellow + "\n___________Start Update locations Manual b2b" + reset)
                    move_id._update_locations_with_bin_location(property_stock_bin_id)

# Ahmed Salama Code End.
