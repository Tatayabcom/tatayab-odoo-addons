# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
# Ahmed Salama Code Start ---->


class ResPartnerInherit(models.Model):
	_inherit = 'res.partner'
	
	vendor_code = fields.Char("Vendor Code", help="Used to mark vendor with unique code.",
	                          tracking=True)
	ref = fields.Char(help="Used to mark Customer with unique code of Backend")
	
	@api.constrains('vendor_code')
	def vendor_code_constrain(self):
		""" Constrain in SKU it should be unique """
		for partner in self:
			if partner.vendor_code:
				other_vendors = self.env['res.partner'].search(
					[('vendor_code', '=', partner.vendor_code.lstrip().rstrip()),
					 ('id', '!=', partner.id)])
				if other_vendors:
					raise UserError(_("Vendor Code Must Be Unique!!!\n this Code is used on Vendors: \n%s"
					                % '\n- '.join(other_vendors.mapped('name'))))

# Ahmed Salama Code End.
