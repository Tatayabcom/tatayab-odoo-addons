from odoo import models, fields


class BTBResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    btb_warehouse_ids = fields.Many2many('stock.warehouse', relation='btb_stock_warehouse_rel',
                                         string="BTB Warehouses")
    btb_location_kwt = fields.Many2one('stock.location', string="B2B Location KWT")
    btb_location_ksa = fields.Many2one('stock.location', string="B2B Location KSA")
    alkhalij_country_ids = fields.Many2many('res.country',string="ِAlkhalij Countries")

    def get_values(self):
        vals = super(BTBResConfigSettings, self).get_values()
        warehouse_ids = self.env['ir.config_parameter'].sudo().get_param('btb_warehouse_ids')
        warehouse_ids = warehouse_ids and [(6, 0, eval(warehouse_ids))] or False
        country_ids = self.env['ir.config_parameter'].sudo().get_param('alkhalij_country_ids')
        country_ids = country_ids and [(6, 0, eval(country_ids))] or False
        vals.update(btb_warehouse_ids=warehouse_ids,
                    alkhalij_country_ids=country_ids,
                    btb_location_kwt=int(self.env['ir.config_parameter'].sudo().get_param('btb_location_kwt')),
                    btb_location_ksa=int(self.env['ir.config_parameter'].sudo().get_param('btb_location_ksa')),
                    )
        return vals

    def set_values(self):
        super(BTBResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].set_param('btb_warehouse_ids', self.btb_warehouse_ids.ids)
        self.env['ir.config_parameter'].set_param('alkhalij_country_ids', self.alkhalij_country_ids.ids)
        self.env['ir.config_parameter'].set_param('btb_location_kwt', self.btb_location_kwt.id)
        self.env['ir.config_parameter'].set_param('btb_location_ksa', self.btb_location_ksa.id)

class ResCountry(models.Model):
    _inherit = "res.country"

    is_ksa_country = fields.Boolean(string="KSA Country?")
    is_qat_country = fields.Boolean(string="QAT Country?")