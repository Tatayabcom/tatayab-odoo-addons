# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import logging

from odoo import fields, models, _

_logger = logging.getLogger('MagentoEPT')
from odoo.exceptions import UserError
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class ProductProductInherit(models.Model):
    _inherit = 'product.product'

    def _select_seller(self, partner_id=False, quantity=0.0, date=None, uom_id=False, params=False):
        self.ensure_one()
        if date is None:
            date = fields.Date.context_today(self)
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')

        res = self.env['product.supplierinfo']
        sellers = self._prepare_sellers(params)
        sellers = sellers.filtered(lambda s: not s.company_id or s.company_id.id == self.env.company.id)
        for seller in sellers.sorted('price'):
            # Set quantity in UoM of seller
            quantity_uom_seller = quantity
            if quantity_uom_seller and uom_id and uom_id != seller.product_uom:
                quantity_uom_seller = uom_id._compute_quantity(quantity_uom_seller, seller.product_uom)

            if seller.date_start and seller.date_start > date:
                continue
            if seller.date_end and seller.date_end < date:
                continue
            if partner_id and seller.name not in [partner_id, partner_id.parent_id]:
                continue
            # Compare with current available qty on vendor
            if self.env.context.get('btb_logic') and float_compare(quantity_uom_seller, seller.curr_qty,
                                                                   precision_digits=precision) == 1:
                continue
            if quantity is not None and float_compare(quantity_uom_seller, seller.min_qty,
                                                      precision_digits=precision) == -1:
                continue
            if seller.product_id and seller.product_id != self:
                continue
            if not res or res.name == seller.name:
                res |= seller
        return res.sorted('price')[:1]

    def action_seller_view(self):
        action_id = self.env.ref('product.product_supplierinfo_type_action')
        action = action_id.read()[0]
        # action['context'] = {'default_product_tmpl_id', '=', self.mapped('product_tmpl_id.id')[0]}
        action['domain'] = [('product_tmpl_id', 'in', self.mapped('product_tmpl_id.id'))]
        return action


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    is_btb = fields.Boolean("Is BTB?", tracking=True,
                            help="Product that is marked as Back to back\n"
                                 " to be order from vendor when it received on Sales order")

    @api.constrains('is_btb')
    def btb_vendor_pricelist_constrain(self):
        """
        Constrain to raise warning in case of assign product to is btb without any vendor pricelist mentioned
        :return:
        """
        for product in self:
            if isinstance(product.id, int) and product.is_btb and not len(product.seller_ids):
                raise UserError(_("This product %s is Back 2 Back product,"
                                  " at least one vendor price list must be mentioned" % product.display_name))

    @api.onchange('seller_ids')
    @api.depends('seller_ids.curr_qty')
    def _get_total_seller_qty(self):
        for temp in self:
            temp.seller_total_qty = sum(seller.curr_qty for seller in temp.seller_ids)
            temp.count_seller = len(temp.seller_ids)
            temp.kwt_seller_qty = sum(seller.curr_qty for seller in temp.seller_ids if seller.warehouse_id.code == 'KWT')
            temp.ksa_seller_qty = sum(seller.curr_qty for seller in temp.seller_ids if seller.warehouse_id.code == 'KSA')
            temp.qatar_seller_qty = sum(seller.curr_qty for seller in temp.seller_ids if seller.warehouse_id.code == 'QAT')

            temp.ksa_total_free_qty = temp.ksa_free_qty + temp.ksa_seller_qty
            temp.kwt_total_free_qty = temp.kwt_free_qty + temp.kwt_seller_qty
            temp.qatar_total_free_qty = temp.qatar_free_qty + temp.qatar_seller_qty
            temp.ksa_total_on_hand_qty = temp.ksa_on_hand_qty + temp.ksa_seller_qty
            temp.kwt_total_on_hand_qty = temp.kwt_on_hand_qty + temp.kwt_seller_qty
            temp.qatar_total_on_hand_qty = temp.qatar_on_hand_qty + temp.qatar_seller_qty



    seller_total_qty = fields.Float(compute=_get_total_seller_qty)
    count_seller = fields.Integer(compute=_get_total_seller_qty, string="Count", store=True)
    ksa_seller_qty = fields.Float(string="KSA Seller Qty",compute=_get_total_seller_qty,store=True)
    kwt_seller_qty = fields.Float(string="KWT Seller Qty",compute=_get_total_seller_qty,store=True)
    qatar_seller_qty = fields.Float(string="QAT Seller Qty",compute=_get_total_seller_qty,store=True)
    # to get total for each warehouse
    ksa_total_free_qty = fields.Float(string="KSA Total Free Qty",compute=_get_total_seller_qty,store=True)
    kwt_total_free_qty = fields.Float(string="KWT Total Free Qty",compute=_get_total_seller_qty,store=True)
    qatar_total_free_qty = fields.Float(string="QAT TotalFree Qty",compute=_get_total_seller_qty,store=True)
    ksa_total_on_hand_qty = fields.Float(string="KSA Total On Hand Qty",compute=_get_total_seller_qty)
    kwt_total_on_hand_qty = fields.Float(string="KWT Total On Hand Qty",compute=_get_total_seller_qty)
    qatar_total_on_hand_qty = fields.Float(string="QAT Total On Hand Qty",compute=_get_total_seller_qty)


    def action_seller_view(self):
        action_id = self.env.ref('product.product_supplierinfo_type_action')
        action = action_id.read()[0]
        action['domain'] = [('product_tmpl_id', 'in', self.ids)]
        return action

    def _get_seller_warehouse(self, warehouse_code):
        """
        compute seller current qty according to it's warehouse
        :param warehouse_code: unique code of warehouse
        :return:
        """
        return sum(
            seller.curr_qty for seller in self.seller_ids.filtered(lambda sl: sl.warehouse_id.code == warehouse_code))

    def _get_product_inform_details(self, prod_config_id, warehouse_name, warehouse_code):
        """
        Append Seller qty to dict of informing for backend
        :param prod_config_id: sync config object
        :param warehouse_name: active warehouse name
        :param warehouse_code: active warehouse code
        :return:
        """
        details = super(ProductTemplateInherit, self)._get_product_inform_details(prod_config_id, warehouse_name, warehouse_code)
        if self.is_btb:  # todo: to be removed and depend on dynamic warehouse  /*and warehouse_code == 'KWT'*/
            details['seller_qty'] = self._get_seller_warehouse(warehouse_code)
        return details


class ProductSupplierInfoInherit(models.Model):
    _inherit = 'product.supplierinfo'

    curr_qty = fields.Float("Current Quantity", help="Current Available Qty on Vendor side,"
                                                     " if it's below demand this vendor will skiped")
    min_qty = fields.Float("Minimum Quantity")
    update_date = fields.Datetime("Updated @")

    def _get_default_warehouse_id(self):
        return self.env['stock.warehouse'].search([('code', '=', 'KWT')], limit=1).id

    warehouse_id = fields.Many2one('stock.warehouse', "Warehouse", default=_get_default_warehouse_id, required=True)

    def _deduct_qty(self, product_quantity, rfq):
        curr_qty = self.curr_qty - product_quantity
        update_vals = {'curr_qty': curr_qty > 0 and curr_qty or 0,
                       'update_date': fields.Datetime.now()}
        self.write(update_vals)
        message = "Updated for supplier: [<b>%s</b>] Successfully, Product [<b>%s</b>] current qty [%s]" % \
                  (self.name.display_name, self.product_tmpl_id.default_code, curr_qty)
        self.product_tmpl_id.message_post(body="%s by create rfq: [<b>%s</b>] " %
                                               (message, rfq.display_name))
# Ahmed Salama Code End.

class MagentoProductProduct(models.Model):
    """
    Describes fields and methods for Magento products
    """
    _inherit = 'magento.product.product'

    # this method for testing only before push code in server
    def get_magento_product_stock_ept_odoo(self):
        """
        This Method relocates check type of stock.
        :param instance: This arguments relocates instance of amazon.
        :param product_ids: This arguments product listing id of odoo.
        :param warehouse:This arguments relocates warehouse of amazon.
        :return: This Method return product listing stock.
        """
        instance = self.env['magento.instance'].search([('id', '=', '6')])
        product = self.env['product.product'].browse(541)
        product_ids = [541]
        warehouse = self.env['stock.warehouse'].browse(2)
        stock = {}
        if product_ids:
            if instance.magento_stock_field == 'free_qty':
                stock_product = product.get_free_qty_ept(warehouse, product_ids)
                stock = {}
                for key in stock_product.keys():
                    product_object = self.env['product.product'].browse(key)
                    if product_object.is_btb:
                        seller_qty = 0
                        for line in product_object.seller_ids:
                            if line.warehouse_id == warehouse:
                                seller_qty += line.curr_qty
                        seller_total_qty = stock_product[key] + seller_qty
                        stock.update({key: seller_total_qty})
                    else:
                        seller_total_qty = stock_product[key]
                        stock.update({key: seller_total_qty})
            elif instance.magento_stock_field == 'virtual_available':
                stock = product.get_forecasted_qty_ept(warehouse, product_ids)
        return stock

    # def get_magento_product_stock_ept(self, instance, product_ids, warehouse):
    #     """
    #     This Method relocates check type of stock.
    #     :param instance: This arguments relocates instance of amazon.
    #     :param product_ids: This arguments product listing id of odoo.
    #     :param warehouse:This arguments relocates warehouse of amazon.
    #     :return: This Method return product listing stock.
    #     """
    #     product = self.env['product.product']
    #     stock = {}
    #     if product_ids:
    #         if instance.magento_stock_field == 'free_qty':
    #             stock_product = product.get_free_qty_ept(warehouse, product_ids)
    #             # ==================
    #             stock = {}
    #             for key in stock_product.keys():
    #                 product_object = self.env['product.product'].browse(key)
    #                 if product_object.is_btb:
    #                     seller_total_qty = 0.0
    #                     if product_object.seller_ids:
    #                         for line in product_object.seller_ids:
    #                             if line.warehouse_id == warehouse:
    #                                 # seller_qty += line.curr_qty
    #                                 stock_product[key] += line.curr_qty
    #                                 stock.update({key: stock_product[key]})
    #                     else:
    #                         seller_total_qty = stock_product[key]
    #                         stock.update({key: seller_total_qty})
    #                 else:
    #                     seller_total_qty = stock_product[key]
    #                     stock.update({key: seller_total_qty})
    #
    #             # ==================
    #         elif instance.magento_stock_field == 'virtual_available':
    #             stock = product.get_forecasted_qty_ept(warehouse, product_ids)
    #     return stock

    def get_magento_product_stock_ept(self, instance, product_ids, warehouse):
        """
        This Method relocates check type of stock.
        :param instance: This arguments relocates instance of amazon.
        :param product_ids: This arguments product listing id of odoo.
        :param warehouse:This arguments relocates warehouse of amazon.
        :return: This Method return product listing stock.
        """
        product = self.env['product.product']
        stock = {}
        if product_ids:
            if instance.magento_stock_field == 'free_qty':
                stock_product = product.get_free_qty_ept(warehouse, product_ids)
                # ==================
                stock = {}
                for key in stock_product.keys():
                    product_object = self.env['product.product'].browse(key)
                    if product_object.is_btb:
                        seller_total_qty = 0.0
                        if product_object.seller_ids:
                            for line in product_object.seller_ids:
                                if line.warehouse_id == warehouse:
                                    # seller_qty += line.curr_qty
                                    stock_product[key] += line.curr_qty
                                    stock.update({key: stock_product[key]})
                        else:
                            seller_total_qty = stock_product[key]
                            stock.update({key: seller_total_qty})
                    else:
                        seller_total_qty = stock_product[key]
                        stock.update({key: seller_total_qty})

                # ==================
            elif instance.magento_stock_field == 'virtual_available':
                stock = product.get_forecasted_qty_ept(warehouse, product_ids)
            elif instance.magento_stock_field == 'onhand_qty':
                stock_product = product.get_onhand_qty_ept(warehouse, product_ids)
                # stock = {}
                for key in stock_product.keys():
                    product_object = self.env['product.product'].browse(key)
                    if product_object.is_btb:
                        if product_object.seller_ids:
                            for line in product_object.seller_ids:
                                if line.warehouse_id == warehouse:
                                    stock_product[key] += line.curr_qty
                                    stock.update({key: stock_product[key]})
                                else:
                                    seller_total_qty = stock_product[key]
                                    stock.update({key: seller_total_qty})
                        else:
                            seller_total_qty = stock_product[key]
                            stock.update({key: seller_total_qty})
                    else:
                        seller_total_qty = stock_product[key]
                        stock.update({key: seller_total_qty})
        return stock