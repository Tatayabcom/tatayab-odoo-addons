import binascii
import logging
import tempfile
import xlrd
from datetime import datetime

from odoo import fields, models, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)
COLORS = ["red", "orange", 'blue', 'green']

REACH_HOUR_START = datetime.now().replace(hour=6)  # From 8 AM KWI TIME
REACH_HOUR_END = datetime.now().replace(hour=16)  # TO 6 PM KWI TIME


class SupplierInfoUpdateWizard(models.TransientModel):
    _name = 'supplier.info.update.wizard'
    _description = "Supplier Info Update Wizard"
    
    supplier_info = fields.Binary('Uploaded File')
    
    with_price = fields.Boolean("Price Update", help="Update price with vendor qty")
    is_uploaded = fields.Boolean('uploaded?')
    
    action_message = fields.Html("Results...")
    
    def _read_file_details(self):
        """Load Inventory data from the CSV file."""
        keys = ['sku', 'ref', 'quantity']
        if self.with_price:
            keys.append('price')
        fp = tempfile.NamedTemporaryFile(suffix=".xlsx")
        fp.write(binascii.a2b_base64(self.supplier_info))
        fp.seek(0)
        workbook = xlrd.open_workbook(fp.name)
        sheet = workbook.sheet_by_index(0)
        # we done need the file header to be imported as data so we start from range index 1
        values = []
        for row_no in range(0, sheet.nrows):
            # isinstance(row.value, str) and row.value.encode('utf-8') or
            line = (map(lambda row: row.value, sheet.row(row_no)))
            line_value = dict(zip(keys, line))
            values.append(line_value)
        return values
    
    def import_supplier_info(self):
        """
        Action Import Data From Uploaded file
        :return: Message
        """
        values = self._read_file_details()
        if values:
            self._process_data(values)
            # RETURN VIEW WITH FILE AND HIDE FILTER BUTTONS
            return self.re_open_wizard()
        else:
            raise UserError(_("No Valid Records Found"))
    def update_vendor_quantity_product_in_magento_ept(self,product):
        """
        This method are used to export stock in Magento. And it will be called from Multi Action
        Wizard in Magento product tree view.
        :return:
        """
        m_template = self.env['magento.product.template']
        m_export_product = self.env['magento.export.product.ept']
        odoo_product_tem = []
        # if self.env.context.get('active_model') == 'product.template':
        # self._context.get('params').get('model')
        odoo_product_tem = [product.id]
        product_magento = m_template.search([('odoo_product_template_id', 'in', odoo_product_tem)]).ids
        m_product_ids = product_magento
        instances = self.env['magento.instance'].search([])
        logs = list()
        m_templates = m_template.search([('id', 'in', m_product_ids),
                                         ('sync_product_with_magento', '=', True)])
        for instance in instances:
            log = instance.create_common_log_book('export', 'magento.product.template')
            product_ids = m_templates.mapped('magento_product_ids.odoo_product_id').ids
            m_export_product.export_product_stock_magento(instance, product_ids, log)
            if log and not log.log_lines:
                log.unlink()
            else:
                logs.append(log.id)
        if logs:
            return {
                'name': 'Export Product Log',
                'type': 'ir.actions.act_window',
                'res_model': 'common.log.book.ept',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'domain': [('id', 'in', logs)],
            }
        message = " update quantity from (upload Sheet ) "
        product.message_post(body="<b style='color:green'>Update Quantity To Magento : </b><br/><span>%s</span>" % message)

        return {
            'effect': {
                'fadeout': 'slow',
                'message': " 'Update Product stocks(s) in Magento' Process Completed Successfully! {}".format(
                    ""),
                'img_url': '/web/static/src/img/smile.svg',
                'type': 'rainbow_man',
            }
        }
    def _process_data(self, values):
        """
        Apply data on pickings
        :param values: List of dictionaries
        """
        action_message = "<ol>"
        supplier_obj = self.env['res.partner']
        product_obj = self.env['product.template']
        supplier_info_obj = self.env['product.supplierinfo']
        _logger.info("______ Proceed Supplier Info Update ______")
        for idx, record in enumerate(values):
            # Find Supplier
            vendor_code = str(record.get('ref'))
            if vendor_code and '.' in vendor_code:
                vendor_code = vendor_code.split('.')[0]
            supplier_id = supplier_obj.search([('vendor_code', '=', vendor_code)])
            if not supplier_id:
                message = "This Ref[%s] couldn't be found on suppliers." % vendor_code
                record['res'] = 0  # will be in red to mark not valid records
                record['message'] = message
                action_message += "<li style='color:%s'> %s</li>" % \
                                  (COLORS[record['res']], message)
                continue
            # Find Product
            product_sku = str(record.get('sku'))
            product_id = product_obj.search([('default_code', '=', product_sku)])
            if not product_id:
                message = "This SKU[%s] couldn't be found on products." % product_sku
                record['res'] = 0  # will be in red to mark not valid records
                record['message'] = message
                action_message += "<li style='color:%s'> %s</li>" % \
                                  (COLORS[record['res']], message)
                continue
            
            supplier_info_id = supplier_info_obj.search([('name', '=', supplier_id.id),
                                                         ('product_tmpl_id', '=', product_id.id)])
            # Check SKU ia exist and active
            if not supplier_info_id:
                message = "Couldn't found Any Supplier Info detail for product: %s," \
                          " related to vendor: %s" % (product_id.display_name, supplier_id.display_name)
                record['res'] = 0  # will be in red to mark not valid records
                record['message'] = message
                action_message += "<li style='color:%s'> %s</li>" % \
                                  (COLORS[record['res']], message)
                continue
            
            try:
                quantity = float(record.get('quantity'))
                update_vals = {'curr_qty': quantity, 'update_date': fields.Datetime.now()}
                message = "Updated for supplier: <b>%s</b> Successfully, Product [<b>%s</b>] current qty [<b>%s</b>]" % \
                          (supplier_id.display_name, product_id.default_code, quantity)
                if self.with_price:
                    price = float(record.get('price'))
                    message += ", and Price: [%s]" % price
                    update_vals['price'] = price
                supplier_info_id.write(update_vals)
                # supplier_info_id._btb_inform_backend()
                product_id.message_post(body="%s by upload Supplier Info Update action" % message)
                action_message += "<li style='color:%s'>%s</li>" % \
                                  (COLORS[3], message)
                self.update_vendor_quantity_product_in_magento_ept(product_id)
            except Exception as e:
                message = "Updating has faced an issue for Product [%s] with Supplier: %s." \
                          % (product_id.display_name, supplier_id.display_name)
                action_message += "<li style='color:%s'> %s</li>" % \
                                  (COLORS[0], message)
                action_message += "<p style='color:%s'>Error: %s</li>" % (COLORS[0], e)
        action_message += "</ol>"
        self.action_message = action_message
        self.is_uploaded = True
        
    def re_open_wizard(self):
        wizard = {
            'view_mode': 'form',
            'name': _("Supplier Info Update"),
            'res_model': self._name,
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
        if not self.env.context.get("open_new"):
            wizard['res_id'] = self.id
            wizard['context'] = self.env.context
        return wizard

    

