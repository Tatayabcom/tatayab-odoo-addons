# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
{
    'name': "Tatayab Back to Back",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'Sales',
    'summary': """BTB logic module""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '1.0',
    'depends': ['sale_purchase', 'odoo_magento2_ept','product_warehouse_quantity',
                'tatayab_operation_status', 'tatayab_purchase_changes'],
    'data': [
        'data/purchase_data.xml',
        
        'security/security.xml',
        'security/ir.model.access.csv',
        
        'views/product_view_changes.xml',
        'views/res_config_setting_view.xml',
        'views/sale_view_changes.xml',
        'views/purchase_order_view_changes.xml',
        'views/res_partner_view_changes.xml',
        'views/stock_picking_changes_view.xml',
        'wizard/supplier_info_update_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
