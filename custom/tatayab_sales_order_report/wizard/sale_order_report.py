# -*- coding: utf-8 -*-
import base64
import io
from datetime import datetime

from odoo import fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools.misc import xlwt

column_heading_style = xlwt.easyxf(
    'font:height 200;align: horiz center;font:bold True;pattern: pattern solid, fore_color yellow;')
column_heading_style_coll = xlwt.easyxf('font:height 200;align: horiz center;font:color white;'
                                        ' font:bold True;pattern: pattern solid, fore_color green;')
column_heading_style_product = xlwt.easyxf('font:height 200;align: horiz center;font:color black;'
                                           ' font:bold True;')
column_heading_style_pr = xlwt.easyxf('font:height 200;align: horiz center;font:color black;'
                                      ' font:bold True;pattern: pattern solid, fore_color yellow;')

column_heading_style_disc = xlwt.easyxf('font:height 200;align: horiz center;font:color white;'
                                        ' font:bold True;pattern: pattern solid, fore_color gray_ega;')
header_style = xlwt.easyxf(
    'font:height 300; align: vertical center; align: horiz center;pattern: pattern solid,'
    ' fore_color black; font: color white; font:bold True;borders: top thin,bottom thin')
filter_style = xlwt.easyxf('font:height 200;font:bold True;align: horiz center;'
                           'font: color white;pattern: pattern solid, fore_color green;')
_PICKING_STAGES = [('draft', 'Draft'),
                   ('confirm', 'Confirmed'),
                   ('pick', 'Picking'),
                   ('pack', 'Packing'),
                   ('ship', 'Shipping'),
                   ('done', 'Done Shipment'),
                   ('return_ship', 'Return shipment'),
                   ('return_pack', 'Return un-Pack'),
                   ('return_pick', 'Return Un-Pick'),
                   ('return_done', 'Done Return'),
                   ('cancel', 'Canceled')]

_ORDER_STAGES = [('draft', 'Draft'),
                ('without_pickings', 'Without Pickings'),
                ('pick', 'Picking'),
                ('pack', 'Packing'),
                ('ship', 'Shipping'),
                ('done', 'Done Shipment'),
                ('cancel', 'Canceled'),
                ('return_pick', 'Returning'),  # for both return side to show it's on processing
                ('partial_returning', 'Partial Returning'),  # for picking which is partially returned
                ('partial_return', 'Partial Returned'),  # for picking which is partially returned
                ('returned', 'Returned'),  # for picking which is returned to show it's all done return
                # ('return_done', 'Done Return'),  # for picking which is a return to show it's all done return
                ('return_other', 'Returning to other WH'),  # for picking which is Under Other Warehouse Return
                ('return_other_done', 'Returned to other WH'),  # for picking which is Other Warehouse Returned
                ('partial_return_other_done', 'Partial Returned to other WH')]



class SaleOrderReport(models.TransientModel):
    _name = 'sale.order.report'

    from_date = fields.Date(string='From')
    to_date = fields.Date(string='To')
    categ_id = fields.Many2one('product.category', string='Product Category', )
    brand_id = fields.Many2one('product.brand', string='Product Brand', )
    country_id = fields.Many2one('res.country', string='Country', )
    product_type = fields.Selection([('consu', 'Consumable'), ('service', 'Service'), ('product', 'Sortable Product')],
                                    default='product', string="Product Type", )
    purchase_type_id = fields.Many2one('purchase.type', string='Payment Type', )
    vendor_ids = fields.Many2many('res.partner', string='Vendors', domain=[('supplier_rank', '!=', 0)])
    is_pack = fields.Boolean('With Pack Product', default=False, )
    sales_report_file = fields.Binary('Sale Order Report')
    file_name = fields.Char('File Name')
    sale_printed = fields.Boolean('Report Printed')
    date_filter_select = fields.Selection(selection=[('order_date', 'By Order Date'),
                                                     # ('pack_date', 'By Shipping Date'),
                                                     ('shipment_date', 'By Done Shipment Date'),
                                                     ], string="Filter Date By", default='order_date')
    # order_state = fields.Selection(selection=[('draft', 'Draft'),
    #                                           ('sale', 'Sale'),
    #                                           ('done', 'Done'),
    #                                           ('cancel', 'Cancel')], string="Orders State", )
    order_state = fields.Selection(selection=_ORDER_STAGES, string="Operation State" )
    chunk_limit = fields.Integer("Records/Page", default=2000,
                                 help="This value shouldn't exceed 6000 record per page")
    picking_state = fields.Selection(selection=_PICKING_STAGES, string="Picking State", )
    with_consignment = fields.Boolean(string="", )
    total_sales_amount = fields.Float(string="", required=False, readonly=True)
    total_supplier_amount = fields.Float(string="", required=False, readonly=True)
    total_tatayab_amount = fields.Float(string="", required=False, readonly=True)
    company_id = fields.Many2one('res.company', "Company", default=lambda self: self.env.company.id)

    def excel_print_report(self):
        if not self.date_filter_select:
            raise ValidationError(_("Please select filter date first !!!"))

        if self.chunk_limit >= 65536:
            raise ValidationError(
                _("Chunk Limit(Record/Page) %s can't exceed the limit number of xml files 65536." % self.chunk_limit))

        workbook = xlwt.Workbook(style_compression=2)
        # sale_order_lines = self.env['sale.order.line']
        # domain_order_line = []
        # if self.date_filter_select == 'order_date':
        #     domain_order_line = [('create_date', '>=', self.from_date),
        #                          ('create_date', '<=', self.to_date)]
        #
        # # elif self.date_filter_select == 'pack_date':
        # #     domain_order_line = [('order_id.ship_ppack_id.date_done', '>=', self.from_date),
        # #                          ('order_id.ship_ppack_id.date_done', '<=', self.to_date)]
        #
        # elif self.date_filter_select == 'shipment_date':
        #     domain_order_line = [('order_id.active_picking_id.date_done', '>=', self.from_date),
        #                          ('order_id.active_picking_id.date_done', '<=', self.to_date)]
        # filter_column = 1
        #
        # if self.categ_id:
        #     domain_order_line.append(('product_id.categ_id', '=', self.categ_id.id))
        #     filter_column += 1
        # if self.brand_id:
        #     domain_order_line.append(('product_id.brand_id', '=', self.brand_id.id))
        #     filter_column += 1
        # if self.country_id:
        #     domain_order_line.append(('order_id.country_id', '=', self.country_id.id))
        #     filter_column += 1
        # if self.purchase_type_id:
        #     domain_order_line.append(('product_id.purchase_type_id', '=', self.purchase_type_id.id))
        #     filter_column += 1
        # if self.order_state:
        #     domain_order_line.append(('order_id.state', '=', self.order_state))
        #     filter_column += 1
        # if self.picking_state:
        #     domain_order_line.append(('order_id.picking_state', '=', self.picking_state))
        #     filter_column += 1
        # if self.vendor_ids:
        #     domain_order_line.append(('product_id.seller_ids.name', 'in', self.vendor_ids.ids))
        #     filter_column += 1

        # line_ids = sale_order_lines.search(domain_order_line, order='create_date ASC')

        date_from = datetime.strptime(str(self.from_date) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
        date_to = datetime.strptime(str(self.to_date) + ' 23:59:59', '%Y-%m-%d %H:%M:%S')

        # Getting Pickings
        picking_sql = "SELECT id FROM stock_picking sp WHERE "
        if self.date_filter_select == 'shipment_date':
            picking_sql += "sp.date_done >= %s AND sp.date_done <= %s;"
            stock_filters_value = (str(date_from),) + (str(date_to),)
            self.env.cr.execute(picking_sql, stock_filters_value)
        pickings = self.env['stock.picking'].browse(self.env.cr.fetchall())

        # Getting Orders
        filters_value = ()
        sql = "SELECT id FROM sale_order so"
        if self.order_state or self.picking_state or self.country_id or pickings:
            sql += " WHERE "
            if self.order_state:
                sql += "so.operation_state = %s"
                filters_value = (self.order_state,)
            if self.picking_state:
                if filters_value != ():
                    sql += " OR so.operation_state = %s"
                else:
                    sql += "so.operation_state = %s"
                filters_value = filters_value + (self.picking_state,)
            if self.country_id:
                if filters_value != ():
                    sql += " OR so.country_id = %s"
                else:
                    sql += "so.country_id = %s"
                filters_value = filters_value + (self.country_id.id,)
            if pickings:
                if filters_value != ():
                    sql += " OR so.active_picking_id IN %s"
                else:
                    sql += "so.active_picking_id IN %s"
                filters_value = filters_value + (tuple(pickings.ids),)
        sql += ";"
        self.env.cr.execute(sql, filters_value)
        orders = self.env['sale.order'].browse(self.env.cr.fetchall())

        filters_value = ()
        # Getting Products
        pro_sql = "SELECT id FROM product_template pro"
        if self.categ_id or self.brand_id or self.purchase_type_id:
            pro_sql += " WHERE "
            if self.categ_id:
                pro_sql += "pro.categ_id = %s"
                filters_value = (self.categ_id.id,)
            if self.brand_id:
                if filters_value != ():
                    pro_sql += " OR pro.brand_id = %s"
                else:
                    pro_sql += "pro.brand_id = %s"
                filters_value = filters_value + (self.brand_id.id,)
            if self.purchase_type_id:
                if filters_value != ():
                    pro_sql += " OR pro.purchase_type_id = %s"
                else:
                    pro_sql += "pro.purchase_type_id = %s"
                filters_value = filters_value + (self.purchase_type_id.id,)
        pro_sql += ";"
        self.env.cr.execute(pro_sql, filters_value)
        products = self.env['product.template'].browse((x[0] for x in self.env.cr.fetchall()))

        if products:
            self.env.cr.execute(
                '''SELECT id FROM product_product pro WHERE pro.product_tmpl_id IN %s;''', (tuple(products.ids),))
        product_variants = self.env['product.product'].browse((x[0] for x in self.env.cr.fetchall()))

        # Getting sale order lines to print.
        filters_value = ()
        sale_order_sql = "SELECT id FROM sale_order_line sl WHERE "
        if self.date_filter_select == 'order_date':
            sale_order_sql += "sl.create_date >= %s AND sl.create_date <= %s"
            filters_value = (str(self.from_date),) + (str(self.to_date),)

        if orders:
            if sale_order_sql and filters_value != ():
                sale_order_sql += " AND sl.order_id IN %s"
            else:
                sale_order_sql += "sl.order_id IN %s"
            filters_value = filters_value + (tuple(orders.ids),)

        if product_variants:
            if sale_order_sql:
                sale_order_sql += " AND sl.product_id IN %s"
            else:
                sale_order_sql += "sl.product_id IN %s"
            filters_value = filters_value + (tuple(product_variants.ids),)

        if self.company_id:
            sale_order_sql += " AND sl.company_id = %s"
            filters_value = filters_value + (self.company_id.id,)

        if sale_order_sql:
            sale_order_sql += ";"
            self.env.cr.execute(sale_order_sql, filters_value)

        line_ids = self.env['sale.order.line'].browse((x[0] for x in self.env.cr.fetchall()))

        if line_ids:
            page_num = 1
            worksheet = workbook.add_sheet('Page %s' % page_num, cell_overwrite_ok=False)
            # Reduce code duplicate by Collect Headers & sheets methods
            self.add_filters(worksheet)
            self.add_headers(worksheet)
            row = 9
            for idx, sale_order_line in enumerate(line_ids):
                for sale_lin in sale_order_line:
                    if self.order_state == 'pack' :
                        if sale_lin.order_id.active_picking_id.operation_state == sale_lin.order_id.operation_state:
                            vendors = sale_lin.product_id.seller_ids.mapped('name')
                            # for supp in sale_lin.product_id.seller_ids:
                            for supp in vendors:
                                if supp in self.vendor_ids:
                                        order_state = dict(sale_lin.order_id._fields['operation_state'].selection).get(
                                            sale_lin.order_id.operation_state)
                                        # picking_state = dict(sale_lin.order_id._fields['picking_state'].selection).get(
                                        #     sale_lin.order_id.picking_state)
                                        picking_state_receipt_state = dict(
                                            sale_lin.order_id.active_picking_id._fields['operation_state'].selection).get(
                                            sale_lin.order_id.active_picking_id.operation_state)
                                        supplier = str(supp.name)
                                        total_cost = sale_lin.product_id.standard_price * sale_lin.product_uom_qty
                                        tracking_refrance = sale_lin.order_id.active_picking_id.carrier_tracking_ref
                                        net_invoice = sale_lin.price_unit * sale_lin.product_uom_qty
                                        supplier_amount = (100 - sale_lin.product_id.margin_percent) / 100 * net_invoice
                                        row += 1
                                        if row >= self.chunk_limit:
                                            row, worksheet, page_num = self._increase_row(row, workbook, page_num)
                                        worksheet.write(row, 0, str(sale_lin.create_date.date()),
                                                        column_heading_style_product)

                                        if sale_lin.order_id.active_picking_id.date_done:
                                            worksheet.write(row, 1,
                                                            str(sale_lin.order_id.active_picking_id.date_done.date()),
                                                            column_heading_style_product)
                                        else:
                                            worksheet.write(row, 1, " ", column_heading_style_product)
                                        worksheet.write(row, 2, sale_lin.order_id.name, column_heading_style_product)
                                        worksheet.write(row, 3, order_state, column_heading_style_product)
                                        worksheet.write(row, 4, picking_state_receipt_state, column_heading_style_product)
                                        worksheet.write(row, 5, tracking_refrance, column_heading_style_product)
                                        worksheet.write(row, 6, sale_lin.order_id.country_id.name,
                                                        column_heading_style_product)
                                        worksheet.write(row, 7, sale_lin.product_id.categ_id.complete_name,
                                                        column_heading_style_product)
                                        worksheet.write(row, 8, supplier, column_heading_style_product)
                                        worksheet.write(row, 9, sale_lin.product_id.default_code,
                                                        column_heading_style_product)
                                        worksheet.write(row, 10, sale_lin.product_id.name, column_heading_style_product)
                                        worksheet.write(row, 11, sale_lin.order_id.warehouse_id.name,
                                                        column_heading_style_product)
                                        worksheet.write(row, 12, sale_lin.product_uom_qty, column_heading_style_product)
                                        worksheet.write(row, 13, sale_lin.product_id.standard_price,
                                                        column_heading_style_product)
                                        worksheet.write(row, 14, round(total_cost, 3), column_heading_style_product)
                                        worksheet.write(row, 15, round(sale_lin.price_unit, 3),
                                                        column_heading_style_product)
                                        worksheet.write(row, 16, sale_lin.currency_id.name, column_heading_style_product)
                                        worksheet.write(row, 17, round(net_invoice, 3), column_heading_style_product)
                                        worksheet.write(row, 18, round(sale_lin.product_id.margin_percent, 2),
                                                        column_heading_style_product)
                                        worksheet.write(row, 19,
                                                        round(
                                                            (100 - sale_lin.product_id.margin_percent) / 100 * net_invoice,
                                                            3),
                                                        column_heading_style_product)
                                        worksheet.write(row, 20, round(net_invoice - supplier_amount, 3),
                                                        column_heading_style_product)
                                        payment_list = []
                                        for line in sale_lin.order_id.invoice_ids:
                                            payment_list.append(line.journal_id.name)
                                        # worksheet.write(row, 21, ', '.join(payment_list), column_heading_style_pr)
                                        worksheet.write(row, 21, sale_lin.order_id.magento_payment_method_id.display_name, column_heading_style_pr)
                                        # worksheet.write(row, 21, sale_lin.order_id.payment_journal_id.name,
                                        #                 column_heading_style_product)
                                        worksheet.write(row, 22, sale_lin.product_id.purchase_type_id.name,
                                                        column_heading_style_product)
                                        worksheet.write(row, 23, "Order",
                                                        column_heading_style_product)
                                        worksheet.write(row, 24, sale_lin.order_id.emp_driver_id.name,
                                                        column_heading_style_product)
                                        worksheet.write(row, 25, sale_lin.order_id.external_driver_id.name,
                                                        column_heading_style_product)
                                        worksheet.write(row, 26, sale_lin.order_id.carrier_id.name,
                                                        column_heading_style_product)

                            else:
                                if not self.vendor_ids:
                                    order_state = dict(sale_lin.order_id._fields['operation_state'].selection).get(
                                        sale_lin.order_id.operation_state)
                                    # picking_state = dict(sale_lin.order_id._fields['picking_state'].selection).get(
                                    #     sale_lin.order_id.picking_state)
                                    picking_state_receipt_state = dict(
                                        sale_lin.order_id.active_picking_id._fields['operation_state'].selection).get(
                                        sale_lin.order_id.active_picking_id.operation_state)
                                    supplier = str.join(' / ',
                                                        [str(supp) for supp in
                                                         sale_lin.product_id.seller_ids.mapped('name.name')])
                                    total_cost = sale_lin.product_id.standard_price * sale_lin.product_uom_qty
                                    tracking_refrance = sale_lin.order_id.active_picking_id.carrier_tracking_ref
                                    net_invoice = sale_lin.price_unit * sale_lin.product_uom_qty
                                    supplier_amount = (100 - sale_lin.product_id.margin_percent) / 100 * net_invoice
                                    row += 1
                                    if row >= self.chunk_limit:
                                        row, worksheet, page_num = self._increase_row(row, workbook, page_num)
                                    worksheet.write(row, 0, str(sale_lin.create_date.date()),
                                                    column_heading_style_product)

                                    if sale_lin.order_id.active_picking_id.date_done:
                                        worksheet.write(row, 1, str(sale_lin.order_id.active_picking_id.date_done.date()),
                                                        column_heading_style_product)
                                    else:
                                        worksheet.write(row, 1, " ", column_heading_style_product)
                                    worksheet.write(row, 2, sale_lin.order_id.name, column_heading_style_product)
                                    worksheet.write(row, 3, order_state, column_heading_style_product)
                                    worksheet.write(row, 4, picking_state_receipt_state, column_heading_style_product)
                                    worksheet.write(row, 5, tracking_refrance, column_heading_style_product)
                                    worksheet.write(row, 6, sale_lin.order_id.country_id.name, column_heading_style_product)
                                    worksheet.write(row, 7, sale_lin.product_id.categ_id.complete_name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 8, supplier, column_heading_style_product)
                                    worksheet.write(row, 9, sale_lin.product_id.default_code, column_heading_style_product)
                                    worksheet.write(row, 10, sale_lin.product_id.name, column_heading_style_product)
                                    worksheet.write(row, 11, sale_lin.order_id.warehouse_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 12, sale_lin.product_uom_qty, column_heading_style_product)
                                    worksheet.write(row, 13, sale_lin.product_id.standard_price,
                                                    column_heading_style_product)
                                    worksheet.write(row, 14, round(total_cost, 3), column_heading_style_product)
                                    worksheet.write(row, 15, round(sale_lin.price_unit, 3), column_heading_style_product)
                                    worksheet.write(row, 16, sale_lin.currency_id.name, column_heading_style_product)
                                    worksheet.write(row, 17, round(net_invoice, 3), column_heading_style_product)
                                    worksheet.write(row, 18, round(sale_lin.product_id.margin_percent, 2),
                                                    column_heading_style_product)
                                    worksheet.write(row, 19,
                                                    round((100 - sale_lin.product_id.margin_percent) / 100 * net_invoice,
                                                          3),
                                                    column_heading_style_product)
                                    worksheet.write(row, 20, round(net_invoice - supplier_amount, 3),
                                                    column_heading_style_product)
                                    payment_list = []
                                    for line in sale_lin.order_id.invoice_ids:
                                        payment_list.append(line.journal_id.name)
                                    # worksheet.write(row, 21, ', '.join(payment_list), column_heading_style_pr)
                                    worksheet.write(row, 21, sale_lin.order_id.magento_payment_method_id.display_name, column_heading_style_pr)
                                    # worksheet.write(row, 21, sale_lin.order_id.payment_journal_id.name,
                                    #                 column_heading_style_product)
                                    worksheet.write(row, 22, sale_lin.product_id.purchase_type_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 23, "Order",column_heading_style_product)
                                    worksheet.write(row, 24, sale_lin.order_id.emp_driver_id.name,column_heading_style_product)
                                    worksheet.write(row, 25, sale_lin.order_id.external_driver_id.name,column_heading_style_product)
                                    worksheet.write(row, 26, sale_lin.order_id.carrier_id.name,column_heading_style_product)
                    else:
                        if sale_lin.product_id.seller_ids and self.vendor_ids:
                            vendors = sale_lin.product_id.seller_ids.mapped('name')
                            # for supp in sale_lin.product_id.seller_ids:
                            for supp in vendors:
                                if supp in self.vendor_ids:
                                    order_state = dict(sale_lin.order_id._fields['operation_state'].selection).get(
                                        sale_lin.order_id.operation_state)
                                    # picking_state = dict(sale_lin.order_id._fields['picking_state'].selection).get(
                                    #     sale_lin.order_id.picking_state)
                                    picking_state_receipt_state = dict(
                                        sale_lin.order_id.active_picking_id._fields['operation_state'].selection).get(
                                        sale_lin.order_id.active_picking_id.operation_state)
                                    supplier = str(supp.name)
                                    total_cost = sale_lin.product_id.standard_price * sale_lin.product_uom_qty
                                    tracking_refrance = sale_lin.order_id.active_picking_id.carrier_tracking_ref
                                    net_invoice = sale_lin.price_unit * sale_lin.product_uom_qty
                                    supplier_amount = (100 - sale_lin.product_id.margin_percent) / 100 * net_invoice
                                    row += 1
                                    if row >= self.chunk_limit:
                                        row, worksheet, page_num = self._increase_row(row, workbook, page_num)
                                    worksheet.write(row, 0, str(sale_lin.create_date.date()),
                                                    column_heading_style_product)

                                    if sale_lin.order_id.active_picking_id.date_done:
                                        worksheet.write(row, 1,
                                                        str(sale_lin.order_id.active_picking_id.date_done.date()),
                                                        column_heading_style_product)
                                    else:
                                        worksheet.write(row, 1, " ", column_heading_style_product)
                                    worksheet.write(row, 2, sale_lin.order_id.name, column_heading_style_product)
                                    worksheet.write(row, 3, order_state, column_heading_style_product)
                                    worksheet.write(row, 4, picking_state_receipt_state, column_heading_style_product)
                                    worksheet.write(row, 5, tracking_refrance, column_heading_style_product)
                                    worksheet.write(row, 6, sale_lin.order_id.country_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 7, sale_lin.product_id.categ_id.complete_name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 8, supplier, column_heading_style_product)
                                    worksheet.write(row, 9, sale_lin.product_id.default_code,
                                                    column_heading_style_product)
                                    worksheet.write(row, 10, sale_lin.product_id.name, column_heading_style_product)
                                    worksheet.write(row, 11, sale_lin.order_id.warehouse_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 12, sale_lin.product_uom_qty, column_heading_style_product)
                                    worksheet.write(row, 13, sale_lin.product_id.standard_price,
                                                    column_heading_style_product)
                                    worksheet.write(row, 14, round(total_cost, 3), column_heading_style_product)
                                    worksheet.write(row, 15, round(sale_lin.price_unit, 3),
                                                    column_heading_style_product)
                                    worksheet.write(row, 16, sale_lin.currency_id.name, column_heading_style_product)
                                    worksheet.write(row, 17, round(net_invoice, 3), column_heading_style_product)
                                    worksheet.write(row, 18, round(sale_lin.product_id.margin_percent, 2),
                                                    column_heading_style_product)
                                    worksheet.write(row, 19,
                                                    round(
                                                        (100 - sale_lin.product_id.margin_percent) / 100 * net_invoice,
                                                        3),
                                                    column_heading_style_product)
                                    worksheet.write(row, 20, round(net_invoice - supplier_amount, 3),
                                                    column_heading_style_product)
                                    payment_list = []
                                    for line in sale_lin.order_id.invoice_ids:
                                        payment_list.append(line.journal_id.name)
                                    # worksheet.write(row, 21, ', '.join(payment_list), column_heading_style_pr)
                                    worksheet.write(row, 21, sale_lin.order_id.magento_payment_method_id.display_name,
                                                    column_heading_style_pr)
                                    # worksheet.write(row, 21, sale_lin.order_id.payment_journal_id.name,
                                    #                 column_heading_style_product)
                                    worksheet.write(row, 22, sale_lin.product_id.purchase_type_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 23, "Order",
                                                    column_heading_style_product)
                                    worksheet.write(row, 24, sale_lin.order_id.emp_driver_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 25, sale_lin.order_id.external_driver_id.name,
                                                    column_heading_style_product)
                                    worksheet.write(row, 26, sale_lin.order_id.carrier_id.name,
                                                    column_heading_style_product)

                        else:
                            if not self.vendor_ids:
                                order_state = dict(sale_lin.order_id._fields['operation_state'].selection).get(
                                    sale_lin.order_id.operation_state)
                                # picking_state = dict(sale_lin.order_id._fields['picking_state'].selection).get(
                                #     sale_lin.order_id.picking_state)
                                picking_state_receipt_state = dict(
                                    sale_lin.order_id.active_picking_id._fields['operation_state'].selection).get(
                                    sale_lin.order_id.active_picking_id.operation_state)
                                supplier = str.join(' / ',
                                                    [str(supp) for supp in
                                                     sale_lin.product_id.seller_ids.mapped('name.name')])
                                total_cost = sale_lin.product_id.standard_price * sale_lin.product_uom_qty
                                tracking_refrance = sale_lin.order_id.active_picking_id.carrier_tracking_ref
                                net_invoice = sale_lin.price_unit * sale_lin.product_uom_qty
                                supplier_amount = (100 - sale_lin.product_id.margin_percent) / 100 * net_invoice
                                row += 1
                                if row >= self.chunk_limit:
                                    row, worksheet, page_num = self._increase_row(row, workbook, page_num)
                                worksheet.write(row, 0, str(sale_lin.create_date.date()),
                                                column_heading_style_product)

                                if sale_lin.order_id.active_picking_id.date_done:
                                    worksheet.write(row, 1, str(sale_lin.order_id.active_picking_id.date_done.date()),
                                                    column_heading_style_product)
                                else:
                                    worksheet.write(row, 1, " ", column_heading_style_product)
                                worksheet.write(row, 2, sale_lin.order_id.name, column_heading_style_product)
                                worksheet.write(row, 3, order_state, column_heading_style_product)
                                worksheet.write(row, 4, picking_state_receipt_state, column_heading_style_product)
                                worksheet.write(row, 5, tracking_refrance, column_heading_style_product)
                                worksheet.write(row, 6, sale_lin.order_id.country_id.name, column_heading_style_product)
                                worksheet.write(row, 7, sale_lin.product_id.categ_id.complete_name,
                                                column_heading_style_product)
                                worksheet.write(row, 8, supplier, column_heading_style_product)
                                worksheet.write(row, 9, sale_lin.product_id.default_code, column_heading_style_product)
                                worksheet.write(row, 10, sale_lin.product_id.name, column_heading_style_product)
                                worksheet.write(row, 11, sale_lin.order_id.warehouse_id.name,
                                                column_heading_style_product)
                                worksheet.write(row, 12, sale_lin.product_uom_qty, column_heading_style_product)
                                worksheet.write(row, 13, sale_lin.product_id.standard_price,
                                                column_heading_style_product)
                                worksheet.write(row, 14, round(total_cost, 3), column_heading_style_product)
                                worksheet.write(row, 15, round(sale_lin.price_unit, 3), column_heading_style_product)
                                worksheet.write(row, 16, sale_lin.currency_id.name, column_heading_style_product)
                                worksheet.write(row, 17, round(net_invoice, 3), column_heading_style_product)
                                worksheet.write(row, 18, round(sale_lin.product_id.margin_percent, 2),
                                                column_heading_style_product)
                                worksheet.write(row, 19,
                                                round((100 - sale_lin.product_id.margin_percent) / 100 * net_invoice,
                                                      3),
                                                column_heading_style_product)
                                worksheet.write(row, 20, round(net_invoice - supplier_amount, 3),
                                                column_heading_style_product)
                                payment_list = []
                                for line in sale_lin.order_id.invoice_ids:
                                    payment_list.append(line.journal_id.name)
                                # worksheet.write(row, 21, ', '.join(payment_list), column_heading_style_pr)
                                worksheet.write(row, 21, sale_lin.order_id.magento_payment_method_id.display_name,
                                                column_heading_style_pr)
                                # worksheet.write(row, 21, sale_lin.order_id.payment_journal_id.name,
                                #                 column_heading_style_product)
                                worksheet.write(row, 22, sale_lin.product_id.purchase_type_id.name,
                                                column_heading_style_product)
                                worksheet.write(row, 23, "Order", column_heading_style_product)
                                worksheet.write(row, 24, sale_lin.order_id.emp_driver_id.name,
                                                column_heading_style_product)
                                worksheet.write(row, 25, sale_lin.order_id.external_driver_id.name,
                                                column_heading_style_product)
                                worksheet.write(row, 26, sale_lin.order_id.carrier_id.name,
                                                column_heading_style_product)
            if self.with_consignment:
                self.add_consigment_table(worksheet)
        else:
            raise ValidationError(_("Not Found Any Sales Order With This Filters."))

        # To Get wizard back when user click on print button for first time..
        for wizard in self:
            fp = io.BytesIO()
            workbook.save(fp)
            # excel_file = base64.encodestring(fp.getvalue())
            excel_file = base64.b64encode(fp.getvalue())
            wizard.sales_report_file = excel_file
            wizard.file_name = 'Sales Order Report%s - %s:%s .xls' % \
                               (self.is_pack and "(With Pack Product)" or "", str(self.from_date), str(self.to_date))
            wizard.sale_printed = True
            fp.close()
            return {
                'view_mode': 'form',
                'res_id': wizard.id,
                'res_model': 'sale.order.report',
                'view_type': 'form',
                'type': 'ir.actions.act_window',
                'context': self.env.context,
                'target': 'new',
            }

    def _increase_row(self, row, workbook, page_num):
        """
        This method work to avoid reaching limit of lines
        :param row: number of row before increase
        :param workbook: major xlwt workbook
        :param page_num: page_number
        :return: active (row, worksheet which will be new)
        """
        page_num += 1
        worksheet = workbook.add_sheet('Page %s' % page_num, cell_overwrite_ok=False)
        self.add_filters(worksheet)
        self.add_headers(worksheet)
        row = 10
        return row, worksheet, page_num

    def add_filters(self, worksheet):
        filter_column = 1
        if self.categ_id:
            filter_column += 1
            worksheet.write(6, filter_column, _('Product Category'), column_heading_style_pr)
            worksheet.write(7, filter_column, self.categ_id.complete_name, column_heading_style_product)

        if self.brand_id:
            filter_column += 1
            worksheet.write(6, filter_column, _("Product Brand"), column_heading_style_pr)
            worksheet.write(7, filter_column, self.brand_id.name, column_heading_style_product)

        if self.country_id:
            filter_column += 1
            worksheet.write(6, filter_column, _("Country"), column_heading_style_pr)
            worksheet.write(7, filter_column, self.country_id.name, column_heading_style_product)

        if self.purchase_type_id:
            filter_column += 1
            worksheet.write(6, filter_column, _("Purchase Type"), column_heading_style_pr)
            worksheet.write(7, filter_column, self.purchase_type_id.name, column_heading_style_product)

        if self.order_state:
            filter_column += 1
            worksheet.write(6, filter_column, _("Orders State"), column_heading_style_pr)
            worksheet.write(7, filter_column, self.order_state, column_heading_style_product)

        if self.picking_state:
            filter_column += 1
            worksheet.write(6, filter_column, _("Picking State"), column_heading_style_pr)
            worksheet.write(7, filter_column, self.picking_state, column_heading_style_product)

        if self.vendor_ids:
            filter_column += 1
            worksheet.write(6, filter_column, _("Product Suppliers"), column_heading_style_pr)
            worksheet.write(7, filter_column, ' / '.join(self.vendor_ids.mapped('name')), column_heading_style_product)

        if filter_column != 1:
            worksheet.write_merge(6, 7, 0, 0, _("Filters"), filter_style)

    def add_consigment_table(self, worksheet):
        worksheet.write(2, 0, _('TOTAL SALES AMOUNT'), column_heading_style_pr)
        worksheet.write(2, 1, round(self.total_sales_amount, 3), column_heading_style_product)
        worksheet.write(3, 0, _('TOTAL TATAYAB AMOUNT'), column_heading_style_pr)
        worksheet.write(3, 1, round(self.total_tatayab_amount, 3), column_heading_style_product)
        worksheet.write(4, 0, _('TOTAL SUPPLIER AMOUNT'), column_heading_style_pr)
        worksheet.write(4, 1, round(self.total_supplier_amount, 3), column_heading_style_product)

    def add_headers(self, worksheet):
        report_head = 'TATAYAB \n Sales Order Report \n'
        from_date_str = self.from_date.strftime('%Y-%m-%d')
        to_date_str = self.to_date.strftime('%Y-%m-%d')
        report_head += "From" + " " + from_date_str + " " + 'To' + " " + to_date_str
        worksheet.write_merge(0, 4, 4, 7, report_head, header_style)

        worksheet.write(9, 0, _('ORDER DATE'), column_heading_style)
        #     worksheet.write(9, 0, _('SHIPPING DATE'), column_heading_style)
        worksheet.write(9, 1, _('Done SHIPMENT DATE'), column_heading_style)
        worksheet.write(9, 2, _('ORDER NUMBER'), column_heading_style)
        worksheet.write(9, 3, _('STATUS'), column_heading_style)
        worksheet.write(9, 4, _('PICKING STATE'), column_heading_style)
        worksheet.write(9, 5, _('TRACKING REFRANCE'), column_heading_style)
        worksheet.write(9, 6, _('COUNTRY'), column_heading_style)
        worksheet.write(9, 7, _('CATEGORY'), column_heading_style)
        worksheet.write(9, 8, _('SUPPLIER'), column_heading_style)
        worksheet.write(9, 9, _('SKU'), column_heading_style)
        worksheet.write(9, 10, _('PRODUCT'), column_heading_style)
        worksheet.write(9, 11, _('WAREHOUSE'), column_heading_style)
        worksheet.write(9, 12, _('QUANTITY'), column_heading_style)
        worksheet.write(9, 13, _('COST/UNIT'), column_heading_style)
        worksheet.write(9, 14, _('TOTAL COST'), column_heading_style)
        worksheet.write(9, 15, _('PRICE/UNIT'), column_heading_style)
        # worksheet.write(9, 16, _('TOTAL DISC.'), column_heading_style)
        worksheet.write(9, 16, _('CURRENCY'), column_heading_style)
        worksheet.write(9, 17, _('NET INVOICE'), column_heading_style)
        worksheet.write(9, 18, _('TATAYAB MARGIN'), column_heading_style)
        worksheet.write(9, 19, _('SUPPLIER AMOUNT'), column_heading_style)
        worksheet.write(9, 20, _('TATAYAB MARGIN AMOUNT'), column_heading_style)
        worksheet.write(9, 21, _('MAGENTO PAYMENT METHOD'), column_heading_style)
        worksheet.write(9, 22, _('VENDOR TYPE'), column_heading_style)
        worksheet.write(9, 23, _('ORDER TYPE'), column_heading_style)
        worksheet.write(9, 24, _('INTERNAL DRIVER'), column_heading_style)
        worksheet.write(9, 25, _('EXTERNAL DRIVER'), column_heading_style)
        worksheet.write(9, 26, _('SHIPPING COMP.'), column_heading_style)
        worksheet.col(0).width = 10000
        worksheet.col(1).width = 10000
        worksheet.col(2).width = 5000
        worksheet.col(3).width = 4000
        worksheet.col(4).width = 10000
        worksheet.col(5).width = 8000
        worksheet.col(6).width = 4000
        worksheet.col(7).width = 10000
        worksheet.col(8).width = 10000
        worksheet.col(9).width = 10000
        worksheet.col(10).width = 10000
        worksheet.col(11).width = 10000
        worksheet.col(12).width = 3500
        worksheet.col(13).width = 3500
        worksheet.col(14).width = 5000
        worksheet.col(15).width = 5000
        worksheet.col(16).width = 5000
        worksheet.col(17).width = 8000
        worksheet.col(18).width = 8000
        worksheet.col(19).width = 5000
        worksheet.col(20).width = 5000
        worksheet.col(21).width = 8000
        worksheet.col(22).width = 3500
        worksheet.col(23).width = 3500
        worksheet.col(24).width = 3500
        worksheet.col(25).width = 3500
        worksheet.col(26).width = 3500
        worksheet.row(9).height = 500

    def print_other_report(self):
        return {
            'view_mode': 'form',
            'res_model': 'sale.order.report',
            'name': 'Tatayab Sale Order Report',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self.env.context,
            'target': 'new',
        }

    def genrate_consignment_journal_entry(self):
        account_move = self.env['account.move']
        timenow = datetime.now()
        debit_vals = {
            'name': 'Sales Amount',
            'account_id': 271,
            'date': timenow,
            'debit': self.total_sales_amount,
            'credit': 0.0,
        }
        credit_vals = {
            'name': 'Tatayab Amount',
            'account_id': 271,
            'date': timenow,
            'credit': self.total_tatayab_amount,
            'debit': 0.0,
        }
        credit_vals_2 = {
            'name': 'Seller Amount',
            'account_id': self.vendor_ids[0].property_account_payable_id.id if self.vendor_ids else False,
            'date': timenow,
            'credit': self.total_supplier_amount,
            'debit': 0.0,
        }
        #==== Doaa
        # account_move.create({
        #     'ref': 'Consignment Amounts in Date :  %s' % timenow,
        #     'type': 'entry',
        #     'line_ids': [(0, 0, debit_vals), (0, 0, credit_vals), (0, 0, credit_vals_2)]
        # })
        return {
            'view_mode': 'form',
            'res_model': 'sale.order.report',
            'name': 'Tatayab Sale Order Report',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self.env.context,
            'target': 'new',
        }
