# -*- coding: utf-8 -*-

{
    'name': "Tatayab Sales Report",

    'summary': """
        Sale Order Report""",
    'author': "Tatayab,",
    'website': "https://tatayab.com",
    'category': 'Sale',
    'version': '15.0',
    'depends': ['base', 'sale', 'tatayab_magento_connect_changes', 'purchase','sale_enterprise'],
    'data': [
        'security/ir.model.access.csv',
        'security/consignment_security.xml',
        'wizard/sale_order_report_view.xml',
        'views/product_template.xml',
        'views/sale_report_view_inherit.xml',
    ],
}
