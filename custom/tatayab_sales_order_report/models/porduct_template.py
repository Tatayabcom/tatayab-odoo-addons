# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import timedelta





class SaleReport(models.Model):
    _inherit = 'sale.report'

    # ======== to invisible fields ==================
    discount = fields.Float('Discount %', readonly=True, groups="tatayab_sales_order_report.hide_fields")
    discount_amount = fields.Float('Discount Amount', readonly=True, groups="tatayab_sales_order_report.hide_fields")
    weight = fields.Float('Gross Weight', readonly=True,groups="tatayab_sales_order_report.hide_fields")
    volume = fields.Float('Volume', readonly=True,groups="tatayab_sales_order_report.hide_fields")
    price_total = fields.Float('Total', readonly=True)
    untaxed_amount_to_invoice = fields.Float('Untaxed Amount To Invoice', readonly=True,groups="tatayab_sales_order_report.hide_fields")
    untaxed_amount_invoiced = fields.Float('Untaxed Amount Invoiced', readonly=True,groups="tatayab_sales_order_report.hide_fields")
    price_subtotal = fields.Float('Untaxed Total', readonly=True)

    # ======================================================
    brand_id = fields.Many2one('product.brand', string='Product Brand')
    product_parent_category_id = fields.Many2one('product.parent.category', string="Odoo Category")
    product_sub_category_id = fields.Many2one('product.sub.category', string=" Sub Category")
    product_vendor_id = fields.Many2one('res.partner', string="Product Vendor")
    usd_row_total = fields.Float('Total USD', readonly=True,digits="Product Price")
    magento_discount = fields.Float('Discount(KWD)', readonly=True,digits="Product Price")
    subtotal_magento_discount = fields.Float('Subtotal Without Discount(KWD)',digits="Product Price", readonly=True)
    base_row_total_incl_tax = fields.Float(string="Total KWD",digits="Product Price",
                                           help="The value in this  field  after discount the there are discount on lines")
    detailed_type = fields.Selection([
        ('consu', 'Consumable'),
        ('service', 'Service'),
        ('product', 'Storable Product')
    ], string='Product Type')
    operation_state = fields.Selection(selection=[('draft', 'Draft'),
                                                  ('without_pickings', 'Without Pickings'),
                                                  ('pick', 'Picking'),
                                                  ('pack', 'Packing'),
                                                  ('ship', 'Shipping'),
                                                  ('done', 'Done Shipment'),
                                                  ('lock', 'Locked'),
                                                  ('cancel', 'Canceled'), ('return_pick', 'Returning'),
                                                  ('partial_returning', 'Partial Returning'),
                                                  ('partial_return', 'Partial Returned'),
                                                  ('returned', 'Returned'),
                                                  ('return_done', 'Done Return'),
                                                  ('return_other', 'Returning to other WH'),
                                                  ('return_other_done', 'Returned to other WH'),
                                                  ('partial_return_other_done', 'Partial Returned to other WH'),
                                                  ], string="Operation State", )
    def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
        fields['brand_id'] = ", t.brand_id as brand_id"
        fields['product_parent_category_id'] = ", t.product_parent_category_id as product_parent_category_id"
        fields['product_sub_category_id'] = ", t.product_sub_category_id as product_sub_category_id"
        fields['product_vendor_id'] = ", t.product_vendor_id as product_vendor_id"
        fields['operation_state'] = ", s.operation_state as operation_state"
        fields['detailed_type'] = ", t.detailed_type as detailed_type"
        fields['base_row_total_incl_tax'] = ", CASE WHEN l.product_id IS NOT NULL THEN sum(l.base_row_total_incl_tax) ELSE 0 END as base_row_total_incl_tax"
        fields['magento_discount'] = ", CASE WHEN l.product_id IS NOT NULL AND l.base_row_total_incl_tax < 0 THEN sum(l.base_row_total_incl_tax ) ELSE 0 END as magento_discount"
        fields['subtotal_magento_discount'] = ", CASE WHEN l.product_id IS NOT NULL AND l.base_row_total_incl_tax > 0  THEN sum(l.base_row_total_incl_tax ) ELSE 0 END as subtotal_magento_discount"
        fields['usd_row_total'] = ", CASE WHEN l.product_id IS NOT NULL THEN sum(l.usd_row_total) ELSE 0 END as usd_row_total"
        groupby += ', t.brand_id'
        groupby += ', t.product_parent_category_id'
        groupby += ', t.product_sub_category_id'
        groupby += ', t.product_vendor_id'
        groupby += ', s.operation_state'
        groupby += ', t.detailed_type'
        groupby += ', l.base_row_total_incl_tax'
        return super(SaleReport, self)._query(with_clause, fields, groupby, from_clause)

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    purchase_type_id = fields.Many2one('purchase.type', string='Payment Type', )

