# -*- coding: utf-8 -*-
{
    'name': "Tatayab Inventory Report",
    'summary': """Tatayab Inventory Report""",
    'author': "Tatayab, (Omnya Rashwan)",
    'website': "https://tatayab.com",
    'category': 'Stock',
    'version': '15.0',
    'depends': ['tatayab_product_bin_location', 'tatayab_out_of_stock_report',
                'tatayab_return_cycle', 'tatayab_delivery_core','purchase'],
    'data': [
        'security/ir.model.access.csv',
        'security/group.xml',

        'views/stock_movement_view.xml',
        'views/quantity_overview_report.xml',
        'views/stock_valuation_layer_view.xml',
        'views/menuitems.xml',
        'views/stock_quant_inherit_view.xml',

        'wizards/stock_product_report.xml',
        'wizards/stock_movement_wizard_view.xml',
        'wizards/stock_aging_wizard.xml',
        'wizards/stock_turnover_wizard.xml',
        'wizards/picking_state_report.xml',

        'reports/stock_location_barcode.xml',
        'reports/stock_ageing_report.xml',
        'reports/report_stockpicking_operations_inherit.xml',
        'reports/stock_turnover_report.xml',
        'reports/delivery_receipt_temp.xml',
        'reports/stock_report_picking.xml',
        'reports/vendor_return_receipt_template.xml',
        'data/cron_data.xml',
    ],
}
