from odoo import models, fields, api, exceptions, _


class PurchaseSaleWizard(models.TransientModel):
    _name = 'ps.product.wizard'

    date = fields.Datetime(string="Today's Date", default=fields.Datetime.now())

    def get_report(self):
        products = self.env['product.template'].with_context(stop_product_internal_update=True).search(
            [('creation_out_of_stock', '=', False), ('qty_available', '>=', 0.0)])
        products.with_context(stop_product_internal_update=True).get_purchase_sale_date()
        action_id = self.env.ref('tatayab_inventory_reports.action_stock_movement_report')
        action = action_id.read()[0]
        return action
