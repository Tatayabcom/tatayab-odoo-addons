from datetime import timedelta

from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError


class StockAgeingWizard(models.TransientModel):
    _name = 'stock.ageing.wizard'

    @api.onchange('filter_by')
    def _clear_unneeded_fields(self):
        if self.filter_by == 'all':
            self.category_ids = False
            self.product_ids = False
        elif self.filter_by == 'product':
            self.category_ids = False
        elif self.filter_by == 'category':
            self.product_ids = False

    company = fields.Many2one(comodel_name='res.company', string='Company', )
    company_id = fields.Many2one(comodel_name='res.company', string='Company', default=lambda self: self.env.company)
    from_date = fields.Date(string='From Date')
    to_date = fields.Date(string='To Date')
    filter_by = fields.Selection(selection=[('all', 'All Products'), ('product', 'Product'), ('category', 'Category')],
                                 default='all')
    category_ids = fields.Many2many(comodel_name='product.category', string='Category')
    product_ids = fields.Many2many(comodel_name='product.product', string='Product',
                                   domain="[('type', '=', 'product')]")
    based_on = fields.Selection(selection=[('warehouse', 'Warehouse'), ('location', 'Location')],
                                string='Based On')
    detailed = fields.Boolean("Detailed(moves)", help="Show detailed of In/Out move results")
    exclude_empty = fields.Boolean("Exclude Total Zero", help="exclude results with total 0 qty on stock")
    warehouse_ids = fields.Many2many(comodel_name='stock.warehouse', string='Warehouse')
    location_ids = fields.Many2many(comodel_name='stock.location', string='Locations')

    @api.constrains('to_date', 'from_date')
    def _date_constrain(self):
        if self.from_date and self.to_date and (self.from_date - self.to_date).days < 121:

            raise ValidationError("'To date' must be before 'From date' by 120 days at least!!!")
            # raise Warning("'To date' must be before 'From date' by 120 days at least!!!")

    def get_dates(self, product_id, company_ids, date_from, date_to=False, ):
        cr = self.env.cr
        args_list = (product_id, tuple(company_ids, ), date_from,)
        query = """ SELECT COALESCE(SUM(mv.qty_done),0) 
           FROM  stock_move_line mv LEFT JOIN product_product pr ON pr.id = mv.product_id
           LEFT JOIN product_template pt ON pt.id = pr.product_tmpl_id
           LEFT JOIN stock_location src_loc ON mv.location_id = src_loc.id
           LEFT JOIN stock_location dest_loc ON mv.location_dest_id = dest_loc.id
           WHERE (pt.active = true AND pt.type = 'product' AND pr.id = %s) 
           AND (mv.state = 'done') AND (mv.company_id in %s OR mv.company_id = NULL) AND (mv.date < %s)"""
        if date_to:
            query += " AND (mv.date >= %s) "
            args_list += (date_to,)

        def _execute_query(cr, query, args_list, action):
            if action == 'in':
                query += " AND (src_loc.usage != 'internal' AND dest_loc.usage = 'internal') "
            if action == 'out':
                query += " AND (src_loc.usage = 'internal' AND dest_loc.usage != 'internal') "
            cr.execute(query, args_list)
            output = cr.fetchall()
            return output[0][0]

        in_total = _execute_query(cr, query, args_list, 'in')  # Incoming Shipment
        out_total = _execute_query(cr, query, args_list, 'out')  # Outgoing Shipment
        return {'in': in_total, 'out': out_total, 'balance': in_total - out_total}

    def collect_data(self, data):
        product_ids = data.get('product_ids')
        result_lines, doc_ids = [], []
        total_qty = 0
        for line in product_ids:
            total_balance = 0
            value_over_120 = self.get_dates(line[0], data.get('company_ids'), data.get('date5'), data.get('date6'))
            total_balance += value_over_120.get('balance')
            value_over_120['total_balance'] = total_balance

            value_90_120 = self.get_dates(line[0], data.get('company_ids'), data.get('date4'), data.get('date5'))
            total_balance += value_90_120.get('balance')
            value_90_120['total_balance'] = total_balance

            value_60_90 = self.get_dates(line[0], data.get('company_ids'), data.get('date3'), data.get('date4'))
            total_balance += value_60_90.get('balance')
            value_60_90['total_balance'] = total_balance

            value_30_60 = self.get_dates(line[0], data.get('company_ids'), data.get('date2'), data.get('date3'))
            total_balance += value_30_60.get('balance')
            value_30_60['total_balance'] = total_balance

            value_0_30 = self.get_dates(line[0], data.get('company_ids'), data.get('date1'), data.get('date2'))
            total_balance += value_0_30.get('balance')
            value_0_30['total_balance'] = total_balance
            total_qty += total_balance
            if data.get('exclude_empty') and not total_balance:
                # Skip showing line results in case of empty total
                continue
            doc_ids.append(line[0])
            result_lines.append({
                'product_name': line[1],
                'product_sku_code': line[3],
                'product_uom': line[4],
                'value_0_30': value_0_30,
                'value_30_60': value_30_60,
                'value_60_90': value_60_90,
                'value_90_120': value_90_120,
                'value_over_120': value_over_120,
                'total_value': round(int(total_balance * line[2]), 2),
                'total_qty': int(total_balance),
            })
        return result_lines

    def create_stock_ageing_report(self):
        first_30_days = self.from_date - timedelta(days=30)
        for_60_days = self.from_date - timedelta(days=60)
        for_90_days = self.from_date - timedelta(days=90)
        for_120_days = self.from_date - timedelta(days=120)

        if self.filter_by == 'all':
            self.product_ids = self.env['product.product'].search([])
        if self.filter_by == 'category':
            self.product_ids = self.env['product.product'].search([('categ_id', 'in', self.category_ids.ids)])
        data = {
            'from_date': self.from_date, 'to_date': self.to_date,
            'company_ids': self.company_id.ids,
            'product_ids': [(prod.id, prod.name, prod.list_price, prod.default_code, prod.uom_id.display_name,
                             prod.qty_available) for prod in self.product_ids],
            'categories': self.category_ids.mapped('display_name'),
            'category_ids': self.category_ids.mapped('id'),
            'warehouses': self.category_ids.mapped('display_name'),
            'warehouse_ids': self.category_ids.mapped('id'),
            'locations': self.category_ids.mapped('display_name'),
            'location_ids': self.category_ids.mapped('id'),
            'filter_by': dict(self._fields['filter_by'].selection).get(self.filter_by),
            'detailed': self.detailed,
            'exclude_empty': self.exclude_empty,
            'date1': self.from_date,
            'date2': first_30_days,
            'date3': for_60_days,
            'date4': for_90_days,
            'date5': for_120_days,
            'date6': self.to_date,
        }
        data.update({'lines': self.collect_data(data)})
        report_action = self.env.ref('tatayab_inventory_reports.action_report_stock_Ageing').report_action(self,
                                                                                                           data=data)
        return report_action
