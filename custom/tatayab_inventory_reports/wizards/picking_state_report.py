from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class PickingStateReport(models.TransientModel):
    _name = 'picking.state.report'

    date_from = fields.Datetime(
        string="FROM", required=True,
        default=fields.Datetime.now().replace(hour=22, minute=00, second=00) - relativedelta(days=1))
    date_to = fields.Datetime(
        string="TO", required=True, default=fields.Datetime.now().replace(hour=21, minute=59, second=59))
    done_shipped_local = fields.Integer(string="LOCAL", required=False, readonly=1)
    done_shipped_inter = fields.Integer(string="INTERNATIONAL", required=False, readonly=1)
    done_total = fields.Integer(string="DONE SHIPPED TOTAL", required=False, readonly=1)
    shipping_local = fields.Integer(string="LOCAL", required=False, readonly=1)
    shipping_inter = fields.Integer(string="INTERNATIONAL", required=False, readonly=1)
    shipping_total = fields.Integer(string="SHIPPING TOTAL", required=False, readonly=1)
    return_total = fields.Integer(string="RETURN TOTAL", required=False, readonly=1)
    return_local = fields.Integer(string="LOCAL RETURN", required=False, readonly=1)
    return_inter = fields.Integer(string="OTHER COMP. RETURN", required=False, readonly=1)
    pick = fields.Integer(string="PICK", required=False, readonly=1)
    pack = fields.Integer(string="PACK", required=False, readonly=1)
    done_shipped_amount = fields.Float(string="DONE SHIPPED AMOUNT", required=False, readonly=1)
    shipping_amount = fields.Float(string="SHIPPING AMOUNT", required=False, readonly=1)
    all_return_amount = fields.Float(string="ALL RETURN AMOUNT", required=False, readonly=1)
    all_operation_amount = fields.Float(string="ALL OTHER OPERATION AMOUNT", required=False, readonly=1)
    total_amount = fields.Float(string="ALL AMOUNT", required=False, readonly=1)
    other_operation = fields.Integer(string="OTHER OPERATION", required=False, readonly=1)
    total_picking = fields.Integer(string="TOTAL", required=False, readonly=1)

    @api.constrains("date_from", "date_to")
    def _check_date(self):
        if self.date_from and self.date_to:
            if self.date_from > self.date_to:
                raise ValidationError(_("Date start must be greater than date end"))

    def get_picking_status(self):

        sale_order = self.env['sale.order']
        main_domain = [('create_date', '>=', self.date_from),
                       ('create_date', '<=', self.date_to),
                       ('state', 'in', ['sale', 'done']),
                       ('company_id', '=', self.env.company.id),
                       ('operation_state', '=', 'done')]

        # Done Shipment
        done_shipped = sale_order.search(main_domain)
        self.done_shipped_local = len(
            done_shipped.filtered(lambda s: s.shipping_method_id.driver_type in ['internal', 'external']))
        self.done_shipped_inter = len(
            done_shipped.filtered(lambda s: s.shipping_method_id.driver_type in ['international']))

        self.done_total = len(done_shipped)
        self.done_shipped_amount = sum(done_shipped.mapped('amount_total'))

        # Shipping
        main_domain[4] = ('operation_state', '=', 'ship')
        shipping = sale_order.search(main_domain)
        self.shipping_local = len(
            shipping.filtered(lambda s: s.shipping_method_id.driver_type in ['internal', 'external']))
        self.shipping_inter = len(shipping.filtered(lambda s: s.shipping_method_id.driver_type in ['international']))
        self.shipping_total = len(shipping)
        self.shipping_amount = sum(shipping.mapped('amount_total'))

        # Return
        main_domain[4] = ('operation_state', 'in',
                          ['return_pick', 'returned', 'return_done', 'return_other', 'return_other_done'])
        returns = sale_order.search(main_domain)
        self.return_total = len(returns)
        self.return_local = len(returns.filtered(
            lambda s: s.operation_state in ['return_pick', 'returned', 'return_done']))
        self.return_inter = len(returns.filtered(
            lambda s: s.operation_state in ['return_other', 'return_other_done']))
        self.all_return_amount = sum(returns.mapped('amount_total'))

        # PICK AND PACK
        main_domain[4] = ('operation_state', 'in', ['pick', 'pack'])
        pick_pack = sale_order.search(main_domain)
        self.other_operation = len(pick_pack)
        self.pick = len(pick_pack.filtered(lambda s: s.operation_state == 'pick'))
        self.pack = len(pick_pack.filtered(lambda s: s.operation_state == 'pack'))
        self.all_operation_amount = sum(pick_pack.mapped('amount_total'))

        # Final Total
        self.total_picking = self.done_total + self.other_operation + self.shipping_total + self.return_total
        self.total_amount = self.all_operation_amount + self.done_shipped_amount + self.shipping_amount

        if not self.total_picking:
            raise ValidationError(_("Not Found Any Picking With This Filters"))
        return {
            'view_mode': 'form',
            'res_id': self.id,
            'res_model': 'picking.state.report',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self.env.context,
            'target': 'new',
        }
