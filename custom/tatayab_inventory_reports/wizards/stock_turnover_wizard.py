from datetime import datetime, timedelta

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class StockTurnOverWizard(models.TransientModel):
    _name = 'stock.turn.over.wizard'

    company = fields.Many2one(comodel_name='res.company', string='Company', )
    company_id = fields.Many2one(comodel_name='res.company', string='Company', default=lambda self: self.env.company)
    period_length = fields.Integer(string='Period Length (Days)', default=30)
    from_date = fields.Date(string='From Date')
    filter_by = fields.Selection(selection=[('all', 'All Products'), ('product', 'Product'), ('category', 'Category')],
                                 default='all')
    category_ids = fields.Many2many(comodel_name='product.category', string='Category')
    product_ids = fields.Many2many(comodel_name='product.product', string='Product',
                                   domain="[('type', '=', 'product')]")
    based_on = fields.Selection(selection=[('warehouse', 'Warehouse'), ('location', 'Location')],
                                string='Based On')
    warehouse_ids = fields.Many2many(comodel_name='stock.warehouse', string='Warehouse')
    location_ids = fields.Many2many(comodel_name='stock.location', string='Locations')

    turn_over_result_lines = fields.One2many(comodel_name='turnover.result.line', inverse_name='result_turnover_id',
                                             string='Result Lines')

    def create_stock_overturn_report(self):
        if not self.from_date:
            raise UserError(_('You must set a start date.'))

        product_obj = self.env['product.product']
        move_line_obj = self.env['account.move.line']
        po_line_obj = self.env['purchase.order.line']

        end_date = self.from_date - timedelta(days=30)
        d1 = datetime.strptime(str(self.from_date), '%Y-%m-%d')
        d2 = datetime.strptime(str(end_date), '%Y-%m-%d')
        n_of_days = (d2 - d1).days

        products = self.product_ids

        if self.filter_by == 'category':
            products = product_obj.search([('categ_id', 'in', self.category_ids.ids), ('type', '=', 'product'),
                                           '|', ('company_id', '=', self.company_id.id), ('company_id', '=', False)])

        if self.filter_by == 'all':
            self.env.cr.execute(
                "SELECT id FROM product_template pro WHERE pro.type = 'product';")
            products_temp = self.env['product.template'].browse((x[0] for x in self.env.cr.fetchall()))

            if products_temp:
                products = products_temp.mapped('product_variant_id')

        price, total_qty = 0, 0
        for line in products.with_context(warehouse=self.warehouse_ids.ids):

            beginning = sum(move_line_obj.search([('product_id', '=', line.id),
                                                  ('date', '=', self.from_date),
                                                  ('company_id', '=', self.company_id.id),
                                                  ('debit', '!=', 0.0)]).mapped('debit'))

            ending = sum(move_line_obj.search([('product_id', '=', line.id),
                                               ('date', '=', end_date),
                                               ('company_id', '=', self.company_id.id),
                                               ('credit', '!=', 0.0)]).mapped('credit'))

            purchases = po_line_obj.search([('product_id', '=', line.id),
                                            ('date_order', '>=', self.from_date),
                                            ('date_order', '<=', end_date),
                                            ('company_id', '=', self.company_id.id),
                                            ('order_id.warehouse_id', 'in',
                                             self.warehouse_ids.ids)], order="id desc")
            if purchases:
                for purchase in purchases:
                    price += purchase.price_unit
                    total_qty += purchase.product_qty

            average_cost = (beginning + ending) / 2

            cost_of_goods_sold = beginning + price - ending

            turn_over_ratio = average_cost / cost_of_goods_sold if cost_of_goods_sold else 0.0

            average_age = n_of_days / turn_over_ratio if turn_over_ratio else 0.0

            self.turn_over_result_lines = [(0, 0, {
                'product_name': line.id,
                'product_sku_code': line.default_code,
                'product_uom': line.uom_id.id,
                'value': round(average_age, 2),
                'total_value': round(average_age, 2),
                'total_qty': total_qty,
            })]

        report_action = self.env.ref('tatayab_inventory_reports.action_report_stock_turn_over').report_action(self)
        report_action['close_on_report_download'] = True

        return report_action


class TurnOverLines(models.TransientModel):
    _name = 'turnover.result.line'

    product_name = fields.Many2one(comodel_name='product.product')
    product_sku_code = fields.Char(related='product_name.default_code')
    product_uom = fields.Many2one(related='product_name.uom_id')
    current_quantity = fields.Float()
    value = fields.Float()
    total_value = fields.Float()
    total_qty = fields.Float()

    # relational field
    result_turnover_id = fields.Many2one(comodel_name='stock.turn.over.wizard')
