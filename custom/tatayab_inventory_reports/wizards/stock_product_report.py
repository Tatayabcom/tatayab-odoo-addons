# -*- coding: utf-8 -*-
import base64
import io
from odoo.exceptions import ValidationError
from odoo.tools.misc import xlwt
from odoo import fields, models, api, _

column_row_style = xlwt.easyxf('font:height 200;align: horiz center;font:color black;'
                               ' font:bold True;')
column_heading_style = xlwt.easyxf(
    'font:height 200;align: horiz center;font:bold True;pattern: pattern solid, fore_color yellow;')


class StockProductReport(models.TransientModel):
    _name = 'stock.product.report'

    warehouse_id = fields.Many2one(comodel_name="stock.warehouse", string="Warehouse", required=True, )
    product_report_file = fields.Binary('Stock Product Order Report')
    file_name = fields.Char('File Name')
    product_printed = fields.Boolean('Report Printed')

    def print_excel_report(self):
        self.product_printed = True
        workbook = xlwt.Workbook()
        worksheet = workbook.add_sheet('Stock Product Report', cell_overwrite_ok=True)

        report_head = 'TATAYAB Stock Product Report '
        report_head += "For " + self.warehouse_id.name
        worksheet.write_merge(0, 4, 4, 7, report_head, xlwt.easyxf(
            'font:height 300; align: vertical center; align: horiz center;pattern: pattern solid, fore_color black;'
            ' font: color white; font:bold True;' "borders: top thin,bottom thin"))

        worksheet.write(9, 0, _('PRODUCT'), column_heading_style)
        worksheet.write(9, 1, _('PRODUCT/BIN LOCATION'), column_heading_style)
        worksheet.write(9, 2, _('VENDOR'), column_heading_style)
        worksheet.write(9, 3, _('INVENTORY QTY'), column_heading_style)
        worksheet.write(9, 4, _('RESERVED QTY'), column_heading_style)
        worksheet.write(9, 5, _('VALUE'), column_heading_style)
        worksheet.write(9, 6, _('PRODUCT BRAND'), column_heading_style)
        worksheet.write(9, 7, _('PRODUCT COST'), column_heading_style)
        worksheet.write(9, 8, _('PRODUCT SKU'), column_heading_style)
        worksheet.write(9, 9, _('PRODUCT CATEGORY'), column_heading_style)
        worksheet.write(9, 10, _('SALE PRICE'), column_heading_style)
        worksheet.col(0).width = 10000
        worksheet.col(1).width = 10000
        worksheet.col(2).width = 10000
        worksheet.col(3).width = 4000
        worksheet.col(4).width = 6000
        worksheet.col(5).width = 8000
        worksheet.col(6).width = 10000
        worksheet.col(7).width = 10000
        worksheet.col(8).width = 10000
        worksheet.col(9).width = 10000
        worksheet.col(10).width = 10000
        worksheet.row(9).height = 500

        product_template = self.env['product.template'].with_context(stop_product_internal_update=True).search(
            [('stock_bin_ids.warehouse_id', '=', self.warehouse_id.id)])

        row = 9
        bin_location = ''
        if product_template:
            for product in product_template:
                row += 1
                supplier = str.join(',', [str(supp) for supp in product.seller_ids.mapped('name.name')])
                if product.stock_bin_ids:
                    bin_location = str.join(',', [str(location) for location in
                                                  product.stock_bin_ids.location_id.mapped('name')])

                worksheet.write(row, 0, product.name, column_row_style)
                worksheet.write(row, 1, bin_location, column_row_style)
                worksheet.write(row, 2, supplier, column_row_style)
                worksheet.write(row, 3, product.qty_available, column_row_style)
                worksheet.write(row, 4, product.qty_available - product.free_qty, column_row_style)
                worksheet.write(row, 5, product.qty_available * product.standard_price, column_row_style)
                worksheet.write(row, 6, product.brand_id.name, column_row_style)
                worksheet.write(row, 7, product.standard_price, column_row_style)
                worksheet.write(row, 8, product.default_code, column_row_style)
                worksheet.write(row, 9, product.categ_id.name, column_row_style)
                worksheet.write(row, 10, product.list_price, column_row_style)

            for wizard in self:
                fp = io.BytesIO()
                workbook.save(fp)
                excel_file = base64.b64encode(fp.getvalue())
                wizard.product_report_file = excel_file
                wizard.file_name = 'Stock Product Report.xls'
                fp.close()
                return {
                    'view_mode': 'form',
                    'res_id': wizard.id,
                    'res_model': 'stock.product.report',
                    'view_type': 'form',
                    'type': 'ir.actions.act_window',
                    'context': self.env.context,
                    'target': 'new',
                }
        else:
            raise ValidationError(_("Not Found Any Product On That Warehouse."))

    def print_other_report(self):
        return {
            'view_mode': 'form',
            'res_model': 'stock.product.report',
            'name': 'Stock Product Report',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self.env.context,
            'target': 'new',
        }
