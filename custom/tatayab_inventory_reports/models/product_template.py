import base64
import io
import logging
import datetime as time
from datetime import datetime, date

from odoo import fields, models,_
from odoo.tools.misc import xlsxwriter

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class ExportProductTemplateInherit(models.Model):
    _name = 'export.product.template'

    date = fields.Datetime()
    date_today = fields.Date()
    file = fields.Binary('export file')
    attachment_id = fields.Many2one('ir.attachment')
    is_send = fields.Boolean(string="Send Mail")
    is_month_end = fields.Boolean(string="Is Month End")

    def send_export_product_quantity(self):
        files = self.env['export.product.template'].search([('is_send','=',False),('is_month_end','=',True)])
        for file in files:
            pass
            email_notify_group = self.env.ref('tatayab_inventory_reports.send_quantity_of_stock_end_the_month').id
            email_users = self.env['res.users'].sudo().search(
                [('groups_id', 'in', [email_notify_group])]).mapped('email')
            if email_users:
                if False in email_users:
                    email_users.remove(False)

            month = file.date_today.strftime("%B")
            print("month===",month)
            subject = month+" Inventory Report"
            _logger.info(green + "\nSENDING Mail For quantities mails: %s" % email_users + reset)
            mail_content = "Dear All, <br/> Kindly find attached the inventory report showing the closing stock at the end of the month. ( %s )<br/>" %(file.date_today)
            mail_content += "it's system notification mail"
            send_notif = self.env['mail.mail'].sudo().create({
                'subject': _(subject),
                'author_id': self.env.user.partner_id.id,
                'body_html': mail_content,
                'email_to': email_users and ', '.join(email_users) or False,
                'attachment_ids': [(6, 0, [file.attachment_id.id])],

            })
            print("send_notif ===>>", send_notif)
            logging.info(
                yellow + "Done to send product quantities -----------%s-----------------" + str(send_notif.id) + reset)

            send_notif.send()
            file.is_send = True


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    number_of_po = fields.Integer(string='No. of Purchase Orders', compute='get_purchase_sale_date')
    number_of_purchase = fields.Integer(string='Number of Purchases')
    number_of_sale = fields.Integer(string='Number of Sales')
    last_purchase_date = fields.Datetime(string='Last Purchase Date')
    last_sale_date = fields.Datetime(string='Last Sale Date')
    parent_category = fields.Many2one(related='categ_id.parent_id', string='Parent Category', store=True)
    no_purchase_sale = fields.Boolean(string='No Po Or So')
    free_qty = fields.Float(related='product_variant_id.free_qty')

    def get_purchase_sale_date(self):
        for idx, template in enumerate(self):
            template = template.with_context(stop_product_internal_update=True)

            self.env.cr.execute("""SELECT id FROM sale_order_line so
                                WHERE so.product_id = %s AND so.state = 'sale' ORDER BY so.id DESC;""" %
                                str(template.product_variant_id.id))
            sale_order_lines_ids = self.env['sale.order.line'].browse((x[0] for x in self.env.cr.fetchall()))
            template.write({'number_of_sale': len(sale_order_lines_ids)})

            # Get last sale order date.
            self.env.cr.execute("""SELECT id FROM sale_order_line so WHERE so.product_id = %s
                    AND so.state = 'sale' ORDER BY so.id DESC LIMIT 1;""" % str(template.product_variant_id.id))
            last_sale_order_date = self.env['sale.order.line'].browse((x[0] for x in self.env.cr.fetchall()))
            template.write({'last_sale_date': last_sale_order_date.order_id.date_order})

            self.env.cr.execute("""SELECT id FROM purchase_order_line po WHERE po.product_id = %s
                                AND po.state = 'purchase' ORDER BY po.id DESC;""" % str(template.product_variant_id.id))
            purchase_order_lines_ids = self.env['purchase.order.line'].browse(
                (x[0] for x in self.env.cr.fetchall()))
            template.write({'number_of_po': len(purchase_order_lines_ids)})
            template.write({'number_of_purchase': template.number_of_po})

            # Get last purchase order date.
            self.env.cr.execute("""SELECT id FROM purchase_order_line po WHERE po.product_id = %s
                       AND po.state = 'purchase' ORDER BY po.id DESC LIMIT 1;""" % str(template.product_variant_id.id))
            last_purchase_order = self.env['purchase.order.line'].browse((x[0] for x in self.env.cr.fetchall()))
            template.write({'last_purchase_date': last_purchase_order.date_planned})

            if not template.number_of_purchase and not template.number_of_sale:
                template.write({'no_purchase_sale': True})

                # Change cursor
                logging.info(
                    yellow + "%s - product %s updated" % (idx + 1, template.display_name) + reset)
        logging.info(yellow + "Done Update All ----------------------------" + reset)

    def export_product_quantity(self):

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet('Order Lines')
        worksheet.set_column(0, 16, 20)
        # add header
        header = [ 'Product', 'Location', 'Lot/Serial Number', 'Removal Date', 'Inventoried Quantity',
                  'Available Quantity', 'Unit of Measure', 'Value']
        for col in range(len(header)):
            worksheet.write(0, col, header[col])

        order_lines = self.env['stock.quant'].search([('location_id.usage','=', 'internal')])

        # add data
        # for row in range(1, len(order_lines) + 1):
        #     line = order_lines[row - 1]
        row = 1
        for line in order_lines:
            worksheet.write(row, 0, line.product_id.display_name)
            worksheet.write(row,1, line.location_id.display_name)
            worksheet.write(row,2, line.lot_id.display_name)
            worksheet.write(row,3, line.removal_date)
            worksheet.write(row,4, line.inventory_quantity_auto_apply)
            worksheet.write(row,5, line.available_quantity)
            worksheet.write(row,6, line.product_uom_id.name)
            worksheet.write(row,7, line.value)
            row +=1
        # save
        workbook.close()
        output.seek(0)
        next_month = date.today().replace(day=28) + time.timedelta(days=4)
        # subtracting the number of the current day brings us back one month
        end_month = next_month - time.timedelta(days=next_month.day)
        is_month_end = False
        if end_month == date.today():
            is_month_end = True
        export_product = self.env['export.product.template'].create(
            {
                'file': base64.b64encode(output.getvalue()),
                'date': datetime.now(),
                'date_today': date.today(),
                'is_month_end':is_month_end,
            }
        )
        attachment = self.env['ir.attachment'].create({
            'name': 'product_export_{}.xlsx'.format(datetime.now().strftime("%m_%d_%Y-%H_%M_%S")),
            'datas': base64.b64encode(output.getvalue()),
            'res_model': 'export.product.template',
            'res_id': export_product.id,
            'type': 'binary',
        })
        export_product.attachment_id = attachment.id
        print("attachment",attachment)

        return base64.b64encode(output.getvalue())