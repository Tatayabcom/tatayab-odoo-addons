from odoo import api, fields, models


class StockValuationLayer(models.Model):
    _inherit = 'stock.valuation.layer'

    warehouse_id = fields.Many2one('stock.warehouse',related='stock_move_id.warehouse_id',store=True)


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    product_name = fields.Char(related='product_id.name',string="Product",store=True)
    sku = fields.Char(related='product_id.default_code',string="SKU",store=True)
    brand_id = fields.Many2one(comodel_name='product.brand', related='product_id.brand_id',string="Brand", store=True)