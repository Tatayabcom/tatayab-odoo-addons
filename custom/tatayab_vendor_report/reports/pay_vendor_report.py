from odoo import fields, models, api, _


class PayVendorReport(models.Model):
    _name = 'pay.vendor.report'

    product_sku = fields.Char(related='product_id.default_code')
    product_id = fields.Many2one(comodel_name='product.product')
    sale_price = fields.Float(related='product_id.lst_price')
    no_item_sold = fields.Float()
    vendor_id = fields.Char()
    purchased_qty = fields.Float()
    unit_cost_price = fields.Float()
    total_cost_price = fields.Float()
