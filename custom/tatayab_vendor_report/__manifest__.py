{
    'name': "Tatayab: Pay To Vendors Report",
    'author': 'Tatayab, Omnya Rashwan',
    'summary': """Tatayab Pay To Vendors Report""",
    'website': 'http://www.tatayab.com',
    'description': """""",
    'version': '13.0',
    'depends': ['stock', 'sale', 'product', 'purchase', 'tatayab_purchase_changes', 'account','tatayab_product_sold'],
    'data': [
        'security/ir.model.access.csv',
        'reports/pay_vendor_report.xml',
        'wizards/pay_to_vendor_wiz_view.xml',
        'views/res_partner_inh_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
