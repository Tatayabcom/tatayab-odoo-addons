from odoo import fields, models, api, _


class Vendor(models.Model):
    _inherit = 'res.partner'

    vendor_type = fields.Selection(selection=[('b2b', 'B2B'), ('consignment', 'Consignment'), ('direct', 'Direct')],
                                   string='Vendor Type')
