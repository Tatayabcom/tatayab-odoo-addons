from dateutil.relativedelta import relativedelta

from odoo import fields, models, api, _
from datetime import datetime, date


class PayToVendor(models.TransientModel):
    _name = 'pay.vendor.wiz'

    date_from = fields.Date(string='Date From', readonly=False, required=True,
                            default=lambda self: fields.Date.to_string(date.today().replace(day=1)), )
    date_to = fields.Date(string='Date To', readonly=False, required=True,
                          default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1,
                                                                                                     days=-1)).date()))
    vendors_ids = fields.Many2many(comodel_name='res.partner', domain=[('supplier_rank', '>', 0)])
    products_ids = fields.Many2many(comodel_name='product.template')
    categories_ids = fields.Many2many(comodel_name='product.category')
    brands_ids = fields.Many2many(comodel_name='product.brand')

    def get_pay_vendor_report(self):
        for line in self.env['pay.vendor.report'].search([]):
            line.unlink()

        # to get number of sold items
        domain = [('order_id.date_order', '>=', self.date_from), ('order_id.date_order', '<=', self.date_to),
                  ('order_id.state', '!=', 'cancel')]
        if self.products_ids:
            domain += [('product_id', 'in', self.products_ids.mapped('product_variant_id').ids)]

        sale_order_lines = self.env['sale.order.line'].search(domain)

        in_product_list = []
        for sale_line in sale_order_lines:
            product_domain = [('id', '=', sale_line.product_id.product_tmpl_id.id)]
            if self.products_ids:
                product_domain = []
                product_domain += ['|', ('id', '=', sale_line.product_id.product_tmpl_id.id),
                                   ('id', 'in', self.products_ids.ids)]
            if self.categories_ids:
                product_domain += [('categ_id', 'in', self.categories_ids.ids)]
            if self.brands_ids:
                product_domain += [('brand_id', 'in', self.brands_ids.ids)]
            products = self.env['product.template'].search(product_domain)

            vendors = []
            for product in products:
                sold_quantity, total_sold_qty = 0.0, 0.0

                seller_domain = [('product_tmpl_id', '=', product.id), ('price', '!=', 0.0)]
                if self.vendors_ids:
                    seller_domain += [('name', 'in', self.vendors_ids.ids)]

                sellers = self.env['product.supplierinfo'].search(seller_domain, order='price asc')

                if product.product_variant_id == sale_line.product_id:
                    sold_quantity += sale_line.product_uom_qty

                    for seller in sellers:
                        key = seller.name.name
                        total_sold_qty = sum(
                            sale_order_lines.search([('order_id.date_order', '>=', self.date_from),
                                                     ('order_id.date_order', '<=', self.date_to),
                                                     ('product_id', '=', product.product_variant_id.id)]).mapped(
                                'product_uom_qty'))

                        purchase_orders = self.env['purchase.order'].search([('partner_id', '=', seller.name.id)])

                        purchased_quantity, purchased_price = 0.0, 0.0
                        for pur_line in purchase_orders:
                            if seller.name == pur_line.partner_id:
                                for line in pur_line.order_line:
                                    if line.product_id == product.product_variant_id:
                                        purchased_quantity += line.product_qty
                                        purchased_price = line.price_unit
                        value = [purchased_quantity, purchased_price]
                        vendors.append({key: value})

                    if vendors:
                        if product.name not in in_product_list:
                            for key, value in vendors[0].items():
                                if sold_quantity < value[0]:
                                    self.env['pay.vendor.report'].create({
                                        'product_id': sale_line.product_id.id,
                                        'no_item_sold': sold_quantity,
                                        'vendor_id': key,
                                        'purchased_qty': value[0],
                                        'unit_cost_price': value[1],
                                        'total_cost_price': value[1] * sold_quantity,
                                    })

                        elif product.name in in_product_list:
                            pay = self.env['pay.vendor.report'].search(
                                [('product_id', '=', product.product_variant_id.id)])

                            if len(vendors) > 1:
                                if [value[0] for key, value in vendors[1].items()][0] > sold_quantity:
                                    for p in pay:
                                        if p.purchased_qty < total_sold_qty:
                                            for key, value in vendors[1].items():
                                                if sold_quantity < value[0]:
                                                    self.env['pay.vendor.report'].create({
                                                        'product_id': sale_line.product_id.id,
                                                        'no_item_sold': sold_quantity,
                                                        'vendor_id': key,
                                                        'purchased_qty': value[0],
                                                        'unit_cost_price': value[1],
                                                        'total_cost_price': value[1] * sold_quantity,
                                                    })
                                        else:
                                            p.no_item_sold += sale_line.product_uom_qty
                                            p.total_cost_price = p.no_item_sold * p.unit_cost_price
                                else:
                                    pay.no_item_sold += sale_line.product_uom_qty
                                    pay.total_cost_price = pay.no_item_sold * pay.unit_cost_price
                            else:
                                pay.no_item_sold += sale_line.product_uom_qty
                                pay.total_cost_price = pay.no_item_sold * pay.unit_cost_price
                    in_product_list.append(product.name)

        action_id = self.env.ref('tatayab_vendor_report.action_pay_to_vendor_report')
        action = action_id.read()[0]
        return action
