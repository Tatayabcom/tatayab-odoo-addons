# -*- coding: utf-8 -*-

import json

import base64
import logging
import requests

from odoo import fields, models
from odoo.exceptions import UserError

logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
ESNAD_DATETIME_FORMAT = "%Y/%m/%d %H:%M:%S"


class Carrier(models.Model):
    _inherit = 'delivery.carrier'
    
    delivery_type = fields.Selection(selection_add=[('esnad', 'ESNAD')]
                                     , ondelete={'esnad': lambda recs: recs.write({'delivery_type': 'fixed', 'fixed_price': 0})})
    esnad_client_token = fields.Char("ESNAD Token")
    esnad_code = fields.Char("ESNAD Code")
    esnad_seq_id = fields.Many2one('ir.sequence', "Awb Seq")
    awb_number = fields.Char("Last AWB Num")
    esnad_shipment_create_url = fields.Char("URL")
    esnad_send_shipment = fields.Char("Shipping Operation")
    esnad_receive_label = fields.Char("Label Operation")
    esnad_receive_barcode = fields.Char("Barcode Operation")
    esnad_show_log = fields.Boolean(string='Show Api Log')
    content_type = fields.Char(string="Content-Type")
    
    def esnad_datetime_format(self, dt):
        return dt.strftime(ESNAD_DATETIME_FORMAT) if dt else ''
    
    def esnad_get_amount(self, currency, amount):
        comp = self.env.company
        comp_cur = comp.currency_id
        if comp_cur.id == currency.id:
            return amount
        return round(comp_cur.compute(amount, currency))
    
    def esnad_send_shipping(self, pickings):
        esnad_label_url_str = ''
        esnad_tracking_ref_str = ''
        carrier_log_obg = self.env['delivery.carrier.log']
        attachment_obj = self.env['ir.attachment']
        
        company = self.env.company
        
        def create_attachment(picking, name, datas):
            return attachment_obj.sudo().create({
                'name': ("%s-%s" % (picking.name, name)).replace('/', '_'),
                'datas': datas,
                'res_model': picking._name,
                'res_id': picking.id
            }).id
        
        def get_file_content(file_url):
            pdf_response = requests.get(file_url, stream=True)
            file_content = base64.encodebytes(pdf_response.content)
            return file_content
        # TODO: stopped because of esnad number to be using sequence
        awb_number = int(self.esnad_seq_id.next_by_id())
        box_fields = ['pkg_half', 'pkg_one', 'pkg_two', 'pkg_three']
        for picking in pickings:
            order = picking.sale_id
            partner = picking.partner_id
            moves = picking.move_ids_without_package
            is_cod, cod_amount, currency = self.order_cod_amount(order)  # Method implemented on tatayab_delivery_core
            # check number of bixes
            boxes = 0
            for box_field in box_fields:
                boxes += getattr(picking, box_field)
            # Check Values of Custom Value
            currency, custom_amount = self._get_order_custom_amount(order)  # Method implemented on tatayab_delivery_core
            if order.invoice_ids:
                inv_number = order.invoice_ids[0].name
            else:
                inv_number = ''
            product_details = []
            total_qty = 0
            for move in moves:
                product = move.product_id
                total_qty += move.product_qty
                unit_price = move.sale_line_id.price_unit
                # so_price_unit = unit_price
                # total_price_order = move.sale_line_id.order_id.amount_total
                # if move.sale_line_id.pack_line:
                #     so_price_unit = unit_price / sum(
                #         move.sale_line_id.pack_line_ids.mapped('quantity'))
                # price_unit = each_qty_price or get_amount(so_price_unit or move.price_unit, currency, so_date)
                product_details.append({
                    "productPrice": unit_price,
                    "productPcs": int(move.product_qty),
                    "productHscode": product.default_code,
                    "productName": product.name,
                })
            shipper = order.warehouse_id.partner_id
            req_body = [{
                "esnadAwbNo": awb_number,
                "orderType": "DOM",  # International client use INT, Domestic client use DOM, B2B client use TOB
                "clientOrderNo": order.name or picking.origin,
                "deliveryTime": str(fields.Datetime.now()),
                "deliveryService": "EXP",  # normal order use EXP, urgent order use UGT
                "consignor": shipper.name,
                "pickupAddress": shipper.street,
                "pickupPincode": shipper.zip,
                "pickupContact": shipper.phone,
                "originCity": shipper.city,
                "originCountry": shipper.country_id.name,
                "consignee": partner.name or " ",
                "deliveryAddress": partner.street,
                "deliveryContact": partner.phone,
                "destCity": partner.state_id.name or partner.city,
                "destCountry": partner.country_id and partner.country_id.name or company.country_id.name,
                "invoiceNumber": inv_number,
                "productDescription": "Cosmetics",
                "paymentMode": is_cod and "COD" or "PPD",
                #  **cash on delivery use COD , prepaid use PPD**
                "amountToCollect": is_cod and cod_amount or 0.0,  # order total price **Currency is SAR**
                "declaredValue": custom_amount,  # self.esnad_get_amount(currency, ),
                # customs declared value **Currency is Dollar**
                "pcs": picking.restrict_boxes and 1 or boxes,  # boxes
                "packageWeight": picking.weight,
                "volWeight": picking.shipping_weight,
                "invoicePdf": "",
                "productDetails": product_details
            }]
            # Create Carrier Log
            carrier_log = carrier_log_obg.create({
                'carrier_id': self.id,
                'carrier_request': req_body})
            # Get Request Header
            headers = {'Content-Type': self.content_type,
                       'token': self.esnad_client_token,
                       }
            url = "%s%s" % (self.esnad_shipment_create_url, self.esnad_send_shipment)
            if self.esnad_show_log:
                logger.info(yellow + "__ Esnad awb_number %s" % awb_number + reset)
                logger.info(yellow + "__ Esnad url %s" % url + reset)
                logger.info(yellow + "__ Esnad headers %s" % headers + reset)
                logger.info(yellow + "__ Esnad req_body %s" % req_body + reset)
            response = requests.post(url, headers=headers, json=req_body)
            self.awb_number = awb_number
            if response.status_code != 200:
                logger.info(red + "Response: %s " % response.text + reset)
                raise UserError("Response Code is %s.\n%s"
                                "\n-----------------------------\n Data send: %s"
                                % (response.status_code, response.text, json.dumps(req_body)))
            result = response.json()
            if result.get('code') == '1000':
                response_details = result.pop('data')
                # Get Tracking Ref
                esnad_tracking_ref = [ref['esnadAwbNo'] for ref in response_details]
                carrier_tracking_ref = esnad_tracking_ref_str.join(esnad_tracking_ref)
                # Get Label Content
                esnad_pdf_label_url = [label['esnadAwbPdfLink'] for label in response_details]
                attach_label_url = esnad_label_url_str.join(esnad_pdf_label_url)
                # file_content = get_file_content(attach_label_url)
                pdf_response = requests.get(attach_label_url, stream=True)
                file_content = base64.encodebytes(pdf_response.content)
                pdf_lable_file = create_attachment(picking, 'Label-Esnad.pdf', file_content)
                # Get Barcode Content
                esnad_barcode_url = "%s%s%s" % (self.esnad_shipment_create_url, self.esnad_receive_barcode, awb_number)
                barcode_file_content = get_file_content(esnad_barcode_url)
                
                logging.info(" ESNAD Success Response : %s" % result)
                logging.info(" ESNAD RESPONSE  DETAILS : %s" % response_details)
                picking.write({
                    'esnad_label_url': attach_label_url,
                    'attach_label_id': pdf_lable_file,
                    'carrier_tracking_ref': carrier_tracking_ref,
                    'carrier_log_id': carrier_log,
                    'esnad_awb_number': awb_number,
                    'esnad_barcode_url': esnad_barcode_url,
                    'barcode_label': barcode_file_content,
                })
                # TODO: to be called and handle to inform backend
                picking.inform_backend_tracking_ref('esnad')
                if carrier_tracking_ref:
                    # Inform customer by his shippment tracking ref
                    mail_template = self.env.ref('tatayab_delivery_esnad.esnad_tracking_ref_email_template')
                    mail_template.send_mail(picking.id, force_send=True)


            elif result.get('code') in ['2000', '3000']:  # ('consignor can't be null!'):
                logging.info("\nESNAD Error Response : %s" % result.get('msg'))
                raise UserError("ESNAD Error Response : %s "
                                "\n-----------------------------\n Data send: %s"
                                % (result.get('msg'), json.dumps(req_body)))


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'
    
    esnad_awb_number = fields.Char("Shippment AWB number")
    esnad_label_url = fields.Char("Esnad Label URL")
    esnad_barcode_url = fields.Char("Esnad Barcode URL")
    restrict_boxes = fields.Boolean("Restrict 1 box/shipment", default=True)
