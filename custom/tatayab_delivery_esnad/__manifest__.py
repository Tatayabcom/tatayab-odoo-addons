{
    'name': 'ESNAD Delivery',
    'author': 'Tatayab , Elsayed Iraky',
    'category': 'ALL',
    'summary': """Tatayab """,
    'website': 'http://www.tatayab.com',
    'description': '''
- ESNAD Delivery
''',
    'depends': ['stock', 'sale_stock', 'delivery', 'tatayab_delivery_core'],
    'data': [
        'data/data.xml',
        'data/esnad_mail_data.xml',
        
        'views/delivery_carrier_view_changes.xml',
        'views/stock_picking_view_changes.xml',
    ]
}
