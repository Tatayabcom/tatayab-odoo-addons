import base64

import werkzeug

from odoo import http
from odoo.http import request


class HelpDeskTicket(http.Controller):

    @http.route('/ticket_web_form', type="http", auth="user", website=True)
    def ticket_web_form(self, **kw):
        categories = http.request.env['helpdesk.ticket.category'].search([('active', '=', True)])
        name = http.request.env.user
        users = request.env['res.users'].sudo().search([('active', '=', True)])
        return http.request.render('tatayab_portal_ticket.create_ticket', {
            'categories': categories, 'name': name,
            'users': users,
        })

    @http.route('/create/web_ticket', type="http", auth="user", website=True)
    def create_web_helpdesk_ticket(self, redirect=None, **kw):
        user = request.env['res.users'].sudo().search([('id', '=', kw.get('user_id'))])
        team = request.env['helpdesk.ticket.team'].sudo().search([('user_ids.id', '=', kw.get('user_id'))])
        vals = {
            'category_id': kw.get('category_id'),
            'description': kw.get('description'),
            'name': kw.get('name'),
            'attachment_ids': False,
            'user_id': user.id,
            'team_id': team.id if team else False
        }
        created_tick = request.env['helpdesk.ticket'].sudo().create(vals)
        attachments = request.env['ir.attachment']
        if kw.get('attachment'):
            name = kw.get('attachment').filename
            file = kw.get('attachment')
            attachment_id = attachments.sudo().create({
                'name': name,
                'type': 'binary',
                'datas': base64.b64encode(file.read()),
                'res_model': created_tick._name,
                'res_id': created_tick.id
            })
            created_tick.update({
                'attachment_ids': [(4, attachment_id.id)],
            })
        return request.render("tatayab_portal_ticket.new_portal_ticket_id", {})
        # return request.render("tatayab_portal_ticket.ticket_user_thanks", {})
        # return werkzeug.utils.redirect("/my/tickets")