# -*- coding: utf-8 -*-
{
    'name': "Tatayab Helpdesk Portal Tickets",
    'author': "Tatayab, Omnya Rashwan",
    'website': 'http://www.tatayab.com',
    'category': 'helpdesk',
    'version': '13.0',
    'depends': ['base', 'website', 'de_helpdesk'],

    'data': [
        'views/website_form.xml',
    ],

}