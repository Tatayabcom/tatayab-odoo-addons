# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging


# Ahmed Salama Code Start ---->


class ReturnPicking(models.TransientModel):
	_inherit = 'stock.return.picking'

	def _create_returns(self):
		# Work with default return cycle
		new_picking_id, pick_type_id = super(ReturnPicking, self)._create_returns()
		stock_obj = self.env['stock.picking']
		new_pick = stock_obj.browse(new_picking_id)
		return_state = self._compare_lines(self.picking_id.move_lines.filtered(
			lambda mv: mv.state != 'cancel' and not mv.scrapped), new_pick.move_lines)
		# Edit pickings
		if return_state =='partial_returning':
			self.picking_id.write({'is_partial':True})

		self.picking_id.write({'is_returned': True, 'operation_state': return_state, 'return_picking_id': new_pick.id})
		new_pick.write(new_pick._return_picking_changes(self.picking_id))
		new_pick.move_lines.write({'operation_state': return_state})
		return new_picking_id, pick_type_id
	
	def _compare_lines(self, origin_moves, returned_moves):
		return_state = 'return_pick'
		if len(origin_moves) != len(returned_moves):
			return_state = 'partial_returning'
		else:
			for move in origin_moves:
				match_returned = returned_moves.filtered(lambda rmv: rmv.origin_returned_move_id == move)
				if not match_returned or match_returned.product_uom_qty != move.product_uom_qty:
					return_state = 'partial_returning'
		return return_state
		

# Ahmed Salama Code End.
