# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round

_logger = logging.getLogger(__name__)


class ReturnOtherWarehouse(models.TransientModel):
	_name = 'return.other.warehouse'
	_description = "Return Other Warehouse"
	
	company_id = fields.Many2one('res.company', "Return Company")
	warehouse_id = fields.Many2one('stock.warehouse', "Return Warehouse", required=True)
	from_order = fields.Boolean("Mark Wizard open from order")
	picking_id = fields.Many2one('stock.picking', "Shipping")
	product_return_moves = fields.One2many('return.other.warehouse.line', 'wizard_id', 'Moves')
	order_id = fields.Many2one('sale.order', "Order", domain=[('picking_state_str', 'in', ('ship', 'done'))])
	active_picking_type = fields.Many2one('stock.picking.type', "Type")
	active_warehouse_id = fields.Many2one('stock.warehouse', "Warehouse")
	
	@api.model
	def default_get(self, field_list):
		result = super(ReturnOtherWarehouse, self).default_get(field_list)
		if self.env.context.get('active_model') == 'stock.picking.type':
			picking_type = self.env[self.env.context.get('active_model')].browse(self.env.context.get('active_id'))
			if picking_type:
				result['active_picking_type'] = picking_type.id
				result['active_warehouse_id'] = picking_type.warehouse_id and picking_type.warehouse_id.id or False
		if self.env.context.get('active_model') == 'sale.order':
			order_id = self.env[self.env.context.get('active_model')].browse(self.env.context.get('active_id'))
			if order_id and order_id.warehouse_id:
				warehouse = order_id.warehouse_id
				result['active_picking_type'] = warehouse.out_type_id and warehouse.out_type_id.id or False
				result['active_warehouse_id'] = warehouse.id
		return result
	
	@api.onchange('order_id')
	def _onchange_picking_id(self):
		if self.order_id:
			self.picking_id = self.get_picking()
			product_return_moves = [(5,)]
			if self.picking_id and (self.picking_id.operation_state in ('done', 'cancel') or
					 self.picking_id.is_return or self.picking_id.is_returned):
				raise UserError(_("You may only return in progress pickings."))
			# In case we want to set specific default values (e.g. 'to_refund'), we must fetch the
			# default values for creation.
			line_fields = [f for f in self.env['return.other.warehouse.line']._fields.keys()]
			product_return_moves_data_tmpl = self.env['return.other.warehouse.line'].default_get(line_fields)
			for move in self.picking_id.move_lines:
				if move.state == 'cancel':
					continue
				if move.scrapped:
					continue
				product_return_moves_data = dict(product_return_moves_data_tmpl)
				product_return_moves_data.update(self._prepare_stock_return_picking_line_vals_from_move(move))
				product_return_moves.append((0, 0, product_return_moves_data))
			if self.picking_id and not product_return_moves:
				raise UserError(_("No products to return (only lines in Done state and not fully returned yet can be returned)."))
		else:
			product_return_moves = [(5,)]
			self.picking_id = False
		self.product_return_moves = product_return_moves
	
	def get_picking(self):
		picking_id = self.order_id.picking_ids.filtered(lambda p: p.state in ('assigned', 'done') and
		                                                          p.operation_state == 'ship' and not p.is_return)
		if not picking_id:
			logging.info("Couldn't match filters for picking_id in %s" % self.order_id.picking_ids)
			raise UserError(_("Couldn't found shipping picking to work with!!!"))
		self.picking_id = picking_id[0].id
		return picking_id[0].id
	
	def return_other_warehouse(self):
		"""
		This method shall do:
		 1- approve out picking as fully delivered.
		 2- mark order as returned to other company to change state and inform Backend.
		 3- create receipt order for other company with ref of original order
		"""
		if self.order_id and not self.picking_id:
			self.picking_id = self.get_picking()
		stock_immediate = self.env['stock.immediate.transfer']
		stock_picking_obj = self.env['stock.picking']
		stock_move_obj = self.env['stock.move']
		other_warehouse_receipt = self.warehouse_id.in_type_id
		other_warehouse_input = self.warehouse_id.lot_stock_id
		if not other_warehouse_input or not other_warehouse_receipt:
			raise UserError(_("Missing main data, Please Contact your odoo team"))
		# Get return Source location
		source_location = other_warehouse_receipt.wh_return_stock_loc_id
		if not source_location:
			raise UserError(_("No return location set yet for the receipt [%s]\n "
			                "please get back to odoo team support" % other_warehouse_receipt.display_name))
		if not self.picking_id:
			raise UserError(_("Couldn't found correct picking to use, please contact odoo team"))
		# approve out picking as fully delivered.
		if self.picking_id.state != 'done':
			stock_immediate.create({'pick_ids': self.picking_id.ids}).with_context(stop_informing=True).process()
		# mark order as returned to other company to change state and inform Backend.
		self.order_id.write({'other_company_return': True})
		new_shipping_method = self.picking_id.shipping_method_id
		
		# create receipt order for other company with ref of original order
		picking_data = {
			'company_id': self.env.company.id,
			'user_id': self.env.user.id,
			'is_approved': True,
			'partner_id': self.picking_id.partner_id and self.picking_id.partner_id.id or False,
			'scheduled_date': fields.Datetime.now(),
			'picking_type_id': other_warehouse_receipt.id,
			'location_id': source_location.id,
			'location_dest_id': other_warehouse_input.id,
			'state': 'draft',
			'operation_state': 'return_other',
			'weight': self.picking_id.weight,
			'origin': _("Other Warehouse Order %s") % (self.order_id.name),
			'backend_ref': self.order_id.magento_order_reference,
			'shipping_method_id': new_shipping_method and new_shipping_method.id or False,
			'returned_from_other_company': True,
		}
		_logger.info("---- Returned picking Data:: %s" % picking_data)
		other_picking_id = stock_picking_obj.sudo().create(picking_data)
		_logger.info("---- Returned other_picking_id:: %s" % other_picking_id)
		
		for move_id in self.product_return_moves:
			move_vals = self._prepare_move_default_values(move_id, other_picking_id)
			_logger.info("---- Returned move_vals:: %s" % move_vals)
			stock_move_obj.sudo().create(move_vals)
		other_picking_id.action_confirm()
		other_picking_id.message_post(body=_("Created by Other Warehouse %s Order %s[%s] returned") %
		                                   (self.warehouse_id.display_name, self.order_id.name, self.order_id.origin))
		self.order_id.write({'other_warehouse_receipt_id': other_picking_id.id,
		                     'returned_warehouse_id': self.warehouse_id.id})
		self.picking_id.write({'returned_picking_id': other_picking_id.id, 'operation_state': 'return_other', 'is_returned': True})
		view_id = self.env.ref('sale.view_order_form').id
		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'sale.order',
			'res_id': self.order_id.id,
			'view_id': view_id,
			'type': 'ir.actions.act_window',
			'target': 'current',
		}
	
	def _prepare_move_default_values(self, move, new_picking):
		vals = {
			'product_id': move.product_id.id,
			'product_uom_qty': move.quantity,
			'product_uom': move.uom_id.id,
			'picking_id': new_picking.id,
			'state': 'draft',
			'location_id': new_picking.location_id.id,
			'location_dest_id': new_picking.location_dest_id.id,
			'picking_type_id': new_picking.picking_type_id.id,
			'warehouse_id': self.warehouse_id.id,
			'procure_method': 'make_to_stock',
			'name': new_picking.name,
			'origin': new_picking.origin or new_picking.picking_id.name or new_picking.name,
			'company_id': self.env.company.id,
		}
		return vals
	
	@api.model
	def _prepare_stock_return_picking_line_vals_from_move(self, stock_move):
		quantity = stock_move.product_qty - sum(
			stock_move.move_dest_ids
				.filtered(lambda m: m.state in ['partially_available', 'assigned', 'done'])
				.mapped('move_line_ids.product_qty')
		)
		quantity = float_round(quantity, precision_rounding=stock_move.product_uom.rounding)
		return {'product_id': stock_move.product_id.id,
		        'quantity': quantity,
		        'move_id': stock_move.id,
		        'uom_id': stock_move.product_id.uom_id.id}


class ReturnOtherCompanyLine(models.TransientModel):
	_name = "return.other.warehouse.line"
	_rec_name = 'product_id'
	_description = 'Return Other Warehouse Picking Line'
	
	product_id = fields.Many2one('product.product', string="Product", required=True, domain="[('id', '=', product_id)]")
	quantity = fields.Float("Quantity", digits='Product Unit of Measure', required=True)
	uom_id = fields.Many2one('uom.uom', string='Unit of Measure', related='move_id.product_uom', readonly=False)
	wizard_id = fields.Many2one('return.other.warehouse', string="Wizard")
	move_id = fields.Many2one('stock.move', "Move")

