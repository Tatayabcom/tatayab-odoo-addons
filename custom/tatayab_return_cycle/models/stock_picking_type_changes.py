# -*- coding: utf-8 -*-
from odoo.addons.tatayab_operation_status.models.stock_picking_type_changes import _TYPE_DOMAINS

from odoo import models, fields

igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->
_TYPE_DOMAINS['returned_count'] = [('is_approved', '=', True), '|', ('is_return', '=', True), ('is_returned', '=', True)]


class StockPickingTypeInherit(models.Model):
	_inherit = 'stock.picking.type'
	
	def _compute_returned_count(self):
		picking_obj = self.env['stock.picking']
		for rec in self:
			partial_return_count = picking_obj.search_count([('is_approved', '=', True), ('is_returned', '=', True),
			                                                 ('operation_state', '=', 'partial_return'),
			                                                 ('picking_type_id.warehouse_id', '=', rec.warehouse_id.id)])
			return_count = picking_obj.search_count([
				('is_approved', '=', True), ('is_return', '=', True),
				('picking_type_id', '=', rec.id), ('state', 'in', ('assigned', 'partially_available'))])
			returned_count = picking_obj.search_count([
				('is_approved', '=', True), ('is_returned', '=', True), ('picking_type_id', '=', rec.id)])
			rec.return_count = return_count
			rec.returned_count = returned_count
			rec.partial_return_count = partial_return_count
	
	def open_returned_pickings(self):
		action = self._get_action('stock.stock_picking_action_picking_type')
		action['context'] = {'contact_display': 'partner_address'}
		action['domain'] = [('is_approved', '=', True), ('is_return', '=', True), ('is_returned', '=', False),
		                    ('state', 'in', ('assigned', 'partially_available')),
		                    ('picking_type_id.warehouse_id', '=', self.warehouse_id.id)]
		return action
	
	def open_partial_returned_pickings(self):
		action = self._get_action('stock.stock_picking_action_picking_type')
		action['context'] = {'contact_display': 'partner_address'}
		action['domain'] = [('is_approved', '=', True), ('is_returned', '=', True), ('operation_state', '=', 'partial_return'),
		                    ('picking_type_id.warehouse_id', '=', self.warehouse_id.id)]
		return action
	
	def _get_core_domain(self):
		
		domain = super(StockPickingTypeInherit, self)._get_core_domain()
		if self.name == 'Returns':
			return domain
		return domain + [('is_returned', '=', False), ('is_return', '=', False)]
	
	wh_return_stock_loc_id = fields.Many2one('stock.location', "Return Location SRC", check_company=True,
	                                         help="Used on Return Other Warehouse source location")
	partial_return_count = fields.Integer("Partial Return", compute=_compute_returned_count)
	return_count = fields.Integer("Return", compute=_compute_returned_count)
	returned_count = fields.Integer("Returned", compute=_compute_returned_count)

# Ahmed Salama Code End.
