# -*- coding: utf-8 -*-
from odoo.addons.tatayab_return_cycle.models.sale_order_changes import RETURN_STATUS
from odoo.addons.tatayab_operation_status.models.stock_picking_type_changes import _TYPE_DOMAINS
_TYPE_DOMAINS['done_count'] = [('state', '=', 'done'),
                               ('operation_state', 'in', ('done', 'return_done', 'return_other_done', 'returned')),
                               ('is_approved', '=', True)]
from odoo import models, fields
from odoo.exceptions import UserError
import logging
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->


class StockPickingInherit(models.Model):
	_inherit = 'stock.picking'
	
	is_returned = fields.Boolean("Returned picking?", help="Checked if this picking is returned.")
	is_return = fields.Boolean("Return picking?", help="Checked if this picking is return of another picking.")
	is_partial =  fields.Boolean("Partial picking?", help="Checked if this picking is Partial",tracking=True)
	returned_picking_id = fields.Many2one("stock.picking", "Returned From Picking"
	                                      , help="picking ref of the source picking which returned here.")
	return_picking_id = fields.Many2one("stock.picking", "Return on Picking"
	                                    , help="picking ref of the returned picking.")
	operation_state = fields.Selection(selection_add=RETURN_STATUS)
	returned_from_other_company = fields.Boolean("Returned From Other Warehouse")
	
	def _return_picking_changes(self, returned_picking_id):
		"""
		Collect details to be edited on return picking, separated to be inherited on other modules
		:param returned_picking_id: picking which is returned
		:return: dict of fields
		"""
		return {
			'returned_picking_id': returned_picking_id.id,
			'operation_state': 'return_pick',
			'is_return': True,
			'note': self.env.context.get('return_reason'),
		}
	
	def button_validate(self):
		"""
		change state of picking and order in case of return done and inform backend
		:return: SUPER
		"""
		order_obj = self.env['sale.order']
		for picking in self:
			if picking.is_return:
				# Return itself
				return_vals = {'operation_state': 'return_done'}
				picking.move_lines.write(return_vals)
				picking.write(return_vals)
				# Returned Picking doaa
				returned_vals = {'operation_state': 'returned'}
				status = 'fully_returned'
				if picking.returned_picking_id.is_partial:
					returned_vals = {'operation_state': 'partial_return'}
					status = 'partial_returned'
				picking.returned_picking_id.move_lines.write(returned_vals)
				picking.returned_picking_id.write(returned_vals)

				data = {
					"entity": {
						"entity_id": picking.sale_id.magento_order_id,
						"status": status,
						"increment_id": picking.sale_id.magento_order_reference
					}
				}
				picking.sale_id.send_state_order_from_bulk_in_magento(data)
			if picking.returned_from_other_company:
				picking.operation_state = 'return_other_done'
				order_id = order_obj.sudo().search([('magento_order_reference', '=', picking.backend_ref)], limit=1)
				logging.warning("\n ___Other Warehouse return order: %s" % order_id.display_name)
				log_extra_details = "Returned from another warehouse"
				if order_id:
					order_id.sudo().write({'other_company_returned': True, 'state': 'done'})
					order_id.active_picking_id.write({'operation_state': 'return_other_done'})
					log_extra_details += "<br/>From warehouse: [%s] to warehouse [%s]" % \
					                     (order_id.warehouse_id.display_name,
					                      order_id.returned_warehouse_id.display_name)
					order_id.message_post(body=log_extra_details)
				else:
					raise UserError("Something went wrong while trying to find order with ref: %s on other Warehouse"
					              % picking.backend_ref)
				data = {
					"entity": {
						"entity_id": picking.sale_id.magento_order_id,
						"status": 'fully_returned',
						"increment_id": picking.sale_id.magento_order_reference
					}
				}
				picking.sale_id.send_state_order_from_bulk_in_magento(data)

		return super(StockPickingInherit, self).button_validate()


class StockMoveLineInherit(models.Model):
	_inherit = 'stock.move.line'
	
	operation_state = fields.Selection(selection_add=RETURN_STATUS)


class StockMoveInherit(models.Model):
	_inherit = 'stock.move'
	
	operation_state = fields.Selection(selection_add=RETURN_STATUS)

# Ahmed Salama Code End.
