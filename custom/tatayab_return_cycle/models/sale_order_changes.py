# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import logging

from odoo.exceptions import ValidationError

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->
RETURN_STATUS = [
		('return_pick', 'Returning'),  # for both return side to show it's on processing
		('partial_returning', 'Partial Returning'),  # for picking which is partially returned
		('partial_return', 'Partial Returned'),  # for picking which is partially returned
		('returned', 'Returned'),  # for picking which is returned to show it's all done return
		('return_done', 'Done Return'),  # for picking which is a return to show it's all done return
		('return_other', 'Returning to other WH'),  # for picking which is Under Other Warehouse Return
		('return_other_done', 'Returned to other WH'),  # for picking which is Other Warehouse Returned
		('partial_return_other_done', 'Partial Returned to other WH'),  # for Partial picking which is Other Warehouse Returned
	]


class SaleOrderInherit(models.Model):
	_inherit = 'sale.order'

	@api.onchange('state', 'picking_ids', 'other_company_return', 'other_company_returned')
	@api.depends('picking_ids.state', 'other_company_return', 'other_company_returned')
	def _compute_active_picking_operation_stage(self):
		"""
		compute last picking and it's status
		"""
		for order in self:
			if order.other_company_returned:
				operation_state = 'return_other_done'
				active_picking_id = order.active_picking_id
			elif order.other_company_return:
				operation_state = 'return_other'
				active_picking_id = order.active_picking_id
			else:
				last_picking = order.picking_ids.sorted(lambda l: l.id, reverse=True)
				if last_picking:
					last_picking = last_picking[0]
				operation_state = active_picking_id = False
				if order.state in ('sale', 'done'):
					if last_picking:  # Case of getting status from last picking
						operation_state = last_picking.operation_state
						active_picking_id = last_picking
					else:  # Case of critical issue where there are no pickings
						operation_state = 'without_pickings'
				else:
					if order.state != 'sent':  # fix problem when install module and there is order in sent state.
						operation_state = order.state
			order.comp_op_state = operation_state
			if order.operation_state != operation_state:
				logging.info(yellow + "__ Picking State updated For order %s" % order.name + reset)
				order.write({'operation_state': operation_state})
				for invoice in order.invoice_ids:
					invoice.operation_state = operation_state
			if order.active_picking_id != active_picking_id:
				logging.info(yellow + "__ Active Picking updated For order %s" % order.name + reset)
				order.active_picking_id = active_picking_id

	operation_state = fields.Selection(selection_add=RETURN_STATUS)
	comp_op_state = fields.Selection(selection_add=RETURN_STATUS, compute=_compute_active_picking_operation_stage)
	other_company_return = fields.Boolean("Under Other Warehouse Return")
	other_company_returned = fields.Boolean("Other Warehouse Returned")
	returned_warehouse_id = fields.Many2one('stock.warehouse', "Returned Warehouse")
	other_warehouse_receipt_id = fields.Many2one('stock.picking', "Other Warehouse Receipt")

	def workflow_full_cancel(self):
		super(SaleOrderInherit, self).workflow_full_cancel()
		# -> Prepare Return Picking
		if self.state == 'sale':
			picking_obj = self.env['stock.picking']
			picking_return_obj = self.env['stock.return.picking']
			pickings_to_return = self.picking_ids.filtered(lambda p: p.state == 'done' and not p.is_returned)
			logging.info(
				yellow + "___ pickings_to_return : %s are returned" % pickings_to_return.mapped('display_name') + reset)

			return_pickings = picking_obj

			for return_pick in pickings_to_return:
				new_ctx = {
					'active_id': return_pick.id,
					'active_ids': return_pick.ids,
					'active_model': 'stock.picking'
				}
				return_wiz = picking_return_obj.with_context(**new_ctx).create({'picking_id': return_pick.id})
				return_wiz._onchange_picking_id()
				# Check For type of return
				new_picking_id, pick_type_id = return_wiz.with_context(stop_informing=True)._create_returns()
				return_pick.write({
					'is_returned': True
				})
				new_picking = picking_obj.browse(new_picking_id)
				new_picking.write({
					'is_return': True,
					'is_approved': True,
					'from_picking_id': return_pick.id,
					'runsheet_id': False
				})
				return_pickings += new_picking
				logging.info(yellow + "___ New Return Picking : %s" % new_picking.mapped('display_name') + reset)
			if return_pickings:
				# Auto Confirm return picking
				Transfer = self.env['stock.immediate.transfer']
				Transfer.with_context(stop_informing=True).create({'pick_ids': return_pickings.ids}).process()
				logging.info("___ Total Picking After Return : %s" % self.picking_ids)

# Ahmed Salama Code End.
	# TODO complete this  method as development
	def cancel_order_shipments_invoice(self):
		for rec in self:
			if rec.state == "cancel":
				raise ValidationError(_("This order already cancelled"))
			if rec.magento_payment_method_id.is_cod:
				if not rec.is_approved:
					pickings_to_cancel = rec.picking_ids.filtered(lambda p: p.state not in ['done', 'cancel'])
					if pickings_to_cancel:
						pickings_to_cancel.action_cancel()
						msg = "___ Before approve order pickings_to_cancel : %s are cancelled" % pickings_to_cancel.mapped('display_name')
						logging.info(yellow + msg + reset)
						rec.message_post(body="<b style='color:green'>Cancel COD order not approved </b><br/><span>%s</span>" % msg)

				else:
					# cancel shipment when it's  not out going from location
					pickings_to_cancel = rec.picking_ids.filtered(lambda p: p.operation_state in ['pick'])
					if pickings_to_cancel:
						pickings_to_cancel.action_cancel()
						logging.info(yellow + "___ After approve order pickings_to_cancel : %s are cancelled"
									 % pickings_to_cancel.mapped('display_name') + reset)
						msg = "___ After approve order pickings_to_cancel : %s are cancelled"  % pickings_to_cancel.mapped('display_name')
						rec.message_post(
							body="<b style='color:green'>Cancel COD Picking order  approved </b><br/><span>%s</span>" % msg)
					# TODO cancel invoice in magento
					posted_invoices_to_cancel = rec.invoice_ids.filtered(lambda p: p.state in ['posted','draft'])
					if posted_invoices_to_cancel:
						for inv_cancel in posted_invoices_to_cancel:
							inv_cancel.cancel_invoice_from_odoo_to_magento()
					# cancel invoice when it's  not paid
					invoices_to_cancel = rec.invoice_ids.filtered(lambda p: p.payment_state in ['not_paid'])
					if invoices_to_cancel:
						invoices_to_cancel.button_draft()
						invoices_to_cancel.button_cancel()
						logging.info(yellow + "___ After approve order invoices_to_cancel : %s are cancelled"
									 % invoices_to_cancel.mapped('display_name') + reset)
				# cancel sale order from url of magento
				rec.cancel_order_in_magento()
				rec.write({"state": "cancel"})
			else:
				# cancel shipment when it's  not out going from location
				pickings_to_cancel = rec.picking_ids.filtered(lambda p: p.operation_state in ['pick'])
				if pickings_to_cancel:
					pickings_to_cancel.action_cancel()
					logging.info(yellow + "___ After approve order pickings_to_cancel : %s are cancelled"
								 % pickings_to_cancel.mapped('display_name') + reset)

				# create credit note to return many invoice when it's  paid
				out_refund_invoice = rec.invoice_ids.filtered(lambda p: p.state in ['posted'] and p.move_type in ['out_refund'])
				if not out_refund_invoice:
					invoices_to_returned = rec.invoice_ids.filtered(lambda p: p.state  in ['posted'] and p.move_type in ['out_invoice'])
					if invoices_to_returned:
						for inv in invoices_to_returned:
							# We refund the invoice
							refund_wizard = self.env['account.move.reversal'].with_context(active_model="account.move",active_ids=inv.ids).create(
								{
									'reason': ' refund from invoice %s' %(inv.name),
									'refund_method': 'refund',
									'date': inv.date,
									'journal_id': inv.journal_id.id,
								})
							res = refund_wizard.reverse_moves()
							refund = self.env['account.move'].browse(res['res_id'])
							for refund_inv in refund:
								refund_inv.action_create_credit_memo('offline', True)
							refund.action_post()
							message = "<b style='color:orange'>create credit note to return  : %s  </b>" %inv.name
							rec.message_post(body=message)
				rec.write({'state': 'cancel'})




	def launch_return_other_warehouse(self):
		"""
        Open Wizard of return other warehouse
        :return: wizard
        """
		view_id = self.env.ref('tatayab_return_cycle.return_other_warehouse_form_view').id
		return {
			'context': {'default_order_id': self.id, 'default_from_order': True},
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'return.other.warehouse',
			'view_id': view_id,
			'type': 'ir.actions.act_window',
			'target': 'new',
		}
