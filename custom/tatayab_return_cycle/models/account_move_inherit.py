from odoo import fields, models


class AccountMoveInherit(models.Model):
    _inherit = 'account.move'

    operation_state = fields.Selection(selection=[('draft', 'Draft'),
                                                  ('without_pickings', 'Without Pickings'),
                                                  ('pick', 'Picking'),
                                                  ('pack', 'Packing'),
                                                  ('ship', 'Shipping'),
                                                  ('done', 'Done Shipment'),
                                                  ('lock', 'Locked'),
                                                  ('cancel', 'Canceled'), ('return_pick', 'Returning'),
                                                  ('partial_returning', 'Partial Returning'),
                                                  ('partial_return', 'Partial Returned'),
                                                  ('returned', 'Returned'),
                                                  ('return_done', 'Done Return'),
                                                  ('return_other', 'Returning to other WH'),
                                                  ('return_other_done', 'Returned to other WH'),
                                                  ('partial_return_other_done', 'Partial Returned to other WH'),
                                                  ], string="Operation State", )

