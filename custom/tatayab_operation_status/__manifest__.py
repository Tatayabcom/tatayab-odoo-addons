# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Operation Status",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'Inventory',
    'summary': """Tatayab Operation Status""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '1.0',
    'depends': ['sale_stock', 'tatayab_magento_connect_changes', 'tatayab_delivery_core','account'],
    'data': [
        'data/warehouse_data.xml',
        'data/picking_runsheet_data.xml',
        'data/fixing_old_data.xml',
        
        'security/security.xml',
        'security/ir.model.access.csv',
        
        'reports/paper_format.xml',
        'reports/report_operation_label.xml',
        'reports/report_picklist_order.xml',
        'reports/delivery_label_by_pkg.xml',
        'reports/sale_report_templates_changes.xml',
        'reports/picking_report_barcode_views.xml',
        'reports/picking_runsheet_report_views.xml',
        
        'wizard/bulk_transfer_views.xml',
    
        'views/res_config_settings.xml',
        'views/account_move_view_changes.xml',
        'views/delivery_carrier_inherit_view.xml',
        'views/stock_warehouse_view_changes.xml',
        'views/stock_picking_view_changes.xml',
        'views/sale_order_view_changes.xml',
        'views/stock_picking_type_view_changes.xml',
        'views/picking_runsheet_views.xml',
        'views/courier_sale_order.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
