# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->


class ReportPickList(models.AbstractModel):
	_name = 'report.tatayab_operation_status.report_picklist_order'
	_description = "Report Order Pick List"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['stock.picking'].browse(docids)
		records.update_print_count('picklist_print_count')
		return {
			'doc_ids': records.ids,
			'doc_model': 'stock.picking',
			'docs': records,
		}


class DeliveryLabelReport(models.AbstractModel):
	_name = 'report.tatayab_operation_status.report_delivery_label_by_pkg'
	_description = "Report Delivery Label by PKG"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['stock.picking'].browse(docids)
		return {
			'doc_ids': records.ids,
			'doc_model': 'stock.picking',
			'docs': records,
		}
	
	
class OperationLabelReport(models.AbstractModel):
	_name = 'report.tatayab_operation_status.report_operation_label'
	_description = "Report Delivery Label by PKG"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['stock.picking'].browse(docids)
		return {
			'doc_ids': records.ids,
			'doc_model': 'stock.picking',
			'docs': records,
		}
	
	
class PickingORderBarcodeReport(models.AbstractModel):
	_name = 'report.tatayab_operation_status.report_picking_order_barcode'
	_description = "Order Picklist(Barcodes)"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['stock.picking'].browse(docids)
		records.update_print_count('picklist_print_count')
		return {
			'doc_ids': records.ids,
			'doc_model': 'stock.picking',
			'docs': records,
		}


class SaleOrderPrintReport(models.AbstractModel):
	_name = 'report.sale.action_report_saleorder'
	_description = "Report Delivery Label by PKG"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['stock.picking'].browse(docids)
		records.update_print_count('order_print_count')
		
		return {
			'doc_ids': records.ids,
			'doc_model': 'sale.order',
			'docs': records,
		}


class PickingRunsheetReport(models.AbstractModel):
	_name = 'report.tatayab_operation_status.report_picking_runsheet'
	_description = "Report Picking Run-sheet"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['picking.runsheet'].browse(docids)
		records.update_print_count('runsheet_print_count')
		return {
			'doc_ids': records.ids,
			'doc_model': 'picking.runsheet',
			'docs': records,
		}
	
	
class ReportSaleOrderList(models.AbstractModel):
	_name = 'report.sale.report_saleorder'
	_description = "Report Sale Order"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['sale.order'].browse(docids)
		records.update_print_count('order_print_count')
		return {
			'doc_ids': records.ids,
			'doc_model': 'sale.order',
			'docs': records,
		}


class ReportSaleOrderChanges(models.AbstractModel):
	_name = 'report.tatayab_operation_status.sale_report_templates_changes'
	_description = "Report Sale Order"
	
	def _get_report_values(self, docids, data=None):
		if not docids and data.get('ids'):
			docids = data['ids']
		records = self.env['sale.order'].browse(docids)
		records.update_print_count('order_print_count')
		return {
			'doc_ids': records.ids,
			'doc_model': 'sale.order',
			'docs': records,
		}
# Ahmed Salama Code End.
