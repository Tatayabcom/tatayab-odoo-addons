# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons.tatayab_operation_status.models.stock_picking_changes import _PICKING_STAGES
import logging

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    @api.onchange('state', 'picking_ids')
    @api.depends('picking_ids.state')
    def _compute_active_picking_operation_stage(self):
        """
        compute last picking and it's status
        :return:
        """
        for order in self:
            last_picking = order.picking_ids.sorted(lambda l: l.id, reverse=True)
            if last_picking:
                last_picking = last_picking[0]
            operation_state = active_picking_id = False
            if order.state in ('sale', 'done'):
                if last_picking:  # Case of getting status from last picking
                    operation_state = last_picking.operation_state
                    active_picking_id = last_picking
                else:  # Case of critical issue where there are no pickings
                    operation_state = 'without_pickings'
            else:
                if order.state != 'sent':  # fix problem when install module and there is order in sent state.
                    operation_state = order.state

            order.comp_op_state = operation_state
            if order.operation_state != operation_state:
                logging.info(yellow + "__ Picking State updated For order %s[%s]" % (order.name, order.origin) + reset)
                order.write({'operation_state': operation_state})
            if order.active_picking_id != active_picking_id:
                logging.info(yellow + "__ Active Picking updated For order %s[%s]" % (order.name, order.origin) + reset)
                order.active_picking_id = active_picking_id
            # order.send_state_order_in_magento()

    operation_state = fields.Selection(_PICKING_STAGES, "Operation State", default='pick',
                                       compute=_compute_active_picking_operation_stage, store=True)
    comp_op_state = fields.Selection(_PICKING_STAGES, compute=_compute_active_picking_operation_stage,
                                     help="This field is created to be not stored and call compute method")
    picking_policy = fields.Selection(default='one', tracking=True)
    active_picking_id = fields.Many2one('stock.picking', "Active Picking",
                                        store=True, tracking=True, compute='_compute_active_picking_operation_stage',
                                        help="Completion last active delivery order for this order.")
    runsheet_id = fields.Many2one('picking.runsheet', string="Run-sheet", related="active_picking_id.runsheet_id")
    order_print_count = fields.Integer()

    country_id = fields.Many2one(comodel_name="res.country", string="Order Country", tracking=True,
                                 related='partner_shipping_id.country_id', store=True)
    # these fields  to pass value from runsheet  to SO
    driver_type = fields.Selection(selection=[('internal', "Internal Driver"),
                                              ('external', "External Driver"),
                                              ('international', 'Shipping Comp.')],
                                   string="Shipping Type", default='internal', tracking=True)
    external_driver_id = fields.Many2one("res.partner", domain=[('is_driver', '=', True)],
                                 string="External Driver", tracking=True)
    emp_driver_id = fields.Many2one("hr.employee", string="Driver", tracking=True,
                                    domain=[('is_driver', '=', True)])
    carrier_id = fields.Many2one('delivery.carrier', string="Carrier")

    @api.model
    def update_old_address(self):
        sql = """ UPDATE stock_picking AS pk SET country_id = partner.country_id FROM res_partner AS partner 
        WHERE partner.id = pk.partner_id;"""
        self.env.cr.execute(sql)

    def update_print_count(self, key):
        if self.exists():
            if not key:
                return
            sql = """UPDATE sale_order SET {key} = COALESCE({key}, 0) + 1 WHERE id IN %s""".format(key=key)
            self.env.cr.execute(sql, (tuple(self.ids),))

# Ahmed Salama Code End.
