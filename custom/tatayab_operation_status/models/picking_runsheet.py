# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import logging
from odoo.addons.tatayab_delivery_core.models.shipping_method import DRIVER_TYPE
from odoo.tools import float_compare
igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->


class PickingRunsheet(models.Model):
	_name = 'picking.runsheet'
	_description = 'Picking Runsheet'
	_inherit = ['mail.thread', 'mail.activity.mixin']
	
	@api.onchange('state')
	def _get_picking_domain(self):
		for runsheet in self:
			domain = [('is_returned', '=', False), ('picking_type', '=', 'ship'),
			          ('state', '!=', 'cancel')]
			if runsheet.state == 'done':
				domain = [('is_returned', '=', False), ('picking_type', '=', 'done'),
				          ('state', '!=', 'cancel')]
			return {'picking_ids': {'domain': domain}}
	
	name = fields.Char("Runsheet #", default="Draft", readonly=True, tracking=True)
	date_delivery = fields.Date("Delivery Date", tracking=True)
	handover_date = fields.Datetime("Handover Date", tracking=True)
	pre_date_delivery = fields.Date("Pre Delivery")
	driver_type = fields.Selection(DRIVER_TYPE, "Shipping Type", default='internal',
	                               tracking=True)
	partner_id = fields.Many2one("res.partner", domain=[('is_driver', '=', True)],
	                             string="External Driver", tracking=True)
	emp_driver_id = fields.Many2one("hr.employee", string="Driver", tracking=True,
	                                domain=[('is_driver', '=', True)])
	carrier_id = fields.Many2one('delivery.carrier', string="Carrier")
	picking_type_id = fields.Many2one('stock.picking.type', "Picking Type")
	picking_ids = fields.One2many("stock.picking", "runsheet_id", string="Deliveries", help="Picking on stage shipping")
	active = fields.Boolean("Active", default=True)
	note = fields.Text("Note", tracking=True)
	state = fields.Selection([('draft', 'Draft'), ('hand_over', 'Handover'),
	                          ('confirm', 'Confirmed'), ('done', 'Done'), ('cancel', 'Cancelled')], default="draft",
	                         tracking=True)
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company.id)
	runsheet_print_count = fields.Integer("Runsheet Print Count")
	picking_count = fields.Integer("Pickings Count", compute='_count_picking')
	check_access_usr = fields.Boolean(compute='dispatchar_control_access')
	user_id = fields.Many2one(comodel_name="res.users", string="Dispatcher",
	                          required=False, readonly=False, default=lambda self: self.env.user.id)
	static_ids = fields.One2many('picking.runsheet.statics', 'runsheet_id', "Statics")

	@api.depends('user_id', 'check_access_usr')
	def dispatchar_control_access(self):
		if self.env.user.has_group('pick_pack_ship.runsheet_dispatcher_control_group'):
			self.check_access_usr = True
		else:
			self.check_access_usr = False
	
	@api.onchange('picking_ids')
	def _count_picking(self):
		for runsheet in self:
			runsheet.picking_count = len(runsheet.picking_ids)
	
	@api.model
	def create(self, vals):
		code = 'picking.runsheet'
		if vals.get('driver_type') == 'international':
			code = 'picking.runsheet.int'
		vals['name'] = self.env['ir.sequence'].next_by_code(code) or ' '
		return super(PickingRunsheet, self).create(vals)
	
	def action_validate(self):
		self.ensure_one()
		# Validate Success Shipping
		if self.picking_ids:
			self.picking_ids.action_done_shipment()
			self.action_done()
		else:
			raise Warning(_("No pickings to be validate with"))
	
	def action_confirm(self):
		self.ensure_one()
		self.picking_ids.write({
			'picking_emp_id': self.emp_driver_id and self.emp_driver_id.id,
			'runsheet_id': self.id,
			'date_delivery': self.date_delivery,
		})
		# if not self.emp_driver_id:
		if not self.emp_driver_id and not self.carrier_id and not self.partner_id:
			raise UserError(_('You can not confirm without Driver.'))
		self.write({'state': 'confirm'})

	def create_entry_reverse(self,pick,runsheet_id,journal_id):
		prec = self.env.ref('base.KWD').decimal_places
		move_lines = []
		move_lines.append((0, 0, {
			'name': pick.name,
			'partner_id': pick.sale_id.partner_id.id,
			'account_id': pick.sale_id.partner_id.property_account_receivable_id.id,
			'debit':  pick.sale_id.base_grand_total if float_compare(pick.sale_id.base_grand_total, 0.0,
																		 precision_digits=prec) > 0 else 0.0,
			'credit': 0.0 if float_compare(pick.sale_id.base_grand_total, 0.0,
										  precision_digits=prec) > 0 else -pick.sale_id.base_grand_total,
			'currency_id': self.env.ref('base.KWD').id, }))
		move_lines.append((0, 0, {
			'name': runsheet_id.carrier_id.name,
			'partner_id': runsheet_id.carrier_id.com_partner_id.id,
			'account_id': runsheet_id.carrier_id.com_partner_id.property_account_receivable_id.id,
			'debit': 0.0 if float_compare(pick.sale_id.base_grand_total, 0.0,
										   precision_digits=prec) > 0 else -pick.sale_id.base_grand_total,
			'credit': pick.sale_id.base_grand_total if float_compare(pick.sale_id.base_grand_total, 0.0,
															   precision_digits=prec) > 0 else 0.0,
			'currency_id': self.env.ref('base.KWD').id,
		}))
		move_vals = {
			'ref': "Run-Sheet reverse for pick [" + pick.name + " ]",
			'journal_id': journal_id,
			'line_ids': move_lines,
			'amount_total': pick.sale_id.base_grand_total,
			'name': '/',
			'move_type': 'entry',
			'currency_id': self.env.ref('base.KWD').id,
		}
		move = self.env['account.move'].create(move_vals)
		move.action_post()
		message = "Now , reverse entry created  %s with total amount :%s KWD for company %s to shipment %s" % \
				  (move.ref, pick.sale_id.base_grand_total, runsheet_id.carrier_id.name,pick.name)
		self.message_post(body="<b style='color:red'>Create reverse Entry : </b><br/><span>%s</span>" % message)

	def create_entry_for_sale_order(self):
		move_lines = []
		for rec in self:
			prec = self.env.ref('base.KWD').decimal_places
			if rec.driver_type == 'international':
				journal_id = int(self.env['ir.config_parameter'].sudo().get_param('journal_receivable_id'))
				if not journal_id:
					raise ValidationError(_("Please set the journal in run-sheet configuration "))

				if not rec.carrier_id.com_partner_id:
					raise ValidationError(_("Please set company partner for %s ")% rec.name)

				total_sales_order_amount = 0.0
				for pick in rec.picking_ids:
					if pick.sale_id and pick.sale_id.magento_payment_method_id.is_cod:
						# to create reverse of entry
						if pick.runsheet_id:
							rec.create_entry_reverse(pick,pick.runsheet_id,journal_id)
						# all credit for sale  order
						move_lines.append((0,0,{
						'name': pick.name,
						'partner_id': pick.sale_id.partner_id.id,
						'account_id': pick.sale_id.partner_id.property_account_receivable_id.id,
						'debit': 0.0 if float_compare(pick.sale_id.base_grand_total, 0.0, precision_digits=prec) > 0 else -pick.sale_id.base_grand_total,
						'credit': pick.sale_id.base_grand_total if float_compare(pick.sale_id.base_grand_total, 0.0, precision_digits=prec) > 0 else 0.0,
						'currency_id': self.env.ref('base.KWD').id,}))
						total_sales_order_amount += pick.sale_id.base_grand_total
						logging.info(yellow + "========== pick.sale_id.base_grand_total ========= %s "%str(pick.sale_id.base_grand_total) + reset)
				logging.info(yellow + "========== total_sales_order_amount ========= %s "%str(total_sales_order_amount) + reset)
				#debit for all SO
				if total_sales_order_amount > 0.0:
					move_lines.append((0,0,{
							'name': rec.carrier_id.name,
							'partner_id': rec.carrier_id.com_partner_id.id,
							'account_id': rec.carrier_id.com_partner_id.property_account_receivable_id.id,
							'credit': 0.0 if float_compare(total_sales_order_amount, 0.0, precision_digits=prec) > 0 else -total_sales_order_amount,
							'debit': total_sales_order_amount if float_compare(total_sales_order_amount, 0.0, precision_digits=prec) > 0 else 0.0,
							'currency_id': self.env.ref('base.KWD').id,
						}))
					move_vals = {
						'ref': "Run-Sheet [" + rec.name + " ]",
						'journal_id': journal_id,
						'line_ids': move_lines,
						'amount_total': total_sales_order_amount,
						'name': '/',
						'move_type': 'entry',
						'currency_id': self.env.ref('base.KWD').id,
					}
					move = self.env['account.move'].create(move_vals)
					move.action_post()
					message = "Now , new entry created  %s with total amount :%s KWD for company %s" % \
							  (move.ref, total_sales_order_amount, rec.carrier_id.name)
					self.message_post(body="<b style='color:red'>Create Entry : </b><br/><span>%s</span>" % message)
				else:

					self.message_post(body="<b style='color:red'> not  create Entry for run sheet  </b><br/>")


	def action_handover(self):
		self.ensure_one()
		if not self.emp_driver_id and not self.carrier_id and not self.partner_id:
			raise UserError(_('You can not confirm without Driver.'))
		else:
			for runsheet in self:
				for pick in runsheet.picking_ids:
					if pick.sale_id:
						pick.sale_id.write({
							'emp_driver_id': runsheet.emp_driver_id and runsheet.emp_driver_id.id or False,
							'external_driver_id': runsheet.partner_id and runsheet.partner_id.id or False,
							'carrier_id': runsheet.carrier_id and runsheet.carrier_id.id or False,
							'driver_type': runsheet.driver_type})
				runsheet.create_entry_for_sale_order()

			self.write({'state': 'hand_over'})
	
	def action_done(self):
		self.write({"state": "done"})
	
	def action_cancel(self):
		self.write({"state": "cancel"})
	
	def action_reset(self):
		self.write({"state": "draft"})
	
	def write(self, vals):
		if vals.get('date_delivery') and self.date_delivery:
			vals['pre_date_delivery'] = self.date_delivery
		if vals.get('driver_type'):
			if vals.get('driver_type') != 'external' and self.partner_id:
				vals['partner_id'] = False
			elif vals.get('driver_type') != 'internal' and self.emp_driver_id:
				vals['emp_driver_id'] = False
		if vals.get('state') == 'hand_over':
			vals['handover_date'] = fields.Datetime.now()
		return super(PickingRunsheet, self).write(vals)
	
	@api.onchange('date_delivery')
	@api.depends('date_delivery')
	def _change_date_delivery(self):
		for sheet in self:
			if sheet.date_delivery and sheet.picking_ids:
				for picking in sheet.picking_ids:
					picking.date_delivery = sheet.date_delivery
	
	@api.onchange('driver_type')
	def _remove_unneeded_date(self):
		# for picking in self:
		self.ensure_one()
		if self.driver_type:
			if self.driver_type != 'external' and self.partner_id:
				self.partner_id = False
			elif self.driver_type != 'internal' and self.emp_driver_id:
				self.emp_driver_id = False
	
	def update_driver(self):
		for runsheet in self:
			runsheet.picking_ids.write({
				'picking_emp_id': runsheet.emp_driver_id and runsheet.emp_driver_id.id or False,
				'external_driver_id': runsheet.partner_id and runsheet.partner_id.id or False,
				'carrier_id': runsheet.carrier_id and runsheet.carrier_id.id or False,
				'driver_type': runsheet.driver_type})
	
	def update_print_count(self, key):
		if self.exists():
			if not key:
				return
			sql = """
                UPDATE picking_runsheet
                    SET {key} = COALESCE({key}, 0) + 1
                WHERE id IN %s""".format(key=key)
			self.env.cr.execute(sql, (tuple(self.ids),))


class PickingRunsheetStatics(models.Model):
	_name = 'picking.runsheet.statics'
	_description = "Runsheet Statics"
	
	runsheet_id = fields.Many2one('picking.runsheet', "Runsheet")
	magento_payment_method_id = fields.Many2one('magento.payment.method', "Payment Method(JRN)",
	                                            domain=[('type', '=', 'cash')])
	picking_ids = fields.Many2many('stock.picking', string="Deliveries")
	picking_count = fields.Integer("Count", compute='_compute_data')
	amount_total = fields.Monetary("Total", compute='_compute_data')
	currency_id = fields.Many2one('res.currency', "Currency",
	                              default=lambda self: self.env.user.company_id.currency_id)
	payment_collected = fields.Boolean("Collected", compute='_compute_data')
	
	def _compute_data(self):
		for static in self:
			pickings = static.runsheet_id.picking_ids.filtered(
				lambda l: l.sale_id.magento_payment_method_id == static.magento_payment_method_id)
			static.picking_count = len(pickings)
			static.amount_total = sum(s.sale_id.amount_total for s in pickings)
			static.payment_collected = all(coll for coll in pickings.mapped('payment_collected'))
# Ahmed Salama Code End.
