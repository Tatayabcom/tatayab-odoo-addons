# -*- coding: utf-8 -*-
###############################################################################
#
#    Tatayab Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tatayab(<http://www.tatayab.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from . import res_config_changes
from . import account_move_changes
from . import delivery_carrier_inherit
from . import stock_warehouse_changes
from . import stock_picking_type_changes
from . import stock_picking_changes
from . import sale_order_changes
from . import picking_runsheet