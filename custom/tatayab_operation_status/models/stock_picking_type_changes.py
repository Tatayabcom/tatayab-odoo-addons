# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

import time
import calendar

igrey = '\x1b[38;21m'
yellow = '\x1b[33;21m'
red = '\x1b[31;21m'
bold_red = '\x1b[31;1m'
reset = '\x1b[0m'
green = '\x1b[32m'
blue = '\x1b[34m'
# Ahmed Salama Code Start ---->
_TYPE_DOMAINS = {
    # CORE FIELDS
    'count_picking_draft': [('state', '=', 'draft'), ('is_approved', '=', True)],
    'count_picking_waiting': [('state', '=', 'confirmed'), ('is_approved', '=', True)],
    'count_picking_ready': [('state', '=', 'assigned'), '|', ('is_approved', '=', True),
                            ('picking_type_id.code', '=', 'internal')],
    'count_picking_all_ready': [('state', '=', 'assigned')],
    'count_picking': [('state', 'in', ('assigned', 'waiting', 'confirmed')), ('is_approved', '=', True)],
    'count_picking_late': [('scheduled_date', '<', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
                           ('is_approved', '=', True), ('state', 'in', ('assigned', 'waiting', 'confirmed'))],
    'count_picking_backorders': [('backorder_id', '!=', False),
                                 ('state', 'in', ('confirmed', 'assigned', 'waiting')), ('is_approved', '=', True)],
    # TATAYAB FIELDS
    # All PICKS WITHOUT [GIFT/RETURN/WAITING_APPROVE]
    'picking_count': [('state', '=', 'assigned'), ('operation_state', '=', 'pick'),
                      ('is_approved', '=', True), ('is_gift', '=', False)],
    'waiting_approve': [('operation_state', '=', 'pick'), ('is_approved', '=', False)],
    # 'picking_assigned': [('state', '=', 'assigned'), ('operation_state', '=', 'pick'),
    #                      ('is_approved', '=', True), ('picking_emp_id', '!=', False)],
    # 'picking_un_assigned': [('state', '=', 'assigned'), ('operation_state', '=', 'pick'),
    #                         ('is_approved', '=', True), ('picking_emp_id', '=', False)],
    # All PACKS WITHOUT [RETURN/WAITING_APPROVE]
    'packing_count': [('state', '=', 'done'), ('operation_state', '=', 'pack'), ('is_approved', '=', True)],
    'packing_assigned': [('state', '=', 'done'), ('operation_state', '=', 'pack'),
                         ('is_approved', '=', True), ('picking_emp_id', '!=', False)],
    'packing_un_assigned': [('state', '=', 'done'), ('operation_state', '=', 'pack'),
                            ('is_approved', '=', True), ('picking_emp_id', '=', False)],
    # All SHIPS WITHOUT [RETURN/WAITING_APPROVE]
    'shipping_count': [('state', '=', 'done'), ('operation_state', '=', 'ship'), ('is_approved', '=', True)],
    'open_runsheet_count': [('state', '=', 'done'), ('operation_state', '=', 'ship'),
                            ('is_approved', '=', True), ('runsheet_id', '!=', False)],
    'out_runsheet_count': [('state', '=', 'done'), ('operation_state', '=', 'ship'),
                           ('is_approved', '=', True), ('runsheet_id', '=', False)],
    # All DONE WITHOUT [RETURN/WAITING_APPROVE]
    'done_count': [('state', '=', 'done'), ('operation_state', '=', 'done'), ('is_approved', '=', True)],
    'waiting_approve_count': [('is_approved', '=', False), ('state', 'not in', ('cancel', 'done'))],
    # Gifts
    'gift_count': [('state', '=', 'assigned'), ('operation_state', '=', 'pick'), ('is_approved', '=', True),
                   ('is_gift', '=', True)],
    # All Inter/Local
    'international_count': [('state', '=', 'assigned'), ('operation_state', '=', 'pick'),
                            ('is_approved', '=', True), ('is_gift', '=', False),
                            ('shipping_method_id.driver_type', '=', 'international')],
    'local_count': [('state', '=', 'assigned'), ('operation_state', '=', 'pick'),
                    ('is_approved', '=', True), ('is_gift', '=', False),
                    ('shipping_method_id.driver_type', 'in', ['internal', 'external'])],

    # All LOCAL PICKS WITHOUT [GIFT/RETURN/WAITING_APPROVE]

}


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    delivery_steps = fields.Selection(related='warehouse_id.delivery_steps')
    image = fields.Binary(related='warehouse_id.image')
    stock_type_return_vendor = fields.Boolean(string="Return Vendor")
    pos_picking = fields.Boolean(string="Is Car Picking Type")

    def _get_core_domain(self):
        return [('picking_type_id', 'in', self.ids)]

    def _compute_picking_count(self):
        # TODO count picking can be done using previous two
        picking_obj = self.env['stock.picking']
        print(_TYPE_DOMAINS)
        for record in self:
            for field in _TYPE_DOMAINS:
                if field == 'done_count':
                    logging.info(
                        blue + "DOMAIN: %s ==> %s" % (field, _TYPE_DOMAINS[field] + record._get_core_domain()) + reset)
                data = picking_obj.read_group(_TYPE_DOMAINS[field] + record._get_core_domain(),
                                              ['picking_type_id'], ['picking_type_id'])
                count = {x['picking_type_id'][0]: x['picking_type_id_count']
                         for x in data if x['picking_type_id']}
                record[field] = count.get(record.id, 0)
                print('count============', count.get(record.id, 0))

    # Run-sheet
    def _compute_open_runsheet(self):
        runsheet_obj = self.env['picking.runsheet']
        for op_type in self:
            op_type.open_runsheet_count = runsheet_obj.search_count([
                ('picking_type_id', '=', op_type.id),
                ('state', 'not in', ('cancel', 'done')),
            ])

    def open_runsheets(self):
        """
        Open Wizard of return other company
        :return: wizard
        """
        action = self.env.ref('tatayab_operation_status.stock_picking_runsheet_action').read()[0]
        action['domain'] = [('picking_type_id', '=', self.id), ('state', 'not in', ('cancel', 'done'))]
        action['context'] = {'search_default_open_runsheet': 1}
        return action

    # Operation Counters
    picking_count = fields.Integer("Picking", compute=_compute_picking_count)
    waiting_approve = fields.Integer("Waiting Approve", compute=_compute_picking_count)
    # picking_un_assigned = fields.Integer("Picking Un-Assigned", compute=_compute_picking_count)
    packing_count = fields.Integer("Packing", compute=_compute_picking_count)
    packing_assigned = fields.Integer("Packing Assigned", compute=_compute_picking_count)
    packing_un_assigned = fields.Integer("Packing Un-Assigned", compute=_compute_picking_count)
    shipping_count = fields.Integer("Shipping", compute=_compute_picking_count)
    done_count = fields.Integer("Done", compute=_compute_picking_count)
    international_count = fields.Integer("International Shipping", compute=_compute_picking_count)
    local_count = fields.Integer("Local Shipping", compute=_compute_picking_count)
    gift_count = fields.Integer("Gift Orders", compute=_compute_picking_count)
    waiting_approve_count = fields.Integer("Waiting Approve", compute=_compute_picking_count)
    open_runsheet_count = fields.Integer("Open Runsheet", compute=_compute_open_runsheet)
    out_runsheet_count = fields.Integer("Out Of Runsheet", compute=_compute_picking_count)
    count_picking_all_ready = fields.Integer("All Ready", compute=_compute_picking_count)

    def operation_action_open(self):
        if self.env.context.get('domain_for_field'):
            domain = _TYPE_DOMAINS[self.env.context.get('domain_for_field')]
            print("%s -> %s" % (self.env.context.get('domain_for_field'), domain))
        else:
            domain = self._get_core_domain()
        action = self._get_action('stock.stock_picking_action_picking_type')
        action['domain'] = domain
        return action

# Ahmed Salama Code End.
