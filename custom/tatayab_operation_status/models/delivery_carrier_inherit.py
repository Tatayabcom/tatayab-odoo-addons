from odoo import api, fields, models


class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    com_partner_id = fields.Many2one('res.partner', string="Partner Company")
