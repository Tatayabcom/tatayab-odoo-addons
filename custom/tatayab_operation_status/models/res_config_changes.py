
from odoo import models, fields


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    shipping_receivable_account_id = fields.Many2one('account.account',string="Shipping Receivables")
    journal_receivable_id = fields.Many2one('account.journal',string="Journal")

    def set_values(self):
        self.env['ir.config_parameter'].set_param('shipping_receivable_account_id', (self.shipping_receivable_account_id.id))
        self.env['ir.config_parameter'].set_param('journal_receivable_id', (self.journal_receivable_id.id))
        super(ResConfigSettings, self).set_values()

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            shipping_receivable_account_id=int(self.env['ir.config_parameter'].sudo().get_param('shipping_receivable_account_id')),
            journal_receivable_id=int(self.env['ir.config_parameter'].sudo().get_param('journal_receivable_id')),
        )
        return res
