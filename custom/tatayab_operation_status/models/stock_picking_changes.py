# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError

PICKING_WIZARD_VIEWS = {'pick': 'tatayab_operation_status.wiz_pick_to_pack_transfer_view',
                        'pack': 'tatayab_operation_status.wiz_pack_to_ship_transfer_view',
                        'ship': 'tatayab_operation_status.wiz_prepare_runsheet_form_view'}
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->
_PICKING_STAGES = [('draft', 'Draft'),
                   ('without_pickings', 'Without Pickings'),
                   ('pick', 'Picking'),
                   ('pack', 'Packing'),
                   ('ship', 'Shipping'),
                   ('done', 'Done Shipment'),
                   ('cancel', 'Canceled')]

NET_WEIGHT = [
    ('half_kg', '.50 Kg'),
    ('1_kg', '1 Kg'), ('1_half_kg', '1.50 Kg'),
    ('2_kg', '2 Kg'), ('2_half_kg', '2.50 Kg'),
    ('3_kg', '3 Kg'), ('3_half_kg', '3.50 Kg'),
    ('4_kg', '4 Kg'), ('4_half_kg', '4.50 Kg'),
    ('5_kg', '5 Kg'), ('5_half_kg', '5.50 Kg'),
    ('6_kg', '6 Kg'), ('6_half_kg', '6.50 Kg'),
    ('7_kg', '7 Kg'), ('7_half_kg', '7.50 Kg'),
    ('8_kg', '8 Kg'), ('8_half_kg', '8.50 Kg'),
    ('9_kg', '9 Kg'), ('9_half_kg', '9.50 Kg'),
    ('10_kg', '10 Kg'),
]


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'
    
    # core edited fields
    # picking_type = fields.Selection(related="picking_type_id.picking_type", store=True)
    currency_id = fields.Many2one('res.currency', "Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    runsheet_id = fields.Many2one('picking.runsheet', string="Run-sheet", tracking=True)
    date_delivery = fields.Datetime("Delivery Date")
    # Operation Fields
    operation_state = fields.Selection(_PICKING_STAGES, "Operation State", default='pick', tracking=True, copy=False)
    picking_date = fields.Datetime("Picking Date", tracking=True)
    packing_date = fields.Datetime("Packing Date", tracking=True)
    pos_picking = fields.Boolean(string="Is Car Picking Type",related='picking_type_id.pos_picking')
    amount_total = fields.Monetary(related='sale_id.amount_total')
    # Print Count
    picklist_print_count = fields.Integer("Picklist Print Count")
    net_weight = fields.Selection(NET_WEIGHT)
    country_id = fields.Many2one(comodel_name="res.country",related='partner_id.country_id', store=True,
                                 string="Country", tracking=True)
    old_shipment = fields.Boolean(string="Old Shipment")
    
    @api.model
    def update_old_address(self):
        sql = """ UPDATE stock_picking AS pk SET country_id = partner.country_id FROM res_partner AS partner 
        WHERE partner.id = pk.partner_id;"""
        self.env.cr.execute(sql)
    
    @api.model
    def action_done_shipment_multi(self):
        self.env['stock.picking'].browse(self.env.context.get('active_ids')).action_done_shipment()
    
    def update_print_count(self, key):
        if self.exists():
            if not key:
                return
            sql = """
	               UPDATE stock_picking
	                   SET {key} = COALESCE({key}, 0) + 1
	               WHERE id IN %s""".format(key=key)
            self.env.cr.execute(sql, (tuple(self.ids),))
    
    def action_done_shipment(self):
        """
        Mark picking as done shipment, and inform backend
        :return:
        """
        ship_pickings = self.filtered(lambda p: p.operation_state == 'ship')
        if ship_pickings:
            # Set Picking Done
            ship_pickings.write({'operation_state': 'done', 'date_done': fields.Datetime.now()})
            # SET SALE ORDER DONE/LOCKED
            ship_pickings.mapped('sale_id').write({'state': 'done'})
            # Change move lines state
            ship_pickings.move_lines.write({'operation_state': 'done'})
            # Set Orders which is COD as collected
            un_collected_cod_orders = ship_pickings.filtered(
                lambda pick: pick.sale_id.magento_payment_method_id.is_cod and not pick.payment_collected)
            if un_collected_cod_orders:
                    un_collected_cod_orders.write({'payment_collected': True})
                    un_collected_cod_orders.mapped('sale_id').write({'payment_collected': True})
            for line in ship_pickings:
                data = {
                    "entity": {
                        "entity_id": line.sale_id.magento_order_id,
                        "status": 'done',
                        "increment_id": line.sale_id.magento_order_reference
                    }
                }
                line.sale_id.send_state_order_from_bulk_in_magento(data)

        # TODO::  Inform Backend
        # for picking in ship_pickings:
        # 	picking.sync_backend('ship', 'done')
        else:
            raise UserError(_("No shipping pickings to be validate with"))
    
    def action_cancel(self):
        self.write({'operation_state': 'cancel'})
        # # SET SALE ORDER DONE/LOCKED
        # self.mapped('sale_id').write({'state': 'cancel'})
        # Change move lines state
        self.move_lines.write({'operation_state': 'cancel'})
        # TODO::  Inform Backend
        # for picking in ship_pickings:
        # 	picking.sync_backend(picking.operation_state, 'cancel')
        return super(StockPickingInherit, self).action_cancel()
    
    def bulk_scan(self):
        """
        Open Bulk Wizard From Tree View
        :return: Bulk Wizard View
        """
        if not all(p.picking_type_code == 'outgoing' for p in self):
            raise UserError(_("One of selected picking is not from picking type Delivery Orders!!!"))
        
        operation_state = []
        for op_state in self.mapped('operation_state'):
            if op_state not in operation_state:
                operation_state.append(op_state)
        if len(operation_state) > 1:
            raise UserError(_("Selected item should be from the same Operation State it's from %s\n"
                              "[Picking Or Packing Or Shipping]\n Please Use Operation Type filter Group" % operation_state))
        view_ref = PICKING_WIZARD_VIEWS.get(operation_state[0])
        view_id = view_ref and self.env.ref(view_ref)
        if not view_id:
            raise UserError(_("This Operation state %s isn't allowed to open bulk scan" % operation_state[0]))
        vals = {
            'picking_type_id': self[0].picking_type_id.id,
            'operation_state': operation_state[0],
            'line_ids': [(0, 0, {'picking_id': picking.id}) for picking in
                         self.filtered(lambda p: p.picking_type_code == 'outgoing')]
        }
        wizard_id = self.env['bulk.order.transfer'].create(vals)
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'bulk.order.transfer',
            'res_id': wizard_id.id,
            'view_id': view_id.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    
    def write(self, vals):
        """
        If this picking is from runsheet and have payment journal
        :param vals:
        :return:
        """
        if 'runsheet_id' in vals and not vals.get('runsheet_id'):
            # Case of picking have deleted from runsheet it need to be removed from static records
            self._clear_statics_data()
        super(StockPickingInherit, self).write(vals)
        if vals.get('runsheet_id'):
            # Case of add runsheet to picking
            self._set_runsheet_statics()
        return True
    
    def _clear_statics_data(self):
        """
        Clear Statiscs values in case of remove picking from runsheet
        """
        for picking in self:
            magento_payment_method_id = picking.sale_id.magento_payment_method_id
            static_id = picking.runsheet_id.static_ids.filtered(
                lambda l: l.magento_payment_method_id == magento_payment_method_id)
            if static_id and static_id.picking_ids and picking.id in static_id.picking_ids.ids:
                picking_ids = static_id.picking_ids.mapped('id')
                picking_ids.remove(picking.id)
                static_id.picking_ids = [(6, 0, picking_ids)]
    
    def _set_runsheet_statics(self):
        """
        Handle statistics logic on runsheet
        """
        for picking in self:
            magento_payment_method_id = picking.sale_id.magento_payment_method_id
            print("magento_payment_method_id: ", magento_payment_method_id)
            if picking.runsheet_id and magento_payment_method_id:
                static_id = picking.runsheet_id.static_ids.filtered(
                    lambda l: l.magento_payment_method_id == magento_payment_method_id)
                if static_id:
                    print("static_id: ", static_id)
                    picking_ids = static_id.picking_ids.ids
                    picking_ids.append(picking.id)
                    static_id.picking_ids = [(6, 0, picking_ids)]
                else:
                    print("Vals: ", {
                        'runsheet_id': picking.runsheet_id.id,
                        'magento_payment_method_id': magento_payment_method_id.id,
                        'picking_ids': [(6, 0, [picking.id])]
                    })
                    static_id.create({
                        'runsheet_id': picking.runsheet_id.id,
                        'magento_payment_method_id': magento_payment_method_id.id,
                        'picking_ids': [(6, 0, [picking.id])]
                    })


class StockMoveLineInherit(models.Model):
    _inherit = 'stock.move.line'
    
    operation_state = fields.Selection(_PICKING_STAGES, "Operation State", default='pick')


class StockMoveInherit(models.Model):
    _inherit = 'stock.move'
    
    operation_state = fields.Selection(_PICKING_STAGES, "Operation State", default='pick')

# Ahmed Salama Code End.
