# -*- coding: utf-8 -*-
from odoo import models, fields
# Ahmed Salama Code Start ---->


class StockWarehouseInherit(models.Model):
	_inherit = 'stock.warehouse'
	
	image = fields.Binary("Image")
	kwt_warehouse = fields.Boolean("Kuwait Warehouse")
# Ahmed Salama Code End.
