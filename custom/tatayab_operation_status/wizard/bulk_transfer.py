import logging
from datetime import datetime
from odoo.addons.tatayab_delivery_core.models.shipping_method import DRIVER_TYPE
from odoo.addons.tatayab_operation_status.models.stock_picking_changes import _PICKING_STAGES, PICKING_WIZARD_VIEWS
from odoo.tests import  Form
from odoo import api, models, fields, _
from odoo.exceptions import ValidationError, UserError

REACH_HOUR_START = datetime.now().replace(hour=6)  # From 8 AM KWI TIME
REACH_HOUR_END = datetime.now().replace(hour=16)  # TO 6 PM KWI TIME
_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"

NET_WEIGHT = [
    ('half_kg', '.50 Kg'),
    ('1_kg', '1 Kg'), ('1_half_kg', '1.50 Kg'),
    ('2_kg', '2 Kg'), ('2_half_kg', '2.50 Kg'),
    ('3_kg', '3 Kg'), ('3_half_kg', '3.50 Kg'),
    ('4_kg', '4 Kg'), ('4_half_kg', '4.50 Kg'),
    ('5_kg', '5 Kg'), ('5_half_kg', '5.50 Kg'),
    ('6_kg', '6 Kg'), ('6_half_kg', '6.50 Kg'),
    ('7_kg', '7 Kg'), ('7_half_kg', '7.50 Kg'),
    ('8_kg', '8 Kg'), ('8_half_kg', '8.50 Kg'),
    ('9_kg', '9 Kg'), ('9_half_kg', '9.50 Kg'),
    ('10_kg', '10 Kg'),
]


class BulkTransfer(models.TransientModel):
    _name = 'bulk.order.transfer'
    _description = "Picking Bulk Transfer"

    picking_type_id = fields.Many2one('stock.picking.type', "Picking Type")
    picking_warehouse_id = fields.Many2one(related='picking_type_id.warehouse_id')
    operation_state = fields.Selection(_PICKING_STAGES, "Operation State", default='pick')
    line_ids = fields.One2many("wiz.transfer.line", 'bulk_transfer_id', string="Orders")
    driver_type = fields.Selection(DRIVER_TYPE, "Shipping Type", default='internal')
    partner_id = fields.Many2one("res.partner",
                                 domain=[('is_driver', '=', True)],
                                 string="External Driver")
    employee_id = fields.Many2one("hr.employee", string="Internal Driver")
    runsheet_id = fields.Many2one('picking.runsheet', string="Runsheet", domain="[('state', '=', 'draft')]")
    date_delivery = fields.Date("Delivery Date")
    # only_unassigned_runsheet = fields.Boolean("Only Unassigned Runsheet")
    assign_all = fields.Boolean("Assign All", help="Assign Employee to all lines \n"
                                                   " Except: it will assign only to empty  lines")
    carrier_id = fields.Many2one('delivery.carrier', string="Carrier", )
    carrier_type = fields.Selection(related="carrier_id.delivery_type")
    create_runsheet = fields.Boolean("Create Runsheet",
                                     help="If selected system will create runsheet by default for selected pickings")
    has_gift = fields.Boolean("has gift?", compute="check_has_gift")
    set_box = fields.Boolean("Set boxes?")
    box_size = fields.Selection([('pkg_half', 'Half KG'), ('pkg_one', 'One KG'),
                                 ('pkg_two', 'Two KG'), ('pkg_three', 'Three KG')], default='pkg_half', string="Boxes")
    box_num = fields.Integer("Box Num")

    set_net_weight = fields.Boolean("Set Net Weight?")
    net_weight = fields.Selection(NET_WEIGHT, default='half_kg', string='Net Weight')

    def action_apply_changes(self):
        self.update_picking()
        return self.open_wizard_again()

    def update_picking(self):
        """
        update pickings with lines data
        """
        return self.line_ids.update_line_data()

    def open_wizard_again(self):
        operation_state = []
        for op_state in self.mapped('operation_state'):
            if op_state not in operation_state:
                operation_state.append(op_state)

        if len(operation_state) > 1:
            raise UserError(_("Selected item should be from the same Operation State it's from %s\n"
                              "[Picking Or Packing Or Shipping]\n Please Use Operation Type filter Group" % operation_state))
        view_ref = PICKING_WIZARD_VIEWS[operation_state[0]]
        view_id = self.env.ref(view_ref).id
        ctx = self.env.context
        return {
            'context': ctx,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'bulk.order.transfer',
            'res_id': self.id,
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def _check_operation_list(self):
        operation_state_list = set()
        operation_state_list_add = operation_state_list.add
        operation_state = [x for x in self.line_ids.mapped('picking_id.operation_state') if
                           not (x in operation_state_list or operation_state_list_add(x))]
        if len(operation_state) > 1:
            raise UserError(_("Selected item should be from the same Operation State it's from %s\n"
                              "[Picking Or Packing Or Shipping]\n Please Use Operation Type filter Group" % operation_state))
        return operation_state

    def _check_emp_assign(self):
        person = ""
        if self.operation_state == 'pick':
            person = "Picker"
        elif self.operation_state == 'pack':
            person = "Packer"
        for line in self.line_ids:
            if not line.picking_emp_id:
                raise UserError(_("Can't Validate While there is lines Missing %s!!!" % person))

    def _check_no_of_boxes(self):
        if any(box == 0 for box in self.line_ids.mapped('no_of_boxes')):
            raise UserError(_("Can't Validate While there is lines have 0 no of boxes!!!"))

    def action_assign_emp(self):
        if not self.line_ids:
            raise ValidationError(_('Please add at least one line.'))
        # if self.driver_type != 'international' and not self.employee_id and not self.partner_id:
        #     raise ValidationError(_('Please assign the value to use.'))
        if self.assign_all:
            lines = self.line_ids
        elif self.employee_id:
            lines = self.line_ids.filtered(lambda l: not l.picking_emp_id)
        elif self.partner_id:
            lines = self.line_ids.filtered(lambda l: not l.external_driver_id)
        else:
            lines = self.line_ids.filtered(lambda l: not l.carrier_id)
        lines.mapped('picking_id').write({
            'picking_emp_id': self.employee_id and self.employee_id.id,
            'external_driver_id': self.partner_id and self.partner_id.id,
            'carrier_id': self.carrier_id and self.carrier_id.id or False,
            'driver_type': self.driver_type
        })
        for line in lines:
            if line.picking_id.sale_id:
                for inv in line.picking_id.sale_id.invoice_ids:
                    inv.driver_type = self.driver_type

    def action_assign_box(self):
        """
        Assign box count with box size
        """
        if not self.line_ids:
            raise UserError(_('Please add at least one line.'))
        setattr(self.line_ids, self.box_size, self.box_num)
        setattr(self.line_ids.mapped('picking_id'), self.box_size, self.box_num)

    def tatayabe_process_transfer(self):
        """
        Automatic Validate multi pickings without backorder
        """
        immediate_transfer_obj = self.env['stock.immediate.transfer']
        picking_ids = self.line_ids.mapped('picking_id')
        logging.info(blue + "______ User '%s' process %s picking ______" % (self.env.user.name, len(picking_ids)) + reset)
        immediate_transfer_line_ids = [(0, 0, {'picking_id': picking.id, 'to_immediate': True}) for picking in picking_ids]
        immediate_transfer_vals = {
            'pick_ids': [(4, p.id) for p in picking_ids],
            'show_transfers': False,
            'immediate_transfer_line_ids': immediate_transfer_line_ids
        }
        # res = picking_ids.button_validate()
        # Form(self.env['stock.immediate.transfer'].with_context(res['context'])).save().process()
        # for pick in picking_ids:
        #     backorder_wizard_dict =pick.button_validate()
        #     backorder_wizard = Form(self.env[backorder_wizard_dict['res_model']].with_context(backorder_wizard_dict['context'])).save()
        #     backorder_wizard.process()
        #     logging.info(blue + "______ State picking %s ______" % (pick.state) + reset)
        immediate_transfer_obj.create({'pick_ids': picking_ids.ids}).process()
        # wizard = immediate_transfer_obj.create(immediate_transfer_vals)
        # return wizard.with_context(button_validate_picking_ids=wizard.pick_ids.ids).process()

    # ================= #
    # Buttons Actions   #
    # ================= #

    def action_update_emp(self):
        if self.set_box:
            self.action_assign_box()
        if self.set_net_weight:
            self.action_assign_net_weight()
        self.action_assign_emp()
        return self.open_wizard_again()

    def action_assign_net_weight(self):
        if not self.line_ids:
            raise Warning(_('Please add at least one line.'))
        self.line_ids.mapped('picking_id').write({'net_weight': self.net_weight})
        self.line_ids.write({'net_weight': self.net_weight})

    def action_pick_to_pack(self):
        # Work with one root logic
        self.ensure_one()
        self._check_emp_assign()
        pickings = self.line_ids.mapped('picking_id')
        for p in pickings:
            if p.old_shipment:
                raise ValidationError(_("Not validated ... picking [ %s ] is old " %(p.name)))
        if all(p.state == 'assigned' for p in pickings):
            logging.info(yellow + "\n pickings %s" % pickings.mapped('display_name') + reset)
            pickings.write({'operation_state': 'pack', 'picking_emp_id': False, 'picking_date': fields.Datetime.now()})
            pickings.move_lines.write({'operation_state': 'pack'})
            # for picking_id in pickings:
            #     # inform Backend
            #     picking_id.sync_backend('pick', 'pack')
            # self.tatayabe_process_transfer()
            # if not all(p.state == 'done' for p in pickings):
            #     raise ValidationError(_("Not validated ... pickings not done"))
            skip_sms = {"skip_sms": True}
            for ma_picking in pickings:
                ma_picking.magento_send_shipment()
            for picking in pickings:
                for line in picking.move_ids_without_package:
                    if line.quantity_done != line.product_uom_qty:
                        raise ValidationError(_("Quantity Done must to be  equal Demand"))


                result = picking.with_context(**skip_sms).button_validate()
                if isinstance(result, dict):
                    dict(result.get("context")).update(skip_sms)
                    context = result.get("context")  # Merging dictionaries.
                    model = result.get("res_model", "")
                    # model can be stock.immediate.transfer or stock backorder.confirmation
                    if model:
                        record = self.env[model].with_context(context).create({})
                        record.process()
                data = {
                    "entity": {
                        "entity_id": picking.sale_id.magento_order_id,
                        "status": 'pack',
                        "increment_id": picking.sale_id.magento_order_reference
                    }
                }

                # picking.sale_id.send_state_order_from_bulk_in_magento(data)
        else:
            raise UserError(_("Pickings [%s] are not ready to pick" %
                              pickings.filtered(lambda p: p.state != 'assigned').mapped('display_name')))

    def action_pack_to_ship(self):
        self._check_emp_assign()
        self._check_no_of_boxes()
        packing = self.line_ids.mapped('picking_id')
        for pack in packing:
            if pack.sale_id.magento_order_id:
                if not pack.magento_shipping_id:
                    value_send_shipment =pack.magento_send_shipment()
                    if not value_send_shipment:
                        raise ValidationError(
                            _("The picking that didn't create shipment in Magento  [%s]" % pack.sale_id.display_name))

        if not all(p.magento_shipping_id for p in packing if p.sale_id.magento_order_id):
            pickings_to_cancel = packing.filtered(lambda p: not p.magento_shipping_id)
            raise ValidationError(_("The pickings that didn't create shipment in Magento are [%s]"%pickings_to_cancel.mapped('sale_id.display_name')))

        packing.write({'operation_state': 'ship', 'picking_emp_id': False, 'packing_date': fields.Datetime.now()})
        packing.move_lines.write({'operation_state': 'ship'})
        for line in packing:
            if line.sale_id.magento_order_id:
                data = {
                    "entity": {
                        "entity_id": line.sale_id.magento_order_id,
                        "status": 'ship',
                        "increment_id": line.sale_id.magento_order_reference
                    }
                }
                line.sale_id.send_state_order_from_bulk_in_magento(data)
        if self.create_runsheet:
            international_pickings, local_pickings = self._check_shipping_method()
            return self._create_runsheet(international_pickings, local_pickings)

    def _check_shipping_method(self):
        """
        Check for lines picking methods
        :return: local, international separated
        """
        international_pickings = local_pickings = self.env['stock.picking']
        for line in self.line_ids:
            driver_type = line.driver_type or line.picking_id.driver_type
            print("driver_type: ", driver_type)
            if driver_type == 'international':
                local_pickings += line.picking_id
            else:
                local_pickings += line.picking_id
        return international_pickings, local_pickings

    def _create_runsheet(self, international_pickings, local_pickings):
        # Create & Open Runsheet
        runsheets = runsheet_obj = self.env['picking.runsheet']
        if local_pickings:
            local_vals = {
                'date_delivery': fields.Date.today(),
                'picking_type_id': self.picking_type_id and self.picking_type_id.id or False,
                # 'driver_type': self.driver_type,
                # 'partner_id': self.partner_id and self.partner_id.id,
                # 'emp_driver_id': self.employee_id and self.employee_id.id,
                'picking_ids': [(4, pid) for pid in local_pickings.ids],
            }
            runsheets += runsheet_obj.create(local_vals)
        if international_pickings:
            inter_vals = {
                'date_delivery': fields.Date.today(),
                'picking_type_id': self.picking_type_id and self.picking_type_id.id or False,
                'driver_type': 'international',
                # 'partner_id': self.partner_id and self.partner_id.id,
                # 'carrier_id': self.carrier_id and self.carrier_id.id or False,
                'picking_ids': [(4, pid) for pid in international_pickings.ids],
            }
            runsheets += runsheet_obj.create(inter_vals)
        if runsheets:
            return self.get_runsheet_action(runsheets)

    def get_runsheet_action(self, runsheets):
        action = self.env.ref('tatayab_operation_status.stock_picking_runsheet_action').read()[0]
        if len(runsheets.ids) > 1:
            upd = {'domain': [('id', 'in', runsheets.ids)]}
        else:
            upd = {
                'domain': [('id', '=', runsheets.id)],
                'view_mode': 'form',
                'res_id': runsheets.id,
            }
        action.update(upd)
        return action

    # ==================== #
    # Print Buttons Action #
    # ==================== #
    def action_print_invoice(self):
        self.ensure_one()
        return self.line_ids.mapped('picking_id').print_picking_order()

    def action_print_report_picklist_order(self):
        self.ensure_one()
        pickings = self.line_ids.mapped('picking_id')
        datas = {
            'ids': pickings.ids,
            'model': 'stock.picking',
            'active_ids': pickings.ids,
        }
        return self.env.ref('tatayab_operation_status.action_report_picking_order').report_action(pickings, data=datas)

    def action_print_operation_label_report(self):
        self.ensure_one()
        self.update_picking()
        pickings = self.line_ids.mapped('picking_id')
        datas = {
            'ids': pickings.ids,
            'model': 'stock.picking',
            'active_ids': pickings.ids,
        }
        return self.env.ref('tatayab_operation_status.action_operation_label_report').report_action(pickings,
                                                                                                    data=datas)

    def action_print_delivery_label_by_pkg(self):
        self.ensure_one()
        self.update_picking()
        for line in self.line_ids.mapped('picking_id'):
            if not line.picking_emp_id:
                raise UserError(_("Can't Print While there is lines have no Packer!!!"))
        if any(box == 0 for box in self.line_ids.mapped('picking_id.no_of_boxes')):
            raise UserError(_("Can't Print While there is lines have 0 no of boxes!!!"))
        pickings = self.line_ids.mapped('picking_id')
        datas = {
            'ids': pickings.ids,
            'model': 'stock.picking',
            'active_ids': pickings.ids,
        }
        return self.env.ref('tatayab_operation_status.action_report_delivery_label_by_pkg').report_action(pickings,
                                                                                                          data=datas)

    def action_print_picking_order_barcode(self):
        self.ensure_one()
        pickings = self.line_ids.mapped('picking_id')
        datas = {
            'ids': pickings.ids,
            'model': 'stock.picking',
            'active_ids': pickings.ids,
        }
        return self.env.ref('tatayab_operation_status.action_report_picking_order_barcode').report_action(pickings,
                                                                                                          data=datas)

    def action_internation_shipping(self):
        carrier = self.carrier_id

        if not carrier:
            raise ValidationError(_('Please select carrier after selecting Shipping Type International.'))

        if any(tracking_ref for tracking_ref in self.line_ids.mapped('picking_id.carrier_tracking_ref')):
            raise Warning(_("Can't Validate While there is lines Already have tracking ref!!!"))

        if self.assign_all:
            self.line_ids.mapped('picking_id').write({'carrier_id': carrier.id,
                                                      'driver_type': self.driver_type})
        all_pickings = self.line_ids.filtered(lambda l: l.driver_type == 'international').mapped('picking_id')
        all_pickings.write({'carrier_id': carrier.id, 'driver_type': self.driver_type})
        carrier.send_shipping(all_pickings)
        international_pickings = self.line_ids.filtered(lambda l: l.picking_id.carrier_tracking_ref).mapped(
            'picking_id')
        # if international_pickings:
        #     self.sync_backend(international_pickings)
        return self.open_wizard_again()

    def action_prepare_runsheet(self):
        self.ensure_one()
        if any(box == 0 for box in self.line_ids.mapped('no_of_boxes')):
            raise Warning(_("Can't Validate While there is lines have 0 no of boxes!!!"))
        all_runsheet = Runsheet = self.env['picking.runsheet']
        all_other_pickings = international_pickings = self.env['stock.picking']
        vals = {}
        vals.update({'driver_type': self.driver_type,
                     'picking_type_id': self.picking_type_id and self.picking_type_id.id or False,
                     'partner_id': self.partner_id and self.partner_id.id or False,
                     'emp_driver_id': self.employee_id and self.employee_id.id or False,
                     'carrier_id': self.carrier_id and self.carrier_id.id or False,
                     })
        if self.date_delivery:
            vals.update({'date_delivery': self.date_delivery})
            self.runsheet_id.picking_ids.write({'date_delivery': self.date_delivery})
        if self.runsheet_id:
            runsheet = self.runsheet_id
            # Change Those lines to be in the new runsheet
            if self.line_ids:
                self.line_ids.mapped('picking_id').write({'runsheet_id': runsheet.id})
            runsheet.write(vals)
            all_runsheet += runsheet
        else:
            for line in self.line_ids:
                if line.driver_type == 'international':
                    international_pickings += line.picking_id
                else:
                    all_other_pickings += line.picking_id

            if international_pickings:
                runsheet = Runsheet.create({
                    'emp_driver_id': self.employee_id and self.employee_id.id or False,
                    'date_delivery': self.date_delivery,
                    'picking_type_id': self.picking_type_id and self.picking_type_id.id or False,
                    'driver_type': 'international',
                    'partner_id': self.partner_id and self.partner_id.id or False,
                    'carrier_id': self.carrier_id and self.carrier_id.id or False,
                    'picking_ids': [(4, _id) for _id in international_pickings.ids],
                })
                all_runsheet += runsheet

            if all_other_pickings:
                runsheet = Runsheet.create({
                    'emp_driver_id': self.employee_id and self.employee_id.id or False,
                    'picking_type_id': self.picking_type_id and self.picking_type_id.id or False,
                    'date_delivery': self.date_delivery,
                    'driver_type': self.driver_type,
                    'partner_id': self.partner_id and self.partner_id.id or False,
                    'carrier_id': self.carrier_id and self.carrier_id.id or False,
                    'picking_ids': [(4, _id) for _id in all_other_pickings.ids],
                })
                all_runsheet += runsheet
        return self.get_runsheet_action(all_runsheet)


class BulkTransferLine(models.TransientModel):
    _name = 'wiz.transfer.line'
    _description = "Bulk Transfer Order"
    _rec_name = 'picking_id'

    @api.depends('pkg_half', 'pkg_one', 'pkg_two', 'pkg_three')
    def _compute_pkg_count(self):
        for rec in self:
            rec.pkg_count = rec.pkg_half + rec.pkg_one + rec.pkg_two + rec.pkg_three

    @api.onchange('picking_id')
    def _control_emp(self):
        for picking in self:
            emp_control = False
            if picking.env.user.has_group('tatayab_operation_status.group_inventory_ext_control'):
                emp_control = True
            picking.emp_control = emp_control

    @api.onchange('picking_id')
    @api.depends('picking_id.move_lines.product_uom_qty')
    def _count_items(self):
        for line in self:
            items_count = 0
            if line.picking_id:
                items_count = sum(l.product_uom_qty for l in line.picking_id.move_lines)
            line.items_count = items_count

    bulk_transfer_id = fields.Many2one('bulk.order.transfer')
    picking_id = fields.Many2one('stock.picking', string="Order", required=True)
    order_id = fields.Many2one(related='picking_id.sale_id')
    carrier_tracking_ref = fields.Char(related='picking_id.carrier_tracking_ref')
    order_origin = fields.Char(related='picking_id.sale_id.origin', string="Backend Ref")
    items_count = fields.Integer(compute=_count_items)
    emp_control = fields.Boolean("Open Employee", compute=_control_emp)
    driver_type = fields.Selection(DRIVER_TYPE, string="Shipping Type",
                                   related='picking_id.driver_type')
    external_driver_id = fields.Many2one(related='picking_id.external_driver_id')
    carrier_id = fields.Many2one(related='picking_id.carrier_id')
    picking_emp_id = fields.Many2one(related='picking_id.picking_emp_id', readonly=False)
    runsheet_id = fields.Many2one(related="picking_id.runsheet_id", readonly=True)
    note = fields.Text(string="Note")
    no_of_boxes = fields.Integer(related='picking_id.no_of_boxes')
    pkg_half = fields.Integer("Half KG", related='picking_id.pkg_half', store=True)
    pkg_one = fields.Integer("One KG", related='picking_id.pkg_one', store=True)
    pkg_two = fields.Integer("Two KG", related='picking_id.pkg_two', store=True)
    pkg_three = fields.Integer("Three KG", related='picking_id.pkg_three', store=True)
    pkg_count = fields.Integer("Total Packages", compute=_compute_pkg_count)
    net_weight = fields.Selection(related='picking_id.net_weight', store=True)

    @api.constrains('picking_id')
    def onchagne_picking_id(self):
        for line in self:
            picking = line.picking_id
            if picking:
                line.driver_type = picking.driver_type
                other_lines = line.bulk_transfer_id.line_ids. \
                    filtered(lambda l: l.picking_id == picking)
                if len(other_lines) > 2:
                    raise UserError(_("You can't Scan same order twice %s " % picking.name))

    def get_line_update_data(self):
        """
        Collect data from line to edit
        :return:
        """
        return {'picking_emp_id': self.picking_emp_id and self.picking_emp_id.id or False,
                'external_driver_id': self.external_driver_id and self.external_driver_id.id or False,
                'driver_type': self.driver_type,
                'net_weight': self.net_weight,
                'pkg_half': self.pkg_half,
                'pkg_one': self.pkg_one,
                'pkg_two': self.pkg_two,
                'pkg_three': self.pkg_three,
                'note': self.note,
                'no_of_boxes': self.pkg_count}

    def update_line_data(self):
        """
        Update picking with line data
        :return:
        """
        for line in self:
            line.picking_id.write(line.get_line_update_data())
