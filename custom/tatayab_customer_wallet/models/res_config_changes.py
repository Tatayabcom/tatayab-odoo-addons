
from odoo import models, fields


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    wallet_journal_id = fields.Many2one('account.journal',string="Wallet Journal")
    account_discount_id = fields.Many2one('account.account',string="Discount Account",
                                          domain="[('internal_type', '=', 'other'), ('deprecated', '=', False), ('company_id', '=', company_id), ('is_off_balance', '=', False)]",)
    account_wallet_id = fields.Many2one('account.account',string="Wallet Account")
    account_cash_bank_id = fields.Many2one('account.account',string="Cash/Bank Account")

    def set_values(self):
        self.env['ir.config_parameter'].set_param('wallet_journal_id', (self.wallet_journal_id.id))
        self.env['ir.config_parameter'].set_param('account_discount_id', (self.account_discount_id.id))
        self.env['ir.config_parameter'].set_param('account_wallet_id', (self.account_wallet_id.id))
        self.env['ir.config_parameter'].set_param('account_cash_bank_id', (self.account_cash_bank_id.id))
        super(ResConfigSettings, self).set_values()

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            wallet_journal_id=int(self.env['ir.config_parameter'].sudo().get_param('wallet_journal_id')),
            account_discount_id=int(self.env['ir.config_parameter'].sudo().get_param('account_discount_id')),
            account_wallet_id=int(self.env['ir.config_parameter'].sudo().get_param('account_wallet_id')),
            account_cash_bank_id=int(self.env['ir.config_parameter'].sudo().get_param('account_cash_bank_id')),
        )
        return res
