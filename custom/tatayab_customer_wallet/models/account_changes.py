# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
from odoo.exceptions import Warning
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import json
_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"
# Ahmed Salama Code Start ---->


class AccountMove(models.Model):
	_inherit = 'account.move'

	wallet_sale_order_id = fields.Many2one('sale.order', string="Order")
	customer_wallet_id = fields.Many2one('customer.wallet', string="Wallet")
	has_wallet = fields.Boolean(string="Use Wallet")
	use_wallet = fields.Boolean(string="Use Wallet")

	
	def wallet_generate_payment(self, params=None):
		"""
		Method to set payment on system (Add money on wallet)
		:param
		:return:
		"""
		_logger.info(green + "___________ Connection to Odoo set Wallet ____________" + reset)

	def _post(self, soft=True):
		# OVERRIDE to generate cross invoice based on company rules.
		posted = super(AccountMove, self)._post(soft)
		for rec in posted:
			if rec.has_wallet and rec.move_type == 'out_invoice':
				to_reconcile_payments_widget_vals = json.loads(rec.invoice_outstanding_credits_debits_widget)
				if to_reconcile_payments_widget_vals:
					current_amounts = {line['move_id']: line['amount'] for line in to_reconcile_payments_widget_vals['content']}
					pay_term_lines = rec.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable'))
					to_reconcile = self.env['account.move'].browse(list(current_amounts.keys())).line_ids.filtered(lambda line: line.account_id == pay_term_lines.account_id)
					(pay_term_lines + to_reconcile).reconcile()
		return posted

	def action_post(self):
		for rec in self:
			parent_id = rec.reversed_entry_id
			if rec.reversed_entry_id:
				rec.has_wallet = rec.reversed_entry_id.has_wallet
			if parent_id:
				order = parent_id.invoice_line_ids.mapped('sale_line_ids.order_id')
				if order:
					self.env['customer.wallet'].create({
						'partner_id': order.partner_id.id,
						'amount': order.base_credit_amount,
						'type': 'credit',
						'type_credit':'liability',
						'client_order_ref': order.name,
					})
		return super(AccountMove, self).action_post()

# Ahmed Salama Code End.
class AccountInvoice(models.Model):
	_inherit = 'account.move'

	def _get_payload_values(self, refund_type, return_stock, order):
		"""
        This method is used to prepare the request data that will
        :Task_id: 173739
        -----------------
        :param: refund_type: possible values ('online', 'offline')
        :param: return_stock: True, if customer want to back item to stock.
        :param: order: sale order object
        :return: dict(values)
        """
		values = dict()
		ship_charge = self._get_shipping_charge()
		if order.magento_order_id:
			items = self._prepare_line_data()
			values = self._prepare_order_payload(items=items, ship_charge=ship_charge,
												 refund_type=refund_type,
												 return_stock=return_stock)
			values.get('extension_attributes').update({'use_wallet':self.use_wallet})
			print("values",values)
			logging.info(blue + "values of refund invoice:%s" % values + reset)
		return values
