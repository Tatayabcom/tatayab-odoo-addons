from odoo import fields, models, api, _


class MagentoQueueProcessEpt(models.TransientModel):
    _inherit = 'magento.queue.process.ept'

    def manual_queue_process(self):
        res = super(MagentoQueueProcessEpt, self).manual_queue_process()
        queue_process = self._context.get('queue_process')
        if queue_process == "process_wallet_queue_manually":
            queue = self.env['magento.wallet.data.queue.ept']
            queues = queue.browse(self._context.get('active_ids'))
            queues.process_customer_wallet_queues(is_manual=True)
        return res
