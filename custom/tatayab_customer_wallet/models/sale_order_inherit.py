import logging

from odoo import fields, models
import logging

from odoo import models, fields, api
from odoo.exceptions import AccessError
from odoo.tools import float_compare, groupby

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"




class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    wallet_id = fields.Many2one('customer.wallet', string="Wallet")
    has_wallet = fields.Boolean(string="Use Wallet?",tracking=True)
    wallet_type = fields.Selection(selection=[('debit', 'Debit'), ('credit', 'Credit')], string="Type")
    type_credit = fields.Selection(selection=[('liability', 'Liability'), ('expense', 'Expense')], string="Type Credit")
    base_credit_refunded = fields.Monetary(string=" Credit Return (KWD)")
    credit_refunded = fields.Monetary(string=" Credit Return")
    base_credit_amount = fields.Monetary(string="Credit Amount (KWD)")
    credit_amount = fields.Monetary(string="Credit Amount")
    base_credit_earned = fields.Monetary(string="Credit Earn (KWD)")
    credit_earned = fields.Monetary(string="Credit Earn")
    so_move_id = fields.Many2one('account.move',string="Move", tracking=True)
    base_cod_amount = fields.Monetary(string="COD Amount (KWD)", compute="compute_so_cod_amount")
    cod_amount = fields.Monetary(string="COD Amount",compute="compute_so_cod_amount")
    cod_amount_usd = fields.Monetary(string="COD Amount(USD)",compute="compute_so_cod_amount")
    total_with_wallet = fields.Monetary(string="Total With Wallet",compute="compute_total_with_wallet",store=True)

    @api.depends('base_grand_total','base_credit_amount')
    def compute_total_with_wallet(self):
        for rec in self:
            rec.total_with_wallet = rec.base_grand_total + rec.base_credit_amount

    @api.depends('base_grand_total','base_credit_amount','grand_total','credit_amount','base_grand_total_usd')
    def compute_so_cod_amount(self):
        for rec in self:
            rec.base_cod_amount = rec.base_grand_total - rec.base_credit_amount
            rec.cod_amount = rec.grand_total - rec.credit_amount
            rec.cod_amount_usd = 0.0
            if rec.base_grand_total:
                rec.cod_amount_usd = (rec.base_grand_total_usd * rec.base_cod_amount)/rec.base_grand_total

    def prepare_order_vals_after_onchange_ept(self, vals, fpos):
        """
        Inherited to
        :param vals:
        :param fpos:
        :return:
        """
        order_vals = super(SaleOrder, self).prepare_order_vals_after_onchange_ept(vals, fpos)
        update_vals ={
            'base_credit_refunded': vals.get('base_credit_refunded',0.0),
            'credit_refunded': vals.get('credit_refunded',0.0),
            'base_credit_earned': vals.get('base_credit_earned',0.0),
            'credit_earned': vals.get('credit_earned',0.0),
            'base_credit_amount': vals.get('base_credit_amount',0.0),
            'credit_amount': vals.get('credit_amount',0.0),
            'wallet_type': vals.get('wallet_type',False),
            'type_credit': vals.get('type_credit',False),
            'has_wallet': vals.get('has_wallet',False),

        }
        logging.info(red + "Update Wallet Vals: %s" % update_vals + reset)
        order_vals.update(update_vals)
        return order_vals
    

    def _prepare_invoice(self):
        """
        This method is used for set necessary value(is_magento_invoice,
        is_exported_to_magento,magento_instance_id) in invoice.
        :return:
        """
        invoice_vals = super(SaleOrder, self)._prepare_invoice()
        if self.magento_instance_id:
            invoice_vals.update({
                'has_wallet': self.has_wallet,
            })
        invoice_vals.update({
            'has_wallet': self.has_wallet,
        })
        print("has_wallet in _prepare_invoice",invoice_vals )
        return invoice_vals

    def wallet_changes(self, order_vals, item):
        """
        This method is declared on module tatayab_magento_connect_changes
        And implemented here to add wallet values
        :param order_vals: magent order vals
        :param item: order
        :return: order_vals after changes
        """
        wallet_type = False
        type_credit = False
        has_wallet = False
        amount=0.0
        store_credit = item.get('extension_attributes', {}).get('store_credit', {})
        if float(store_credit.get('base_credit_refunded'))  or float(store_credit.get('credit_refunded')) :
            wallet_type = 'credit'
            type_credit = 'liability'
            has_wallet = True
            amount = float(store_credit.get('base_credit_refunded'))
        if float(store_credit.get('base_credit_earned'))  or float(store_credit.get('credit_earned')) :
            wallet_type = 'credit'
            type_credit = 'expense'
            has_wallet = True
            amount = float(store_credit.get('base_credit_earned'))
        if float(store_credit.get('base_credit_amount')) or float(store_credit.get('credit_amount')) :
            wallet_type = 'debit'
            type_credit = ''
            has_wallet = True
            amount = float(store_credit.get('base_credit_amount'))
        order_vals.update({
            'base_credit_refunded': store_credit.get('base_credit_refunded'),
            'credit_refunded': store_credit.get('credit_refunded'),
            'base_credit_earned': store_credit.get('base_credit_earned'),
            'credit_earned': store_credit.get('credit_earned'),
            'base_credit_amount': store_credit.get('base_credit_amount'),
            'credit_amount': store_credit.get('credit_amount'),
            'wallet_type': wallet_type,
            'type_credit': type_credit,
            'has_wallet': has_wallet,
        })
        if has_wallet:
            currency_code=''
            self.env['customer.wallet'].create({
                'partner_id': item.get('parent_id'),
                'amount': amount ,
                'type': wallet_type,
                'type_credit': type_credit,
                'currency_code': '',
                'client_order_ref': item.get('increment_id')})
        return order_vals

    #=========================== Wallet move =====================
    def _prepare_move_line(self, wallet,amount ,debit, credit):
        wallet_journal_id = int(self.env['ir.config_parameter'].sudo().get_param('wallet_journal_id'))
        company_currency = self.env.user.company_id.currency_id
        prec = company_currency.decimal_places
        amount = amount
        move_line_1 = {
            'name': wallet.name,
            'partner_id': wallet.partner_id.id,
            'account_id': credit,
            'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'currency_id': company_currency.id,
        }
        move_line_2 = {
            'name': wallet.name,
            'partner_id': wallet.partner_id.id,
            'account_id': debit,
            'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'currency_id': company_currency.id,
        }
        move_vals = {
            'ref': "From Wallet ["+wallet.name +" ]",
            'journal_id': wallet_journal_id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
            'amount_total': amount,
            'name': '/',
            'move_type': 'entry',
            'currency_id': company_currency.id,
            'wallet_sale_order_id': wallet.id if wallet else False,
            'has_wallet':True
            # 'customer_wallet_id': wallet.id,
        }
        return move_vals

    def create_customer_payment(self):
        for wallet in self:
            vals = {}
            account_discount_id = int(self.env['ir.config_parameter'].sudo().get_param('account_discount_id'))
            account_wallet_id = int(self.env['ir.config_parameter'].sudo().get_param('account_wallet_id'))
            account_cash_bank_id = int(self.env['ir.config_parameter'].sudo().get_param('account_cash_bank_id'))
            account_customer = wallet.partner_id.property_account_receivable_id.id
            if wallet.wallet_type == 'debit':
                vals = wallet._prepare_move_line(wallet=wallet,amount= wallet.credit_amount,debit=account_wallet_id, credit=account_customer)
            if wallet.wallet_type == 'credit' and wallet.type_credit == 'liability':
                vals = wallet._prepare_move_line(wallet=wallet,amount= wallet.base_credit_refunded,debit=account_cash_bank_id, credit=account_wallet_id)
            if wallet.wallet_type == 'credit' and wallet.type_credit == 'expense':
                vals = wallet._prepare_move_line(wallet=wallet, amount= wallet.base_credit_earned,debit=account_discount_id, credit=account_wallet_id)
            move = self.env['account.move'].create(vals)
            move.action_post()
            wallet.so_move_id = move.id
            if wallet.wallet_type == 'credit':
                wallet.partner_id.total_wallet += (wallet.base_credit_refunded + wallet.base_credit_earned)
            elif wallet.wallet_type == 'debit':
                wallet.partner_id.total_wallet -= wallet.credit_amount


    def action_confirm(self):
        """
        This method inherited for identify the Magento order and pass create the wallet
        amount
        :return: super call.
        """
        res = super(SaleOrder, self).action_confirm()
        for rec in self:
            if rec.has_wallet:
                if not rec.so_move_id:
                    rec.create_customer_payment()
        return res
