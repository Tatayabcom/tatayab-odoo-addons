from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    total_wallet = fields.Float(string="Total Wallet")
