import json
import logging

from odoo import models, fields, api
from odoo.tools import float_compare

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class CustomerWallet(models.Model):
    _name = 'customer.wallet'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string="Name")
    partner_id = fields.Many2one('res.partner', string="Customer")
    other_customer_id = fields.Many2one('res.partner', string="To Customer")
    order_id = fields.Many2one('sale.order', string="Order")
    client_order_ref = fields.Char(string='Order Reference')
    amount = fields.Float(string="Amount")
    type = fields.Selection(selection=[('debit', 'Debit'), ('credit', 'Credit')], string="Type")
    type_credit = fields.Selection(selection=[('liability', 'Liability'), ('expense', 'Expense')], string="Type Credit")
    state = fields.Selection(selection=[('draft', 'Draft'), ('add', 'Added')], string="State", default='draft',
                             copy=False)
    transaction_id = fields.Char(string="Magento Transaction ID")
    currency_code = fields.Char(string="Currency")
    balance_delta = fields.Float(string="Balance Delta")
    action = fields.Selection(selection=[('refunded', 'Refunded'),
                                         ('earning', 'Earning'),
                                         ('deducted', 'Deducted'),
                                         ('manual', 'Manual'),
                                         ('used', 'Used'),
                                         ])
    email = fields.Char(string="Email")
    magento_customer_id = fields.Char(string="Customer ID")
    customer_name = fields.Char(string="Customer Name")
    message = fields.Char(string="Message")

    @api.model
    def create(self, vals_list):
        wallet = super(CustomerWallet, self).create(vals_list)
        wallet.name = self.env['ir.sequence'].next_by_code('customer.wallet')
        return wallet

    def wallet_generate_payment(self, params):
        """
        Method to set payment on system (Add money on wallet)
        :param
        :return:
        """
        pass

        _logger.info(green + "___________ Connection to Odoo set Wallet  ____________" + reset)
        # return wallet

    def _prepare_move_line(self, wallet, amount, debit, credit,currency_code):
        wallet_journal_id = int(self.env['ir.config_parameter'].sudo().get_param('wallet_journal_id'))
        company_currency = self.env.user.company_id.currency_id
        prec = company_currency.decimal_places
        amount = amount
        currency_code = self.env.ref('base.%s'%currency_code)
        move_line_1 = {
            'name': wallet.name,
            'partner_id': wallet.partner_id.id,
            'account_id': credit,
            'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'currency_id': currency_code.id,
        }
        move_line_2 = {
            'name': wallet.name,
            'partner_id': wallet.partner_id.id,
            'account_id': debit,
            'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'currency_id': currency_code.id,
        }
        move_vals = {
            'ref': "From Wallet [" + wallet.name + " ]",
            'journal_id': wallet_journal_id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
            'amount_total': amount,
            'name': '/',
            'move_type': 'entry',
            'currency_id': currency_code.id,
            # 'wallet_sale_order_id': wallet.id if wallet else False,
            'has_wallet': True
            # 'customer_wallet_id': wallet.id,
        }
        return move_vals

    def create_customer_payment(self, wallet):
        account_discount_id = int(self.env['ir.config_parameter'].sudo().get_param('account_discount_id'))
        account_wallet_id = int(self.env['ir.config_parameter'].sudo().get_param('account_wallet_id'))
        vals = wallet._prepare_move_line(wallet=wallet, amount=wallet.balance_delta, debit=account_discount_id,
                                         credit=account_wallet_id,currency_code = wallet.currency_code)
        move = self.env['account.move'].create(vals)
        move.action_post()
        wallet.partner_id.total_wallet = wallet.amount

    # Added By Omnya
    def create_magento_customer_wallet(self, line, is_order=False):
        instance = ''
        if is_order:
            data = line
            instance = line.get('instance_id')
        else:
            data = json.loads(line.data)
            instance = line.instance_id
        customer = self.env['magento.res.partner.ept'].search([('magento_customer_id', '=', data.get('customer_id')),
                                                               ('magento_instance_id', '=', instance.id)], limit=1)
        values = self._prepare_magento_customer_values(instance=instance,
                                                       data=data, customer_id=customer.partner_id.id,
                                                       balance_amount=data.get('balance_amount'))
        # check the  transaction is  get or not
        is_exist_wallet = self.env['customer.wallet'].search([('transaction_id', '=', data.get('transaction_id'))])
        logging.info(green + "___________ is_exist_wallet  ____________:%s " % (is_exist_wallet) + reset)
        if not is_exist_wallet:
            if data.get('action') == 'deducted':
                deducted_wallet_object = self.create(values)
                deducted_wallet_object.message_post(body=data)
            if data.get('action') == 'manual':
                wallet_object = self.create(values)
                wallet_object.message_post(body=data)
                if wallet_object:
                    self.create_customer_payment(wallet_object)
            if customer:
                total_wallet = sum(self.env['customer.wallet'].search([('partner_id', '=', customer.partner_id.id)]).mapped('amount'))
                customer.partner_id.write({'total_wallet': round(float(values.get('amount')), 2)})
                # customer.partner_id.write({'total_wallet': total_wallet + round(float(values.get('amount')), 2)})
        return data

    @staticmethod
    def _prepare_magento_customer_values(**kwargs):
        transaction_id = kwargs.get('data').get('transaction_id') if kwargs.get('data') else ''
        balance_delta = kwargs.get('data').get('balance_delta') if kwargs.get('data') else ''
        currency_code = kwargs.get('data').get('currency_code') if kwargs.get('data') else ''
        action = kwargs.get('data').get('action') if kwargs.get('data') else ''
        email = kwargs.get('data').get('email') if kwargs.get('data') else ''
        magento_customer_id = kwargs.get('data').get('customer_id') if kwargs.get('data') else ''
        customer_name = kwargs.get('data').get('name') if kwargs.get('data') else ''
        message = kwargs.get('data').get('message') if kwargs.get('data') else ''
        type = ''
        type_credit = ''
        if action == 'refunded':
            type = 'credit'
            type_credit = 'liability'
        elif action in ['earning', 'manual']:
            type = 'credit'
            type_credit = 'expense'
        elif action in ['deducted', 'used']:
            type = 'debit'
        return {
            'partner_id': kwargs.get('customer_id'),
            'type': type,
            'type_credit': type_credit,
            'amount': kwargs.get('balance_amount'),
            'transaction_id': transaction_id,
            'balance_delta': balance_delta,
            'currency_code': currency_code,
            'action': action,
            'email': email,
            'magento_customer_id': magento_customer_id,
            'customer_name': customer_name,
            'message': message,
        }
