from odoo import api, fields, models


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    sale_order_id = fields.Many2one('sale.order', string="Order")
    wallet_id = fields.Many2one('customer.wallet', string="Wallet")
    is_wallet = fields.Boolean(string="Wallet")