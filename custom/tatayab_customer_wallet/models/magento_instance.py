from odoo import fields, models, api, _
from datetime import datetime,timedelta


class MagentoInstance(models.Model):
    _inherit = 'magento.instance'

    last_wallet_import_date = fields.Datetime(string='Last Import Wallets date',
                                              help="Last Import Products date")
    magento_import_wallet_page_count = fields.Integer(string="Magento Import wallet Page Count",
                                                     default=1,
                                                     help="It will fetch transaction of Magento from "
                                                          "given page numbers.")

    @api.model
    def _scheduler_import_customer_wallet(self, args=None):
        """
        This method is used to import customer wallet from Magento via cron job.
        :param args: arguments to import wallets
        :return:
        """
        # if not args:
        #     return True
        wallet_queue = self.env['magento.wallet.data.queue.ept']
        instance = self.env['magento.instance']
        # instance_id = args.get('magento_instance_id')
        instance_id = 1
        if instance_id:
            instance = instance.browse(instance_id)
            from_date = ''
            if instance.last_wallet_import_date:
                from_date = instance.last_wallet_import_date - timedelta(days=3)
            to_date = datetime.now() + timedelta(days=3)
            kwargs = {'instance': instance, 'from_date': from_date, 'to_date': to_date}
            created_wallet = wallet_queue.create_customer_wallet_queues(**kwargs)
            if created_wallet:
                instance.write({
                    'last_wallet_import_date': datetime.now()
                })
        return True
