# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

{
    'name': "Tatayab Customer Wallet",
    'author': 'Tatayab, Ahmed Salama',
    'category': 'ALL',
    'summary': """Tatayab Customer Wallet""",
    'website': 'http://www.tatayab.com',
    'license': 'AGPL-3',
    'description': """
""",
    'version': '1.0',
    'depends': ['account', 'sale', 'tatayab_magento_connect_changes'],
    'data': [
        'security/ir.model.access.csv',
        'data/customer_wallet_seq.xml',
        'data/customer_wallet_data.xml',
        'views/account_payment_view.xml',
        'views/customer_wallet_view.xml',
        'views/res_config_settings.xml',
        'views/res_partner_view_inherit.xml',
        'views/sale_order_view_inherit.xml',
        'views/wallet_queue_view.xml',
        'views/process_wizard_view.xml',
        'views/magento_instance_inh_view.xml',
        'views/menus.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
