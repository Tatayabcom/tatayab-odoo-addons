# -*- coding: utf-8 -*-
import functools
import logging

from odoo import http
from odoo.http import request, Response, Controller, route

logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


# Ahmed Salama Code Start ---->


def webservice(f):
	@functools.wraps(f)
	def wrap(*args, **kw):
		try:
			return f(*args, **kw)
		except Exception as e:
			return Response(response=str(e), status=500)
	
	return wrap


class WalletCycle(Controller):
	
	@route('/api/wallet', auth='public', csrf=False, type='json', methods=['POST'])
	@webservice
	def account_payment_wallet(self, **kwargs):
		"""
		Receive receipt_id, product_lines=[]
		:param kwargs: receipt_id, product_lines[]
		:return: response from success message api_inbound or error message + response.status_code
		:JSON_Example: {"jsonrpc":"2.0", "params":{'receipt_id':123,'product_ids': [{'line_id': 5380766, 'qty_done': [23]}]}}
		"""
		uid = request.session.uid
		if uid is None:
			logger.info(yellow + "-------------- Auto Auth -------------" + reset)
			# Depend on user/pass to connect
			email = request.httprequest.headers.get('Login')
			password = request.httprequest.headers.get('Password')
			db = request.httprequest.headers.get('DB')
			uid = request.session.authenticate(db, email, password)
			logger.info(green + "NEW UID: %s" % uid + reset)
			if not uid:
				logger.error(red + "Credentials Incorrect Details" + reset)
				return {'success': {}, 'errors': {0: "Credentials Incorrect Details"}, 'status_code': 501}
		params = kwargs.get('vals')
		logger.info(blue + "------ Start Wallet Cycle -----" + reset)
		logger.info(yellow + "Args: %s" % params + reset)
		try:
			# http.request.env['account.move'].with_user(uid).wallet_generate_payment(params)
			http.request.env['customer.wallet'].with_user(uid).wallet_generate_payment(params)
		except Exception as e:
			return {'success': {}, 'errors': {0: "Something unexpected happened with customer wallet\n" \
			                                     "Details: \n %s." % str(e)}, 'status_code': 502}

# Ahmed Salama Code End.
