import logging
from datetime import datetime, timedelta

from odoo import fields, models, api, _

_logger = logging.getLogger(__name__)
grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"
green = "\x1b[32m"
blue = "\x1b[34m"


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    last_update_stock_qty = fields.Datetime(compute='get_last_date')
    last_qty_update_stock = fields.Datetime("Date Out of stock KWT")
    last_qty_update_stock_ksa = fields.Datetime("Date Out of stock KSA")
    last_qty_update_stock_qatar = fields.Datetime("Date Out of stock QAT")
    is_out_of_stock = fields.Boolean("Out of stock KWT")
    is_out_of_stock_ksa = fields.Boolean("Out of stock KSA")
    is_out_of_stock_qatar = fields.Boolean("Out of stock QAT")
    creation_out_of_stock = fields.Boolean("Out Of stock from creation")
    parent_category = fields.Many2one(related='categ_id.parent_id', string='Parent Category', store=True)

    @api.onchange('qty_available')
    def get_last_date(self):
        stock_obj = self.env['stock.move']
        # Use with context to avoid informing operation to backend while install/upgrade
        for idx, template in enumerate(self):
        # for template in self:
            template = template.with_context(stop_product_internal_update=True)
            last_update_stock_qty = creation_out_of_stock = is_out_of_stock = False
            # if template.qty_available <= 0 or template.free_qty <= 0:
            if template.qty_available <= 0.0:
                is_out_of_stock = True
                last_stock_move_id = stock_obj.search([('product_id', '=', template.product_variant_id.id),
                                                       ('state', '=', 'done'),
                                                       ('location_dest_id.usage', '=', 'customer')],
                                                      order='create_date DESC', limit=1)
                if last_stock_move_id:
                    last_update_stock_qty = last_stock_move_id.create_date
                else:
                    last_update_stock_qty = template.create_date
                    creation_out_of_stock = True

            if is_out_of_stock:
                template.last_update_stock_qty = last_update_stock_qty
                # Force save stored field
                template.write({'last_qty_update_stock': last_update_stock_qty,
                                'is_out_of_stock': True,
                                'creation_out_of_stock': creation_out_of_stock})
            else:
                template.write({'is_out_of_stock': False})

            # Change cusrsor
            logging.info(
                yellow + "%s - product %s OUt Of Stock Fields updated" % (idx + 1, template.display_name) + reset)

        logging.info(yellow + "Done Update All ----------------------------" + reset)

    @api.depends('product_variant_ids',
                 'product_variant_ids.stock_move_ids.product_qty',
                 'product_variant_ids.stock_move_ids.state')
    @api.depends_context('company_owned', 'force_company')
    def _compute_quantities(self):
        super(ProductTemplateInherit, self)._compute_quantities()
        for template in self:
            reordered = self.env['stock.warehouse.orderpoint'].search(
                [('product_id', '=', template.product_variant_id.id)])
            for order in reordered:
                stock_quant = self.env['stock.quant'].search(
                    [('product_id', '=', template.product_variant_id.id),
                     ('location_id', '=', order.location_id.id),
                     ('quantity', '=', order.product_min_qty)])
                if stock_quant:
                    template._send_mail(order.warehouse_id.name)

    def _send_mail(self, warehouse):
        email_notify_group = self.env.ref('tatayab_out_of_stock_report.group_min_qty_notification').id
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_notify_group])]).mapped('email')

        if email_users:
            if False in email_users:
                email_users.remove(False)
        mail_content = "Dear All, <br/> Please be notified those mentioned products reached to Minimum Quantity." \
                       " on warehouse: <b>%s</b>.<br/>" % warehouse
        for template in self:
            mail_content += "- <i>%s</i> <br/>" % template.display_name
        mail_content += "it's system notification mail, advice and take the needed action."
        send_notification = self.env['mail.mail'].sudo().create({
            'subject': _('Product reached Minimum Quantity'),
            'author_id': self.env.user.partner_id.id,
            'body_html': mail_content,
            'email_to': email_users and ', '.join(email_users) or False,
        })
        send_notification.send()

    def send_product_out_of_stock_mail(self):
        """
        Send Email with out of stock products
        :param warehouse:
        :return:
        """

        email_notify_group = self.env.ref('tatayab_out_of_stock_report.group_out_of_stock_notification').id
        email_users = self.env['res.users'].sudo().search(
            [('groups_id', 'in', [email_notify_group])]).mapped('email')
        if email_users:
            if False in email_users:
                email_users.remove(False)
        _logger.info(green + "\nSENDING Mail For out of stock to mails: %s" % email_users + reset)

        mail_content = "Dear Sir, <br/> Please be notified that the below mentioned Products got out of stock.<br/>"

        new_date = datetime.strptime(str(datetime.now().date() - timedelta(days=1)) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')

        # self.env['out.stock.product.wizard'].get_last_stock()
        products = self.env['product.template'].search([('qty_available', '<=', 0.0),
                                                        ('is_out_of_stock', '=', True),
                                                        ('last_qty_update_stock', '>=', new_date)])
        products_ksa = self.env['product.template'].search([('qty_available', '<=', 0.0),
                                                        ('is_out_of_stock_ksa', '=', True),
                                                        ('last_qty_update_stock_ksa', '>=', new_date)])
        products_qatar = self.env['product.template'].search([('qty_available', '<=', 0.0),
                                                        ('is_out_of_stock_qatar', '=', True),
                                                        ('last_qty_update_stock_qatar', '>=', new_date)])
        if products or products_ksa or products_qatar:
            for template in products:
                mail_content += "- <b>%s</b> on warehouse: <b>%s</b>.<br/>" % (
                    template.display_name, "KWT Warehouse")
            for template_ksa in products_ksa:
                mail_content += "- <b>%s</b> on warehouse: <b>%s</b>.<br/>" % (
                    template_ksa.display_name, "KSA Warehouse")
            for template_qatar in products_qatar:
                mail_content += "- <b>%s</b> on warehouse: <b>%s</b>.<br/>" % (
                    template_qatar.display_name, "Qatar Warehouse")
            mail_content += "it's system notification mail, advice and take the needed action."
            send_notif = self.env['mail.mail'].sudo().create({
                'subject': _('Product Out Of Stock'),
                'author_id': self.env.user.partner_id.id,
                'body_html': mail_content,
                'email_to': email_users and ', '.join(email_users) or False,
            })
            print("send_notif ===>>",send_notif)
            logging.info(yellow + "Done to send out stock mail -----------%s-----------------"+str(send_notif.id) + reset)

            send_notif.send()
