from odoo import models, fields, api, exceptions, _


class OutOfStockWizard(models.TransientModel):
    _name = 'out.stock.product.wizard'

    date = fields.Datetime(string="Today's Date", default=fields.Datetime.now())
    limit_records = fields.Integer(string="Limit",
                                   help="Used on testing propose to minimize test time for limit number of products")

    def get_last_stock(self):
        products = self.env['product.template'].with_context(stop_product_internal_update=True).search(
            [('is_out_of_stock', '=', False), ('qty_available', '<=', 0.0)], limit=self.limit_records or False)
        products.with_context(stop_product_internal_update=True).get_last_date()


    def get_report(self):
        products = self.env['product.template'].with_context(stop_product_internal_update=True).search(
            ['|','|','|', ('is_out_of_stock_ksa', '=', True), ('is_out_of_stock_qatar', '=', True), ('is_out_of_stock', '=', True), ('qty_available', '<=', 0.0)])
        # products.with_context(stop_product_internal_update=True).get_last_date()
        action_id = self.env.ref('tatayab_out_of_stock_report.action_out_of_stock_product_report')
        action = action_id.read()[0]
        return action
