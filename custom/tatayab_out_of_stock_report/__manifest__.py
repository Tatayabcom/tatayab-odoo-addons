{
    'name': "Tatayab: Out of Stock Product Report",
    'author': 'Tatayab, Omnya Rashwan',
    'summary': """Tatayab Out of Stock Product Report""",
    'website': 'http://www.tatayab.com',
    'description': """""",
    'version': '13.0',
    'depends': ['stock', 'product','sale'],
    'data': [
        'security/ir.model.access.csv',
        'security/stock_min_qty_security.xml',
        'data/send_outof_stock_mail.xml',
        'views/product_out_of_stock_report.xml',
        'wizard/out_of_stock_wizard.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
