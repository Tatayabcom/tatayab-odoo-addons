# -*- coding: utf-8 -*-
{
    'name': 'Expiry Notification',
    'summary': """Generates notification for Expired Products, When its life data expired.""",
    'version': '13.0.1.0.0',
    'author': 'Tatayab (Omnya Rashwan)',
    'company': 'Tatayab',
    "category": "Stock",
    "depends": ["base", "stock", "product", "mail"],
    "data": [
        'data/pro_expiry_mail_data.xml',
        'data/ir_corn.xml',
        'security/product_expiry_group.xml',
    ],
    'installable': True,
    'application': False,
}
