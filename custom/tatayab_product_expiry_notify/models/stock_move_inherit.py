from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare, float_round, float_is_zero
from odoo.exceptions import UserError
from odoo.tools import OrderedSet


class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    @api.onchange('expiration_date')
    def set_lot_name(self):
        for rec in self:
            if rec.expiration_date:
                rec.lot_name = str(rec.expiration_date)
            else:
                rec.lot_name = ''

