from datetime import date

from odoo import fields, models, api, _
from dateutil.relativedelta import relativedelta


class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    def _get_date_values(self, time_delta, new_date=False):
        ''' Return a dict with different date values updated depending of the
        time_delta. Used in the onchange of `expiration_date` and when user
        defines a date at the receipt. '''
        vals = super(StockProductionLot, self)._get_date_values(time_delta, new_date)
        vals = {
            'use_date':  False,
            'removal_date': self.removal_date and (self.removal_date + time_delta) or new_date,
            'alert_date': self.alert_date and (self.alert_date + time_delta) or new_date,
        }
        return vals
    def get_warehouse(self):
        subject = "Purchase Expiry Notification"

        for lot in self.search([('expiration_date', '!=', False)]):
            stock_moves = self.env['stock.move.line'].search(
                [('lot_id', '=', lot.id), ('state', '=', 'done')]).mapped('move_id')
            stock_moves = stock_moves.search([('id', 'in', stock_moves.ids)]).filtered(
                lambda move: move.picking_id.location_id.usage == 'supplier' and move.state == 'done')
            warehouse_purchase_order = stock_moves.mapped('purchase_line_id.order_id').warehouse_id.name

            if warehouse_purchase_order:
                subject = "Purchase Expiry Notification - " + warehouse_purchase_order

            stock_moves = stock_moves.search([('id', 'in', stock_moves.ids)]).filtered(
                lambda move: move.picking_id.location_dest_id.usage == 'customer' and move.state == 'done')
            warehouse_sale_order = stock_moves.mapped('sale_line_id.order_id').warehouse_id.name

            if warehouse_sale_order:
                subject = "Purchase Expiry Notification - " + warehouse_sale_order
        return subject

    def send_mail_notification(self):
        email_notify_group = self.env.ref('tatayab_product_expiry_notify.group_product_expiry_notify').id
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_notify_group])])

        for lot in self.search([('expiration_date', '!=', False)]):
            expiration_date = fields.Datetime.from_string(lot.expiration_date)
            today_date = date.today() + relativedelta(months=8)
            if today_date == expiration_date.date():
                mail_template = self.env.ref('tatayab_product_expiry_notify.product_expiry_email_template')
                for user in email_users:
                    mail_template.send_mail(lot.id, force_send=True, email_values={'email_to': user.email})

        email_po_notify_group = self.env.ref('tatayab_product_expiry_notify.group_product_expiry_po_notify').id
        email_tot_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_po_notify_group])])

        for lot in self.search([('expiration_date', '!=', False)]):
            po_expiration_date = fields.Datetime.from_string(lot.expiration_date)
            po_today_date = date.today() + relativedelta(days=120)
            if po_today_date == po_expiration_date.date():
                mail_template = self.env.ref('tatayab_product_expiry_notify.product_po_expiry_email_template')
                for user in email_tot_users:
                    mail_template.send_mail(lot.id, force_send=True,
                                            email_values={'email_to': user.email, 'subject': self.get_warehouse()})

    def send_removal_mail_notification(self):
        email_notify_group = self.env.ref('tatayab_product_expiry_notify.group_product_removal_notify').id
        email_users = self.env['res.users'].sudo().search([('groups_id', 'in', [email_notify_group])])

        for lot in self.search([('removal_date', '!=', False)]):
            removal_date = fields.Datetime.from_string(lot.removal_date)
            today_date = date.today() + relativedelta(months=3)
            if today_date == removal_date.date():
                mail_template = self.env.ref('tatayab_product_expiry_notify.product_removal_email_template')
                for user in email_users:
                    mail_template.send_mail(lot.id, force_send=True, email_values={'email_to': user.email})
